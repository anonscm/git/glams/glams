GLAMS - The Global Laboratory and Administration Management System
==================================================================

This is a graphical interface to the ELVIS project aimed to focus on management
of scientific projects and experiment/sample traceability.
It has been instanciated by the Bioinformatic team at IRHS.
