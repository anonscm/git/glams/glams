/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("glams.theme.Theme",
{
  meta :
  {
    color : glams.theme.Color,
    decoration : glams.theme.Decoration,
    font : glams.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : glams.theme.Appearance
  }
});