/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("glams.theme.Color",
{
  extend : qx.theme.modern.Color,

  colors :
  {
    // group boxes
    "text-title" : "#314a6e",

    // states
    "text-hovered"  : "#001533",
    "text-active"   : "#26364D",

    // text fields
    "border-input" : "#334866",

    // tab view, window
    "border-pane" : "#00204D",

    // focus state of text fields
    "border-focused" : "#5FE653",

    // drag & drop
    "border-dragover" : "#33508D",

    // own table colors
    // "table-row-background-selected" and "table-row-background-focused-selected"
    // are inspired by the colors of the selection decorator
    "table-focus-indicator" : "#0880EF",
    "table-row-background-focused-selected" : "#084FAB",
    "table-row-background-focused" : "#80B4EF",
    "table-row-background-selected" : "#084FAB",

    "selected-start" : "#4AB341",
    "selected-end" : "#35802E",
    "background-selected" : "#4AB341",

    "tabview-background" : "#07125A",

    "checkbox-border" : "#314A6E",
    "checkbox-focus" : "#87AFE7",
    "checkbox-hovered" : "#B2D2FF",
    "checkbox-hovered-inner" : "#D1E4FF",

    "radiobutton-checked" : "#005BC3",

    "button-hovered-start" : "#C7F3C3",
    "button-hovered-end" : "#5FE653",
    "button-focused" : "#8ADA83",

    "input-focused-start" : "#5FE653", 
    "input-focused-end" : "#5ADA4E",

    "window-border" : "#2A6625",
    "window-caption-active-start" : "#3F9937",
    "window-caption-active-end" : "#2A6625"
  }
});
