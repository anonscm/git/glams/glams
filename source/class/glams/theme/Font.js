/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("glams.theme.Font", {
  extend: qx.theme.modern.Font,

  fonts: {
    "bold": {
      size: 11,
      lineHeight: 1.4,
      family: ["Arial"],
      bold: true
    }
  }
});