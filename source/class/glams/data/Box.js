/**
 * This is the widget to view basic information of one Box
 *
 */
qx.Class.define("glams.data.Box",{
  extend : qx.ui.core.Widget,	
  events : {    
    /**
     * Fired when the introduction is saved in the database.
     */
    "saved" : "qx.event.type.Data",
    /**
     * Fired when get all container into the box
     */
    "getAllContainerInformations" : "qx.event.type.Event"       
    },
	
  properties : {
  /**
    * id of the box
    */
    idBox : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdBox"
    },
  /**
    * name of the box
    */
    name : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeName"
    },
  /**
    * id user group
    */
    idUserGroup : {      
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdUserGroup"
    },  
  /**
    *name type of the box
    */
    boxTypeName : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeBoxTypeName"
    },  
  /**
    * row number
    */
    rowNumber : {      
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeRowNumber"
    },  
  /**
    *column number
    */
    columnNumber : {      
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeColumnNumber"
    },
  /**
    * list of place and type of place
    */
    place :{      
      init : null,
      nullable : true,
      check : "Array",
      event : "changePlace"
    },
  /**
    * number of container in the box
    */
     numberInBox:{      
      init : 0,
      nullable : true,
      check : "Integer",
      event : "changeNumberInBox"
    }
  },
  members : {
    
    
     /**
       * converti un label en nombre pour évaluer la place dans la boite
       * @param label {character} lettre
       * @return {integer} numéro de ligne ou de colonne 
       */
     __convertLabel : function(label) {
       var num=Number(label)-1;
       if (isNaN(label)) {
         var liste=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
         num=liste.indexOf(label);
         if (num==-1){
           var liste=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
           num=liste.indexOf(label);
         };       
       };     
       return(num);
     },
     /**
       * renseigne toutes les informations sur la boite 
       */
     __getAllInformations : function() {
      this.__tab={};
      this.__tab['idBox']=this.getIdBox();
      //recupère les informations sur le type de boites
      this.__services = glams.services.Sample.getInstance();
      this.__services.QueryGetAllInformationsByBox("getAllInformationsByBox", this.__sessionId, this.__tab);
      this.__listener1 = this.__services.addListener("getAllInformationsByBox", function(e){ 
       var loadData=e.getData();
       if (loadData[0]['id']==this.__tab['idBox']){   //controle qu'il s'agit de la bonne boite 
        this.__services.removeListenerById(this.__listener1);
        this.setRowNumber(e.getData()[0]['row_number']);
        this.setColumnNumber(e.getData()[0]['column_number']);
        this.setName(e.getData()[0]['name'])
        this.setBoiteVide();    
        this.setNumberInBox(0);
        if(loadData[0]['id_container'] ){ //presence de container               
          for (var c in loadData){  
            if(loadData[c]['date_off']== null ){        
              var tab={};            
              tab['idContainer']=loadData[c]['id_container'];
              tab['code']=loadData[c]['code'];         
              var container = new glams.ui.Container() ;
              container.setId(loadData[c]['id_container']);
              container.setCode(loadData[c]['code']);
              container.setValid(loadData[c]['valid']);
              container.setUsable(loadData[c]['usable']);
              container.setRemark(loadData[c]['remark']);
              container.setTypeContainer(loadData[c]['typecontainer']);
              container.setLevel(loadData[c]['level']);
              container.setContent(loadData[c]['contenttype']);            
              container.setRow(loadData[c]['row']);
              container.setColumn(loadData[c]['column']);                    
              container.setDateOn(loadData[c]['date_on']);
              container.setDateOff(loadData[c]['date_off']);
              container.setIdBox(loadData[c]['id']); 

              //ajout du container à la boite
              this.addContainer(container,loadData[c]['row'],loadData[c]['column'],loadData[c]['date_on'],loadData[c]['date_off']);
            };              
          };          
          this.fireDataEvent("getAllContainerInformations");                     
        }else{
          this.setBoiteVide();
          this.fireDataEvent("getAllContainerInformations");         
        };
       };   
      },this);     
     },     
     
     /**
       * initialise une boite vrac vide
       */
    setBoiteVracVide : function() {      
        this.__places = [];
        this.setNumberInBox(0);           

	},  
     /**
       * Rempli le vecteur de valeurs nulles
       */
    setBoiteVide : function() {      
      for (var p=0; p< this.__places.length ; p++ ){
        this.__places[p]=null;             
      };
	}, 
     /**
       * updateOneContainer
       * @param infoContainer {Array} Dictionnaire contenant les informations sur un container
       */
    updateOneContainer : function(infoContainer) {
      var loadData = infoContainer;

      for (var p=0; p< this.__places.length ; p++ ){
        if (this.__places[p] != null) {
          if (this.__places[p].getId()== loadData['id']) {
            this.__places[p].setCode(loadData['code']); 
            this.__places[p].setValid(loadData['valid']);
            this.__places[p].setUsable(loadData['usable']);
            this.__places[p].setRemark(loadData['remark']);
            this.__places[p].setContent(loadData['typeContent']);
            this.__places[p].setTypeContainer(loadData['typeContainer']);
            this.__places[p].setLevel(loadData['level']);   
            this.__places[p].setDateOff(loadData['dateOff']);
            this.__places[p].setNotations(loadData['notations']);  
          break;
          };
        };             
      };   
	  },
 
     /**
       * Save Box on elvis
       */
    saveBox : function() {
      var listeC = [];
      for (var p=0; p< this.__places.length ; p++ ){
        if (this.__places[p] != null) {
          listeC.push(this.__places[p].toDict());
        };
      };
      //ajout du usergroup pour les notations
      for (p in listeC ){
        listeC[p]['userGroup']=JSON.parse(localStorage.getItem('writeGroups'));
      };   
      var tab={};
      tab['listeContainer']=listeC;
      this.__services.QueryUpdateBox("updateBox",this.__sessionId, tab);
      this.__services.addListener("updateBox", function(e){     
      var loadData = e.getData();
      this.fireDataEvent("saved",this.getIdBox());
      }, this);
	  },
	  
     /**
       * add a container
       *@param container {Object} container
       *@param rowLabel {integer} numero de ligne
       *@param columnLabel {integer} numero de colonne
       *@param dateOn {date} date d'ajout du container
       *@param dateOff {date} date de suppression du container
       */
    addContainer : function(container,rowLabel,columnLabel,dateOn,dateOff) {
      if ( rowLabel == null ) {
       var position = this.getNumberInBox();
       this.setNumberInBox ( position+ 1)  ;
      }else{      
        var position =this.__convertLabel(columnLabel)+this.__convertLabel(rowLabel) * this.getColumnNumber();      
      };          
      if (dateOff == null ){
        this.__places[position]=container;
      };
    },
    /**
     * evalue les places vides et renvoie la liste des places avec les informations des containers
     * @return {Array} liste des places
     */
    getListePlaces : function() {
      for (var p=0; p< this.__places.length ; p++ ){
        if (typeof this.__places[p]=='undefined') { this.__places[p]=null } ;             
      }; 
      return(this.__places);
    }
	
  },
    
  construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */  
    //contenance par défaut utile si boite vrac
    this.__places = Array( 100) ;
    this.setBoiteVide();
    this.setBoiteVracVide();
    this.setNumberInBox(0);
   /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    
    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
     
    //calcule le nombre de places selon le nombre de lignes et de colonnes
    this.addListener ("changeRowNumber", function(e){
      if ( this.getColumnNumber() != null) {                
        this.__places = Array(this.getColumnNumber()*this.getRowNumber());
      };
    },this);         
    this.addListener ("changeColumnNumber", function(e){     
      if ( this.getRowNumber() != null) {
        this.__places = Array(this.getColumnNumber()*this.getRowNumber()); 
      };    
    },this); 
    
    //charge les données quand le IdBox est renseigné
    this.addListener ("changeIdBox", function(e){
    this.__getAllInformations();
          },this);  //fin du changeIdBox  
  
	}
});
