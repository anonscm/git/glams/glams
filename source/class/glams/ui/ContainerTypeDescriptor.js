/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Formulaire de recherche de Container
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.ContainerTypeDescriptor", {
  extend: qx.ui.core.Widget,

  events: {
	 /**
    * fire when the container type is updated
    */   
   "updatedContainerType": "qx.event.type.Event",
   /**
    * fire when the container type is created
    */
   "createdContainerType": "qx.event.type.Event"
  },

  properties: {

  },

  members: {
    __updateContainer: function(item) {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container._add(new qx.ui.basic.Label(this.tr("Type de Contenant")));
      var type = new qx.ui.form.TextField();
      type.setValue(item.getLabel());
      container._add(type);
      var savebtn = new qx.ui.form.Button(this.tr("Save"));
      container._add(savebtn);
      savebtn.addListener("execute", function() {
         var tab = {};
         tab['type'] = type.getValue();
         tab['id'] =item.getUserData("id");        
         this.__services.QueryUpdateContainerType("updateContainerType", this.__sessionId, tab);
         this.__services.addListener("updateContainerType", function(e) {     
         this.fireEvent("updatedContainerType")
         }, this);
      }, this);
      return (container)
    },
    __newContainer: function() {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container._add(new qx.ui.basic.Label(this.tr("Type de Contenant")));
      var type = new qx.ui.form.TextField();
      container._add(type);
      var savebtn = new qx.ui.form.Button(this.tr("Save"));
      container._add(savebtn);      
      savebtn.addListener("execute", function() {
         var tab = {};
         tab['type'] = type.getValue();       
         this.__services.QueryCreateContainerType("createContainerType", this.__sessionId, tab);
         this.__services.addListener("createContainerType", function(e) {     
         this.fireEvent("createdContainerType");
         }, this);
      }, this);
      return (container)
    }
  },

  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */

    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.VBox();
    layout.setSpacing(10);

    this._setLayout(layout);
    //creation des widgets
    this._add(new qx.ui.basic.Label(this.tr("Types de Contenant")));

    this.__liste = new qx.ui.form.List().set({
      width: 250,
      allowStretchY: true,
      allowStretchX: true
    });
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));


    //positionnement des widgets
    this._add(this.__liste, {
      flex: 1
    });
    this._add(this.__buttonEdit);
    this._add(this.__buttonNew);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryGetAllContainerType("getAllContainerType", this.__sessionId, null);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //type de container
    this.__services.addListener("getAllContainerType", function(e) {
      this.__liste.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['type']);
        tempItem.setUserData("id", loadData[i]['id']);
        this.__liste.add(tempItem);
      };
    }, this);

    //bouton modifier
    this.__buttonEdit.addListener("execute", function() {
      var window = new qx.ui.window.Window("Modify Container type");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__updateContainer(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updatedContainerType", function() {window.close()}, this);
   },this);  
   //doubleclick sur type de container 
   this.__liste.addListener("dblclick", function(e){ 
      var window = new qx.ui.window.Window("Modify Container type");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__updateContainer(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updatedContainerType", function() { window.close()}, this);
        window.addListener("close",function(){  this.fireEvent("updatedContainerType");   },this);    

    }, this);
    //bouton ajouter
    this.__buttonNew.addListener("execute", function(e) {
      var window = new qx.ui.window.Window("Add Container type");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__newContainer());
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("createdContainerType", function() { window.close()}, this);
    }, this);
   //mise à jour de la liste
   this.addListener("updatedContainerType",function(){ 
     this.__services.QueryGetAllContainerType("getAllContainerType", this.__sessionId, null);
   },this);
   this.addListener("createdContainerType",function(){ 
     this.__services.QueryGetAllContainerType("getAllContainerType", this.__sessionId, null);
   },this); 

  }
})
