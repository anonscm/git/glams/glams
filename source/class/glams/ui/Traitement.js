/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to add a treatment to one culture.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.Traitement", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * close the windows
     */
    "fermetureTraitement": "qx.event.type.Event",
    /**
     * Returns the input data to NewCreateCulture
     */
    "okTraitement": "qx.event.type.Data"
  },

  properties: {
    /**
     * Data on the crop for which the treatment is being applied
     */
    culture: {
      init: null,
      event: "changeCulture"
    }

  },

  members: {

  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(15);
    this._setLayout(layout);


    var form = new qx.ui.form.Form();
    //Implémentation des widget visible à l'écran
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du lot : ")), {
      row: 0,
      column: 0
    });
    this.__namelot = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__namelot, {
      row: 0,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Protocole de culture : ")), {
      row: 1,
      column: 0
    });
    this.__protocoleTraitementCulture = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__protocoleTraitementCulture, {
      row: 1,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Protocole de traitement : ")), {
      row: 2,
      column: 0
    });
    this.__protocoleTraitement = new glams.ui.FiltredComboBox();
    this.__protocoleTraitement.setPlaceholder("MO-042");
    this._add(this.__protocoleTraitement, {
      row: 2,
      column: 1
    });
    var ajoutProtocole = new qx.ui.form.Button(this.tr("Nouveau protocole"));
    this._add(ajoutProtocole, {
      row: 2,
      column: 2
    });
    var affichageProtocole = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    affichageProtocole.setWidth(5);
    this._add(affichageProtocole, {
      row: 3,
      column: 0,
      colSpan: 3
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du traitement : ")), {
      row: 4,
      column: 0
    });
    this.__nameTraitement = new qx.ui.form.TextField();
    this._add(this.__nameTraitement, {
      row: 4,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Date du traitement : ")), {
      row: 5,
      column: 0
    });
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    this._add(this.__date, {
      row: 5,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Experimentateur : ")), {
      row: 6,
      column: 0
    });
    this.__experimentateur = new qxelvis.ui.users.ContributorsSelector();
    this._add(this.__experimentateur, {
      row: 7,
      column: 0,
      colSpan: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Remarque : ")), {
      row: 8,
      column: 0
    });
    this.__remarks = new qx.ui.form.TextArea();
    this.__remarks.setPlaceholder("Remarque sur le protocole de traitement");
    this._add(this.__remarks, {
      row: 8,
      column: 1
    });
    this.__labelValidation = new qx.ui.basic.Label('Obligatoire : ');
    this.__labelValidation.setTextColor('red');
    this.__labelValidation.setVisibility("hidden");
    this._add(this.__labelValidation, {
      row: 9,
      column: 0,
      colSpan: 2
    });
    var Valider = new qx.ui.form.Button(this.tr("Valider"));
    this._add(Valider, {
      row: 10,
      column: 0
    });
    var Annuler = new qx.ui.form.Button(this.tr("Annuler"));
    this._add(Annuler, {
      row: 10,
      column: 2
    });

    var validationProtocole = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez selectionner un Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );
    var validationNomTraitement = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez renseigner le nom du nouveau Lot");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    this.__manager = new qx.ui.form.validation.Manager();
    this.__manager.add(this.__protocoleTraitement, validationProtocole);
    this.__manager.add(this.__nameTraitement, validationNomTraitement);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryAllUser("loadedAllUser", null, null);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //Permet de charger les informations relatives à la culture sur laquelle le traitement est appliqué
    this.addListener("changeCulture", function() {
      var loadData = this.getCulture();
      if (loadData.nomLot != null) {
        this.__namelot.setValue(loadData.nomLot);
      } else {
        this.__namelot.setValue("Non renseigné");
      };
      if (loadData.protocolCulture != null) {
        this.__protocoleTraitementCulture.setValue(loadData.protocolCulture);
      } else {
        this.__protocoleTraitementCulture.setValue("Non renseigné");
      };

    }, this);

    // Charge tous les utilisateurs
    this.__services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__experimentateur.setUsers(loadData);
    }, this);

    // Fonctions qui chargent les protocole en fonction des carractères saisi dans la barre de texte
    this.__protocoleTraitement.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 1) {
        var tab = {};
        tab['filtredName'] = e.getData();
        this.__services.QueryGetAllProtocolFiltred("getAllProtocolFiltred", this.__sessionId, tab);
      };
    }, this);
    this.__services.addListener("getAllProtocolFiltred", function(e) {
      this.__listProtocol = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listProtocol.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id'],
          'adress': loadData[i]['adress'],
          'summary': loadData[i]['summary'],
          'refLab': loadData[i]['reference_laboratory_handbook'],
          'refAl': loadData[i]['reference_alfresco']
        });
      };
      this.__protocoleTraitement.setListModel(this.__listProtocol);
    }, this);

    //Ouverture de la fenêtre de création d'un protocole
    ajoutProtocole.addListener("execute", function() {
      var newContainer = new glams.ui.CreateProtocoleCulture();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container.add(newContainer);
      var window = new qx.ui.window.Window("New Protocol");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(container);
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      //Fonction de fermeture de la fenetre
      newContainer.addListener("fermetureCreateProtocol", function() {
        window.close();
      }, this);
    }, this);

    //Fonction qui permet l'affichage des informations relatives au protocole selectionné
    this.__protocoleTraitement.addListener("changeAllValues", function(e) {
      affichageProtocole.removeAll();
      var displayProtocole = new glams.ui.AffichageProtocole();
      displayProtocole.setProtocole(this.__protocole.getAllValues());
      affichageProtocole.add(displayProtocole);
    }, this);

    //Ouverture de la fenêtre de création d'un protocole
    ajoutProtocole.addListener("execute", function() {
      var newContainer = new glams.ui.CreateProtocoleCulture();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container.add(newContainer);
      var window = new qx.ui.window.Window("New Protocol");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(container);
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
    }, this);

    // Listener sur le bouton Valider qui Sauvegarde les données saisie et les envoie dans la classe NewCreateCulture
    Valider.addListener("execute", function() {
      this.__labelValidation.setValue('Obligatoire : ');
      if (!this.__experimentateur.getContributors()) {
        this.__labelValidation.setVisibility("visible");
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le ou les utilisateur(s) ");
      } else {
        this.__manager.validate();
      }
    }, this);

    this.__manager.addListener("complete", function() {
      if (this.__manager.getValid()) {
        var tab = {};
        tab['type_notation'] = 'traitement particulier'
        tab['valeur_notation'] = this.__nameTraitement.getValue();
        tab['listeUser'] = this.__experimentateur.getContributors();
        tab['remarque_notation'] = this.__remarks.getValue();
        tab['protocol'] = this.__protocoleTraitement.getAllValues().getId();
        var format = new qx.util.format.DateFormat("dd/MM/YYYY");
        tab['date'] = format.format(this.__date.getValue());
        //Déclenche l'evenement okTraitement de la classe NewCreateCulture
        this.fireDataEvent("okTraitement", tab);
        // Déclenchement de la fonction de fermeture de la fenêtre
        this.fireEvent("fermetureTraitement");
      };
    }, this);

    // Listener sur le bouton Annuler qui déclenche la fermetured de la fenêtre
    Annuler.addListener("execute", function() {
      this.fireEvent("fermetureTraitement");
    }, this);


  }
})