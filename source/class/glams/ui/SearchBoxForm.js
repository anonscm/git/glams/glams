/**
 * Formulaire de recherche de contenair
 * Fabrice Dupuis février 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.SearchBoxForm",{
   extend : qx.ui.core.Widget,
	
  members : {    
    /*
    * Liste des boites ouvertes pour controle et modifications
    */
    __listeOpenBox : [],
    
    //*************	affichage plans de boite*********************************************	
    /*
    *affiche le plan de boites
    */ 
    __showMapBox : function (idBox,label) {
      this.__idBox = idBox;
      //teste si la boite est déjà ouverte
      var boite='absente';
       for (var l in this.__listeOpenBox){
        if ( this.__idBox == this.__listeOpenBox[l]['idBox']){ 
          var boite='présente';
          break;
        };
      };
      if (boite=='absente'){
        //creation de la structure data dataBox
        this.__serviceBox.createBox(this.__idBox);
        //creation de la structure graphique MapBox
        this.__mapBox = new glams.ui.MapBox();
        this.__mapBox.setAllowStretchX(true);
        //creation de la fenêtre d'affichage du plan
        var planBoxWindow = new qx.ui.window.Window(label);
        planBoxWindow.setWidth(200);
        planBoxWindow.moveTo(Math.round(850+Math.random()*80),Math.round(70+Math.random()*80));
        planBoxWindow.setLayout(new qx.ui.layout.Grow());
        planBoxWindow.open();
        
        //listener sur la fenêtre planBoxWindow
        planBoxWindow.addListener("close", function (e){ //si fermeture alors mise à jour de la liste des boites ouvertes
        for (var l in this.__listeOpenBox){
          if (planBoxWindow  == this.__listeOpenBox[l]['planBoxWindow']){ 
            this.__listeOpenBox[l]['mapBox'].destroy();
            this.__listeOpenBox[l]['dataBox'].destroy();
            this.__listeOpenBox.splice(l,1);           
            break;
        };
      };
        
        }, this);
        
        //listeners sur la couche data      
        this.__serviceBox.addListenerOnce("createBox", function(e){ //toutes les données sont récupérées
          this.__dataBox=e.getData(); 
          this.__mapBox.setIdBox(this.__dataBox.getIdBox());
          this.__mapBox.setRowNumber(this.__dataBox.getRowNumber());
          this.__mapBox.setColumnNumber(this.__dataBox.getColumnNumber());
          this.__mapBox.setPlace(this.__dataBox.getListePlaces());
          var tab={};
          tab['idBox']=this.__idBox;
          tab['mapBox']=this.__mapBox;
          tab['dataBox']=this.__dataBox;
          tab['planBoxWindow']=planBoxWindow;
          this.__listeOpenBox.push(tab);
          
          this.__dataBox.addListenerOnce("saved", function(e){ //les données sont sauvegardée dans la base. Effectue un reset d'affichage après      
            for (var l in this.__listeOpenBox){
              this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
              this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
                this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
                this.__listeOpenBox[l]['mapBox'].update();
            
              },this);
            };
          },this);
          
        },this);
      };  

      //listener sur la couche graphique
      this.__mapBox.addListener("drowed", function(){//la structure de dessin est réalisée
       planBoxWindow.add(this.__mapBox);
      },this); 
      this.__mapBox.addListener("updateOnecontainer", function(e){//les valeurs d'un container sont modifiées
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData);
            this.__listeOpenBox[l]['mapBox'].update();
            break;
          };
        };
        
      },this);    
      this.__mapBox.addListener("saveBox", function(e){//sauvegarde la box sur elvis
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].saveBox();
        };          
      },this);  
      
      this.__mapBox.addListener("reInitialiseMapBox", function(e){//reinitialise toutes les box pour effacer les modifications
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
          this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
            this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
            this.__listeOpenBox[l]['mapBox'].update();
          },this);

        };
      },this);    
      this.__mapBox.addListener("containerPutOff", function(e){//action drag du drag and drop
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            loadData['container'].setDateOff(new Date());
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData['container']);        
            break;
          };
        };
      },this);  
         
      this.__mapBox.addListener("containerPutOn", function(e){//action drop du drag and drop
        var loadData=e.getData();
        //cree un nouveau container
        loadData['container'].setDateOn(new Date());
        loadData['container'].setDateOff(null);
        if (loadData['row'] == null ) {
          loadData['container'].setRow(null);
        }else{
          loadData['container'].setRow(loadData['row'].toString());
        }; 
        if (loadData['column'] == null ) {
          loadData['container'].setColumn(null);
        }else{
          loadData['container'].setColumn(loadData['column'].toString());
        };        
        loadData['container'].setIdBox(loadData['idBox'] );
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){        
            this.__listeOpenBox[l]['dataBox'].addContainer(loadData['container'],loadData['container'].getRow(),loadData['container'].getColumn(),loadData['container'].getDateOn(),loadData['container'].getDateOff());       
            break;
          };
        };    
      },this);                   
    
    }//fin de la methode affichage des boites ******************************************************************************************************************
  },
	
	construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */    
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();
    
		var layout = new qx.ui.layout.Grid();
		layout.setSpacing(10);		
		layout.setColumnFlex(2,1);
		this._setLayout(layout);
		
		this._add(new qx.ui.basic.Label(
		  this.tr("Code ou Nom de la boite : ")), 
		  {row : 0, column : 0});
	  		  
		//init of the widget
    this.__nameBox = new glams.ui.FiltredComboBox();

		
		//liste des boites
		//initialise l'autocompleBox
                          
    this.__nameBox.addListener("changeSelectedString", function(e){
      var loadData = e.getData();
      if (loadData.length >2){
        var tab={};        
        tab['userGroup']=  JSON.parse(localStorage.getItem('readGroups'));
        tab['filtredText']=e.getData();
        this.__services.QueryGetAllBoxsFiltred("getAllBoxsFiltred", this.__sessionId, tab );      
      }; 
    }, this);			

		//Affichage resultat requete position Box
		var widgetLocateBox = new qx.ui.core.Widget();
		widgetLocateBox._setLayout(new qx.ui.layout.VBox());
		this.__listLocateBox = new qx.ui.form.List().set({
		  height : 60,
		  width : 600
		});
		widgetLocateBox._add(this.__listLocateBox);
		widgetLocateBox._add(new qx.ui.basic.Label(this.tr("double click = plan de boite")));

    
    //positionnement des widgets
		this._add(this.__nameBox, {row : 0, column : 1});
		this._add(widgetLocateBox, {row : 1, column : 1});

		

                
		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
     
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */ 
    //liste des noms de boite dans la liste à selectionner
    this.__services.addListener("getAllBoxsFiltred", function(e){
      var loadData = e.getData();
      this.itemData2 = [];
      for(var i in loadData){
        this.itemData2.push({'label':loadData[i]['name'],'id':loadData[i]['id']});
      };    
      this.__nameBox.setListModel(this.itemData2);
      this.__nameBox.open();             
    }, this);     
    
    //choix du nom de boite réalisé
    this.__nameBox.addListener("changeValue", function(e){
      var tab={};
      tab['userGroup']=  JSON.parse(localStorage.getItem('readGroups'));
      tab['filtredText']=e.getData();
      this.__services.QueryGetPlaceBox("getPlaceBox", this.__sessionId, tab);
    },this);     

    //position de la boite
      this.__services.addListener("getPlaceBox", function(e){
      var loadData = e.getData();
      this.debug(loadData);
      this.__listLocateBox.removeAll();
      for(var i in loadData){
      };
      
      var nBox=0 ;
      var label='';
      for(var i in loadData){
        if (loadData[i]['idbox']!= nBox){ //boite nouvelle
          if (label !=''){ //boite précédente existante
            var tempItem = new qx.ui.form.ListItem(label);  
            tempItem.setUserData("label", label);             
            tempItem.setUserData("idBox", loadData[i]['idbox']);   
            this.__listLocateBox.add(tempItem);
          };
          //nouvelle boite
          label='';
          nBox=loadData[i]['idbox'];
          label = " Boite : "+loadData[i]['name']+" > " +loadData[i]['type']+" : " +loadData[i]['valeur']+" / "
        }else{ //suite des informations
          label= label +loadData[i]['type']+" : " +loadData[i]['valeur']+" / "
        };
      };
      if (label !=''){ //dernière boite
        var tempItem = new qx.ui.form.ListItem(label);
        tempItem.setUserData("label", label);                
        tempItem.setUserData("idBox", loadData[i]['idbox']);   
        this.__listLocateBox.add(tempItem);
     };
    }, this);    
    
    


    //affichage du plan de la boite apres dblclick
     this.__listLocateBox.addListener("dblclick", function(){   
      this.__showMapBox (this.__listLocateBox.getSelection()[0].getUserData("idBox"),this.__listLocateBox.getSelection()[0].getUserData("label"));
    },this);   
 
     

   

	}
});
