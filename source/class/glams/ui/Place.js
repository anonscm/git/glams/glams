/**
 * This is the widget to a place of one Box
 * fabrice dupuis septembre 2017
 */
qx.Class.define("glams.ui.Place",{
  extend : qx.ui.core.Widget,	
  events : {    
   /**
     * Fired quand uncontainer bouge par drag
     */
    "containerPutOff" : "qx.event.type.Data",
   /**
     * Fired quand uncontainer bouge par drop
     */    
    "containerPutOn" : "qx.event.type.Data",
   /**
     * Fired quand on doubleclick sur une place
     */
    "dbleClckPlace" : "qx.event.type.Data"   
       },
	
  properties : { 
    /**
    * id of the web session
    */
    __sessionId : {
	    init : null
    },
  /**
    * idBox
    */
    idBox : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdBox"
    },
  /**
    * position ligne
    */
    row : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeRow"
    },
  /**
    * position colonne
    */
    column : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeColumn"
    },
  /**
    * position sur la liste de la boite
    */
    position : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changePosition"
    },
  /**
    * tube dans cette place
    */
    tube : {
      init : null,
      nullable : true,
      check : "glams.ui.Container",
      event : "changeTube"
    },
  /**
    * date on
    */
    dateOn : {
      init : null,
      nullable : true,
//      check : "",
      event : "changeDateOn"
    },       
  /**
    * date off
    */
    dateOff : {
      init : null,
      nullable : true,
//      check : "",
      event : "changeDateOff"
    }    
    
  },
  members : {  
    
  },
  
    
  construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */      
    var layout = new qx.ui.layout.Grow();    
    this._setLayout(layout);           

    this.__icon= new qx.ui.basic.Image();
    this.__icon.setAllowStretchX(true,true);
    this.__icon.setAllowStretchY(true,true);
    //active drag and drop    
    this.__icon.setDraggable(true);
    this.setDroppable(true);   
    

    
     
   /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
    this.addListener("changeTube", function(){
    //presence d'un tube et ajout d'une image dans le widget Place
    // image png de taille 7.9 mm de hauteur
      if(this.getTube() != null) {      
        this._add(this.__icon);
        this.setDroppable(false); 
        var couleur = '';
        if (this.getTube().getLevel()=='critique') couleur='J';
        if (this.getTube().getValid()== false ) couleur='R';
        if (this.getTube().getUsable()== false ) couleur = 'N';
        if (this.getTube().getLevel()=='vide') couleur='B';
        if (this.getTube().getDateOff()!= null) couleur='X';

        switch (couleur) {
          case 'J' :
            this.__icon.setSource("glams/sample/tubeJaune.png");
            break;
          case 'R' :
            this.__icon.setSource("glams/sample/tubeRouge.png");
            break;            
          case 'N' :
            this.__icon.setSource("glams/sample/tubeNoir.png");
            break;                       
          case 'B' :
            this.__icon.setSource("glams/sample/tubeVide.png");
            break; 
          case 'X' :
            this.__icon.setSource("glams/sample/tubeX.png");
            break;                      
          default :
            this.__icon.setSource("glams/sample/tubeVert.png");
          } 
                                                             
      }else{
        this.setDroppable(true);
        this.__icon.setSource("glams/sample/tubeX.png");
      };     
    },this);
    
    //popup 
    this.addListener("mouseover", function(e){               
      if (this.getTube() != null){              	
        this.setCursor("pointer");             
        this.__popup = new qx.ui.popup.Popup(new qx.ui.layout.Grow()).set({backgroundColor: "#FFFAD3"});                 
        this.__popup.add(new qx.ui.basic.Label(this.getTube().getCode()));
        this.__popup.placeToPointer(e);
        this.__popup.show();
      };          
    },this);      
    this.addListener("mouseout", function(e){
      if (this.getTube() != null && this.__popup!= null ){
        this.__popup.setVisibility("excluded");
      };
    },this); 
    
    //Drag and Drop
    
    //active l'action du déplacement
    this.addListener ( "dragstart" ,function (e)  { 
      e.addAction ( "move" );               //type de l'action
      //e.addType ( "qx.ui.basic.Image" ); //type d'element déplacé       
      e.addType ( "glamssample.ui.Container" ); //type d'element déplacé   
    },this);  
    
    //action à la fin du drag sur la place drag
    this.addListener ( "dragend" ,function (e)  {

      var tab={};
      tab['idBox']=this.getIdBox();
      tab['container']=this.getTube();
      this.fireDataEvent("containerPutOff",tab); //event de put off
      //console.log("put off  " +this.getIdBox()+"  "+this.getTube());
      this.setTube(null);     
    },this);  
    //listener sur la place de dépot
    this.addListener ( "drop" ,  function (e)  { 
      //this.__icon=e.getData("qx.ui.basic.Image"); // e.getData fire droprequest   
      //this._add(this.__icon); 
      
      
      var newTube = new glams.ui.Container(); //reinstancier nouvel objet pour qu'il soit indépendant de celui de depart
      var tube=e.getData("glamssample.ui.Container"); // e.getData fire droprequest    

     
      newTube.setIdBox ( tube.getIdBox() );
      newTube.setId ( tube.getId() ); 
      newTube.setCode ( tube.getCode() );  
      newTube.setTypeContainer ( tube.getTypeContainer() );
      newTube.setLevel ( tube.getLevel() );  
      newTube.setRemark ( tube.getRemark() );
      newTube.setContent ( tube.getContent() );
      newTube.setValid ( tube.getValid() ); 
      newTube.setUsable ( tube.getUsable() ); 
      newTube.setNotations ( tube.getNotations() ); 
 
      this.setTube(newTube);     //ajoute le tube à la place      
      var tab={};
      tab['idBox']=this.getIdBox();
      tab['container']=this.getTube();
      if (this.getRow() != null) {
        var listeChar=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        tab['row']=listeChar[this.getRow()-1];
      }else {
        tab['row']= null;
      };
      tab['column']=this.getColumn();
      this.fireDataEvent("containerPutOn",tab); //event de put on      
    },this); 
    
    //après le drop le transfert des données (declare ce qui est transféré)
    this.__icon.addListener("droprequest", function(e){ 
      var type = e.getCurrentType();
      //e.addData(type, this.__icon);
      e.addData(type, this.getTube());
    },this);
    
    //doubleclick pour renvoyer la position 
    this.addListener("dblclick", function(e){        
      var listeChar=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        var row =listeChar[this.getRow()-1];
      var tab={}
      tab['row']=row;
      tab['column']= this.getColumn();
      tab['container']= this.getTube();
      this.fireDataEvent("dbleClckPlace",tab);
    },this);  
	}
});
