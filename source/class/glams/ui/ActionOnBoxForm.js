/**
 * Classe qui réalise une action sur tous les container d'une box
 * Fabrice Dupuis mars 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.ActionOnBoxForm", {
  extend: qx.ui.core.Widget,

  members: {
    /**
     * Concentre toutes les informations nécessaires dans un dictionnaire et lance le service de crétaion de la nouvelle boite et des containers associés
     *
     */
    __actionOnBox: function() {
      //stocke les données dans un dictionnaire
      this.__loading.show();
      var tab = {};
      tab['date_on'] = this.__date.getValue();
      var newDate = this.__date.getValue();
      newDate.setDate(this.__date.getValue().getDate() + 1); //correction pour conserver la date
      tab['date_on'] = this.__date.getValue();
      tab['typeContainer'] = this.__typeContainer.getSelection()[0].getLabel();
      tab['typeContent'] = this.__typeContent.getSelection()[0].getLabel();
      tab['nameBoxType'] = this.__typeBox.getSelection()[0].getLabel();
      tab['nameOriginBox'] = this.__nameOriginBox.getAllValues().getLabel();
      tab['idBox'] = this.__nameOriginBox.getAllValues().getId();
      tab['nameBox'] = this.__nameBox.getValue();
      tab['serie'] = this.__experience;
      tab['typeAction'] = this.__actionType.getSelection()[0].getLabel();
      tab['notationsAction'] = [];
      tab['notationsContainer'] = [];
      tab['placesBox'] = this.__place.toArray();
      tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'))[0];
      tab['idUser'] = this.__user.getId();
      tab['protocol'] = this.__protocole.getAllValues().getId();

      for (var i in this.__otherNotationCont.getChildrenContainer()._getChildren()) {
        var x = {};
        x['type'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("type");
        x['value'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("value");
        x['date'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("date");
        x['notateur'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("notateur");
        x['remarque'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("remarque");
        tab['notationsContainer'].push(x);
      };
      for (var i in this.__otherNotationAct.getChildrenContainer()._getChildren()) {
        var x = {};
        x['type'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("type");
        x['value'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("value");
        x['date'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("date");
        x['notateur'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("notateur");
        x['remarque'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("remarque");
        tab['notationsAction'].push(x);
      };

      this.__nameBox.setValue(null);
      this.__saveActionButton.setEnabled(false);
      this.__services.QuerySetCreateBoxAndContainer("setCreateBoxAndContainer", this.__sessionId, tab);
      this.__services.addListenerOnce("setCreateBoxAndContainer", function(e) {
console.log(e.getData());
        this.__loading.exclude();
        this.__labelValidation.setValue(' Action sur boite réalisée ');
      }, this);

    }
  },

  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(10);
    this._setLayout(layout);

    //positionnement des labels
    this._add(new qx.ui.basic.Label(
      this.tr("Nom de la boite Origine: ")), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type d'action : ")), {
      row: 1,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Date : ")), {
      row: 2,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Attachée au groupe : ")), {
      row: 3,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Notations sur l'action : ")), {
      row: 4,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Nom de la boite Destination : ")), {
      row: 0,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type de boite : ")), {
      row: 1,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type de contenant : ")), {
      row: 2,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type de contenu : ")), {
      row: 3,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Positionnement : ")), {
      row: 5,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Notations sur le contenu : ")), {
      row: 4,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Protocole : ")), {
      row: 7,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Réalisation : ")), {
      row: 7,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Affecté à une expérience : ")), {
      row: 6,
      column: 2
    });


    //init of the widget
    this.__nameOriginBox = new glams.ui.FiltredComboBox();
    this.__nameOriginBox.setPlaceholder(this.tr("Filter Name ( 3 carracters minimum)"));
    this.__nameOriginBox.setWidth(200);
    this.__actionType = new qx.ui.form.SelectBox();
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    this.__groupName = new qx.ui.form.SelectBox();
    this.__nameBox = new qx.ui.form.TextField();
    this.__nameBox.setWidth(200);
    this.__typeBox = new qx.ui.form.SelectBox();
    this.__typeContainer = new qx.ui.form.SelectBox();
    this.__typeContent = new qx.ui.form.SelectBox();
    this.__user = new qxelvis.ui.users.UserSelector();
    this.__serie = new glams.ui.WidgetExperience();
    this.__protocole = new glams.ui.FiltredComboBox();
    this.__protocole.setPlaceholder(this.tr("Filter Name Protocole ( 3 carracters minimum)"));
    this.__labelValidation = new qx.ui.basic.Label('important : ');
    this.__labelValidation.setTextColor('red');
    // List pour ajout de notations
    this.__otherNotationCont = new qx.ui.form.List().set({
      height: 100
    });
    this.__otherNotationAct = new qx.ui.form.List().set({
      height: 100
    });
    //Ajout de notations sur le container
    var addWindowContainer = new glams.ui.AddContainerNotationWindow();
    var widgetNotationContainer = new qx.ui.core.Widget();
    widgetNotationContainer._setLayout(new qx.ui.layout.VBox());
    var addButtonC = new qx.ui.form.Button("+");
    var removeButtonC = new qx.ui.form.Button("-");
    widgetNotationContainer._add(this.__otherNotationCont);
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    widgetButton._add(addButtonC);
    widgetButton._add(removeButtonC);
    widgetNotationContainer._add(widgetButton);
    //ajout de notations sur l'action
    var addWindowAction = new glams.ui.AddActionNotationWindow();
    var widgetNotationAction = new qx.ui.core.Widget();
    widgetNotationAction._setLayout(new qx.ui.layout.VBox());
    var addButtonA = new qx.ui.form.Button("+");
    var removeButtonA = new qx.ui.form.Button("-");
    widgetNotationAction._add(this.__otherNotationAct);
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    widgetButton._add(addButtonA);
    widgetButton._add(removeButtonA);
    widgetNotationAction._add(widgetButton);
    //liste pour choix de la boite
    this.__selectNameBox = new qx.ui.form.TextField();
    this.__selectNameBox.setPlaceholder(this.tr("Filter Name Box"));
    //widgetPlace
    this.__boxPlaces = new glams.ui.WidgetPlace();
    var widgetLocateBox = new qx.ui.core.Widget();
    widgetLocateBox._setLayout(new qx.ui.layout.Grow());
    widgetLocateBox._add(this.__boxPlaces);

    this.__loading = new qx.ui.basic.Image("glams/loading.gif");
    this.__loading.exclude();

    //positionnement des widgets
    this._add(this.__nameOriginBox, {
      row: 0,
      column: 1
    });
    this._add(this.__actionType, {
      row: 1,
      column: 1
    });
    this._add(this.__date, {
      row: 2,
      column: 1
    });
    this._add(this.__groupName, {
      row: 3,
      column: 1
    });
    this._add(widgetNotationAction, {
      row: 4,
      column: 1
    });
    this._add(this.__nameBox, {
      row: 0,
      column: 3
    });
    this._add(this.__typeBox, {
      row: 1,
      column: 3
    });
    this._add(this.__typeContainer, {
      row: 2,
      column: 3
    });
    this._add(this.__typeContent, {
      row: 3,
      column: 3
    });
    this._add(widgetLocateBox, {
      row: 5,
      column: 3
    });
    this._add(widgetNotationContainer, {
      row: 4,
      column: 3
    });
    this._add(this.__serie, {
      row: 6,
      column: 3
    });
    this._add(this.__protocole, {
      row: 7,
      column: 3
    });
    this._add(this.__user, {
      row: 7,
      column: 1
    });
    this._add(this.__loading, {
      row: 5,
      column: 1
    });
    this._add(this.__labelValidation, {
      row: 8,
      column: 1
    });


    //enregistrement
    this.__saveActionButton = new qx.ui.form.Button("Save");
    this.__saveActionButton.setEnabled(false);
    this._add(this.__saveActionButton, {
      row: 9,
      column: 3
    });

    //validator du mon de boite**************************************************
    // create the form manager
    var manager = new qx.ui.form.validation.Manager();
    // create a async validator function
    var that = this;
    var boxNameValidator = new qx.ui.form.validation.AsyncValidator(function(validator, value) {
      validator.setValid(true);
      that.__saveActionButton.setEnabled(true);
      var tab = {};
      tab['writeGroup'] = JSON.parse(localStorage.getItem('writeGroups'))[0];
      that.__services.QueryGetAllBox("getAllBox", this.__sessionId, tab);
      that.__services.addListener("getAllBox", function(e) {
        var loadData = e.getData();
        for (var i in loadData) {
          if (that.__nameBox.getValue() == loadData[i]['namebox']) {
            validator.setValid(false, "Nom de boite existant, entrez un nouveau nom de boite");
            this.__saveActionButton.setEnabled(false);
          };
        };
      }, that);
    }, this);
    manager.add(this.__nameBox, boxNameValidator);

    /******************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
    // Session management
    this.__session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = this.__session.getSessionId();

    this.__services.QueryGetAllContentType("getAllContentType", null, null);
    this.__services.QueryGetAllActionType("getAllActionType", null, null);
    this.__services.QueryAllTissuType("loadedAllTissuType", null, null); //déjà présent dans un autre onglet
    this.__services.QueryGetAlltypesLieux("getAlltypesLieux", this.__sessionId, null);
    this.__services.QueryGetAllTypesBox("getAllTypesBox", this.__sessionId, null);
    var tab = {};
    tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));
    this.__services.QueryGetAllExperiments("getAllExperiments", this.__sessionId, tab);
    this.__services.QueryAllUser("loadedAllUser", null, null);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************notation
    */

    //liste des boites
    //initialise l'autocompleBox
    this.__nameOriginBox.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {};
        tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
        tab['filtredText'] = e.getData();
        this.__services.QueryGetAllBoxsFiltred("getAllBoxsFiltred", this.__sessionId, tab);
      };
    }, this);
    //liste des noms de boite dans la liste à selectionner
    this.__services.addListener("getAllBoxsFiltred", function(e) {
      var loadData = e.getData();

      var itemData2 = [];
      for (var i in loadData) {
        itemData2.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      };
      this.__nameOriginBox.setListModel(itemData2);
      this.__nameOriginBox.open();
    }, this);

    // Permet de récupérer les collections via le WidgetExperience
    this.__serie.addListener("changeListCollection", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.__experience = loadData;
    }, this);

    //type de contenus
    this.__services.addListener("getAllContentType", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__typeContent.add(tempItem);
      };
    }, this);
    //type de actions
    this.__services.addListener("getAllActionType", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__actionType.add(tempItem);
      };
    }, this);
    //choix du type de contenant
    this.__services.addListener("getAllTypeContainerByBox", function(e) {
      this.__typeContainer.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__typeContainer.add(tempItem);
      };
    }, this);

    //affiche du type des boites
    this.__services.addListener("getAllTypesBox", function(e) {
      this.__typeBox.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].name);
        this.__typeBox.add(tempItem);
      };
    }, this);

    //test affichage filtredComboBox
    this.__protocole.addListener("changeAllValues", function(e) {
      console.log("this.__protocole.changeAllValues()");
      var allVal = this.__protocole.getAllValues()
      //  console.log(allVal.getId());
      //  console.log(allVal.getLabel());
    }, this);

    //affichage du type de container
    this.__typeBox.addListener("changeSelection", function() {
      var tab = {};
      tab['typeBox'] = this.__typeBox.getSelection()[0].getLabel();
      this.__services.QueryGetAllTypeContainerByBox("getAllTypeContainerByBox", this.__sessionId, tab);
    }, this);

    //bouton de notations
    addButtonA.addListener("execute", function() {
      addWindowAction.updateData();
      addWindowAction.open();
      addWindowAction.center();
    }, this);
    //bouton de notations
    addButtonC.addListener("execute", function() {
      addWindowContainer.updateData();
      addWindowContainer.open();
      addWindowContainer.center();
    }, this);

    //bouton de notations
    addWindowContainer.addListener("addNotationSample", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value + " ; " + loadData.date + " ; " + loadData.notateur + " ; " + loadData.remarque);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      tempItem.setUserData("date", loadData.date);
      tempItem.setUserData("notateur", loadData.notateur);
      tempItem.setUserData("remarque", loadData.remarque);
      this.__otherNotationCont.add(tempItem);
    }, this);

    //bouton de notations
    removeButtonC.addListener("execute", function() {
      this.__otherNotationCont.getSelection()[0].destroy();
    }, this);

    //bouton de notations
    addWindowAction.addListener("addNotationAction", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value + " ; " + loadData.date + " ; " + loadData.notateur + " ; " + loadData.remarque);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      tempItem.setUserData("date", loadData.date);
      tempItem.setUserData("notateur", loadData.notateur);
      tempItem.setUserData("remarque", loadData.remarque);
      this.__otherNotationAct.add(tempItem);
    }, this);

    //bouton de notations
    removeButtonA.addListener("execute", function() {
      this.__otherNotationAct.getSelection()[0].destroy();
    }, this);
    //emplacement de la boite
    this.__place = [];
    this.__boxPlaces.addListener("changeListPlaces", function(e) {
      this.__place = e.getData();
    }, this);

    //activation du manager de validation
    this.__nameBox.addListener("changeValue", function() {
      manager.validate();
    }, this);

    // Nom des groupes
    this.__session.requestGroupsInfo();
    this.__session.addListener("gotGroupsInfo", function(e) {
      var listeGroups = e.getData();
      for (var g in listeGroups) {
        var tempItem = new qx.ui.form.ListItem(listeGroups[g]['name']);
        this.__groupName.add(tempItem);
      };
    }, this);

    //noms des utilisateurs pour le choix du user
    this.__services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__user.setActiveUser(e.getData());
    }, this);

    //protocole
    // Fonctions qui chargent les protocole en fonction des carractères saisi dans la barre de texte
    this.__protocole.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {};
        tab['filtredName'] = e.getData();
        this.__services.QueryGetAllProtocolFiltred("getAllProtocolFiltred", this.__sessionId, tab);
      };
    }, this);
    // Listener pour récuperer les données protocoles
    this.__listProtocol = [];
    this.__services.addListener("getAllProtocolFiltred", function(e) {
      this.__listProtocol = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listProtocol.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      };
      this.__protocole.setListModel(this.__listProtocol);
    }, this);


    //this.__saveActionButton
    this.__saveActionButton.addListener("execute", function() {
      this.__labelValidation.setValue('Important : ');
      if (!this.__user.getId()) {
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Renseigner l'utilisateur ");
      } else {
        if (this.__place.length == 0) {
          this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Renseigner l'emplacement de la nouvelle boite ");
        } else {
          if (!this.__nameOriginBox.getValue()) {
            this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Renseigner le nom de la boite origine ");
          } else {
            if (!this.__nameBox.getValue()) {
              this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Renseigner le nom de la boite destination ");
            } else {

              this.__actionOnBox();

            };
          };
        };
      };
    }, this);
  }
});
