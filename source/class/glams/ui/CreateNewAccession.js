/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new lot from an variete lot in the database.
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.CreateNewAccession", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * Genre of the plant to create
     */
    genre: {
      init: null,
      event: "changeGenre"
    },
    /**
     * Variete of the new Accession
     */
    variete: {
      init: null,
      event: "changeVariete"
    },
    /**
     * Name of the new Accession
     */
    nameAccession: {
      init: null,
      event: "changeNameAccession"
    },
    /**
     * Remark about the new Accession
     */
    remarqueAccession: {
      init: null,
      event: "changeRemarqueAccession"
    },
    /**
     * Name of the New Lot
     */
    newNameLot: {
      init: null,
      event: "changeNewNameLot"
    },
    /**
     * Date of introduction of the new Lot
     */
    date: {
      init: null,
      event: "changeDate"
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.HBox();
    layout.setSpacing(15);
    this._setLayout(layout);


    //Implémentation des widget visible à l'écran
    var containerSaisie = new qx.ui.container.Composite(new qx.ui.layout.VBox().set({
      spacing: 5
    }));
    this._add(containerSaisie);

    var containerAffichage = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this._add(containerAffichage);

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var varOrigine = new qx.ui.container.Composite(grid);
    containerSaisie._add(varOrigine);

    varOrigine._add(new qx.ui.basic.Label(
      this.tr("Variété d'origine")), {
      row: 0,
      column: 0
    });
    this.__varOrigine = new glams.ui.FiltredComboBox();
    varOrigine._add(this.__varOrigine, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var nameNewAccession = new qx.ui.container.Composite(grid);
    containerSaisie._add(nameNewAccession);


    nameNewAccession._add(new qx.ui.basic.Label(
      this.tr("Nouvelle Accession : ")), {
      row: 1,
      column: 0
    });
    this.__newAcc = new qx.ui.form.TextField();
    this.__newAcc.setPlaceholder(this.tr("Numéro d'Accession"));
    nameNewAccession._add(this.__newAcc, {
      row: 1,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var remarqueAccession = new qx.ui.container.Composite(grid);
    containerSaisie._add(remarqueAccession);

    remarqueAccession._add(new qx.ui.basic.Label(
      this.tr("Remarque")), {
      row: 2,
      column: 0
    });
    this.__remarqueAccession = new qx.ui.form.TextArea();
    remarqueAccession._add(this.__remarqueAccession, {
      row: 2,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var nameNewLot = new qx.ui.container.Composite(grid);
    containerSaisie._add(nameNewLot);

    nameNewLot._add(new qx.ui.basic.Label(
      this.tr("Numéro de Lot")), {
      row: 3,
      column: 0
    });
    this.__nameNewLot = new qx.ui.form.TextField();
    this.__nameNewLot.setPlaceholder(this.tr("Numéro de Lot"));
    nameNewLot._add(this.__nameNewLot, {
      row: 3,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var dateVariete = new qx.ui.container.Composite(grid);
    containerSaisie._add(dateVariete);

    dateVariete._add(new qx.ui.basic.Label(
      this.tr("Date d'introduction")), {
      row: 4,
      column: 0
    });
    this.__dateLot = new qx.ui.form.DateField();
    this.__dateLot.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__dateLot.setValue(new Date());
    var format = new qx.util.format.DateFormat("dd/MM/YYYY");
    var date = format.format(this.__dateLot.getValue());
    this.setDate(date);
    dateVariete._add(this.__dateLot, {
      row: 4,
      column: 1
    });


    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Variété d'origine : ")), {
      row: 0,
      column: 0
    });
    var lblVarOr = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblVarOr, {
      row: 0,
      column: 1
    });
    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Accession déjà existante : ")), {
      row: 1,
      column: 0
    });
    var lblAccEx = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblAccEx, {
      row: 1,
      column: 1
    });
    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Lot déjà existante : ")), {
      row: 2,
      column: 0
    });
    var lblLotEx = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblLotEx, {
      row: 2,
      column: 1
    });

    // var validationNomLot = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez renseigner le nom du nouveau Lot");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );
    // var validationNomAcc = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez renseigné le nom de la nouvelle Accession");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );
    // var validationSelectVar = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez selectionner une Variete");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );
    //
    // this.__manager.add(this.__nameNewLot, validationNomLot);
    // this.__manager.add(this.__newAcc, validationNomAcc);
    // this.__manager.add(this.__varOrigine, validationSelectVar);

    /*
     *****************************************************************************
      SERVICE
     *****************************************************************************
     */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */


    //charge toutes les Varité en fonction du genre de la plante modèle
    this.__varOrigine.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {};
        tab['nomVar'] = loadData;
        tab['genre'] = this.getGenre();
        this.__services.QueryGetListeVarieteByNomByGenre("getListeVarieteByNomByGenre", this.__sessionId, tab);
      };
    }, this);

    this.__services.addListener("getListeVarieteByNomByGenre", function(e) {
      this.__listVarOrigine = [];
      var loadData = e.getData();
      for (var i in loadData) {
        for (var j in loadData[i]['noms']) {
          this.__listVarOrigine.push({
            'label': loadData[i]['noms'][j]['valeur'],
            'idVar': loadData[i]['id_variete'],
            'accession': loadData[i]['accession'],
            'lot': loadData[i]['lot']
          });
        };
      };
      this.__varOrigine.setListModel(this.__listVarOrigine);
    }, this);

    //Affiche les informations relatives à la variété choisi par l'utilisateur
    this.__varOrigine.addListener("changeAllValues", function() {
      var idVariete = this.__varOrigine.getAllValues().getIdVar();
      this.__services.QueryGetAllNomsVarieteByIdVariete("getAllNomsVarieteByIdVariete", idVariete, null);
      this.__lot = this.__varOrigine.getAllValues().getLot().toArray();
      this.__accession = this.__varOrigine.getAllValues().getAccession().toArray();
      var label = 1
      for (var i in this.__lot) {
        if (label == 1) {
          label = this.__lot[i] + "</br>"
        } else {
          label = label + this.__lot[i] + "</br>"
        };
      };
      lblLotEx.setValue(label);
      label = 1
      for (var i in this.__accession) {
        if (label == 1) {
          label = this.__accession[i] + "</br>"
        } else {
          label = label + this.__accession[i] + "</br>"
        };
      };
      lblAccEx.setValue(label);
      this.setVariete(this.__varOrigine.getAllValues().getIdVar());
    }, this);

    //Récupère les noms d'une variété en fonction de son ID et l'affiche ensuite
    this.__services.addListener("getAllNomsVarieteByIdVariete", function(e) {
      var loadData = e.getData();
      var label = 1
      for (var i in loadData) {
        var type = loadData[i].type;
        var nom = loadData[i].nom;
        if (label == 1) {
          label = type + " : " + nom + "</br>"
        } else {
          label = label + type + " : " + nom + "</br>"
        };
      }
      lblVarOr.setValue(label);
    }, this);

    this.__newAcc.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNameAccession(loadData);
    }, this);

    this.__remarqueAccession.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setRemarqueAccession(loadData);
    }, this);

    this.__nameNewLot.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNewNameLot(loadData);
    }, this);

    this.__dateLot.addListener("changeValue", function() {
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      var date = format.format(this.__dateLot.getValue());
      this.setDate(date);
      console.log(this.getDate());
    }, this);
  },

  members: {

  }
});