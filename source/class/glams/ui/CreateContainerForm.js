/**
 * Formulaire de create new contenair
 * Fabrice Dupuis novembre 2017
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.CreateContainerForm",{
   extend : qx.ui.core.Widget,
	
   events : {

    /**
    * event for update all mapBox
    */
	  "updateMapBox" : "qx.event.type.Event",
    /**
    * Fire when a container is created and put into a box
    */
	  "NewContainerSaved" : "qx.event.type.Data",
	  /**
    * Clear the page
    */
	  "resetSample" : "qx.event.type.Event" ,  
	  /**
     * Fired quand on doubleclick sur une place sur un plan de boite
     */
    "dbleClckPlace" : "qx.event.type.Data"
  },
	
  properties : {
    __sessionId : {
      init : null
    }
  },
	
  members : {    

  /**
    * Vérifie que la place est libre
    *@param colName {string} name of the column
    *@param rowName {string} name of the row    
    *@param typeBox {string} type of the box    
    *@param namrBox {string} name of the box    
    */                        
    __controlPlace : function(colName,rowName,  typeBox, nameBox){
      //1-recupère idBox
      this.__tab={};
      this.__tab['name']=nameBox;
      this.__tab['type']=typeBox;
      this.__tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'))[0];
      this.__services.QueryGetInfosBoxBoxType("getInfosBoxBoxType", this.__sessionId, this.__tab);
      this.__services.addListenerOnce("getInfosBoxBoxType", function(e){
        if (this.__tab['name']==e.getData()[0]['namebox'] && this.__tab['type']==e.getData()[0]['typebox']){        
          this.__idBox=(e.getData()[0]['idbox']) 
          //2- recupère les positions des tubes dans cette boite  
          this.__tab['idBox']=this.__idBox;
          this.__services.QueryGetAllContainersByBox("getAllContainersByBox", this.__sessionId, this.__tab);
          this.__services.addListenerOnce("getAllContainersByBox", function(e){
            var loadData=e.getData()["container"];
            if(loadData.length >0 ){
              if (loadData[0]['id_box']==this.__tab['idBox']){   //controle qu'il s'agit de la bonne boite         
                for (var c in loadData){           
                  var tab={};            
                    tab['idContainer']=loadData[c]['id_container'];
                    tab['code']=loadData[c]['code'];
                    tab['rowLabel']=loadData[c]['row'];
                    tab['columnLabel']=loadData[c]['column'];
                    tab['dateOn']=loadData[c]['date_on']; 
                    tab['dateOff']=loadData[c]['date_off'];          
                    if ( rowName== tab['rowLabel']  && colName==tab['columnLabel'] && tab['dateOff'] == null){
//                    if ( rowName== tab['rowLabel']  && colName==tab['columnLabel'] ){
                      //this.__saveButton off
                      this.__saveButton.setEnabled(false);
                      //affichage message d'erreur
                      var errorMessageWindow = new qx.ui.window.Window("message d'erreur");
                      errorMessageWindow.setLayout(new qx.ui.layout.VBox());
                      errorMessageWindow.open(); 
                      errorMessageWindow.moveTo(600,10);
                      errorMessageWindow.add(new qx.ui.basic.Label("Ligne : "+rowName+" Colonne : "+colName));
                      errorMessageWindow.add(new qx.ui.basic.Label("Place déjà occupée "));
                      var okButton=new qx.ui.form.Button("OK");
                      okButton.setWidth(40);
                      errorMessageWindow.add(okButton);
                      okButton.addListener("execute", function (){errorMessageWindow.close()},this);
                      
                    };
                };
              };
            };  
          },this)       
        };     
      },this);              
    },
    /**
    * Liste des boites ouvertes pour controle et modifications
    */
    __listeOpenBox : []
  },
	
	construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
		var layout = new qx.ui.layout.Grid();
		layout.setSpacing(10);
		this._setLayout(layout);
		
		//positionnement des labels
		this._add(new qx.ui.basic.Label(
		  this.tr("Code ou Nom : ")), 
		  {row : 0, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Date de creation : ")), 
		  {row : 1, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Type Contenant : ")), 
		  {row : 2, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Type Boite : ")), 
		  {row : 3, column : 0});		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Stockage : ")), 
		  {row : 4, column : 0});		  		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Type contenu : ")), 
		  {row : 5, column : 0});		  		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Remarques : ")), 
		  {row : 6, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Notations : ")), 
		  {row : 7, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Affecté à une expérience : ")), 
		  {row : 8, column : 0});


		
		//init of the widget
		this.__name = new qx.ui.form.TextField();
		this.__date = new qx.ui.form.DateField();
		this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY")); 
		this.__date.setValue(new Date());
		this.__container = new qx.ui.form.SelectBox();
		this.__typeBox = new qx.ui.form.SelectBox();		
		this.__content = new qx.ui.form.SelectBox();		
		this.__remarks = new qx.ui.form.TextArea();
		this.__remarks.setHeight(25);

		this.__serie = new glams.ui.WidgetExperience();		
		this.__experience=[];
		// List pour ajout de notations
		this.__otherNotation = new qx.ui.form.List().set({
		  height : 100
		});
		var addWindow = new glams.ui.AddContainerNotationWindow();
		var widgetNotation = new qx.ui.core.Widget();
		widgetNotation._setLayout(new qx.ui.layout.VBox());
		var addButton = new qx.ui.form.Button("+");
		var removeButton = new qx.ui.form.Button("-");
		widgetNotation._add(this.__otherNotation);
		var widgetButton = new qx.ui.core.Widget();
		widgetButton._setLayout(new qx.ui.layout.HBox());
		widgetButton._add(addButton);
		widgetButton._add(removeButton);
		widgetNotation._add(widgetButton);
		//liste pour choix de la boite
		this.__selectNameBox = new qx.ui.form.TextField();
		this.__selectNameBox.setPlaceholder(this.tr("Filter Name Box"));
		var widgetNameBox = new qx.ui.core.Widget();
		widgetNameBox._setLayout(new qx.ui.layout.VBox());
		this.__listNameBox = new qx.ui.form.List().set({
		  height : 100
		});
		widgetNameBox._add(this.__listNameBox);
		widgetNameBox._add(new qx.ui.basic.Label(this.tr("double click = plan de boite")));
		widgetNameBox._add(new qx.ui.basic.Label(this.tr("sur la boite click gauche = mouvement")));
		widgetNameBox._add(new qx.ui.basic.Label(this.tr("sur la boite click droit = informations")));
		//choix name ou create box
		var widgetNameOrCreateBox = new qx.ui.core.Widget();
		widgetNameOrCreateBox._setLayout(new qx.ui.layout.VBox());
		widgetNameOrCreateBox._add(new qx.ui.basic.Label(this.tr("Code de la boite : "))); 
		widgetNameOrCreateBox._add(new qx.ui.basic.Label(this.tr("Ou")));
		this.__createBoxButton = new qx.ui.form.Button("Nouvelle boite");
		this.__createBoxButton.setWidth(5);
		widgetNameOrCreateBox._add(this.__createBoxButton);
		//positionnement dans la boite
		var widgetLocateBox= new qx.ui.core.Widget();
		widgetLocateBox._setLayout(new qx.ui.layout.VBox());
		this.__colName = new qx.ui.form.TextField();
		this.__rowName = new qx.ui.form.TextField();
		widgetLocateBox._add(new qx.ui.basic.Label(this.tr("Code de la ligne : "))); 
		widgetLocateBox._add(this.__rowName);
		widgetLocateBox._add(new qx.ui.basic.Label(this.tr("Code de la colonne : ")));
		widgetLocateBox._add(this.__colName);
		//positionnement des widgets
		this._add(this.__name, {row : 0, column : 1});
		this._add(this.__date, {row : 1, column : 1});
		this._add(this.__container, {row : 2, column : 1});
		this._add(this.__typeBox, {row : 3, column : 1});
		this._add(widgetNameOrCreateBox, {row : 4, column : 1});
		this._add(this.__content, {row : 5, column : 1});
		this._add(this.__remarks, {row : 6, column : 1});
		this._add(widgetNotation, {row : 7, column : 1});
		this._add(this.__serie, {row : 8, column : 1, colSpan: 2});
		this._add(this.__selectNameBox, {row : 3, column : 2});
		this._add(widgetNameBox, {row : 4, column : 2});
    this._add(widgetLocateBox, {row : 4, column : 3});
		             
		//enregistrement
		this.__saveButton = new qx.ui.form.Button("Save");	
		this.__saveButton.setEnabled(false);	
		this._add(this.__saveButton, {row : 9, column : 3});
		
				

		
		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
    
    this.__services.QueryGetAllContentType("getAllContentType", null, null);
                    
    this.__services.QueryAllTissuType("loadedAllTissuType", null, null);//déjà présent dans un autre onglet
    this.__services.QueryAllTypeContenant("loadedAllTypeContenant", null, null); //déjà présent dans un autre onglet
    
    
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */ 
    //listener sur series
    // Permet de récupérer les collections via le WidgetExperience
    this.__serie.addListener("changeListCollection", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.__experience = loadData;
    }, this);
    
    this.__services.addListener("getAllContentType", function(e){
      var loadData = e.getData();
      for(var i in loadData){
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__content.add(tempItem);
      };
    }, this);
    
    //choix du type de contenant
    this.__services.addListenerOnce("loadedAllTypeContenant", function(e){
      this.__container.removeAll();
      var loadData = e.getData();
      for(var i in loadData){
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__container.add(tempItem);
      };
    }, this);


    //ajout du nom des boites
    this.__services.addListener("getAllNameBoxByType", function(e){
      this.__listNameBox.removeAll();
      var loadData = e.getData();
      for(var i in loadData){
        var tempItem = new qx.ui.form.ListItem(loadData[i].name);
        tempItem.setUserData("idBox", loadData[i]['id'] );
        
        this.__listNameBox.add(tempItem);
      };              
    }, this);
    
    //affiche du type des boites
    this.__services.addListener("getAllTypeBoxByType", function(e){
      this.__typeBox.removeAll();
      var loadData = e.getData();
      for(var i in loadData){
        var tempItem = new qx.ui.form.ListItem(loadData[i].name);
        this.__typeBox.add(tempItem);
      }; 
      //recupère les noms des boites en fonction du type de boite et du userGroup
      var tab={};
      tab['typeBox']=this.__typeBox.getSelection()[0].getLabel(); 
      tab['userGroup']=  JSON.parse(localStorage.getItem('readGroups')); 
      tab['selectString']=this.__selectNameBox.getValue();
      this.__services.QueryGetAllNameBoxByType("getAllNameBoxByType",this.__sessionId, tab);
    }, this);       
    
    //Affichage des noms des boites
    this.__container.addListener("changeSelection", function(){   
      var tab={};
      tab['typeContainer']=this.__container.getSelection()[0].getLabel();  
      this.__services.QueryGetAllTypeBoxByType("getAllTypeBoxByType",this.__sessionId, tab);           
    }, this);
    
    this.__typeBox.addListener("changeSelection", function(){
    //recupère les noms des boites en fonction du type de boite et du userGroup
      if (this.__typeBox.getSelection().length >0) {
        var tab={};
        tab['typeBox']=this.__typeBox.getSelection()[0].getLabel(); 
        tab['userGroup']= JSON.parse(localStorage.getItem('readGroups')); 
        tab['selectString']=this.__selectNameBox.getValue();
        this.__services.QueryGetAllNameBoxByType("getAllNameBoxByType",this.__sessionId, tab);
        };
    }, this);
    this.__selectNameBox.addListener("changeValue", function(){
      var tab={};
      tab['typeBox']=this.__typeBox.getSelection()[0].getLabel(); 
      tab['userGroup']= JSON.parse(localStorage.getItem('readGroups')); 
      tab['selectString']=this.__selectNameBox.getValue();
      this.__services.QueryGetAllNameBoxByType("getAllNameBoxByType",this.__sessionId, tab);
    }, this);
         
    
    //selection de la boite
    this.__listNameBox.addListener('click', function(e){
      this.__colName.setValue(null);
      this.__rowName.setValue(null);
    }, this);
    
    //creation nouvelle boite
    this.__createBoxButton.addListener("execute", function(){
      var createBoxWindow = new qx.ui.window.Window("Création d\'une nouvelle boite");
      createBoxWindow.setLayout(new qx.ui.layout.VBox());
      createBoxWindow.open(); 
      createBoxWindow.moveTo(600,10);
      var createBox= new glams.ui.CreateBoxForm();    
      createBox.setBoxTypeName(this.__typeBox.getSelection()[0].getLabel());
      createBox.setDate_on(this.__date.getValue());
      createBoxWindow.add(createBox);
      this.__typeBox.addListener("changeSelection", function(){
        if (this.__typeBox.getSelection().length >0) {
          createBox.setBoxTypeName(this.__typeBox.getSelection()[0].getLabel());
        };
      }, this);
      //listener quand la nouvelle boite est créee
      createBox.addListener("newBoxCreated", function(){
        //ferme la fenêtre
        createBoxWindow.close();
        //affiche nouvelle boite
        var tab={};
        tab['typeBox']=this.__typeBox.getSelection()[0].getLabel(); 
        tab['userGroup']= JSON.parse(localStorage.getItem('readGroups'));
        tab['selectString']=this.__selectNameBox.getValue();
        this.__services.QueryGetAllNameBoxByType("getAllNameBoxByType",this.__sessionId, tab);
      }, this);
      
    }, this);
    
    //majuscule des positions dans la boite
    this.__colName.addListener("changeValue",function(){
      if (this.__colName.getValue() != null){
        this.__colName.setValue(this.__colName.getValue().toUpperCase());
      };
    }, this);
    this.__rowName.addListener("changeValue",function(){
      if (this.__rowName.getValue() != null){
        this.__rowName.setValue(this.__rowName.getValue().toUpperCase());
      };
    }, this);  
      
    //grisé des positions ligne et colonne
    this.__typeBox.addListener("changeSelection", function(){
      if (this.__typeBox.getSelection().length >0) {
      var tab={};
      tab['typeBox']=this.__typeBox.getSelection()[0].getLabel();
      this.__services.QueryGetInfosBoxByType("getInfosBoxByType",this.__sessionId, tab);
      this.__services.addListener("getInfosBoxByType", function(e){
        var loadData = e.getData();
        if (tab['typeBox']==loadData[0]['name']){
          if ( !loadData[0]['row_number']){
            this.__colName.setEnabled(false);
            this.__rowName.setEnabled(false); 
            this.__colName.setOpacity(0);
            this.__rowName.setOpacity(0);      
          }else{
            this.__colName.setEnabled(true);
            this.__rowName.setEnabled(true);  
            this.__colName.setOpacity(10);   
            this.__rowName.setOpacity(10);  
          };
          //validité des données saisies
        };
      }, this);
      };
    }, this);
      
    //bouton de notations
    addButton.addListener("execute", function(){
      addWindow.updateData();
		  addWindow.open();
		  addWindow.center();
		}, this);
    //bouton de notations		
    addWindow.addListener("addNotationSample", function(e){
		  var loadData = e.getData();
		  var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value+" ; "+loadData.date+" ; "+loadData.notateur+" ; "+loadData.remarque);
		  tempItem.setUserData("type", loadData.type);
		  tempItem.setUserData("value", loadData.value);
		  tempItem.setUserData("date", loadData.date);
		  tempItem.setUserData("notateur", loadData.notateur);
			tempItem.setUserData("remarque", loadData.remarque);	  
		  this.__otherNotation.add(tempItem);
		}, this);
    //bouton de notations		
    removeButton.addListener("execute", function(){
		  this.__otherNotation.getSelection()[0].destroy();
		}, this);
    
    //bouton de reset		
    this.addListener("resetSample", function(){
      this.__name.setValue(null);
      this.__date.setValue(new Date());
      this.__user.resetSelection();
      this.__ref.setValue(null);
      this.__quantity.setValue(null);
      this.__otherNotation.removeAll();
      this.__remarks.setValue(null);
		}, this);
//*********************	affichage plans de boite*********************************************	
    //affichage du plan de la boite apres dblclick
    this.__listNameBox.addListener("dblclick", function(){   
      this.__idBox = this.__listNameBox.getSelection()[0].getUserData("idBox");
      //teste si la boite est déjà ouverte
      var boite='absente';
       for (var l in this.__listeOpenBox){
        if ( this.__idBox == this.__listeOpenBox[l]['idBox']){ 
          var boite='présente';
          break;
        };
      };
      if (boite=='absente'){
        //creation de la structure data dataBox
        this.__serviceBox.createBox(this.__idBox);
        //creation de la structure graphique MapBox
        this.__mapBox = new glams.ui.MapBox();
        this.__mapBox.setAllowStretchX(true);
        //creation de la fenêtre d'affichage du plan
        var planBoxWindow = new qx.ui.window.Window(this.__listNameBox.getSelection()[0].getLabel());
        planBoxWindow.setWidth(200);
        planBoxWindow.moveTo(Math.round(850+Math.random()*80),Math.round(70+Math.random()*80));
        planBoxWindow.setLayout(new qx.ui.layout.Grow());
        planBoxWindow.open();
        
        //listener sur la fenêtre planBoxWindow
        planBoxWindow.addListener("close", function (e){ //si fermeture alors mise à jour de la liste des boites ouvertes
        for (var l in this.__listeOpenBox){
          if (planBoxWindow  == this.__listeOpenBox[l]['planBoxWindow']){ 
            this.__listeOpenBox[l]['mapBox'].destroy();
            this.__listeOpenBox[l]['dataBox'].destroy();
            this.__listeOpenBox.splice(l,1);           
            break;
        };
      };
        
        }, this);
        
        //listeners sur la couche data      
        this.__serviceBox.addListenerOnce("createBox", function(e){ //toutes les données sont récupérées
          this.__dataBox=e.getData(); 
          this.__mapBox.setIdBox(this.__dataBox.getIdBox());
          this.__mapBox.setRowNumber(this.__dataBox.getRowNumber());
          this.__mapBox.setColumnNumber(this.__dataBox.getColumnNumber());
          this.__mapBox.setPlace(this.__dataBox.getListePlaces());
          var tab={};
          tab['idBox']=this.__idBox;
          tab['mapBox']=this.__mapBox;
          tab['dataBox']=this.__dataBox;
          tab['planBoxWindow']=planBoxWindow;
          this.__listeOpenBox.push(tab);
          
          this.__dataBox.addListenerOnce("saved", function(e){ //les données sont sauvegardée dans la base. Effectue un reset d'affichage après
          //console.log( "boite sauvée in DB "+e.getData());        
            for (var l in this.__listeOpenBox){
              this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
              this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
                this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
                this.__listeOpenBox[l]['mapBox'].update();
            
              },this);
            };
          },this);
          
        },this);
      };  

      //listener sur la couche graphique
      this.__mapBox.addListener("drowed", function(){//la structure de dessin est réalisée
       planBoxWindow.add(this.__mapBox);
      },this); 
      this.__mapBox.addListener("updateOnecontainer", function(e){//les valeurs d'un container sont modifiées
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData);
            this.__listeOpenBox[l]['mapBox'].update();
            break;
          };
        };
        
      },this);    
      this.__mapBox.addListener("saveBox", function(e){//sauvegarde la box sur elvis
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].saveBox();
        };          
      },this);  
      
      this.__mapBox.addListener("reInitialiseMapBox", function(e){//reinitialise toutes les box pour effacer les modifications
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
          this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
            this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
            this.__listeOpenBox[l]['mapBox'].update();
          },this);

        };
      },this);    
      this.__mapBox.addListener("containerPutOff", function(e){//action drag du drag and drop
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            loadData['container'].setDateOff(new Date());
//console.log("supprime un tube sur : "+loadData['idBox']+ " "+loadData['container'].getId()+" date on :  "+loadData['container'].getDateOn()+" date off :  "+loadData['container'].getDateOff());       
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData['container']);        
            break;
          };
        };
      },this);  
         
      this.__mapBox.addListener("containerPutOn", function(e){//action drop du drag and drop
        var loadData=e.getData();
        //cree un nouveau container
        loadData['container'].setDateOn(new Date());
        loadData['container'].setDateOff(null);
        if (loadData['row'] == null ) {
          loadData['container'].setRow(null);
        }else{
          loadData['container'].setRow(loadData['row'].toString());
        }; 
        if (loadData['column'] == null ) {
          loadData['container'].setColumn(null);
        }else{
          loadData['container'].setColumn(loadData['column'].toString());
        };        
        loadData['container'].setIdBox(loadData['idBox'] );
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
//console.log("ajout un tube sur : "+loadData['idBox']+ " "+loadData['container'].getId()+ " "+loadData['container'].getRow()+" "+loadData['container'].getColumn()+" "+loadData['container'].getDateOn()+" dateOff : "+loadData['container'].getDateOff());       
            this.__listeOpenBox[l]['dataBox'].addContainer(loadData['container'],loadData['container'].getRow(),loadData['container'].getColumn(),loadData['container'].getDateOn(),loadData['container'].getDateOff());       
            break;
          };
        };    
      },this);      
      //doubleclick sur la place renvoie les informations
      this.__mapBox.addListener("dbleClckPlace", function(e){ 
        this.fireDataEvent("dbleClckPlace",e.getData());
      },this);              
      //listener au niveau de la fenêtre
      this.addListener("NewContainerSaved", function(e){//modification du plan de boite si ajout de tube
        var loadData=e.getData();
        if (loadData==this.__listNameBox.getSelection()[0].getUserData("idBox")){    
          this.__dataBox.fireDataEvent("changeIdBox",loadData); 
        };   
      }, this);
                
          
    }, this); //fin du listener dblclick ******************************************************************************************************************
     
    //affecte les valeurs de ligne et colonne quand on 2click sur le plan de la boite
    this.addListener("dbleClckPlace",function(e){
      var pos=e.getData();
      this.__rowName.setValue(pos['row']);
      this.__colName.setValue(pos['column'].toString());

    }, this);
     
    //verification que la place dans la boite est libre
    this.__colName.addListener("changeValue",function(){
      if (this.__listNameBox.getSelection()[0]!= null){
        if (this.__rowName.getValue() != null && this.__colName.getValue() != null){
          if (this.__rowName.getValue().length>0 && this.__colName.getValue().length>0){
            this.__controlPlace(this.__colName.getValue(),this.__rowName.getValue(),this.__typeBox.getSelection()[0].getLabel(),this.__listNameBox.getSelection()[0].getLabel());
          }; 
        };
      }; 
    }, this);
    this.__rowName.addListener("changeValue",function(){
      if (this.__listNameBox.getSelection()[0]!= null){
        if (this.__rowName.getValue() != null && this.__colName.getValue() != null){
          if (this.__rowName.getValue().length>0 && this.__colName.getValue().length>0){
            this.__controlPlace(this.__colName.getValue(),this.__rowName.getValue(),this.__typeBox.getSelection()[0].getLabel(),this.__listNameBox.getSelection()[0].getLabel());
          };
        };
      };
    }, this);    
    
    //activation du boutton save après la liste de nameBox
    this.__listNameBox.addListener("changeSelection", function(){
      if ( this.__colName.getEnabled() == false) {
        this.__saveButton.setEnabled(true);   
      }else{this.__saveButton.setEnabled(false);};
    }, this);
    //activation du boutton save après code ligne
    this.__colName.addListener("changeValue",function(){
      if (this.__rowName.getValue() != null && this.__colName.getValue() != null){
        if (this.__rowName.getValue().length>0 && this.__colName.getValue().length>0){
          if (this.__listNameBox.getSelection()[0]!= null){
            this.__saveButton.setEnabled(true);
          };
        }else{this.__saveButton.setEnabled(false);}; 
      }else{this.__saveButton.setEnabled(false);};
    }, this);
    this.__rowName.addListener("changeValue",function(){
      if (this.__rowName.getValue() != null && this.__colName.getValue() != null){
        if (this.__rowName.getValue().length>0 && this.__colName.getValue().length>0){
          if (this.__listNameBox.getSelection()[0]!= null){
            this.__saveButton.setEnabled(true);
          };
        }else{this.__saveButton.setEnabled(false);};
      }else{this.__saveButton.setEnabled(false);};
    }, this);    
    //activation du boutton save apres typeBox
    this.__typeBox.addListener("changeSelection", function(){
      this.__saveButton.setEnabled(false);
    }, this);
   
    //this.__saveButton
    this.__saveButton.addListener("execute", function(){
      //stocke les données dans un dictionnaire
       
      var tab={}; //tab['']= ;
      tab['code']=this.__name.getValue(); ;
      //tab['date_on']=this.__date.getValue();
      var newDate = this.__date.getValue() ;
      newDate.setDate(this.__date.getValue().getDate()+1) ; //correction pour conserver la date
      tab['date_on']=newDate;
      tab['typeContainer']=this.__container.getSelection()[0].getLabel() ;
      tab['typeContent']=this.__content.getSelection()[0].getLabel() ;
      tab['typeBox']=this.__typeBox.getSelection()[0].getLabel() ;
      tab['nameBox']= this.__listNameBox.getSelection()[0].getLabel();
      tab['typeContent']=this.__content.getSelection()[0].getLabel() ;
      tab['remark']=this.__remarks.getValue() ;
      tab['serie']=this.__experience ;
      tab['notations']=[];
      tab['colName']=this.__colName.getValue();
      tab['rowName']=this.__rowName.getValue();
      tab['id_action']= null ;
      
      for(var i in this.__otherNotation.getChildrenContainer()._getChildren()){
        var x={};
        x['type']=this.__otherNotation.getChildrenContainer()._getChildren()[i].getUserData("type");
        x['value']=this.__otherNotation.getChildrenContainer()._getChildren()[i].getUserData("value");
        tab['notations'].push(x);
      };
//      console.log("setCreateContainer");
//      console.log(tab);
      this.__services.QuerySetCreateContainer("setCreateContainer", this.__sessionId, tab);
      this.__services.addListenerOnce("setCreateContainer", function(e){ 
        var loadData=e.getData();
//console.log('create container');
//console.log(loadData);        
        if (loadData) { 
          this.fireDataEvent ("NewContainerSaved",this.__listNameBox.getSelection()[0].getUserData("idBox"));
          this.__saveButton.setEnabled(false);
        };
        },this);       
    }, this);
	}
});
