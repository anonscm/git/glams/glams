/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new lot from an existing lot in the database.
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.CreateNewLotFromLot", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * Genre of the plant to create
     */
    genre: {
      init: null,
      event: "changeGenre"
    },
    /**
     * ID of the original Lot
     */
    idLot: {
      init: null,
      event: "changeIdLot"
    },
    /**
     * ID of the original clone
     */
    idClone: {
      init: null,
      event: "changeIdClone"
    },
    /**
     * Name of the new Lot
     */
    newNameLot: {
      init: null,
      event: "changeNumLot"
    },
    /**
     * Date of introduction of the new Lot
     */
    date: {
      init: null,
      event: "changeDate"
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.HBox();
    layout.setSpacing(15);
    this._setLayout(layout);


    //Implémentation des widget visible à l'écran
    var containerSaisie = new qx.ui.container.Composite(new qx.ui.layout.VBox().set({
      spacing: 5
    }));
    this._add(containerSaisie);

    var containerAffichage = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this._add(containerAffichage);

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var lotOrigine = new qx.ui.container.Composite(grid);
    containerSaisie._add(lotOrigine);

    lotOrigine._add(new qx.ui.basic.Label(
      this.tr("Numéro de Lot d'origine")), {
      row: 0,
      column: 0
    });
    this.__lotOrigine = new glams.ui.FiltredComboBox();
    lotOrigine._add(this.__lotOrigine, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var newNameLot = new qx.ui.container.Composite(grid);
    containerSaisie._add(newNameLot);

    newNameLot._add(new qx.ui.basic.Label(
      this.tr("Nouveau numéro de Lot")), {
      row: 0,
      column: 0
    });
    this.__nameNewLot = new qx.ui.form.TextField();
    this.__nameNewLot.setPlaceholder(this.tr("Numéro de Lot"));
    newNameLot._add(this.__nameNewLot, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var dateLot = new qx.ui.container.Composite(grid);
    containerSaisie._add(dateLot);

    dateLot._add(new qx.ui.basic.Label(
      this.tr("Date d'introduction")), {
      row: 0,
      column: 0
    });
    this.__dateLot = new qx.ui.form.DateField();
    this.__dateLot.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__dateLot.setValue(new Date());
    var format = new qx.util.format.DateFormat("dd/MM/YYYY");
    var date = format.format(this.__dateLot.getValue());
    this.setDate(date);
    dateLot._add(this.__dateLot, {
      row: 0,
      column: 1
    });


    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Variété d'origine : ")), {
      row: 0,
      column: 0
    });
    var lblVarOr = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblVarOr, {
      row: 0,
      column: 1
    });
    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Accession d'origine : ")), {
      row: 1,
      column: 0
    });
    var lblAccOr = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblAccOr, {
      row: 1,
      column: 1
    });

    // var validationNomLot = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez renseigner le nom du nouveau Lot");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );
    // var validationSelectLot = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez selectionner un Lot");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.__lotOrigine.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var userGroup = JSON.parse(localStorage.getItem('readGroups'));
        var tab = {};
        tab['genre'] = this.getGenre();
        tab['filtredText'] = e.getData();
        this.__services.QueryAllLotsforPrelevFiltredByGenre("getAllLotsforPrelevFiltredByGenre", this.__sessionId, tab);
      };
    }, this);

    this.__services.addListener("getAllLotsforPrelevFiltredByGenre", function(e) {
      this.__listLot = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listLot.push({
          'label': loadData[i]['numero_lot'],
          'idLot': loadData[i]['id_lot'],
          'idClone': loadData[i]['id_clone'],
          'idVar': loadData[i]['id_variete'],
          'accession': loadData[i]['numero_accession']
        });
      };
      this.__lotOrigine.setListModel(this.__listLot);
    }, this);

    //Affiche les informations relatives au Lot selectionné par l'utilisateur
    this.__lotOrigine.addListener("changeAllValues", function() {
      var idVar = this.__lotOrigine.getAllValues().getIdVar();
      this.__services.QueryGetAllNomsVarieteByIdVariete("getAllNomsVarieteByIdVariete", idVar, null);
      lblAccOr.setValue(this.__lotOrigine.getAllValues().getAccession());
      this.setIdLot(this.__lotOrigine.getAllValues().getIdLot());
      this.setIdClone(this.__lotOrigine.getAllValues().getIdClone());
    }, this);

    //Récupère le nom de la variété en fonction de son ID
    this.__services.addListener("getAllNomsVarieteByIdVariete", function(e) {
      var loadData = e.getData();
      var label = 1
      for (var i in loadData) {
        var type = loadData[i].type;
        var nom = loadData[i].nom;
        if (label == 1) {
          label = type + " : " + nom + "</br>"
        } else {
          label = label + type + " : " + nom + "</br>"
        };
      }
      lblVarOr.setValue(label);
    }, this);

    this.__nameNewLot.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNewNameLot(loadData);
    }, this);

    this.__dateLot.addListener("changeValue", function() {
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      var date = format.format(this.__dateLot.getValue());
      this.setDate(date);
    }, this);
  },

  members: {

  }
});