/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS
************************************************************************ */

/**
 * Form to complete to create a sample in the database.
 * Authors : David Aurégan,
 * May 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.CreatePrelevementFromExisting", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * get information about the sample
     */
    "loadedSampleSelected": "qx.event.type.Data",
    /**
     *
     */
    "launchCreateSample": "qx.event.type.Event",
    /**
     * Launch Add notation to prelevement
     */
    "sampleAddNotationResult": "qx.event.type.Event",
    /**
     * Response event for when the validate button is pressed
     */
    "sampleValidateReturn": "qx.event.type.Event",

    /**
     * Asking event for when the validate button is pressed
     */
    "sampleValidate": "qx.event.type.Event",

    /**
     * Add notation to prelevement
     */
    "startsampleAddNotation": "qx.event.type.Event",

    /**
     * Clear the page
     */
    "resetSample": "qx.event.type.Event",
    /**
     * close the windows
     */
    "fermeture": "qx.event.type.Event"
  },

  properties: {
    /**
     * Plant on which conducted the sample after the creation of a lot since NewCreateCulture
     */
    planteSource: {
      init: null,
      event: "changePlanteSource"
    }
  },

  members: {
    __name: null,
    __date: null,
    __user: null,
    __labbook: null,
    __quantity: null,
    __organe: null,
    __contenant: null,
    __other: null,
    __remarks: null,
    __nameLot: null,
    __numberAcces: null,
    __condition: null,
    __traitement: null,
    __contenu: null,
    __etatcontenant: null,
    __typeBox: null,
    __box: null
  },

  construct: function() {
    this.base(arguments);
    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setRowFlex(0, 1);
    layout.setRowFlex(1, 1);
    layout.setRowFlex(2, 1);
    layout.setRowFlex(3, 1);
    layout.setRowFlex(4, 1);
    layout.setRowFlex(5, 1);
    layout.setRowFlex(6, 1);
    layout.setRowFlex(7, 1);
    layout.setRowFlex(8, 1);
    layout.setColumnFlex(0, 1);
    layout.setColumnFlex(1, 1);
    layout.setColumnFlex(2, 1);
    layout.setColumnFlex(3, 1);
    layout.setSpacing(10);
    this._setLayout(layout)
    var form = new qx.ui.form.Form();
    var manager = new qx.ui.form.validation.Manager();
    this.notations = [];
    //postionnement des labels
    this._add(new qx.ui.basic.Label(
      this.tr("Plante source : ")), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Date de prelevement : ")), {
      row: 1,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du prelevement : ")), {
      row: 2,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Organe : ")), {
      row: 8,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Quantité : ")), {
      row: 12,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Expérimentateur : ")), {
      row: 6,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence cahier labo : ")), {
      row: 10,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Notations : ")), {
      row: 6,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Remarque : ")), {
      row: 6,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type contenu : ")), {
      row: 8,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Etat contenant : ")), {
      row: 10,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type d'unité : ")), {
      row: 12,
      column: 1
    });
    //initialisation des widget
    this.__name = new qx.ui.form.TextField();
    this.__name.setRequired(true);
    this.__name.setPlaceholder("Nom du prelevement");
    this.__name.setAllowStretchX(true);
    this.__name.setAllowStretchY(true);
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    this.__date.setAllowStretchX(true);
    this.__date.setAllowStretchY(true);
    this.__user = new qxelvis.ui.users.ContributorsSelector();
    this.__labbook = new qx.ui.form.TextField();
    this.__labbook.setPlaceholder("Référence du prelevement");
    this.__labbook.setAllowStretchX(true);
    this.__labbook.setAllowStretchY(true);
    this.__quantity = new qx.ui.form.TextField();
    this.__quantity.setPlaceholder("Quantité prélevé");
    this.__quantity.setAllowStretchX(true);
    this.__quantity.setAllowStretchY(true);
    this.__organe = new qx.ui.form.SelectBox();
    this.__organe.setAllowStretchX(true);
    this.__organe.setAllowStretchY(true);
    this.__other = new qx.ui.form.List().set({
      height: 100,
      width: 150
    });
    this.__other.setAllowStretchX(true);
    this.__other.setAllowStretchY(true);
    this.__remarks = new qx.ui.form.TextArea();
    this.__remarks.setPlaceholder("Remarques sur le prélevement");
    this.__remarks.setAllowStretchX(true);
    this.__remarks.setAllowStretchY(true);
    var addWindow = new glams.ui.AddSampleNotationWindow();
    var widgetOther = new qx.ui.core.Widget();
    widgetOther._setLayout(new qx.ui.layout.VBox());
    widgetOther.setAllowStretchX(true);
    widgetOther.setAllowStretchY(true);
    var addButton = new qx.ui.form.Button("+");
    var removeButton = new qx.ui.form.Button("-");
    widgetOther._add(this.__other);
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    widgetButton._add(addButton);
    widgetButton._add(removeButton);
    widgetOther._add(widgetButton);
    this.__contenu = new qx.ui.form.SelectBox();
    this.__contenu.setAllowStretchX(true);
    this.__contenu.setAllowStretchY(true);
    this.__etatcontenant = new qx.ui.form.SelectBox();
    this.__etatcontenant.setAllowStretchX(true);
    this.__etatcontenant.setAllowStretchY(true);
    this.__typeUnit = new glams.ui.FiltredComboBox();
    this.__typeUnit.setPlaceholder("grammes");
    this.__numberLot = new glams.ui.FiltredComboBox();
    this.__numberLot.setPlaceholder("Col0 2015");

    //positionnement des widgets
    this._add(this.__date, {
      row: 1,
      column: 1
    });
    this._add(this.__name, {
      row: 3,
      column: 2
    });
    this._add(this.__organe, {
      row: 9,
      column: 0
    });
    this._add(this.__quantity, {
      row: 13,
      column: 0
    });
    this._add(this.__user, {
      row: 7,
      column: 2,
      colSpan: 3
    });
    this._add(this.__labbook, {
      row: 11,
      column: 1
    });
    this._add(widgetOther, {
      row: 7,
      column: 0
    });
    this._add(this.__remarks, {
      row: 7,
      column: 1
    });
    this._add(this.__contenu, {
      row: 9,
      column: 1
    });
    this._add(this.__etatcontenant, {
      row: 11,
      column: 0
    });
    this._add(this.__typeUnit, {
      row: 13,
      column: 1
    });
    this._add(this.__numberLot, {
      row: 0,
      column: 1
    });
    var labelNomLot = new qx.ui.basic.Label().set({
      value: "Nom Lot :</br>",
      rich: true
    });
    this._add(labelNomLot, {
      row: 2,
      column: 0
    });
    this.__nameLot = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__nameLot, {
      row: 3,
      column: 0
    });
    var labelAccession = new qx.ui.basic.Label().set({
      value: "Accession :"
    });
    this._add(labelAccession, {
      row: 2,
      column: 1
    });
    this.__numberAcces = new qx.ui.basic.Label().set({
      font: "bold"
    })
    this._add(this.__numberAcces, {
      row: 3,
      column: 1
    });
    var labelCondCul = new qx.ui.basic.Label().set({
      value: "Condition(s) de culture :"
    });
    this._add(labelCondCul, {
      row: 4,
      column: 0
    });
    this.__condition = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    });
    this._add(this.__condition, {
      row: 5,
      column: 0
    });
    var labelTrait = new qx.ui.basic.Label().set({
      value: "Traitement(s) :</br>",
      rich: true
    });
    this._add(labelTrait, {
      row: 4,
      column: 1
    });
    this.__traitement = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    });
    this._add(this.__traitement, {
      row: 5,
      column: 1
    })
    //Bouton Valider
    var Valider = new qx.ui.form.Button(this.tr("Valider"));
    Valider.setAllowStretchX(true);
    Valider.setAllowStretchY(true);
    this._add(Valider, {
      row: 14,
      column: 0
    });
    // Bouton Annuler
    var Annuler = new qx.ui.form.Button(this.tr("Annuler"));
    Annuler.setAllowStretchX(true);
    Annuler.setAllowStretchY(true);
    this._add(Annuler, {
      row: 14,
      column: 1
    });
    this.__box = new glams.ui.WidgetBox();
    this._add(this.__box, {
      row: 5,
      column: 2,
      colSpan: 3
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Projet / Série / Experience :")), {
      row: 8,
      column: 2
    })
    this.__experiment = new glams.ui.WidgetExperience();
    this._add(this.__experiment, {
      row: 9,
      column: 2,
      rowSpan: 5
    });
    var regexp = new RegExp("^([0-9]*)?$");
    // Configuration du gestionnaire d'evenement lors de la validation
    var validationName = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer un nom");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );
    var validationQuantity = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (!regexp.test(value)) {
            validator.setValid(false, "Veuillez entrer un nombre");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );
    manager.add(this.__name, validationName);
    manager.add(this.__quantity, validationQuantity);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    var services = glams.services.Sample.getInstance();
    services.QueryAllTissuType("loadedAllTissuType", null, null);
    services.QueryAllUser("loadedAllUser", null, null);
    services.QueryAllTypeContenant("loadedAllTypeContenant", null, null);
    services.QueryAllTypeContenu("loadedAllTypeContenu", null, null);
    services.QueryGetAllContainerLevel("getAllContainerLevel", null, null);
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    /*
    ********************
    Chargement des données pour les widgets
    ********************
    */

    this.addListener("changePlanteSource", function() {
      var loadData = this.getPlanteSource();
      console.log(loadData);
      if (loadData) {
        // this.__numberLot.setValue(loadData.name);
        // this.__nameLot.setValue(loadData.name);
        // this.__numberAcces.setValue(loadData.accession);
        // services.QueryNotationsLotsforPrelev("loadedNotationsLotsforPrelev", loadData.idLot, 'sample : plants informations');
        // var tab = {};
        // tab['idLot'] = loadData.idLot;
        // services.QueryGetProtocolCultureByLot("getProtocolCultureByLot", this.__sessionId, tab);
        // this.selectItem = loadData.idLot;
        var idGroupe = JSON.parse(localStorage.getItem('readGroups'));
        services.QueryAllLotsforPrelevFiltred("getAllLotsforPrelevFiltred", idGroupe, loadData.name);
      };
    }, this);

    services.addListenerOnce("loadedAllTissuType", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__organe.add(tempItem);
      };
    }, this);

    services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__user.setUsers(loadData);
    }, this);

    services.addListener("loadedAllTypeContenu", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        this.__contenu.add(tempItem);
      };
    }, this);

    services.addListener("getAllContainerLevel", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['level']);
        this.__etatcontenant.add(tempItem);
        if (loadData[i].level == "plein") {
          this.__etatcontenant.setSelection([tempItem]);
        };
      };
    }, this);

    this.__typeUnit.addListener("changeSelectedString", function(e) {
      var tabUnit = {};
      tabUnit['typeNotation'] = 'unité';
      services.QueryGetAllValeurByNotation("getAllValeurByNotation", null, tabUnit);
    }, this);

    services.addListener("getAllValeurByNotation", function(e) {
      this.__listUnit = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listUnit.push({
          'label': loadData[i]['valeur']
        });
      };
      this.__typeUnit.setListModel(this.__listUnit);
    }, this);

    //Chargement du Lot
    this.__numberLot.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var filtredText = loadData;
        var idGroupe = JSON.parse(localStorage.getItem('readGroups'));
        services.QueryAllLotsforPrelevFiltred("getAllLotsforPrelevFiltred", idGroupe, filtredText);
      };
    }, this);

    services.addListener("getAllLotsforPrelevFiltred", function(e) {
      this.__listLot = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listLot.push({
          'label': loadData[i]['numero_lot'],
          'id_lot': loadData[i]['id_lot'],
          'accession': loadData[i]['numero_accession']
        });
      };
      this.__numberLot.setListModel(this.__listLot);
    }, this);

    this.__numberLot.addListener("changeAllValues", function() {
      this.__nameLot.setValue(this.__numberLot.getAllValues().getLabel());
      this.__numberAcces.setValue(this.__numberLot.getAllValues().getAccession());
      this.selectItem = this.__numberLot.getAllValues().getId_lot();
      var idLot = this.__numberLot.getAllValues().getId_lot();
      services.QueryNotationsLotsforPrelev("loadedNotationsLotsforPrelev", idLot, 'sample : plants informations');
      var tab = {};
      tab['idLot'] = idLot;
      services.QueryGetProtocolCultureByLot("getProtocolCultureByLot", this.__sessionId, tab);
    }, this);

    services.addListener("getProtocolCultureByLot", function(e) {
      var loadData = e.getData();
      this.__dataCulture = loadData;
      if (loadData[0]) {
        if (loadData[0].reference_alfresco) {
          var ref = loadData[0].reference_alfresco;
        } else {
          var ref = "Reference Alfresco non renseigné"
        };
        if (loadData[0]) {
          if (loadData[0]['name'].length < 1) {
            this.__condition.setValue("Non renseigné");
          } else {
            var label = '';
            label = " - " + loadData[0]['name'] + " - </br>" + loadData[0]['date_multiplication'] + "</br>" + ref;
            this.__condition.setValue(label)
          };
        };
      };
    }, this);

    //chargement des informations sur les conditions de culture et de traitements
    services.addListener("loadedNotationsLotsforPrelev", function(e) {
      var loadData = e.getData();
      if (loadData['conditions de culture.date']) {
        if (loadData['conditions de culture'][0]) {
          var label = ''
          var typcond = loadData['conditions de culture'][0];
          for (var i = 0; i < loadData['conditions de culture'].length; i++) {
            if (typcond == loadData['conditions de culture'][i]) {
              label = " - " + loadData['conditions de culture'][i] + " - </br>" + loadData['conditions de culture.date'][i] + "</br>" + loadData['conditions de culture.remarque'][i];
            } else {
              label = label + "</br>" + "</br>" + "-" + loadData['conditions de culture'][i] + "- </br>" + loadData['conditions de culture.date'][i] + "</br>" + loadData['conditions de culture.remarque'][i];
            };
            this.__condition.setValue(label);
          };
        };
        //chargement des informations sur les conditions de culture
        if (loadData['traitement particulier'].length < 1) {
          this.__traitement.setValue(("Aucun"));
        } else {
          var label = ''
          var typtrait = loadData['traitement particulier'][0];
          for (var i in loadData['traitement particulier']) {
            if (typtrait == loadData['traitement particulier'][i]) {
              label = " - " + loadData['traitement particulier'][i] + " - </br>" + loadData['traitement particulier.date'][i] + "</br>" + loadData['traitement particulier.remarque'][i];
            } else {
              label = label + "</br>" + " - " + loadData['traitement particulier'][i] + " - </br>" + loadData['traitement particulier.date'][i] + "</br>" + loadData['traitement particulier.remarque'][i];
            };
          };
          this.__traitement.setValue(label)
        };
      };
    }, this);

    this.__condition.addListener("mouseover", function(e) {
      if (this.__condition.getValue() != null) {
        this.setCursor("pointer");
        this.__popup = new qx.ui.popup.Popup(new qx.ui.layout.Grow()).set({
          backgroundColor: "#FCA998"
        });
        this.__popup.add(new qx.ui.basic.Label("Protocole : " + this.__dataCulture[0].summary));
        this.__popup.placeToPointer(e);
        this.__popup.show();
      };
    }, this);

    this.__condition.addListener("mouseout", function(e) {
      if (this.__condition != null && this.__popup != null) {
        this.__popup.setVisibility("excluded");
      };
      this.setCursor("default")
    }, this);

    // Permet de récupérer les collections via le WidgetExperience
    this.__experiment.addListener("changeListCollection", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.collection = loadData;
    }, this);
    /*
    ********************
    Fonction d'utilisation des boutons
    ********************
    */

    Valider.addListener("execute", function() {
      Valider.setEnabled(false);
      Valider.setLabel("Vérification");
      manager.validate();
    }, this);

    manager.addListener("complete", function() {
      Valider.setEnabled(true);
      Valider.setLabel("Valider");
      if (manager.getValid()) {
        this.__box.fireDataEvent("saveData");
        this.fireEvent("sampleValidate");
      }
    }, this);

    Annuler.addListener("execute", function() {
      this.fireEvent("resetSample");
    }, this);

    addButton.addListener("execute", function() {
      addWindow.open();
      addWindow.center();
    }, this);

    addWindow.addListener("addNotationSample", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      this.__other.add(tempItem);
    }, this);

    removeButton.addListener("execute", function() {
      this.__other.getSelection()[0].destroy();
    }, this);

    /*
    ********************
    Fonction pour effacer tout ce qui a été saisi
    ********************
    */

    this.addListener("resetSample", function() {
      this.__name.setValue(null);
      this.__date.setValue(new Date());
      this.__labbook.setValue(null);
      this.__quantity.setValue(null);
      this.__other.removeAll();
      this.__remarks.setValue(null);
      this.__numberLot.resetValue();
      this.__traitement.setValue(null);
      this.__condition.setValue(null);
      this.__nameLot.setValue(null);
      this.__numberAcces.setValue(null);
      this.__box.fireEvent("resetData");
    }, this);

    /*
    ********************
    Fonctions de création et d'enregistrement du prelevement
    ********************
    */

    this.addListener("sampleValidate", function() {
      var tab = {};
      tab = this.__box.getDataBox();
      this.typeContenant = tab['typeContainer'];
      this.typeBox = tab['typeBox'];
      this.nameBox = tab['nameBox'];
      this.colName = tab['colName'];
      this.rowName = tab['rowName'];
      this.idBox = tab['idBox']
      this.codeContenant = this.__name.getValue();
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      this.date = format.format(this.__date.getValue());
      this.typeTissu = this.__organe.getSelection()[0].getLabel();
      this.etatContenant = this.__etatcontenant.getSelection()[0].getLabel();
      this.typeContenu = this.__contenu.getSelection()[0].getLabel();
      this.valide = true;
      this.utilisable = true;
      this.remarque = this.__remarks.getValue();
      this.users = this.__user.getContributors();
      var createData = [];
      createData['date'] = this.date;
      createData['type_tissu'] = this.typeTissu;
      createData['type_contenant'] = this.typeContenant;
      createData['typeBox'] = this.typeBox;
      createData['nameBox'] = this.nameBox;
      createData['colName'] = this.colName;
      createData['rowName'] = this.rowName;
      createData['idBox'] = this.idBox;
      createData['etat'] = this.etatContenant;
      createData['type_contenu'] = this.typeContenu;
      createData['code_contenant'] = this.codeContenant;
      createData['valide'] = this.valide;
      createData['utilisable'] = this.utilisable;
      createData['remarque'] = this.remarque;
      createData['users'] = this.users;
      this.fireDataEvent("sampleValidateReturn", this.selectItem);
      this.fireDataEvent("sampleValidateReturn", createData);
    }, this);

    this.addListener("sampleValidateReturn", function(e) {
      var loadData = e.getData();
      this.date = loadData['date'];
      this.typeTissu = loadData['type_tissu'];
      this.typeContenant = loadData['type_contenant'];
      this.typeBox = loadData['typeBox'];
      this.nameBox = loadData['nameBox'];
      this.colName = loadData['colName'];
      this.rowName = loadData['rowName'];
      this.idBox = loadData['idBox'];
      this.etatContenant = loadData['etat'];
      this.typeContenu = loadData['type_contenu'];
      this.codeContenant = loadData['code_contenant'];
      this.valide = loadData['valide'];
      this.utilisable = loadData['utilisable'];
      this.remarque = loadData['remarque'];
      this.users = loadData['users'];
      services.QueryAllGroupsByLot("loadedAllGroupsByLot", e.getData(), null);
    }, this);

    services.addListener("loadedAllGroupsByLot", function(e) {
      var loadData = e.getData();
      this.idGroup = loadData;
      this.fireEvent("launchCreateSample", qx.event.type.Event);
    }, this);

    this.addListener("launchCreateSample", function() {
      var sampleData = {};
      var sampleIdGroup = [];
      for (var i in this.idGroup) {
        sampleIdGroup.push(this.idGroup[i].id_groupe);
      };
      if (this.idGroup[0].id_groupe != null && this.codeContenant != null) {
        sampleData['idGroup'] = this.idGroup[0].id_groupe;
        sampleData['date'] = this.date;
        sampleData['type_tissu'] = this.typeTissu;
        sampleData['type_contenant'] = this.typeContenant;
        sampleData['etat'] = this.etatContenant;
        sampleData['type_contenu'] = this.typeContenu;
        sampleData['code_contenant'] = this.codeContenant;
        sampleData['valide'] = this.valide;
        sampleData['utilisable'] = this.utilisable;
        sampleData['remarque'] = this.remarque;
        sampleData['typeBox'] = this.typeBox;
        sampleData['nameBox'] = this.nameBox;
        sampleData['colName'] = this.colName;
        sampleData['rowName'] = this.rowName;
        sampleData['idBox'] = this.idBox;
        services.QueryCreatePrelev("createSampleFromExisting", sampleData, null);
      };
    }, this);

    services.addListener("createSampleFromExisting", function(e) {
      var loadData = e.getData();
      this.idprelev = loadData[0].id_prelevement;
      for (var i in this.users) {
        services.QueryAddUserToPrelevment("addUserToPrelevment", this.users[i], loadData[0].id_prelevement);
      }
      this.fireEvent("startsampleAddNotation", qx.event.type.Event);
      var tab = {};
      for (var i in this.collection) {
        tab['idExperiment'] = this.collection[i]['id'];
        tab['idContainer'] = loadData[0]['id_container'];
        services.QueryAddExperimentToContainer("addExperimentToContainer", this.__sessionID, tab)
      };
    }, this);

    this.addListener("startsampleAddNotation", function() {
      if (this.__quantity.getValue()) {
        var table = [];
        table["codePrelev"] = this.codeContenant;
        table["date"] = this.date;
        table["remarque"] = null;
        table["site"] = null;
        table["type_notation"] = "quantité prélevée";
        table["valeur"] = this.__quantity.getValue();
        this.fireDataEvent("sampleAddNotationResult", table);
        var table = [];
        table["codePrelev"] = this.codeContenant;
        table["date"] = this.date;
        table["remarque"] = null;
        table["site"] = null;
        table["type_notation"] = "unité";
        table["valeur"] = this.__typeUnit.getValue();
        this.fireDataEvent("sampleAddNotationResult", table);
      };
      for (var i in this.__other.getChildrenContainer()._getChildren()) {
        var tab = [];
        tab["codePrelev"] = this.codeContenant;
        tab["date"] = this.date;
        tab["remarque"] = null;
        tab["site"] = null;
        tab["type_notation"] = this.__other.getChildrenContainer()._getChildren()[i].getUserData("type");
        tab["valeur"] = this.__other.getChildrenContainer()._getChildren()[i].getUserData("value");
        this.fireDataEvent("sampleAddNotationResult", tab);
      };
      this.fireEvent("resetSample");
      this.fireEvent("fermeture");
    }, this);

    this.addListener("sampleAddNotationResult", function(e) {
      var notation = e.getData();
      var tab = notation;
      this.notations.push(tab);
      var tab = {};
      for (var j in this.notations) {
        tab['date'] = this.notations[j].date;
        tab['remarque'] = this.notations[j].remarque;
        tab['site'] = this.notations[j].site;
        tab['type_notation'] = this.notations[j].type_notation;
        tab['valeur'] = this.notations[j].valeur;
        tab['usergroup'] = JSON.parse(localStorage.getItem('writeGroups'))[0];
        tab["idPrelev"] = this.idprelev;
        services.QueryAddNotationPrelev("addNotationPrelev", tab);
      };
      if (this.notations.length > 0) {
        this.notations = [];
      };
    }, this);

  }
});