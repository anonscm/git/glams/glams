/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Formulaire de description modification et ajout de type de places
 * Juin 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.PlaceTypeDescriptor", {
  extend: qx.ui.core.Widget,

  events: {
	 /**
    * fire when the Place is updated
    */   
   "updatePlace": "qx.event.type.Event",
   /**
    * fire when the Place is created
    */
   "createdPlace": "qx.event.type.Event"
  },

  properties: {

  },

  members: {
    __updatePlace: function(item) {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
      return (container)
    },
    __newPlace: function() {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());     
      return (container)
    }
  },

  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */

    // Session management
    this.__session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = this.__session.getSessionId();

    var layout = new qx.ui.layout.VBox();
    layout.setSpacing(10);

    this._setLayout(layout);
    //creation des widgets
    this._add(new qx.ui.basic.Label(this.tr("Nom des Types d'emplacement")));

    this.__liste = new qx.ui.form.List().set({
      width: 250,
      allowStretchY: true,
      allowStretchX: true
    });
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));
    this.__buttonUpdate = new qx.ui.form.Button(this.tr("Synchroniser la terminologie"));

    //positionnement des widgets
    this._add(this.__liste, {
      flex: 1
    });
    this._add(this.__buttonEdit);
    this._add(this.__buttonNew);
    this._add(this.__buttonUpdate);
    
    //terminologie Place
    this.__terminologyPlace = new glams.ui.TerminologyTools();
    this.__terminologyPlace.setName("Place");
    
    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    var tab = {};
    tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));


    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //Label des emplacements
    this.__terminologyPlace.addListener("changeListTerms", function(e) {
      this.__liste.removeAll();
      var loadData = e.getData();
//      console.log("changeListTerms");
//      console.log(loadData)
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['term_label']);        
//        tempItem.setUserData("creationDate", loadData[i]['creation_date']);
//        tempItem.setUserData("description", loadData[i]['description']);
//        tempItem.setUserData("id", loadData[i]['id']);
        this.__liste.add(tempItem);
      };
    }, this);

    //bouton modifier
    this.__buttonEdit.addListener("execute", function() {
      var window = new qx.ui.window.Window("Modify Place name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      //window.add(this.__updatePlace(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updatePlace", function() {window.close()}, this);
    }, this);
   //doubleclick sur type de boite 
   this.__liste.addListener("dblclick", function(e){ 
      var window = new qx.ui.window.Window("Modify Place name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      //window.add(this.__updatePlace(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updatePlace", function() {window.close()}, this);     
   },this);    
    //bouton ajouter
    this.__buttonNew.addListener("execute", function(e) {
      var window = new qx.ui.window.Window("Add Place name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      //window.add(this.__newPlace());
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("createdPlace",function(){ window.close();},this);
    }, this);
    //synchroniser la terminologie
    this.__buttonUpdate.addListener("execute", function(e) {
      var window = new qx.ui.window.Window("Update Place name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);

      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      tab={};
      tab['name']='Place';
      tab['context']='Général';
      this.__services.QuerySynchronyseAllPlaces("synchronyseAllPlaces", this.__sessionId, tab);
      this.__services.addListener("synchronyseAllPlaces",function(e){ console.log(e.getData()); window.close();},this);
    }, this);    
   


  }
})
