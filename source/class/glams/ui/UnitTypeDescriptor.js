/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Formulaire de consultation des unité de notations
 * Mai 2018
 * @asset(glams/*)
 */


qx.Class.define("glams.ui.UnitTypeDescriptor", {
  extend: qx.ui.core.Widget,


  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.VBox();
    layout.setSpacing(10);

    this._setLayout(layout);
    //creation des widgets
    this.__label = new qx.ui.basic.Label(this.tr("Types d'unité"));
    this.__liste = new qx.ui.form.List().set({
      width: 250,
      allowStretchY: true,
      allowStretchX: true
    });
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));

    //positionnement des widgets
    this._add(this.__label);
    this._add(this.__liste, {
      flex: 1
    });

    var tab = {}
    tab['typeNotation'] = 'unité'

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryGetAllValeurByNotation("getAllValeurByNotation", null, tab);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //type de container
    this.__services.addListener("getAllValeurByNotation", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['valeur']);
        this.__liste.add(tempItem);
      };
    }, this);



  }
})
