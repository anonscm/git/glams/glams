/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Class to manage the data of creating a culture
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.SaveNewCulture", {
  extend: qx.core.Object,

  events: {
    /**
     * Return of the service createArbre
     */
    "return": "qx.event.type.Data"
  },

  properties: {

  },

  members:  {
    createArbre: function(data) {
      console.log(data);
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "glamsService_sample.py", "glamsService");
      rpc.addListener("failed", function(e) {
        var loadData = e.getData();
        alert(loadData);
      }, this);
      rpc.addListener("completed", function(e) {
        var loadData = e.getData();
        var tab = {};
        tab["IdNewCulture"] = loadData.id;
        tab["typeValidation"] = data.typeValidation;
        this.fireDataEvent("return", tab);
      }, this);
      var session = qxelvis.session.service.Session.getInstance();
      this.__sessionId = session.getSessionId();
      rpc.callAsyncListeners(true, "createArbre", this.__sessionId, data);
    }

  }
});