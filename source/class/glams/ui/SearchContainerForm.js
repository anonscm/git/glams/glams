/**
 * Formulaire de recherche de contenair
 * Fabrice Dupuis décembre 2017
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.SearchContainerForm",{
   extend : qx.ui.core.Widget,
	
	
  members : {    
    /**
    * Liste des boites ouvertes pour controle et modifications
    */
    __listeOpenBox : [],
    
    //************************************************************************************	affichage plans de boite*********************************************	
    /**
    *affiche le plan de boites
    */ 
    __showMapBox : function (idBox,label) {
      this.__idBox = idBox;
      //teste si la boite est déjà ouverte
      var boite='absente';
       for (var l in this.__listeOpenBox){
        if ( this.__idBox == this.__listeOpenBox[l]['idBox']){ 
          var boite='présente';
          break;
        };
      };
      if (boite=='absente'){
        //creation de la structure data dataBox
        this.__serviceBox.createBox(this.__idBox);
        //creation de la structure graphique MapBox
        this.__mapBox = new glams.ui.MapBox();
        this.__mapBox.setAllowStretchX(true);
        //creation de la fenêtre d'affichage du plan
        var planBoxWindow = new qx.ui.window.Window(label);
        planBoxWindow.setWidth(200);
        planBoxWindow.moveTo(Math.round(850+Math.random()*80),Math.round(70+Math.random()*80));
        planBoxWindow.setLayout(new qx.ui.layout.Grow());
        planBoxWindow.open();
        
        //listener sur la fenêtre planBoxWindow
        planBoxWindow.addListener("close", function (e){ //si fermeture alors mise à jour de la liste des boites ouvertes
        for (var l in this.__listeOpenBox){
          if (planBoxWindow  == this.__listeOpenBox[l]['planBoxWindow']){ 
            this.__listeOpenBox[l]['mapBox'].destroy();
            this.__listeOpenBox[l]['dataBox'].destroy();
            this.__listeOpenBox.splice(l,1);           
            break;
        };
      };
        
        }, this);
        
        //listeners sur la couche data      
        this.__serviceBox.addListenerOnce("createBox", function(e){ //toutes les données sont récupérées
          this.__dataBox=e.getData(); 
          this.__mapBox.setIdBox(this.__dataBox.getIdBox());
          this.__mapBox.setRowNumber(this.__dataBox.getRowNumber());
          this.__mapBox.setColumnNumber(this.__dataBox.getColumnNumber());
          this.__mapBox.setPlace(this.__dataBox.getListePlaces());
          var tab={};
          tab['idBox']=this.__idBox;
          tab['mapBox']=this.__mapBox;
          tab['dataBox']=this.__dataBox;
          tab['planBoxWindow']=planBoxWindow;
          this.__listeOpenBox.push(tab);
          
          this.__dataBox.addListenerOnce("saved", function(e){ //les données sont sauvegardée dans la base. Effectue un reset d'affichage après
          //console.log( "boite sauvée in DB "+e.getData());        
            for (var l in this.__listeOpenBox){
              this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
              this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
                this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
                this.__listeOpenBox[l]['mapBox'].update();
            
              },this);
            };
          },this);
          
        },this);
      };  

      //listener sur la couche graphique
      this.__mapBox.addListener("drowed", function(){//la structure de dessin est réalisée
       planBoxWindow.add(this.__mapBox);
      },this); 
      this.__mapBox.addListener("updateOnecontainer", function(e){//les valeurs d'un container sont modifiées
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData);
            this.__listeOpenBox[l]['mapBox'].update();
            break;
          };
        };
        
      },this);    
      this.__mapBox.addListener("saveBox", function(e){//sauvegarde la box sur elvis
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].saveBox();
        };          
      },this);  
      
      this.__mapBox.addListener("reInitialiseMapBox", function(e){//reinitialise toutes les box pour effacer les modifications
        for (var l in this.__listeOpenBox){
          this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox",this.__listeOpenBox[l]['idBox']);
          this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function(){//mise à jour de la liste des places
            this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
            this.__listeOpenBox[l]['mapBox'].update();
          },this);

        };
      },this);    
      this.__mapBox.addListener("containerPutOff", function(e){//action drag du drag and drop
        var loadData=e.getData();
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
            loadData['container'].setDateOff(new Date());
//console.log("supprime un tube sur : "+loadData['idBox']+ " "+loadData['container'].getId()+" date on :  "+loadData['container'].getDateOn()+" date off :  "+loadData['container'].getDateOff());       
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData['container']);        
            break;
          };
        };
      },this);  
         
      this.__mapBox.addListener("containerPutOn", function(e){//action drop du drag and drop
        var loadData=e.getData();
        //cree un nouveau container
        loadData['container'].setDateOn(new Date());
        loadData['container'].setDateOff(null);
        if (loadData['row'] == null ) {
          loadData['container'].setRow(null);
        }else{
          loadData['container'].setRow(loadData['row'].toString());
        }; 
        if (loadData['column'] == null ) {
          loadData['container'].setColumn(null);
        }else{
          loadData['container'].setColumn(loadData['column'].toString());
        };        
        loadData['container'].setIdBox(loadData['idBox'] );
        for (var l in this.__listeOpenBox){
          if ( loadData['idBox'] == this.__listeOpenBox[l]['idBox']){ 
//console.log("ajout un tube sur : "+loadData['idBox']+ " "+loadData['container'].getId()+ " "+loadData['container'].getRow()+" "+loadData['container'].getColumn()+" "+loadData['container'].getDateOn()+" dateOff : "+loadData['container'].getDateOff());       
            this.__listeOpenBox[l]['dataBox'].addContainer(loadData['container'],loadData['container'].getRow(),loadData['container'].getColumn(),loadData['container'].getDateOn(),loadData['container'].getDateOff());       
            break;
          };
        };    
      },this);                   
    
    }//fin de la methode affichage des boites ******************************************************************************************************************
  },
	
	construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */    
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();
    
		var layout = new qx.ui.layout.Grid();
		layout.setSpacing(10);
		layout.setColumnFlex(2,1);
		layout.setRowFlex(1,1);
		this._setLayout(layout);
		
		//positionnement des labels
		this._add(new qx.ui.basic.Label(
		  this.tr("Code ou Nom du contenant : ")), 
		  {row : 0, column : 0});
//		this._add(new qx.ui.basic.Label(
//		  this.tr("Code ou Nom de la boite : ")), 
//		  {row : 2, column : 0});
	  		  
		//init of the widget
		this.__nameCont = new glams.ui.FiltredComboBox();
		this.__nameCont.setPlaceholder(this.tr("Filter Name Serie ( 3 carracters minimum)"));	
			
//    this.__nameBox = new glams.ui.SimpleAutoCompleteBox();
//		this.__nameBox.setPlaceholder(this.tr("Filter Name Serie ( 3 carracters minimum)"));
		
		//liste des containers
		//initialise l'autocompleBox
    this.itemData = [];
//    this.itemData.push('');
//    this.__nameCont.setListModel(this.itemData);                                   
    this.__nameCont.addListener("changeSelectedString", function(e){
      var loadData = e.getData();
      if (loadData.length >2){
        var tab={};
        
        tab['userGroup']= JSON.parse(localStorage.getItem('readGroups'));
        tab['filtredText']=e.getData();
        
        this.__services.QueryGetAllContainersFiltred("getAllContainersFiltred", this.__sessionId, tab);
      }; 
                
    }, this);	

//		//liste des boites
//		//initialise l'autocompleBox
//    this.itemData2 = new qx.data.Array();
//    this.itemData2.push('');
//    this.__nameBox.setListModel(this.itemData2);                             
//    this.__nameBox.addListener("changeSelectedString", function(e){
//      var loadData = e.getData();
//      if (loadData.length >2){
//        var tab={};
////        tab['userGroup']=1008;
//        tab['filtredText']=e.getData();
//        this.__services.QueryGetAllBoxsFiltred("getAllBoxsFiltred", this.__sessionId, tab);
//      }; 
//    }, this);			


		//Affichage resultat requete code container
		var widgetNameBox = new qx.ui.core.Widget();
		widgetNameBox._setLayout(new qx.ui.layout.VBox());
		this.__listNameBox = new qx.ui.form.List().set({
		  height : 60,
		  width : 600
		});    
		this.__listNameBox.setAllowStretchX(true);
    this.__listNameBox.setAllowStretchY(true);
    widgetNameBox.setAllowStretchX(true);
    widgetNameBox.setAllowStretchY(true);
		widgetNameBox._add(this.__listNameBox,{flex:1});
		widgetNameBox._add(new qx.ui.basic.Label(this.tr("double click = plan de boite")));

//		//Affichage resultat requete position Box
//		var widgetLocateBox = new qx.ui.core.Widget();
//		widgetLocateBox._setLayout(new qx.ui.layout.VBox());
//		this.__listLocateBox = new qx.ui.form.List().set({
//		  height : 60
//		});
//		widgetLocateBox._add(this.__listLocateBox);
//		widgetLocateBox._add(new qx.ui.basic.Label(this.tr("double click = plan de boite")));

    
    //positionnement des widgets
		this._add(this.__nameCont, {row : 0, column : 1});
//		this._add(this.__nameBox, {row : 2, column : 1});
		this._add(widgetNameBox, {row : 1, column : 1});
//		this._add(widgetLocateBox, {row : 3, column : 2});

		

                
		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
     
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */ 

    // Liste des code container dans la liste à selectionner
    this.__services.addListener("getAllContainersFiltred", function(e){
      this.itemData = [];
      var loadData = e.getData();
      for(var i in loadData){
        this.itemData.push({'label':loadData[i]['code'],'id':loadData[i]['id']});
        };    
      this.__nameCont.setListModel(this.itemData);
      this.__nameCont.open();             
    }, this);
//    //liste des noms de boite dans la liste à selectionner
//    this.__services.addListener("getAllBoxsFiltred", function(e){
//      var loadData = e.getData();
//      this.itemData2 = new qx.data.Array();
//      for(var i in loadData){
//        this.itemData2.push(loadData[i]['name']);
//      };    
//      this.__nameBox.setListModel(this.itemData2);
//      this.__nameBox.open();             
//    }, this);     
    //choix du nom de container réalisé
    this.__nameCont.addListener("changeValue", function(e){
      var tab={};
      tab['filtredText']=e.getData();
      tab['userGroup']= JSON.parse(localStorage.getItem('readGroups'));
      this.__services.QueryGetPlaceContainer("getPlaceContainer", this.__sessionId, tab);
    },this);     
//    //choix du nom de boite réalisé
//    this.__nameBox.addListener("changeValue", function(e){
//      var tab={};
//      tab['filtredText']=e.getData();
//      this.__services.QueryGetPlaceBox("getPlaceBox", this.__sessionId, tab);
//    },this);     
    //position du container
      this.__services.addListener("getPlaceContainer", function(e){
      var loadData = e.getData();
      this.__listNameBox.removeAll();
      for(var i in loadData){
        if (loadData[i]['row']!= null){
          var tempItem = new qx.ui.form.ListItem(" Boite : "+loadData[i]['name']+" - Row : "+loadData[i]['row']+" - Column : "+loadData[i]['column']); 
          tempItem.setUserData("label", " Boite : "+loadData[i]['name']+" - Row : "+loadData[i]['row']+" - Column : "+loadData[i]['column']);
        
        }else{
          var tempItem = new qx.ui.form.ListItem(" Boite vrac : "+loadData[i]['name']);
          tempItem.setUserData("label"," Boite vrac : "+loadData[i]['name'] );
        
        };
        tempItem.setUserData("idBox", loadData[i]['idbox']);   
        this.__listNameBox.add(tempItem);
      };
    }, this);
  
    


    //affichage du plan de la boite apres dblclick
//     this.__listLocateBox.addListener("dblclick", function(){   
//      this.__showMapBox (this.__listLocateBox.getSelection()[0].getUserData("idBox"),this.__listLocateBox.getSelection()[0].getUserData("label"));
//    },this);   
    this.__listNameBox.addListener("dblclick", function(){   
      this.__showMapBox (this.__listNameBox.getSelection()[0].getUserData("idBox"),this.__listNameBox.getSelection()[0].getUserData("label"));        
    }, this); 
     

   

	}
});
