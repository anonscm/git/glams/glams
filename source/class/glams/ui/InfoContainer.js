/**
 * This is the widget to view basic information of one container
 *
 * Fabrice dupuis octobre 2017
 * @asset(glams/*)
 */
 qx.Class.define("glams.ui.InfoContainer",{
	extend : qx.ui.core.Widget,
	
	events : {
    /**
     * Fired quand les informations sont sauvegardées
     */
    "saved" : "qx.event.type.Data",
     /**
     * Fired quand les modifications sont validées
     */
    "updateContainer" : "qx.event.type.Data"   
      
	},
	
	properties : {
   /**
    *  container
    */
    container :{
      nullable : true,
      event : "changeContainer"
    }            
	},
   
	members : {

	},
	
	construct : function(){
		this.base(arguments);
		
		/*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
		 
		var layout = new qx.ui.layout.Grid();
		layout.setColumnFlex(2,1);
		this._setLayout(layout);
	        //positionnement des labels
		this._add(new qx.ui.basic.Label(
		  this.tr("Code ou Nom : ")), 
		  {row : 0, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Type Contenu : ")), 
		  {row : 1, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Type Contenant : ")), 
		  {row : 2, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Valide : ")), 
		  {row : 3, column : 0});		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Utilisable ")), 
		  {row : 4, column : 0});		  		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Niveau : ")), 
		  {row : 5, column : 0});		  		  
		this._add(new qx.ui.basic.Label(
		  this.tr("Remarques : ")), 
		  {row : 6, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Ligne : ")), 
		  {row : 7, column : 0});		
		this._add(new qx.ui.basic.Label(
		  this.tr("Colonne : ")), 
		  {row : 8, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Date création : ")), 
		  {row : 9, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Supprimer : ")), 
		  {row : 10, column : 0})		  		   
		this._add(new qx.ui.basic.Label(
		  this.tr("Date suppression : ")), 
		  {row : 11, column : 0});
		this._add(new qx.ui.basic.Label(
		  this.tr("Notations : ")), 
		  {row : 12, column : 0});		  
		
		//init of the widget
		this.__code = new qx.ui.form.TextField();
		this.__container = new qx.ui.form.SelectBox();
		this.__content = new qx.ui.form.SelectBox();
		this.__valid = new qx.ui.form.CheckBox();
		this.__usable = new qx.ui.form.CheckBox();
		this.__level = new qx.ui.form.SelectBox();
		this.__remarks = new qx.ui.form.TextField();
		this.__rowLabel = new qx.ui.basic.Label();
		this.__colLabel = new qx.ui.basic.Label();	
		this.__dateOn = new qx.ui.basic.Label();
		this.__delete = new qx.ui.form.CheckBox();
		this.__dateOff = new qx.ui.form.DateField();		
		this.__dateOff.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY")); 
		//notations
		this.__notations= new qx.ui.container.Composite(new qx.ui.layout.Grow());		
    this.__addWindow = new glams.ui.AddContainerNotationWindow();
 		// List pour ajout de notations
		this.__listNotation = new qx.ui.form.List().set({height : 150}); 
		this.__notations._add(this.__listNotation);   
    this.__addButton = new qx.ui.form.Button("+");
		this.__removeButton = new qx.ui.form.Button("-");
		var widgetButton = new qx.ui.core.Widget();
		widgetButton._setLayout(new qx.ui.layout.HBox());
		widgetButton._add(this.__addButton);
		widgetButton._add(this.__removeButton);

		//positionnement des widgets
		this._add(this.__code, {row : 0, column : 1});
		this._add(this.__container, {row : 2, column : 1});
		this._add(this.__content, {row : 1, column : 1});
		this._add(this.__valid, {row : 3, column : 1});
		this._add(this.__usable, {row : 4, column : 1});
		this._add(this.__level, {row : 5, column : 1});
		this._add(this.__remarks, {row : 6, column : 1});
		this._add(this.__rowLabel, {row : 7, column : 1});		
		this._add(this.__colLabel,{row : 8, column : 1});		
		this._add(this.__dateOn, {row : 9, column : 1});
		this._add(this.__delete, {row :10, column : 1});
    this._add(this.__dateOff, {row : 11, column : 1});
    this._add(this.__notations, {row : 13, column : 0, colSpan: 3});
	  this.__saveButton = new qx.ui.form.Button("Valid");
	  this._add(widgetButton, {row : 14, column : 0});
		this._add(this.__saveButton, {row : 15, column : 1});
   
    
		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
		this.__services = glams.services.Sample.getInstance();
                this.__services.QueryGetAllContentType("getAllContentType", null, null);             
                this.__services.QueryAllTypeContenant("loadedAllTypeContenant", null, null);
                this.__services.QueryGetAllContainerLevel("getAllContainerLevel", null, null); 
    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
    //choix du type de contenu
    this.__services.addListenerOnce("getAllContentType", function(e){
      var loadData = e.getData();
      this.__itemContent=this.__content.getSelection();
      //recupère la liste du selectBox
      var listSelectBx = this.__content.getChildControl("list");
      //creation du controller affecté à cette liste
      this.__controllerContent=new qx.data.controller.List(null,listSelectBx);      
      //creation du model à partir des données codées en Json
      var modelContent= qx.data.marshal.Json.createModel(loadData);      
      //affecte au controller la valeur de "type" du modele au label de la liste
      this.__controllerContent.setDelegate({
        bindItem: function(controllerContent,modelContent,listSelectBx ){
          controllerContent.bindProperty ("type","label",null,modelContent,listSelectBx );
        }    
      });      
      //affecte au controller le modele
      this.__controllerContent.setModel(modelContent);      
    }, this);

      
    //choix du type de contenant
    this.__services.addListenerOnce("loadedAllTypeContenant", function(e){
      //this.__container.removeAll();  //quelle utilité ?
      var loadData = e.getData();
       this.__itemContainer=this.__container.getSelection();
      //recupère la liste du selectBox
      var listSelectBx = this.__container.getChildControl("list");
      //creation du controller affecté à cette liste
      this.__controllerContainer=new qx.data.controller.List(null,listSelectBx);
      //creation du model à partir des données codées en Json
      var modelContainer= qx.data.marshal.Json.createModel(loadData);
      //affecte au controller la valeur de "level" du modele au label de la liste
      this.__controllerContainer.setDelegate({
        bindItem: function(controllerContainer,modelContainer,listSelectBx ){
          controllerContainer.bindProperty ("type","label",null,modelContainer,listSelectBx );
        }    
      });    
      //affecte au controller le modele
      this.__controllerContainer.setModel(modelContainer);             
    }, this);//fin choix du type de contenant
    
    //choix du level de contenu
    this.__services.addListenerOnce("getAllContainerLevel", function(e){
      var loadData = e.getData();
      this.__itemLevel=this.__level.getSelection();
      //recupère la liste du selectBox
      var listSelectBx = this.__level.getChildControl("list");
      //creation du controller affecté à cette liste
      this.__controllerLevel=new qx.data.controller.List(null,listSelectBx);
      //creation du model à partir des données codées en Json
      var modelLevel= qx.data.marshal.Json.createModel(loadData);      
      //affecte au controller la valeur de "level" du modele au label de la liste
      this.__controllerLevel.setDelegate({
        bindItem: function(controllerLevel,modelLevel,listSelectBx ){
          controllerLevel.bindProperty ("level","label",null,modelLevel,listSelectBx );
        }    
      });      
      //affecte au controller le modele
      this.__controllerLevel.setModel(modelLevel);
    }, this); //fin choix du level de contenu       

    //changement de tube :    
        this.addListener("changeContainer", function(e){ 
          this.__code.setValue(this.getContainer().getCode());
          this.__dateOn.setValue(this.getContainer().getDateOn()); 
          this.__dateOff.setValue(this.getContainer().getDateOff()); 
          if (this.__dateOff.getValue()) { this.__delete.setValue(true) }else { this.__delete.setValue(false) } ;           
          this.__rowLabel.setValue(this.getContainer().getRow());
          this.__colLabel.setValue(this.getContainer().getColumn());
          this.__valid.setValue(this.getContainer().getValid());
          this.__usable.setValue(this.getContainer().getUsable());
          this.__remarks.setValue(this.getContainer().getRemark());
          this.__idBox = this.getContainer().getIdBox();

          //change la valeur du selectBox typecontenu          
          for (var l =0 ; l<  this.__controllerContent.getModel().getLength(); l++){
            if (this.__controllerContent.getModel().getItem(l).getType() == this.getContainer().getContent()){
              var iteml= this.__controllerContent.getModel().getItem(l);
              var item0= this.__controllerContent.getModel().getItem(0);
              this.__controllerContent.getModel().setItem(0,iteml);
              this.__controllerContent.getModel().setItem(l,item0);          
              this.__content.setSelection(this.__itemContent);
            };
          } 
          //change la valeur du selectBox typecontenant          
          for (var l =0 ; l<  this.__controllerContainer.getModel().getLength(); l++){
            if (this.__controllerContainer.getModel().getItem(l).getType() == this.getContainer().getTypeContainer()){
              var iteml= this.__controllerContainer.getModel().getItem(l);
              var item0= this.__controllerContainer.getModel().getItem(0);
              this.__controllerContainer.getModel().setItem(0,iteml);
              this.__controllerContainer.getModel().setItem(l,item0);
              this.__container.setSelection(this.__itemContainer);
            };          
          }
          //change la valeur du selectBox level :
          for (var l =0 ; l<  this.__controllerLevel.getModel().getLength(); l++){
            if (this.__controllerLevel.getModel().getItem(l).getLevel() == this.getContainer().getLevel()){
              iteml= this.__controllerLevel.getModel().getItem(l);
              item0= this.__controllerLevel.getModel().getItem(0);
              this.__controllerLevel.getModel().setItem(0,iteml);
              this.__controllerLevel.getModel().setItem(l,item0);
              this.__level.setSelection(this.__itemLevel);
            };          
          };         
        },this);
        
    //chargement des notations sur container :    
    this.addListener("changeContainer", function(e){ 
      //initialisation
      this.__listNotation.removeAll()
      var tab={};
      tab['idContainer']=this.getContainer().getId();
      this.__services.QueryGetNotationsByContainer("getNotationsByContainer", null, tab);   
      this.__services.addListener("getNotationsByContainer", function(e){     
        var loadData = e.getData();
        if (loadData.length > 0){
          if (loadData[0]['id_container']==this.getContainer().getId()){
            for (var n=0; n< loadData.length ; n++){
 		          var tempItem = new qx.ui.form.ListItem(loadData[n]['notation'] + " : " + loadData[n]['valeur']+" ; "+loadData[n]['date']+" ; "+loadData[n]['notateur']+" ; "+loadData[n]['remarque']);
		          tempItem.setUserData("type", loadData[n]['notation'] );
		          tempItem.setUserData("value", loadData[n]['valeur']);
		          tempItem.setUserData("date", loadData[n]['date']);
		          tempItem.setUserData("notateur", loadData[n]['notateur']);
		          tempItem.setUserData("remarque", loadData[n]['remarque']);
		          tempItem.setUserData("idNotation", loadData[n]['id_notation']);

		          this.__listNotation.add(tempItem);               
            };          
          };        
        };
      },this);   
    },this);
    //ajout de notations
    this.__addButton.addListener("execute", function(){
      this.__addWindow.open();
		  this.__addWindow.center();
		  this.__addWindow.updateData();
    }, this);
    //bouton de notations		
    this.__addWindow.addListener("addNotationSample", function(e){
		  var loadData = e.getData();
		  var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value+" ; "+loadData.date+" ; "+loadData.notateur+" ; "+loadData.remarque);
		  tempItem.setUserData("type", loadData.type);
		  tempItem.setUserData("value", loadData.value);
		  tempItem.setUserData("date", loadData.date);		  
		  tempItem.setUserData("notateur", loadData.notateur);		  
		  tempItem.setUserData("remarque", loadData.remarque);		  
		  this.__listNotation.add(tempItem);
		}, this);
    //supprime une notation 	
    this.__removeButton.addListener("execute", function(){
		  this.__listNotation.getSelection()[0].destroy();
		}, this); 
    //activation supprimer
    this.__delete.addListener("changeValue", function(e){
      if(this.__delete.getValue() == true ){
      this.__dateOff.setValue(new Date());
      }else{
      this.__dateOff.setValue(null)
      };     
    },this);
    //action de sauvegarde 
    this.__saveButton.addListener("execute", function(){
      var tab={};
      tab['id']=this.getContainer().getId();
      tab['code']=this.__code.getValue(); 
      tab['valid']=this.__valid.getValue();
      tab['usable']=this.__usable.getValue();
      tab['remark']=this.__remarks.getValue();
      tab['typeContent']= this.__content.getSelection()[0].getLabel();
      tab['typeContainer']= this.__container.getSelection()[0].getLabel();
      tab['level']=this.__level.getSelection()[0].getLabel();       
      //tab['dateOff']= this.__dateOff.getValue() ; 
      if (this.__dateOff.getValue() != null ){
        var newDate = this.__dateOff.getValue() ;
        newDate.setDate(this.__dateOff.getValue().getDate()+1) ; //correction pour conserver la date
        tab['dateOff']=newDate;
      }else{
        tab['dateOff']= null;
      };
      tab['idBox']=this.__idBox;
      tab['notations']=[];

            
      for(var i in this.__listNotation.getChildrenContainer()._getChildren()){
        var x={};
        x['type']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("type");
        x['value']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("value");
        x['date']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("date");        
        x['notateur']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("notateur");        
        x['remarque']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("remarque");        
        x['idNotation']=this.__listNotation.getChildrenContainer()._getChildren()[i].getUserData("idNotation");
        tab['notations'].push(x); 
      };
      
      this.fireDataEvent("updateContainer",tab);

    },this);
  
    
   
	}
});
