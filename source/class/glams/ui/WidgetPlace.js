/* ************************************************************************

  Copyright: 2017 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget that defines all places.
 * Authors : David Aurégan, Fabrice Dupuis,
 * May 2018.
 */
qx.Class.define("glams.ui.WidgetPlace", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Fire when the list of places is changed
     */
    "changeListPlaces": "qx.event.type.Data"
  },

  properties: {

  },

  members: {},

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.Grid()
    layout.setSpacing(5);
    this._setLayout(layout);



    //positionnement de la boite créée *******************************************
    this.__terminologyPlace = new glams.ui.TerminologyTools();
    this.__terminologyPlace.setName("Place");

    var widgetLocateBox = new qx.ui.core.Widget();
    widgetLocateBox._setLayout(new qx.ui.layout.Grid());
    this.__boxPlaces = new qx.data.Array;
    this.__typePlace = new qx.ui.form.SelectBox();
    this.__namePlace = new glams.ui.FiltredComboBox();

    this.__addButton = new qx.ui.form.Button("add");
    this.__reset = new qx.ui.form.Button("Reset");
    var i = 0 //increment des places
    widgetLocateBox._add(this.__typePlace, {
      row: 0 + i,
      column: 0
    });
    widgetLocateBox._add(this.__namePlace, {
      row: 0 + i,
      column: 1
    });
    widgetLocateBox._add(this.__addButton, {
      row: 0 + i,
      column: 2
    });
    this._add(widgetLocateBox, {
      row: 0,
      column: 0
    });
    //fin positionnement de la boite créee***************************************





    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    var services = glams.services.Sample.getInstance();
    // services.QueryGetAlltypesLieux("getAlltypesLieux", null, null);
    services.QueryGetAlltypesLieux("getAlltypesLieux", this.__sessionId, null);



    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */

    //Renvoie le terme racine des l'ontologie Positionnement
    this.__terminologyPlace.addListener("changeRacine", function() {
      var loadData = this.__terminologyPlace.getRacine();
      this.__typePlace.removeAll();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['term_label']);
        tempItem.setUserData("concept_id", loadData[i]['concept_id']);
        this.__typePlace.add(tempItem);
      };
    }, this);
    //Renvoie le terme suivant de l'ontologie Positionnement
    this.__terminologyPlace.addListener("findLeftConcept", function(e) {
      this.__typePlace.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['term_label']);
        tempItem.setUserData("concept_id", loadData[i]['concept_id']);
        this.__typePlace.add(tempItem);
      };

    }, this);

    //affichage dans l'autocompleteBox
    this.__namePlace.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      var tab = {};
      tab['typeLieu'] = this.__typePlace.getSelection()[0].getLabel();
      tab['filtredText'] = e.getData();
      services.QueryGetAllValuesLieuxFiltred("getAllValuesLieuxFiltred", this.__sessionId, tab);
    }, this);
    services.addListener("getAllValuesLieuxFiltred", function(e) {
      this.__listPlace = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listPlace.push({
          'label': loadData[i]['valeur']
        });
      };
      this.__namePlace.setListModel(this.__listPlace);
    }, this);

    //affichage quand le type de lieu change
    this.__typePlace.addListener("changeSelection", function(e) {
      if (this.__typePlace.getSelection() != '') {
        var tab = {};
        tab['typeLieu'] = this.__typePlace.getSelection()[0].getLabel();
        tab['filtredText'] = '';
        services.QueryGetAllValuesLieuxFiltred("getAllValuesLieuxFiltred", this.__sessionId, tab);
      };
    }, this);

    //affichage quand on ajoute un emplacement
    this.__addButton.addListener("execute", function() {
      if (this.__namePlace.getAllValues().getLabel() != null) {
        if (this.__namePlace.getAllValues().getLabel().length > 0) {
          i = i + 1;
          widgetLocateBox._add(new qx.ui.basic.Label(this.__typePlace.getSelection()[0].getLabel()), {
            row: 0 + i,
            column: 0
          });
          widgetLocateBox._add(new qx.ui.basic.Label(this.__namePlace.getValue()), {
            row: 0 + i,
            column: 1
          });
          widgetLocateBox._add(this.__reset, {
            row: 0 + i,
            column: 2
          });
          widgetLocateBox._add(this.__typePlace, {
            row: 1 + i,
            column: 0
          });
          widgetLocateBox._add(this.__namePlace, {
            row: 1 + i,
            column: 1
          });
          widgetLocateBox._add(this.__addButton, {
            row: 1 + i,
            column: 2
          });
          var place = {};
          //          console.log(place);
          place['type'] = this.__typePlace.getSelection()[0].getLabel();
          place['name'] = this.__namePlace.getAllValues().getLabel();
          place['concept_id'] = this.__typePlace.getSelection()[0].getUserData("concept_id");
          place['context_id'] = this.__terminologyPlace.getContext();
          //          console.log(place);
          this.__boxPlaces.push(place);
          //          console.log(this.__boxPlaces.toArray());
          this.__namePlace.setValue(null);
          // affecte au selectBox un nouveau choix de lieux
          this.__terminologyPlace.findLeftConcept(this.__typePlace.getSelection()[0].getUserData("concept_id"));
          this.fireDataEvent("changeListPlaces", this.__boxPlaces)
        };
      };
    }, this);

    //boutton reset
    this.__reset.addListener("execute", function() {
      i = 0;
      widgetLocateBox._removeAll();
      widgetLocateBox._add(this.__typePlace, {
        row: 0 + i,
        column: 0
      });
      widgetLocateBox._add(this.__namePlace, {
        row: 0 + i,
        column: 1
      });
      widgetLocateBox._add(this.__addButton, {
        row: 0 + i,
        column: 2
      });
      this.__boxPlaces.removeAll();
      this.__terminologyPlace.calculRacine();
    }, this);
  }
});