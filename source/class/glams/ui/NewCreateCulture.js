/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new culture in the database.
 *
 * Authors : David Aurégan,
 *
 * June 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.NewCreateCulture", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * close the windows
     */
    "fermetureCulture": "qx.event.type.Event",

    /**
     * Organize the data to save the culture
     */
    "saveCulture": "qx.event.type.Event",

    /**
     * Save the new Culture
     */
    "saveNewCulture": "qx.event.type.Data"
  },

  properties: {
    /**
     * Data on the treatment applied to the crop
     */
    traitement: {
      init: null,
      event: "changeTraitement"
    },

    /**
     * Genre of the plant to creat
     */
    genre: {
      init: null,
      // TODO: Implementer le changement de la selection sur le changement de genre
      event: "changeGenre"
    }
  },

  members: {
    /**
     * Function that returns information from the selected culture creation form
     * @return {object}              [Information about the selected form]
     */
    getCurentNewCulture: function() {
      for (var i in this.__newCulture) {
        if (i == this.__creationForm) {
          return this.__newCulture[i];
        }
      };
    },
    /**
     *
     */
    annuler: function() {
      this.__radioGroup.setSelection([]);
      this.__formulaire.removeAll();
      this.__protocole.setValue(null);
      this.__affichageProtocole.removeAll();
      if (this.__idProtocole) {
        this.__idProtocole.setValue = null;
      }
      this.__labbook.setValue(null);
      this.__notation.removeAll();
      this.__remarks.setValue(null);
      this.__affichageTraitement.removeAll();
      this.__affichageTraitement._add(new qx.ui.basic.Label(this.tr("Aucun")).set({
        font: "bold"
      }), {
        row: 0,
        column: 0
      });
      this.traitement = null;
      this.__SupprTraitement.setVisibility("hidden");
    }

  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(15);
    this._setLayout(layout);

    var form = new qx.ui.form.Form();

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);

    //Initialisation des variable pour l'enregistrement de certaines informations qui apparaissent undefined quand elles ne sont pas renseigné
    this.place = null;
    this.collection = null;
    this.__tabTraitement = null;
    this.__idProtocole = null;
    this.nameNewLot = null;
    this.__nameProtocole = null;
    this.traitement = [];

    //Implémentation des widget visible à l'écran

    /*Interieur de la GroupBox*/
    var groupBoxMatBioOri = new qx.ui.groupbox.GroupBox(this.tr("Matériel biologique d'origine"));
    groupBoxMatBioOri.setLayout(grid);

    groupBoxMatBioOri.add(new qx.ui.basic.Label().set({
      value: "Nouvelle Culture initiée à partir de :",
      font: "bold"
    }), {
      row: 0,
      column: 0,
      colSpan: 3
    });
    groupBoxMatBioOri.add(new qx.ui.basic.Label(
      this.tr("Plante Modèle : ")), {
      row: 1,
      column: 0
    });
    this.__genre = new qx.ui.form.SelectBox();
    groupBoxMatBioOri.add(this.__genre, {
      row: 1,
      column: 1
    });
    this.__newLot = new qx.ui.form.RadioButton("Lot déjà existant");
    groupBoxMatBioOri.add(this.__newLot, {
      row: 2,
      column: 0
    });
    this.__newAccession = new qx.ui.form.RadioButton("Accession déjà existante");
    groupBoxMatBioOri.add(this.__newAccession, {
      row: 2,
      column: 1
    });
    this.__newVariete = new qx.ui.form.RadioButton("Variété déjà existante");
    groupBoxMatBioOri.add(this.__newVariete, {
      row: 2,
      column: 2
    });
    this.__newPlant = new qx.ui.form.RadioButton("Nouvelle Variété");
    groupBoxMatBioOri.add(this.__newPlant, {
      row: 2,
      column: 3
    });
    this.__radioGroup = new qx.ui.form.RadioGroup();
    this.__radioGroup.setAllowEmptySelection(true);
    this.__radioGroup.setSelection([]);
    this.__radioGroup.add(this.__newLot, this.__newVariete, this.__newAccession, this.__newPlant);
    this.__formulaire = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    this.__formulaire.setWidth(5)
    groupBoxMatBioOri.add(this.__formulaire, {
      row: 3,
      column: 0,
      colSpan: 3
    });
    /*Fin de la GroupBox*/
    this._add(groupBoxMatBioOri, {
      row: 0,
      column: 0,
      colSpan: 3
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Protocole de culture : ")), {
      row: 1,
      column: 0
    });
    this.__protocole = new glams.ui.FiltredComboBox();
    this.__protocole.setPlaceholder(this.tr("Nom du protocole"));
    this._add(this.__protocole, {
      row: 1,
      column: 1
    });
    var ajoutProtocole = new qx.ui.form.Button(this.tr("Nouveau protocole"));
    this._add(ajoutProtocole, {
      row: 1,
      column: 2
    });
    this.__affichageProtocole = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    this.__affichageProtocole.setWidth(5);
    this._add(this.__affichageProtocole, {
      row: 2,
      column: 0,
      colSpan: 3
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Date de mise en culture : ")), {
      row: 3,
      column: 0
    });
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    this._add(this.__date, {
      row: 3,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Expérimentateur : ")), {
      row: 4,
      column: 0
    });
    this.__user = new qxelvis.ui.users.ContributorsSelector();
    this._add(this.__user, {
      row: 5,
      column: 0,
      colSpan: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence cahier labo :")), {
      row: 6,
      column: 0
    });
    this.__labbook = new qx.ui.form.TextField();
    this._add(this.__labbook, {
      row: 6,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Emplacement : ")), {
      row: 7,
      column: 0
    });
    this.__place = new glams.ui.WidgetPlace();
    this._add(this.__place, {
      row: 7,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Ajouter une notation : ")), {
      row: 8,
      column: 0
    });
    var addButton = new qx.ui.form.Button("+");
    var removeButton = new qx.ui.form.Button("-");
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox);
    widgetButton._add(addButton);
    widgetButton._add(removeButton);
    this.__notation = new qx.ui.form.List().set({
      height: 100
    });
    var addWindow = new glams.ui.AddSampleNotationWindow();
    var widgetNotation = new qx.ui.core.Widget();
    widgetNotation._setLayout(new qx.ui.layout.VBox());
    widgetNotation._add(this.__notation);
    widgetNotation._add(widgetButton);
    this._add(widgetNotation, {
      row: 8,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Remarque : ")), {
      row: 9,
      column: 0
    });
    this.__remarks = new qx.ui.form.TextArea();
    this.__remarks.setPlaceholder("Remarque sur les conditions de cultures");
    this._add(this.__remarks, {
      row: 9,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Experience :")), {
      row: 10,
      column: 0
    });
    this.__collection = new glams.ui.WidgetExperience();
    this._add(this.__collection, {
      row: 10,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Traitement :")), {
      row: 11,
      column: 0
    });
    this.__affichageTraitement = new qx.ui.container.Composite(new qx.ui.layout.Grid());
    this.__affichageTraitement.setWidth(5);
    this._add(this.__affichageTraitement, {
      row: 11,
      column: 1
    });
    this.__affichageTraitement._add(new qx.ui.basic.Label(this.tr("Aucun")).set({
      font: "bold"
    }), {
      row: 0,
      column: 0
    });
    this.__SupprTraitement = new qx.ui.form.Button(this.tr("Supprimer le Traitement"));
    this.__SupprTraitement.setVisibility("hidden");
    this._add(this.__SupprTraitement, {
      row: 11,
      column: 2
    });
    this.__labelValidation = new qx.ui.basic.Label('Obligatoire : ');
    this.__labelValidation.setTextColor('red');
    this.__labelValidation.setVisibility("hidden");
    this._add(this.__labelValidation, {
      row: 12,
      column: 0,
      colSpan: 2
    });
    var Traitement = new qx.ui.form.Button(this.tr("Ajouter un Traitement"));
    this._add(Traitement, {
      row: 13,
      column: 0
    });
    var ValidationCulture = new qx.ui.form.Button(this.tr("Valider et Créer une autre culture"));
    this._add(ValidationCulture, {
      row: 13,
      column: 1
    });
    var Valider = new qx.ui.form.Button(this.tr("Valider"));
    this._add(Valider, {
      row: 14,
      column: 0
    });
    var ValidationPrelevement = new qx.ui.form.Button(this.tr("Valider et Créer un prélèvement"));
    this._add(ValidationPrelevement, {
      row: 14,
      column: 1
    });
    var Annuler = new qx.ui.form.Button(this.tr("Annuler"));
    this._add(Annuler, {
      row: 14,
      column: 2
    });

    var validationProtocole = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez selectionner un Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var validationRef = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez renseigner la référence du cahier de laboratoire");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    this.__manager = new qx.ui.form.validation.Manager();
    this.__manager.add(this.__protocole, validationProtocole);
    this.__manager.add(this.__labbook, validationRef);

    this.__newCulture = {};

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryAllUser("loadedAllUser", null, null);
    var tab = {}
    this.__services.QueryGetAllGenres("getAllGenres", this.__sessionId, tab);
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //Charge tous les utilisateurs
    this.__services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__user.setUsers(loadData);
    }, this);

    // Charge tout les genres
    this.__services.addListener("getAllGenres", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var itemData = new qx.ui.form.ListItem(loadData[i]['genre']);
        this.__genre.add(itemData);
      };
    }, this);

    //Stock le genre selectionné à chaque changement
    this.__genre.addListener("changeSelection", function() {
      var tab = {};
      tab['genre'] = this.__genre.getSelection()[0].getLabel();
      //Envoie la requete pour charger toutes les espèces a chaque changement de genre
      this.__services.QueryGetAllEspeceByGenre("getAllEspeceByGenre", this.__sessionId, tab);
      if (this.__selectVarOrigine) {
        this.__selectVarOrigine.setValue(null);
        this.__listVarOrigine.removeAll();
      };
      if (this.__selectAccOrigine) {
        this.__selectAccOrigine.setValue(null);
        this.__listAccOrigine.removeAll();
      };
      /// TODO: Changer le modèle de donnée et le setGenre
      this.setGenre(this.__genre.getSelection()[0].getLabel());
    }, this);

    // TODO: Implementer le changement de la selection sur le changement de genre
    // this.addListener("changeGenre", function(e) {
    //   var loadData = e.getData();
    //   if (this.__genre) {
    //     for (var i in this.__genre) {
    //       // if (loadData == i.getLabel()) {
    //       this.debug(i);
    //       this.__genre.resetSelection();
    //       this.__genre.setSelection(i);
    //       console.log("obj");
    //       // break;
    //       // };
    //     };
    //   };
    // }, this);



    //Permet de récuperer les place choisi via le widgetPlace
    this.__place.addListener("changeListPlaces", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.place = loadData;
    }, this);

    // Permet de récupérer les collections via le widgetExperience
    this.__collection.addListener("changeListCollection", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.collection = loadData;
    }, this);

    /*
     * Notation
     */

    //Permet l'ajout d'une notation
    addButton.addListener("execute", function() {
      addWindow.open();
      addWindow.center();
      if (this.__user.getContributors() != null) {
        addWindow.setNotateur(this.__user.getContributors());
      };
      addWindow.setContexteNotation("sample : prelevment informations");
    }, this);

    //Affiche les notations qui ont été effectué
    this.__dataNotation = [];
    addWindow.addListener("addNotationSample", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      this.__notation.add(tempItem);
      this.__dataNotation.push(loadData);
    }, this);

    //Supprime la notation selectionné
    removeButton.addListener("execute", function() {
      this.__notation.getSelection()[0].destroy();
    }, this);

    /*
     * RadioGroup
     */

    //Définie l'affichage du this.__formulaire création du lot en fonction de ce qui à été selectionné dans le RadioGroup
    this.__creationForm = null;
    //Creation Nouvelle Variété
    this.__newPlant.addListener("execute", function() {
      this.__formulaire.removeAll();
      if (this.__radioGroup.getSelection()[0]) {
        var createVariete = new glams.ui.CreateNewVariete();
        this.bind("genre", createVariete, "genre");
        this.__formulaire.add(createVariete);
        this.__creationForm = "rien";
        this.__newCulture["rien"] = createVariete;
      };
    }, this);
    // Création Nouvelle Accession
    this.__newVariete.addListener("execute", function() {
      this.__formulaire.removeAll();
      if (this.__radioGroup.getSelection()[0]) {
        var createAccession = new glams.ui.CreateNewAccession();
        this.bind("genre", createAccession, "genre");
        this.__formulaire.add(createAccession);
        this.__creationForm = "variete";
        this.__newCulture["variete"] = createAccession;
      };
    }, this);
    //Creation Nouveau Lot via une Accession
    this.__newAccession.addListener("execute", function() {
      this.__formulaire.removeAll();
      if (this.__radioGroup.getSelection()[0]) {
        var createLotFromAccession = new glams.ui.CreateNewLotFromAccession();
        this.bind("genre", createLotFromAccession, "genre");
        this.__formulaire.add(createLotFromAccession);
        this.__creationForm = "accession";
        this.__newCulture["accession"] = createLotFromAccession;
      };
    }, this);
    // Création Nouveau Lot via un Lot déjà existant
    this.__newLot.addListener("execute", function() {
      this.__formulaire.removeAll();
      if (this.__radioGroup.getSelection()[0]) {
        var createLotFromLot = new glams.ui.CreateNewLotFromLot();
        this.bind("genre", createLotFromLot, "genre");
        this.__formulaire.add(createLotFromLot);
        this.__creationForm = "lot";
        this.__newCulture["lot"] = createLotFromLot;
      };
    }, this);

    /*
     * Protocole
     */

    // Fonctions qui chargent les protocole en fonction des carractères saisi dans la barre de texte
    this.__protocole.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 1) {
        var tab = {};
        tab['filtredName'] = e.getData();
        this.__services.QueryGetAllProtocolFiltred("getAllProtocolFiltred", this.__sessionId, tab);
      };
    }, this);
    this.__services.addListener("getAllProtocolFiltred", function(e) {
      this.__listProtocol = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listProtocol.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id'],
          'adress': loadData[i]['adress'],
          'summary': loadData[i]['summary'],
          'refLab': loadData[i]['reference_laboratory_handbook'],
          'refAl': loadData[i]['reference_alfresco']
        });
      };
      this.__protocole.setListModel(this.__listProtocol);
    }, this);

    //Ouverture de la fenêtre de création d'un protocole
    ajoutProtocole.addListener("execute", function() {
      var newContainer = new glams.ui.CreateProtocoleCulture();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container.add(newContainer);
      var window = new qx.ui.window.Window("New Protocol");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(container);
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      //Fonction de fermeture de la fenetre
      newContainer.addListener("fermetureCreateProtocol", function() {
        window.close();
      }, this);
    }, this);

    //Fonction qui permet l'affichage des informations relatives au protocole selectionné
    this.__protocole.addListener("changeAllValues", function(e) {
      this.__affichageProtocole.removeAll();
      var displayProtocole = new glams.ui.AffichageProtocole();
      displayProtocole.setProtocole(this.__protocole.getAllValues());
      this.__affichageProtocole.add(displayProtocole);
    }, this);

    /*
     * Traitement
     */
    //Ouverture de la fenêtre d'Ajout d'un traitement
    Traitement.addListener("execute", function() {
      var newContainer = new glams.ui.Traitement();
      //Permet l'envoie des données sur le nom du Lot a créer et sur le protocole de culture dans la classe Traitement
      var tab = {};
      if (this.nameNewLot != null) {
        tab['nomLot'] = this.nameNewLot.getValue();
      } else {
        tab['nomLot'] = null
      };
      if (this.__protocole.getAllValues() != null) {
        tab['protocolCulture'] = this.__protocole.getAllValues().getLabel();
      } else {
        tab['protocolCulture'] = null
      };
      newContainer.setCulture(tab);
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox()).set({
        width: 500
      });
      container.add(newContainer);
      var window = new qx.ui.window.Window("Traitement");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(container);
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      //Fonction de fermeture de la fenetre
      newContainer.addListener("fermetureTraitement", function() {
        window.close();
        this.__SupprTraitement.setVisibility("visible");
      }, this);


      // Fonction qui récupère les données saisie lors de la création du Traitement
      newContainer.addListener("okTraitement", function(e) {
        var loadData = e.getData();
        this.traitement.push(loadData);
        this.__affichageTraitement.removeAll();
        for (var i in this.traitement) {
          i = parseInt(i);
          this.__affichageTraitement._add(new qx.ui.basic.Label(this.traitement[i].valeur_notation).set({
            font: "bold"
          }), {
            row: i,
            column: 0
          });
        };
      }, this);
    }, this);

    this.__SupprTraitement.addListener("execute", function() {
      this.__affichageTraitement.removeAll();
      this.__affichageTraitement._add(new qx.ui.basic.Label(this.tr("Aucun")).set({
        font: "bold"
      }), {
        row: 0,
        column: 0
      });
      this.traitement = null;
      this.__SupprTraitement.setVisibility("hidden");
    }, this);

    /*
     * Bouton de Sauvegarde et de Suppression de l'ensemble des données
     */

    //Listener sur le bouton Annuler qui efface toutes les données saisies
    Annuler.addListener("execute", function() {
      // this.__radioGroup.setSelection([]);
      // this.__formulaire.removeAll();
      // this.__protocole.setValue(null);
      // this.__affichageProtocole.removeAll();
      // if (this.__idProtocole) {
      //   this.__idProtocole.setValue = null;
      // }
      // this.__labbook.setValue(null);
      // this.__notation.removeAll();
      // this.__remarks.setValue(null);
      // this.__affichageTraitement.removeAll();
      // this.__affichageTraitement._add(new qx.ui.basic.Label(this.tr("Aucun")).set({
      //   font: "bold"
      // }), {
      //   row: 0,
      //   column: 0
      // });
      // this.traitement = null;
      // this.__SupprTraitement.setVisibility("hidden");
      this.annuler();
    }, this);

    //Listener sur le bouton Valider qui lance la fonction de Sauvegarde des données dans la base
    Valider.addListener("execute", function() {
      this.__typeValidation = "Normal";
      this.__labelValidation.setValue('Obligatoire : ');
      if (this.__creationForm == null) {
        this.__labelValidation.setVisibility("visible");
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner la plante d'origine");
      } else {
        if (this.__creationForm == "rien" && this.getCurentNewCulture().getVariete() == null) {
          this.__labelValidation.setVisibility("visible");
          this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le nom de la nouvelle variété");
        } else {
          if (!this.__user.getContributors()) {
            this.__labelValidation.setVisibility("visible");
            this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le ou les utilisateur(s) ");
          } else {
            this.__manager.validate();
          };
        };
      };
    }, this);

    ValidationCulture.addListener("execute", function() {
      this.__typeValidation = "Culture";
      this.__labelValidation.setValue('Obligatoire : ');
      if (this.__creationForm == null) {
        this.__labelValidation.setVisibility("visible");
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner la plante d'origine");
      } else {
        if (this.__creationForm == "rien" && this.getCurentNewCulture().getVariete() == null) {
          this.__labelValidation.setVisibility("visible");
          this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le nom de la nouvelle variété");
        } else {
          if (!this.__user.getContributors()) {
            this.__labelValidation.setVisibility("visible");
            this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le ou les utilisateur(s) ");
          } else {
            this.__manager.validate();
          };
        };
      };
    }, this);

    ValidationPrelevement.addListener("execute", function() {
      this.__typeValidation = "Prelevement";
      this.__labelValidation.setValue('Obligatoire : ');
      if (this.__creationForm == null) {
        this.__labelValidation.setVisibility("visible");
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner la plante d'origine");
      } else {
        if (this.__creationForm == "rien" && this.getCurentNewCulture().getVariete() == null) {
          this.__labelValidation.setVisibility("visible");
          this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le nom de la nouvelle variété");
        } else {
          if (!this.__user.getContributors()) {
            this.__labelValidation.setVisibility("visible");
            this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Veuillez renseigner le ou les utilisateur(s) ");
          } else {
            this.__manager.validate();
          };
        };
      };
    }, this);

    this.__manager.addListener("complete", function(e) {
      if (this.__manager.getValid()) {
        this.fireEvent("saveCulture");
      }
    }, this);

    //Fonction qui Sauvegarde les données dans la base en les stockant dans un dictionnaire (tab)
    this.addListener("saveCulture", function() {
      var tabNotation = [];
      //Stockage des notations
      for (var i in this.__notation.getChildrenContainer()._getChildren()) {
        var tab = {}
        tab['type_notation'] = this.__notation.getChildrenContainer()._getChildren()[i].getUserData("type");
        tab['valeur_notation'] = this.__notation.getChildrenContainer()._getChildren()[i].getUserData("value");
        tab['remarque_notation'] = this.__dataNotation[i].remarque;
        tab['listeUser'] = this.__dataNotation[i].notateur;
        tab['protocol'] = this.__dataNotation[i].protocole;
        tabNotation.push(tab);
      };
      //Stockage de la reference cahier de laboratoire
      if (this.__labbook.getValue() != null && this.__labbook.getValue().length > 0) {
        var tabRef = {};
        tabRef['type_notation'] = 'référence du cahier de laboratoire'
        tabRef['listeUser'] = this.__user.getContributors();
        tabRef['valeur_notation'] = this.__labbook.getValue();
        tabRef['remarque_notation'] = null;
        tabRef['protocol'] = null;
        tabNotation.push(tabRef);
      };
      //Stockage des remarques sur la culture
      if (this.__remarks.getValue() != null && this.__remarks.getValue().length > 0) {
        var tabRem = {};
        tabRem['type_notation'] = 'remarque'
        tabRem['listeUser'] = this.__user.getContributors();
        tabRem['valeur_notation'] = this.__remarks.getValue();
        tabRem['remarque_notation'] = null;
        tabRem['protocol'] = null;
        tabNotation.push(tabRem);
      };

      var tabNotationVariete = [];
      // stockage du fond genetique
      if (this.__creationForm == 'rien') {
        var fondGen = {};
        fondGen['type_notation'] = 'fond génétique';
        fondGen['listeUser'] = this.__user.getContributors();
        fondGen['valeur_notation'] = this.getCurentNewCulture().getFondGenetique();
        fondGen['remarque_notation'] = null;
        fondGen['protocol'] = null;
        tabNotationVariete.push(fondGen);
      };

      // stockage du gene mute
      if (this.__creationForm == 'rien') {
        var geneMute = {};
        geneMute['type_notation'] = 'gène muté';
        geneMute['listeUser'] = this.__user.getContributors();
        geneMute['valeur_notation'] = this.getCurentNewCulture().getGeneMute();
        geneMute['remarque_notation'] = null;
        geneMute['protocol'] = null;
        tabNotationVariete.push(geneMute);
      };

      // stockage de l'agent mutagène
      if (this.__creationForm == 'rien') {
        var agent = {};
        agent['type_notation'] = 'agent mutagène';
        agent['listeUser'] = this.__user.getContributors();
        agent['valeur_notation'] = this.getCurentNewCulture().getAgentMutagene();
        agent['remarque_notation'] = null;
        agent['protocol'] = null;
        tabNotationVariete.push(agent);
      };

      // stockage du gene mute
      if (this.__creationForm == 'rien') {
        var resistance = {};
        resistance['type_notation'] = 'résistance associée à la mutation';
        resistance['listeUser'] = this.__user.getContributors();
        resistance['valeur_notation'] = this.getCurentNewCulture().getResistance();
        resistance['remarque_notation'] = null;
        resistance['protocol'] = null;
        tabNotationVariete.push(resistance);
      };

      // stockage du traitement
      if (this.traitement != null) {
        for (var i in this.traitement) {
          tabNotation.push(this.traitement[i]);
        }
      }
      var tab = {};
      tab['from'] = this.__creationForm;
      //Stockage des données du lot d'origine
      if (this.__creationForm == 'lot') {
        tab['lotOrigine'] = {}
        tab['lotOrigine']['idLot'] = this.getCurentNewCulture().getIdLot();
        tab['lotOrigine']['idClone'] = this.getCurentNewCulture().getIdClone();
      };
      //Stockage des données de l'Accession d'origine
      if (this.__creationForm == 'accession') {
        tab['accOrigine'] = this.getCurentNewCulture().getIdClone();;
      };
      //Stockage des données de la Variété d'origine
      if (this.__creationForm == 'variete') {
        tab['varOrigine'] = this.getCurentNewCulture().getVariete();
        tab['newAcc'] = this.getCurentNewCulture().getNameAccession();
        tab['remAcc'] = this.getCurentNewCulture().getRemarqueAccession();
      };
      //Stockage des données de l'espece et du genre d'origine
      if (this.__creationForm == 'rien') {
        var nomVar = {};
        // if (this.getCurentNewCulture().getGenotype() != null) {
        //   nomVar['type'] = 'génotype'
        //   nomVar['nom'] = this.newGenotype.getValue();
        //   this.tabNomVar.push(nomVar);
        // };
        tab['genre'] = this.getCurentNewCulture().getGenre();
        tab['espece'] = this.getCurentNewCulture().getEspece();
        tab['newVar'] = this.getCurentNewCulture().getVariete();
        tab['remVar'] = this.getCurentNewCulture().getRemarqueVariete();
        tab['newAcc'] = this.getCurentNewCulture().getNameAccession();
        tab['remAcc'] = this.getCurentNewCulture().getRemarqueAccession();
        if (this.getCurentNewCulture().getEditeur() == null) {
          tab['editeur'] = null
        } else {
          tab['editeur'] = this.getCurentNewCulture().getEditeur();
        };
        if (this.getCurentNewCulture().getObtenteur() == null) {
          tab['obtenteur'] = null
        } else {
          tab['obtenteur'] = this.getCurentNewCulture().getObtenteur();
        };
      };
      //Stockage des autre informations sur la culture
      tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      tab['date'] = format.format(this.__date.getValue());
      tab['name'] = this.getCurentNewCulture().getNewNameLot();
      tab['annee_premiere_pousse'] = null;
      tab['porte_greffe'] = null;
      tab['protocol'] = this.__protocole.getAllValues().getId();
      tab['listeNotationsLot'] = {};
      tab['listeNotationsLot'] = tabNotation;
      tab['listeNotationsVariete'] = {};
      tab['listeNotationsVariete'] = tabNotationVariete;
      tab['place'] = this.place;
      tab['experience'] = this.collection;
      tab['typeValidation'] = this.__typeValidation;
      console.log(tab);
      this.fireDataEvent("saveNewCulture", tab);
      // Déclenchement du service de création de la culture
      // this.__services.QueryCreateArbre("createArbre", this.sessionId, tab);
    }, this);

    this.__services.addListener("createArbre", function(e) {
      var loadData = e.getData();
      console.log(loadData);
      var newIDLot = loadData;
      if (this.__typeValidation == "Normal") {
        // Déclenchement de la fonction de fermeture de la fenêtre
        this.fireEvent("fermetureCulture");
      };
      if (this.__typeValidation == "Culture") {
        this.__radioGroup.setSelection([]);
        this.__formulaire.removeAll();
        this.__protocole.setValue(null);
        this.__affichageProtocole.removeAll();
        if (this.__idProtocole) {
          this.__idProtocole = null;
        }
        this.__labbook.setValue(null);
        this.__notation.removeAll();
        this.__remarks.setValue(null);
        this.__affichageTraitement.removeAll();
        this.__affichageTraitement._add(new qx.ui.basic.Label(this.tr("Aucun")).set({
          font: "bold"
        }), {
          row: 0,
          column: 0
        });
        this.traitement = null;
        this.__SupprTraitement.setVisibility("hidden");
      };
      if (this.__typeValidation == "Prelevement") {
        var newContainer = new glams.ui.CreatePrelevementFromExisting();
        var tab = {};
        tab['name'] = this.nameNewLot.getValue();
        if (this.__creationForm == 'lot') {
          tab['accession'] = this.accession;
        };
        if (this.__creationForm == 'accession') {
          tab['accession'] = this.__accession;
        };
        if (this.__creationForm == 'variete') {
          tab['accession'] = this.newAcc.getValue()
        };
        if (this.__creationForm == 'rien') {
          tab['accession'] = this.newAcc.getValue()
        };
        tab['idLot'] = newIDLot;
        newContainer.setPlanteSource(tab);
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var windowPrel = new qx.ui.window.Window("Create Prelevement From Existing Cutlure");
        var layout = new qx.ui.layout.VBox();
        windowPrel.setLayout(layout);
        windowPrel.add(container);
        windowPrel.open();
        windowPrel.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
        //Fonction de fermeture de la fenêtre
        newContainer.addListener("fermeture", function() {
          windowPrel.close();
        }, this);
        this.fireEvent("fermetureCulture");
      }
    }, this);
  }
});