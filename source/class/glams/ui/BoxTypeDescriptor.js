/**
 * Formulaire de recherche de contenair
 * Fabrice Dupuis Avril 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.BoxTypeDescriptor",{
   extend : qx.ui.core.Widget,
	
   events : {
	 /**
    * fire when the Box type is updated
    */   
   "updatedBoxType": "qx.event.type.Event",
   /**
    * fire when the Box type is created
    */
   "createdBoxType": "qx.event.type.Event"

  },
	
  properties : {

  },
	
  members : {  
    /**
    *fonction qui affiche tous les types de container correspondants
    * @param idBox {integer} id of the Box
    */
    __typeContainerCorresponding : function(idBox){ 
        var window = new qx.ui.window.Window("Correspondance Container Box");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.setWidth(250);
        window.open();
        this.__listeContainerType=[];
        //recupère la liste des containerType
        window.moveTo(Math.round(550+Math.random()*80),Math.round(70+Math.random()*80));
        this.__services.QueryGetAllContainerType("getAllContainerType", this.__sessionId, null);
        this.__services.addListenerOnce("getAllContainerType", function(e) {
          var loadData = e.getData();
          for (var i in loadData) {
            var tab={}
            tab['type']=loadData[i]['type'];
            tab['id']=loadData[i]['id']; 
            tab['value']=false;
            tab['cb'] = new qx.ui.form.CheckBox();
            this.__listeContainerType.push(tab);
          };
          //recupère les valeurs de correspondance box-container
          this.__services.QueryGetAllBoxTypeAContainerType("getAllBoxTypeAContainerType", this.__sessionId, null);
          this.__services.addListenerOnce("getAllBoxTypeAContainerType", function(e) {   
            var loadData = e.getData();
            for ( var c in this.__listeContainerType){
              for( var bac in loadData){
                if(idBox==loadData[bac]['id_box_type']){
                  if (this.__listeContainerType[c]['id'] == loadData[bac]['id_container_type']){
                    this.__listeContainerType[c]['cb'].setValue(true);
                  };
                };
              };
            };        
            for ( var cx in this.__listeContainerType){         
              var cb = new qx.ui.form.CheckBox();
              var container = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));
              container.add(this.__listeContainerType[cx]['cb']);
              container.add(new qx.ui.basic.Label(this.__listeContainerType[cx]['type']));
              window.add(container);
            }; 
          },this);                 
//            var tempItem = new qx.ui.form.ListItem(loadData[i]['type']);
//            this.__liste.add(tempItem);
          
        }, this); 
        this.addListener("updatedBoxType",function(){ window.close() },this);
        this.addListener("createdBoxType",function(){ window.close() },this);        
        return(this.__listeContainerType);
    },
    /**
    *fonction qui gère la mise à jour d'un type de boite (partie interface+ base de donnée)
    *@param item {}
    */
    __updateBox : function(item){
      var container= new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container._add(new qx.ui.basic.Label(this.tr("Type de boite")));
      var type= new qx.ui.form.TextField();
      type.setValue(item.getLabel());
      container._add(type);
      container._add(new qx.ui.basic.Label(this.tr("Nombre de lignes")));
      var rowNumber= new qx.ui.form.TextField();
      if (item.getUserData("rowNumber")!= null){ 
        rowNumber.setValue(item.getUserData("rowNumber").toString()) 
      };
      container._add(rowNumber);   
      container._add(new qx.ui.basic.Label(this.tr("Nombre de Colonnes")));
      var colNumber= new qx.ui.form.TextField();
      if (item.getUserData("colNumber")!= null){
        colNumber.setValue(item.getUserData("colNumber").toString())      
      }; 
      container._add(colNumber);
      var savebtn = new qx.ui.form.Button(this.tr("Save"));
      container._add(savebtn);
      //ajout windows correspondance avec container
      var liste = this.__typeContainerCorresponding(item.getUserData("id"));
      //listener sur le save bouton
      savebtn.addListener("execute", function (){
        //affecte la valeur du checkBox 
        for (var j in liste){
         liste[j]['value']=liste[j]['cb'].getValue();
         liste[j]['cb']= null;
        };
//      console.log(liste);
        var tab={};
        tab['id']=item.getUserData("id");
        tab['type']=type.getValue();        
        tab['rowNumber']=rowNumber.getValue();
        tab['colNumber']=colNumber.getValue();
        tab['typeContainerCorresponding']=liste
        //test valeur integer ou -
        var regexp = new RegExp("^([1-9]+[0-9]*)?$");           
        if (tab['rowNumber'] != null && !regexp.test(tab['rowNumber'])) {
          rowNumber.setValid(false);
          rowNumber.setInvalidMessage("entrez un entier ou vide");
          return ;
        }else{
          rowNumber.setValid(true);
        };  
        if (tab['colNumber'] != null && !regexp.test(tab['colNumber'])) {
          colNumber.setValid(false);
          colNumber.setInvalidMessage("entrez un entier ou vide");
          return ;
        }else{
          colNumber.setValid(true);
        };        
        this.__services.QueryUpdateBoxType("updateBoxType", this.__sessionId, tab);
        this.__services.addListener("updateBoxType", function (e){
// console.log("retrun update : "+e.getData())
// console.log(e.getData())       
          this.fireEvent("updatedBoxType")
        },this);
      },this);
      return(container)
   }, 
    /**
    * fonction qui gère la création d'un type de boite (partie interface+ base de donnée)
    */   
   __newBox : function(){
     var container= new qx.ui.container.Composite(new qx.ui.layout.VBox());
     container._add(new qx.ui.basic.Label(this.tr("Type de boite")));
     var type= new qx.ui.form.TextField();
     container._add(type);
     container._add(new qx.ui.basic.Label(this.tr("Nombre de lignes")));
     var rowNumber= new qx.ui.form.TextField();
     container._add(rowNumber);   
     container._add(new qx.ui.basic.Label(this.tr("Nombre de Colonnes")));
     var colNumber= new qx.ui.form.TextField();
     container._add(colNumber);
     var savebtn = new qx.ui.form.Button(this.tr("Save"));
     container._add(savebtn);       
     //ajout windows correspondance avec container
     var liste = this.__typeContainerCorresponding(null);
     //listener sur le save bouton
      savebtn.addListener("execute", function (){        
        //affecte la valeur du checkBox 
        for (var j in liste){
         liste[j]['value']=liste[j]['cb'].getValue();
         liste[j]['cb']= null;
        };
        var tab={};
        tab['type']=type.getValue();        
        tab['rowNumber']=rowNumber.getValue();
        tab['colNumber']=colNumber.getValue();
        tab['typeContainerCorresponding']=liste ;
        //test valeur integer ou -
        var regexp = new RegExp("^([1-9]+[0-9]*)?$");           
        if (tab['rowNumber'] != null && !regexp.test(tab['rowNumber'])) {
          rowNumber.setValid(false);
          rowNumber.setInvalidMessage("entrez un entier ou vide");
          return ;
        }else{
          rowNumber.setValid(true);
        };  
        if (tab['colNumber'] != null && !regexp.test(tab['colNumber'])) {
          colNumber.setValid(false);
          colNumber.setInvalidMessage("entrez un entier ou vide");
          return ;
        }else{
          colNumber.setValid(true);
        };        
        this.__services.QueryCreateBoxType("createBoxType", this.__sessionId, tab);
        this.__services.addListener("createBoxType", function (e){
// console.log("retrun update : "+e.getData())
// console.log(e.getData())       
          this.fireEvent("createdBoxType")
        },this);
      },this);      
     return(container)   
   }
  },
	
	construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */    
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();
    
		var layout = new qx.ui.layout.VBox();
		layout.setSpacing(10);		
		
		this._setLayout(layout);
		//creation des widgets
		this.__label= new qx.ui.basic.Label(this.tr("Types de boite"));  
		this.__liste = new qx.ui.form.List().set({
      width : 250,
      allowStretchY : true,
      allowStretchX : true
    });
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));

    //positionnement des widgets
    this._add(this.__label);
		this._add(this.__liste,{flex:1});
		this._add(this.__buttonEdit);
		this._add(this.__buttonNew);		

		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
    this.__services.QueryGetAllBoxType("getAllBoxType", this.__sessionId, null);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */ 

    //type de boites
    this.__services.addListener("getAllBoxType", function(e){
      this.__liste.removeAll();
      var loadData = e.getData();
      for(var i in loadData){
        var tempItem = new qx.ui.form.ListItem(loadData[i]['type']);
        tempItem.setUserData("rowNumber", loadData[i]['row_number']);
        tempItem.setUserData("colNumber", loadData[i]['column_number']);
        tempItem.setUserData("id", loadData[i]['id']);
        if (!tempItem.getUserData("rowNumber")){tempItem.setUserData("rowNumber", null)};   
        if (!tempItem.getUserData("colNumber")){tempItem.setUserData("colNumber", null)};             
        this.__liste.add(tempItem);
      };
   },this);
   //doubleclick sur type de boite 
   this.__liste.addListener("dblclick", function(e){ 
        var window = new qx.ui.window.Window("Modify Container type");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.setWidth(250);
        window.add(this.__updateBox(this.__liste.getSelection()[0]));
        window.open();
        window.moveTo(Math.round(250+Math.random()*80),Math.round(70+Math.random()*80));
        window.addListener("close",function(){  this.fireEvent("updatedBoxType");   },this);
        this.addListener("updatedBoxType",function(){ window.close() },this);     
   },this);
   //bouton modifier
   this.__buttonEdit.addListener("execute", function(){
        var window = new qx.ui.window.Window("Modify Container type");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.setWidth(250);
        window.add(this.__updateBox(this.__liste.getSelection()[0]));
        window.open();
        window.moveTo(Math.round(250+Math.random()*80),Math.round(70+Math.random()*80));
        window.addListener("close",function(){  this.fireEvent("updatedBoxType");   },this);
        this.addListener("updatedBoxType",function(){ window.close() },this);        
   },this);
   //bouton ajouter
   this.__buttonNew.addListener("execute", function(e){
        var window = new qx.ui.window.Window("Add Container type");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(this.__newBox());
        window.open();
        window.moveTo(Math.round(250+Math.random()*80),Math.round(70+Math.random()*80));
        window.addListener("close",function(){  this.fireEvent("updatedBoxType");   },this);        
        this.addListener("createdBoxType",function(){ window.close() },this);   
   },this);
   //mise à jour de la liste
   this.addListener("updatedBoxType",function(){ 
     this.__services.QueryGetAllBoxType("getAllBoxType", this.__sessionId, null);
   },this);
   this.addListener("createdBoxType",function(){ 
     this.__services.QueryGetAllBoxType("getAllBoxType", this.__sessionId, null);
   },this);   
   
	}
});
