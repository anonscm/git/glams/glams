/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new lot from an variete lot in the database.
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.CreateNewVariete", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * Genre of the plant to create
     */
    genre: {
      init: null,
      event: "changeGenre"
    },
    /**
     * Breeder of the new variety
     */
    obtenteur: {
      init: null,
      event: "changeObtenteur"
    },
    /**
     * Publisher of the new variety
     */
    editeur: {
      init: null,
      event: "changeEditeur"
    },
    /**
     * Remark about the new Variety
     */
    remarqueVariete: {
      init: null,
      event: "changeRemarqueVariete"
    },
    /**
     *
     */
    resistance: {
      init: null,
      event: "changeResistance"
    },
    /**
     *
     */
    agentMutagene: {
      init: null,
      event: "changeAgentMutagene"
    },
    /**
     *
     */
    geneMute: {
      init: null,
      event: "changeGeneMute"
    },
    /**
     *
     */
    fondGenetique: {
      init: null,
      event: "changeFondGenetique"
    },
    // /**
    //  *
    //  */
    // genotype: {
    //   init: null,
    //   event: "changeGenotype"
    // },
    /**
     * Name of the new Variety
     */
    variete: {
      init: null,
      event: "changeVariete"
    },
    /**
     * Species of the new Variety
     */
    espece: {
      init: null,
      event: "changeEspece"
    },
    /**
     * Name of the new Accession
     */
    nameAccession: {
      init: null,
      event: "changeNameAccession"
    },
    /**
     * Remark about the new Accession
     */
    remarqueAccession: {
      init: null,
      event: "changeRemarqueAccession"
    },
    /**
     * Name of the New Lot
     */
    newNameLot: {
      init: null,
      event: "changeNewNameLot"
    },
    /**
     * Date of introduction of the new Lot
     */
    date: {
      init: null,
      event: "changeDate"
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.HBox();
    layout.setSpacing(15);
    this._setLayout(layout);


    //Implémentation des widget visible à l'écran
    var containerSaisie = new qx.ui.container.Composite(new qx.ui.layout.VBox().set({
      spacing: 5
    }));
    this._add(containerSaisie);

    var containerAffichage = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this._add(containerAffichage);

    //Implémentation des widget visible à l'écran

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var espece = new qx.ui.container.Composite(grid);
    containerSaisie._add(espece);

    var lblesp = new qx.ui.basic.Label(this.tr("Espèce de la plante modèle :"));
    espece._add(lblesp, {
      row: 0,
      column: 0
    });
    this.__espece = new qx.ui.form.SelectBox();
    espece.add(this.__espece, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 1);
    grid.setColumnFlex(2, 1);
    grid.setColumnFlex(3, 1);
    var newVarName = new qx.ui.container.Composite(grid);
    containerSaisie._add(newVarName);

    var lblVar = new qx.ui.basic.Label(this.tr("Nouvelle Variété : "));
    newVarName._add(lblVar, {
      row: 0,
      column: 0
    });
    this.__typenom = new qx.ui.form.SelectBox();
    newVarName.add(this.__typenom, {
      row: 0,
      column: 1
    });
    this.__newVariete = new qx.ui.form.TextField();
    this.__newVariete.setPlaceholder(this.tr("Nom de la Variété"));
    newVarName._add(this.__newVariete, {
      row: 0,
      column: 2
    });
    var buttonAdd = new qx.ui.form.Button("Add");
    newVarName.add(buttonAdd, {
      row: 0,
      column: 3
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var gentotype = new qx.ui.container.Composite(grid);
    containerSaisie._add(gentotype);

    var lblGenotype = new qx.ui.basic.Label(this.tr("Genotype : "));
    gentotype._add(lblGenotype, {
      row: 0,
      column: 0
    });
    this.__newGenotype = new qx.ui.form.TextField();
    gentotype._add(this.__newGenotype, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var fondGenetique = new qx.ui.container.Composite(grid);
    containerSaisie._add(fondGenetique);

    var lblFondGenetique = new qx.ui.basic.Label(this.tr("Fond Genetique : "));
    fondGenetique._add(lblFondGenetique, {
      row: 0,
      column: 0
    });
    this.__fondGenetique = new qx.ui.form.TextField();
    fondGenetique._add(this.__fondGenetique, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var geneMute = new qx.ui.container.Composite(grid);
    containerSaisie._add(geneMute);

    var lblGeneMute = new qx.ui.basic.Label(this.tr("Gene Muté : "));
    geneMute._add(lblGeneMute, {
      row: 0,
      column: 0
    });
    this.__geneMute = new qx.ui.form.TextField();
    geneMute._add(this.__geneMute, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var agentMutagene = new qx.ui.container.Composite(grid);
    containerSaisie._add(agentMutagene);

    var lblAgentMutagene = new qx.ui.basic.Label(this.tr("Agent Mutagène : "));
    agentMutagene._add(lblAgentMutagene, {
      row: 0,
      column: 0
    });
    this.__agentMutagene = new qx.ui.form.TextField();
    agentMutagene._add(this.__agentMutagene, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var resistance = new qx.ui.container.Composite(grid);
    containerSaisie._add(resistance);

    var lblResistance = new qx.ui.basic.Label(this.tr("Resistance associé à la mutation : "));
    resistance._add(lblResistance, {
      row: 0,
      column: 0
    });
    this.__resistance = new qx.ui.form.TextField();
    resistance._add(this.__resistance, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var remarqueVariete = new qx.ui.container.Composite(grid);
    containerSaisie._add(remarqueVariete);

    var lblRemVar = new qx.ui.basic.Label(this.tr("Remarque Variété"));
    remarqueVariete._add(lblRemVar, {
      row: 0,
      column: 0
    });
    this.__remarqueVariete = new qx.ui.form.TextArea();
    remarqueVariete._add(this.__remarqueVariete, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var editeur = new qx.ui.container.Composite(grid);
    containerSaisie._add(editeur);

    var lblEd = new qx.ui.basic.Label(this.tr("Editeur"));
    editeur._add(lblEd, {
      row: 0,
      column: 0
    });
    this.__editeur = new glams.ui.FiltredComboBox();
    editeur._add(this.__editeur, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var obtenteur = new qx.ui.container.Composite(grid);
    containerSaisie._add(obtenteur);

    var lblObt = new qx.ui.basic.Label(this.tr("Obtenteur"));
    obtenteur._add(lblObt, {
      row: 0,
      column: 0
    });
    this.__obtenteur = new glams.ui.FiltredComboBox();
    obtenteur._add(this.__obtenteur, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var nameNewAccession = new qx.ui.container.Composite(grid);
    containerSaisie._add(nameNewAccession);

    var lblAcc = new qx.ui.basic.Label(this.tr("Nouvelle Accession : "));
    nameNewAccession._add(lblAcc, {
      row: 0,
      column: 0
    });
    this.__newAcc = new qx.ui.form.TextField();
    this.__newAcc.setPlaceholder(this.tr("Numéro d'Accession"));
    nameNewAccession._add(this.__newAcc, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var remarqueAccession = new qx.ui.container.Composite(grid);
    containerSaisie._add(remarqueAccession);

    var lblRem = new qx.ui.basic.Label(this.tr("Remarque Accession"));
    remarqueAccession._add(lblRem, {
      row: 0,
      column: 0
    });
    this.__remarqueAccession = new qx.ui.form.TextArea();
    remarqueAccession._add(this.__remarqueAccession, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var nameNewLot = new qx.ui.container.Composite(grid);
    containerSaisie._add(nameNewLot);

    var lblLot = new qx.ui.basic.Label(this.tr("Numéro de Lot"));
    nameNewLot._add(lblLot, {
      row: 0,
      column: 0
    });
    this.__nameNewLot = new qx.ui.form.TextField();
    this.__nameNewLot.setPlaceholder(this.tr("Numéro de Lot"));
    nameNewLot._add(this.__nameNewLot, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var dateNewVariete = new qx.ui.container.Composite(grid);
    containerSaisie._add(dateNewVariete);

    var lblDate = new qx.ui.basic.Label(this.tr("Date d'introduction"));
    dateNewVariete._add(lblDate, {
      row: 0,
      column: 0
    });
    this.__dateLot = new qx.ui.form.DateField();
    this.__dateLot.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__dateLot.setValue(new Date());
    dateNewVariete._add(this.__dateLot, {
      row: 0,
      column: 1
    });

    this.__resetButton = new qx.ui.form.Button("Reset");
    var i = 0
    this.__tabNomVar = [];

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.addListener("changeGenre", function(e) {
      var loadData = e.getData();
      var tab = {}
      tab['genre'] = loadData;
      this.__services.QueryGetAllEspeceByGenre("getAllEspeceByGenre", this.__sessionId, tab);
    }, this);

    //Chargement de toutes les especes de la plante modèle

    this.__services.addListener("getAllEspeceByGenre", function(e) {
      this.__espece.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        this.itemData = new qx.ui.form.ListItem(loadData[i]['espece']);
        this.__espece.add(this.itemData);
      };
    }, this);

    //Chargement de tous les types de nom pour une variété
    this.__services.QueryGetAllTypesNomVariete("getAllTypesNomVariete", this.__sessionId, null);
    this.__services.addListener("getAllTypesNomVariete", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        this.itemData = new qx.ui.form.ListItem(loadData[i].valeur);
        this.__typenom.add(this.itemData);
      };
    }, this);

    //Charge tous les pépiniériste pour définir l'éditeur et l'obtenteur de la variété à créer
    this.__editeur.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      var tab = {};
      tab['filtredText'] = e.getData();
      this.__services.QueryGetAllPepinieriste("getAllPepinieriste", this.__sessionId, tab);
    }, this);

    this.__services.addListener("getAllPepinieriste", function(e) {
      this.__listEditeur = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listEditeur.push({
          'label': loadData[i]['nom'],
          'id': loadData[i]['id_pepinieriste']
        });
      };
      this.__editeur.setListModel(this.__listEditeur);
    }, this);

    this.__obtenteur.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      var tab = {};
      tab['filtredText'] = e.getData();
      this.__services.QueryGetAllPepinieristeObtenteur("getAllPepinieristeObtenteur", this.__sessionId, tab);
    }, this);

    this.__services.addListener("getAllPepinieristeObtenteur", function(e) {
      this.__listObtenteur = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listObtenteur.push({
          'label': loadData[i]['nom'],
          'id': loadData[i]['id_pepinieriste']
        });
      };
      this.__obtenteur.setListModel(this.__listObtenteur);
    }, this);

    //Listener sur l'execution du boutton Add qui affiche le type de nom choisi et le nom entré par l'utilisateur ainsi qu'un boutton reset
    buttonAdd.addListener("execute", function() {
      if (this.__newVariete.getValue() != null && this.__newVariete.getValue().length > 0) {
        i = i + 1;
        newVarName._add(new qx.ui.basic.Label(this.__typenom.getSelection()[0].getLabel()), {
          row: 0 + i,
          column: 1
        });
        newVarName._add(new qx.ui.basic.Label(this.__newVariete.getValue()), {
          row: 0 + i,
          column: 2
        });
        newVarName._add(this.__resetButton, {
          row: 0 + i,
          column: 3
        });
        newVarName._add(lblVar, {
          row: 1 + i,
          column: 0
        });
        newVarName.add(this.__typenom, {
          row: 1 + i,
          column: 1
        });
        newVarName._add(this.__newVariete, {
          row: 1 + i,
          column: 2
        });
        newVarName.add(buttonAdd, {
          row: 1 + i,
          column: 3
        });
        //stock le type de nom et le nom saisi dans un tableau pour pouvoir garder en mémoir tous les noms saisi
        var nomVar = {};
        nomVar['type'] = this.__typenom.getSelection()[0].getLabel()
        nomVar['nom'] = this.__newVariete.getValue();
        this.__tabNomVar.push(nomVar);
        this.setVariete(this.__tabNomVar);
        this.__newVariete.setValue(null);
      };
    }, this);

    //Listerner sur l'execution du boutton reset qui permet de la suppression de ce qui a été saisi comme nom de variété
    this.__resetButton.addListener("execute", function() {
      i = 0
      newVarName._removeAll();
      newVarName._add(lblVar, {
        row: 0,
        column: 0
      });
      newVarName.add(this.__typenom, {
        row: 0,
        column: 1
      });
      newVarName._add(this.__newVariete, {
        row: 0,
        column: 2
      });
      newVarName.add(buttonAdd, {
        row: 0,
        column: 3
      });
      this.tabNomVar = [];
      this.__newVariete.setValue(null);
    }, this);

    this.__espece.addListener("changeSelection", function(e) {
      var loadData = e.getData();
      this.setEspece(loadData[0].getLabel());
    }, this);

    this.__newGenotype.addListener("changeValue", function(e) {
      var nomVar = {};
      nomVar['type'] = 'génotype'
      nomVar['nom'] = this.__newGenotype.getValue();
      this.__tabNomVar.push(nomVar);
      this.setVariete(this.__tabNomVar);
    }, this);

    this.__fondGenetique.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setFondGenetique(loadData);
    }, this);

    this.__geneMute.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setGeneMute(loadData);
    }, this);

    this.__agentMutagene.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setAgentMutagene(loadData);
    }, this);

    this.__resistance.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setResistance(loadData);
    }, this);

    this.__editeur.addListener("changeAllValues", function(e) {
      this.setEditeur(this.__editeur.getAllValues().getId());
    }, this);

    this.__obtenteur.addListener("changeAllValues", function() {
      this.setObtenteur(this.__obtenteur.getAllValues().getId());
    }, this);

    this.__remarqueVariete.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setRemarqueVariete(loadData);
    }, this);

    this.__newAcc.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNameAccession(loadData);
    }, this);

    this.__remarqueAccession.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setRemarqueAccession(loadData);
    }, this);

    this.__nameNewLot.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNewNameLot(loadData);
    }, this);

    this.__dateLot.addListener("changeValue", function() {
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      var date = format.format(this.__dateLot.getValue());
      this.setDate(date);
    }, this);

  },

  members: {

  }
});