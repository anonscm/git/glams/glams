/**
 * This is the widget to view basic information of one container
 * Auteur  Fabrice Dupuis
 * inspiré de AddContainerNotationWindows
 * avril 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.AddActionNotationWindow", {
  extend: qx.ui.window.Window,

  events: {
    /**
     * Fire pour l'ajout de notation sur une action
     */
    "addNotationAction": "qx.event.type.Data"
  },

  properties: {},

  members: {
    __type: null,
    __value: null,
    __date: null,
    __remarque: null,
    __notateur: null,
    __errorLabel: null,
    /**
     * update notations
     */
    updateData: function() {
      var services = glams.services.Sample.getInstance();
      var userGroup = JSON.parse(localStorage.getItem('readGroups'));
      services.QueryGetAllTypeNotation("getAllTypeNotation", null, "sample : action informations", userGroup[0]);
      services.addListenerOnce("getAllTypeNotation", function(e) {
        var loadData = e.getData();
        var rawData = [];
        for (var i in loadData) {
          var tempItem = new qx.ui.form.ListItem(loadData[i].nom, null, loadData[i].type).set({
            rich: true
          });
          rawData.push({
            label: loadData[i].nom,
            type: loadData[i].type
          });
        }
        var model = qx.data.marshal.Json.createModel(rawData);
        this.__type.setModel(model);
        this.__type.setLabelPath("label");
      }, this);



    }
  },

  construct: function() {
    this.base(arguments, this.tr("Ajout de notation sur une action"));
    this.setModal(true);
    this.setMovable(false);
    this.setAllowGrowX(false);
    this.setAllowGrowY(false);
    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    var layout = new qx.ui.layout.VBox();
    this.setLayout(layout);
    this.setShowMinimize(false);
    this.setShowMaximize(false);

    var valueType = new qx.ui.basic.Label(this.tr("Selection du type : "));
    this.__type = new qx.ui.list.List().set({
      width: 300
    });
    this.__type.setFocusable(false);
    var valueLabel = new qx.ui.basic.Label(this.tr("Valeur : "));
    this.__value = new qx.ui.form.TextField();
    var dateLabel = new qx.ui.basic.Label(this.tr("Date de notation : "));
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    var notateurLabel = new qx.ui.basic.Label(this.tr("Notateurs : "));
    //this.__notateur = new qx.ui.form.TextField();
    this.__notateur = new qxelvis.ui.users.ContributorsSelector();
    var remarqueLabel = new qx.ui.basic.Label(this.tr("Remarque : "));
    this.__remarque = new qx.ui.form.TextField();
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    var buttonOK = new qx.ui.form.Button(this.tr("Enregistrer"));
    var buttonCancel = new qx.ui.form.Button(this.tr("Annuler"));
    widgetButton._add(buttonOK);
    widgetButton._add(buttonCancel);

    this.add(valueType);
    this.add(this.__type);
    this.add(valueLabel);
    this.add(this.__value);
    this.add(dateLabel);
    this.add(this.__date);
    this.add(notateurLabel);
    this.add(this.__notateur);
    this.add(remarqueLabel);
    this.add(this.__remarque);
    this.add(widgetButton);

    this.__errorLabel = new qx.ui.basic.Label();
    this.add(this.__errorLabel);
    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    var services = glams.services.Sample.getInstance();
    services.QueryAllUser("loadedAllUser", null, null);

    services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__notateur.setUsers(loadData);
    }, this);


    buttonCancel.addListener("execute", function() {
      //this.setModal(false);
      this.close();
    }, this);

    buttonOK.addListener("execute", function() {
      var data = {};
      data['type'] = this.__type.getSelection().getItem(0).getLabel();
      data['unity'] = this.__type.getSelection().getItem(0).getType();
      var an = this.__date.getValue().getYear() + 1900;
      var mois = this.__date.getValue().getMonth() + 1;
      var jour = this.__date.getValue().getDate();
      data['date'] = jour.toString() + "/" + mois.toString() + "/" + an.toString();
      //data['notateur'] = this.__notateur.getValue();
      data['notateur'] = this.__notateur.getContributors();
      data['remarque'] = this.__remarque.getValue();
      data['idNotation'] = null;

      //console.log(data);
      switch (data['unity']) {
        case "integer":
          if (typeof(parseInt(this.__value.getValue())) === "number" && Number.isNaN(parseInt(this.__value.getValue())) == false) {
            data['value'] = parseInt(this.__value.getValue());
            //console.log(data);
            this.__errorLabel.setValue("");
            //console.log(this.toString());
            this.fireDataEvent("addNotationAction", data);
            this.close();
          } else {
            this.__errorLabel.setValue(this.tr("Valeur doit être de type : ") + data['unity']);
          }
          break
        case "texte":
          if (typeof this.__value.getValue() === "string") {
            //console.log(this.__value.getValue());
            data['value'] = this.__value.getValue();
            //console.log(data);
            this.fireDataEvent("addNotationAction", data);
            this.close();
            this.__errorLabel.setValue("");
          } else {
            this.__errorLabel.setValue(this.tr("Valeur doit être de type : ") + data['unity']);
          }
          break
        case "date":
          // TODO mettre en place la gestion de date
          break
      }

      //      if(this.__value.getValue()){
      //        data['value'] = this.__value.getValue();
      //        this.fireDataEvent("addNotationSample", data);
      //        this.close();
      //      }
    }, this);
  }
});