/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Hervé Andres, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS
    * Fabrice Dupuis, bioinfo team, IRHS

************************************************************************ */

/**
 * Widget to add notations based on the rating environment.
 * Authors : Hervé Andres, David Aurégan, Fabrice Dupuis,
 * May 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.AddSampleNotationWindow", {
  extend: qx.ui.window.Window,

  events: {
    /**
     * Fire to adding notation to a container
     */
    "addNotationSample": "qx.event.type.Data"
  },

  properties: {
    /**
     * Rating context
     */
    contexteNotation: {
      init: null,
      event: "changeContext"
    },
    /**
     * reporting officer
     */
    notateur: {
      init: null,
      event: "changeNotateur"
    }
  },

  members: {
    __type: null,
    __value: null,
    __date: null,
    __remarque: null,
    __notateur: null,
    __errorLabel: null

  },

  construct: function() {
    this.base(arguments);
    /*
        *****************************************************************************
         INIT
        *****************************************************************************
        */

    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grow();
    //    layout.setSpacing(5);
    this.setLayout(layout);

    var container = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this.add(container);
    this.__idProtocole = null;

    container._add(new qx.ui.basic.Label(
      this.tr("Selection du type : ")), {
      row: 0,
      column: 0
    });

    this.__type = new qx.ui.list.List().set({
      width: 300
    });
    this.__type.setFocusable(false);
    container._add(this.__type, {
      row: 0,
      column: 1
    });

    container._add(new qx.ui.basic.Label(
      this.tr("Valeur : ")), {
      row: 1,
      column: 0
    });

    this.__value = new qx.ui.form.TextField();
    container._add(this.__value, {
      row: 1,
      column: 1
    });

    container._add(new qx.ui.basic.Label(
      this.tr("Date de notation: ")), {
      row: 2,
      column: 0
    });

    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    container._add(this.__date, {
      row: 2,
      column: 1
    });

    container._add(new qx.ui.basic.Label(
      this.tr("Notateur: ")), {
      row: 3,
      column: 0
    });

    this.__notateur = new qxelvis.ui.users.ContributorsSelector();
    container._add(this.__notateur, {
      row: 3,
      column: 1
    });

    container._add(new qx.ui.basic.Label(
      this.tr("Remarque: ")), {
      row: 4,
      column: 0
    });

    this.__remarque = new qx.ui.form.TextField();
    container._add(this.__remarque, {
      row: 4,
      column: 1
    });

    container._add(new qx.ui.basic.Label(
      this.tr("Protocole de notation : ")), {
      row: 5,
      column: 0
    });

    this.__protocole = new glams.ui.FiltredComboBox()
    this.__protocole.setPlaceholder("MO-042")
    container._add(this.__protocole, {
      row: 5,
      column: 1
    });

    var ajoutProtocole = new qx.ui.form.Button(this.tr("Nouveau protocole"));
    container._add(ajoutProtocole, {
      row: 5,
      column: 2
    });

    var affichageProtocole = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    affichageProtocole.setWidth(5);
    container._add(affichageProtocole, {
      row: 6,
      column: 0,
      colSpan: 3
    });

    var buttonOK = new qx.ui.form.Button(this.tr("Enregistrer"));
    container._add(buttonOK, {
      row: 7,
      column: 0
    });

    var buttonCancel = new qx.ui.form.Button(this.tr("Annuler"));
    container._add(buttonCancel, {
      row: 7,
      column: 2
    });

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryAllUser("loadedAllUser", null, null);
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.addListener("changeNotateur", function() {
      var loadData = this.getNotateur();
      // this.__notateur.setContributors(loadData);

    }, this);

    this.addListener("changeContext", function() {
      var loadData = this.getContexteNotation();
      var contexte_notation = loadData;
      var tab = {};
      tab['nomContexte'] = contexte_notation;
      tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
      this.__services.QueryGetAllTypeNotationByContexte("getAllTypeNotationByContexte", this.__sessionId, tab);
    }, this);

    this.__services.addListener("getAllTypeNotationByContexte", function(e) {
      var loadData = e.getData();
      var rawData = [];
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].nom, null, loadData[i].type).set({
          rich: true
        });
        rawData.push({
          label: loadData[i].nom,
          type: loadData[i].type
        });
      }
      var model = qx.data.marshal.Json.createModel(rawData);
      this.__type.setModel(model);
      this.__type.setLabelPath("label");
    }, this);

    this.__services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__notateur.setUsers(loadData);
    }, this);

    // Fonctions qui chargent les protocole en fonction des carractères saisi dans la barre de texte
    this.__protocole.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 1) {
        var tab = {};
        tab['filtredName'] = e.getData();
        this.__services.QueryGetAllProtocolFiltred("getAllProtocolFiltred", this.__sessionId, tab);
      };
    }, this);
    this.__services.addListener("getAllProtocolFiltred", function(e) {
      this.__listProtocol = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listProtocol.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id'],
          'adress': loadData[i]['adress'],
          'summary': loadData[i]['summary'],
          'refLab': loadData[i]['reference_laboratory_handbook'],
          'refAl': loadData[i]['reference_alfresco']
        });
      };
      this.__protocole.setListModel(this.__listProtocol);
    }, this);

    //Ouverture de la fenêtre de création d'un protocole
    ajoutProtocole.addListener("execute", function() {
      var newContainer = new glams.ui.CreateProtocoleCulture();
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container.add(newContainer);
      var window = new qx.ui.window.Window("New Protocol");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(container);
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      //Fonction de fermeture de la fenetre
      newContainer.addListener("fermetureCreateProtocol", function() {
        window.close();
      }, this);
    }, this);

    //Fonction qui permet l'affichage des informations relatives au protocole selectionné
    this.__protocole.addListener("changeAllValues", function(e) {
      affichageProtocole.removeAll();
      var displayProtocole = new glams.ui.AffichageProtocole();
      displayProtocole.setProtocole(this.__protocole.getAllValues());
      affichageProtocole.add(displayProtocole);
    }, this);


    buttonCancel.addListener("execute", function() {
      this.setModal(false);
      this.close();
    }, this);

    buttonOK.addListener("execute", function() {
      var data = {};
      data['type'] = this.__type.getSelection().getItem(0).getLabel();
      data['unity'] = this.__type.getSelection().getItem(0).getType();
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      data['date'] = format.format(this.__date.getValue());
      data['notateur'] = this.__notateur.getContributors();
      data['remarque'] = this.__remarque.getValue();
      data['protocole'] = this.__idProtocole;
      data['idNotation'] = null;

      if (this.__value.getValue()) {
        data['value'] = this.__value.getValue();
        this.fireDataEvent("addNotationSample", data);
        this.close();
      }
    }, this);
  }
});