/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Edit protocol.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.EditProtocol", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Close the window
     */
    "fermetureEditProtocol": "qx.event.type.Event",
    /**
     * Returns the input data to NewCreateCulture
     */
    "updateProt": "qx.event.type.Data"
  },

  properties: {
    /**
     * Data on the protocol to edit
     */
    protocole: {
      init: null,
      event: "changeProtocole"
    }
  },

  members: {


  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(15);
    this._setLayout(layout);

    var container = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    //Implémentation des widget visible à l'écran
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du protocole")), {
      row: 0,
      column: 0
    });
    this.__nomProtocole = new qx.ui.form.TextField();
    this.__nomProtocole.setRequired(true);
    this._add(this.__nomProtocole, {
      row: 0,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence cahier de laboratoire")), {
      row: 1,
      column: 0
    });
    this.__refLabbook = new qx.ui.form.TextField();
    this._add(this.__refLabbook, {
      row: 1,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Résumé : ")), {
      row: 2,
      column: 0
    });
    this.__summary = new qx.ui.form.TextArea();
    this.__summary.setRequired(true);
    this._add(this.__summary, {
      row: 2,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Adresse web vers protocole Alfresco")), {
      row: 3,
      column: 0
    });
    this.__adresseWeb = new qx.ui.form.TextField();
    this._add(this.__adresseWeb, {
      row: 3,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Reference Alfresco")), {
      row: 4,
      column: 0
    });
    this.__nomAlfresco = new qx.ui.form.TextField();
    this._add(this.__nomAlfresco, {
      row: 4,
      column: 1
    });
    this.__btnSave = new qx.ui.form.Button(this.tr("Sauvegarder le protocole"));
    this._add(this.__btnSave, {
      row: 5,
      column: 0,
      colSpan: 2
    });
    //Initialisation des Validator pour rendre obligatoire la saisi du nom, du résumé et de la référence du Protocole
    var validationNom = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer un Nom du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );
    var validationSummary = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer un Résumé du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );
    var validationRef = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer une référence du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var manager = new qx.ui.form.validation.Manager();
    manager.add(this.__nomProtocole, validationNom);
    manager.add(this.__summary, validationSummary);
    manager.add(this.__refLabbook, validationRef);


    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */


    //Charge les données sur le protocole à modifier
    this.addListener("changeProtocole", function() {
      var loadData = this.getProtocole();
      this.data = loadData;
      this.__nomProtocole.setValue(this.data.getLabel());
      this.__refLabbook.setValue(this.data.getRefLab());
      this.__summary.setValue(this.data.getSummary());
      if (this.data.getAdress() != null) {
        this.__adresseWeb.setValue(this.data.getAdress());
      };
      if (this.data.getRefAl() != null) {
        this.__nomAlfresco.setValue(this.data.getRefAl());
      };
    }, this);

    //Lance la vérification de saisi des champs obligatoire
    this.__btnSave.addListener("execute", function() {
      manager.validate();
    }, this);

    //Sauvegarde les informations saisies
    manager.addListener("complete", function(e) {
      if (manager.getValid()) {
        var tab = {};
        tab['id'] = this.data.getId();
        tab['name'] = this.__nomProtocole.getValue();
        tab['adress'] = this.__adresseWeb.getValue();
        tab['summary'] = this.__summary.getValue();
        tab['referenceLaboratoryHandbook'] = this.__refLabbook.getValue();
        tab['referenceAlfresco'] = this.__nomAlfresco.getValue();
        // Modifie le Protocole dans la base de donnée
        this.__services.QueryUpdateProtocol("updateProtocol", this.__sessionId, tab);
        //Déclenche l'evenement updateProt de la classe NewCreateCulture
        this.fireDataEvent("updateProt", tab);
        // Déclenchement de la fonction de fermeture de la fenêtre
        this.fireEvent("fermetureEditProtocol");
      };
    }, this);

  }

})