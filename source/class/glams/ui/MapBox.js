/**
 * Formulaire de plan de Box
 * fonctionnement :
 * dessine le plan de boite  drowMap
 *
 *
 * Fabrice Dupuis novembre 2017
 *
 * @asset(glams/*)
 */
 qx.Class.define("glams.ui.MapBox",{
   extend : qx.ui.core.Widget,
	
  events : {
    /**
     * Fired quand le dessin est réalisé
     */
    "drowed" : "qx.event.type.Event",
    /**
     * Fired quand les informations de la boite sont modifiées (data= idBox)
     */
    "updateMapBox" : "qx.event.type.Data",
    /**
     * Fired quand les informations d'un container de la boite sont modifiées (data= un container)
     */
    "updateOnecontainer" : "qx.event.type.Data",    
    /**
     * Fired quand les modifications peuvent etre sauvegardées(data= idBox)
     */
    "saveBox" : "qx.event.type.Data",
   /**
     * Fired pour reset les modifications(data= idBox)
     */
    "reInitialiseMapBox" : "qx.event.type.Data",    
    /**
     * Fired quand uncontainer bouge par drag
     */
    "containerPutOff" : "qx.event.type.Data",    
    /**
     * Fired quand uncontainer bouge par drop
     */
    "containerPutOn" : "qx.event.type.Data",
   /**
     * Fired quand on doubleclick sur une place
     */
    "dbleClckPlace" : "qx.event.type.Data"         
  },
	
  properties : {
    /**
    * id of the web session
    */
    __sessionId : {
	    init : null
    },
    
    /**
    *name of the box
    */
    boxName : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeBoxName"
    },    
    /**
    *id of the box
    */
    idBox : {      
      init : null,
      nullable : false,
      check : "Integer",
      event : "changeIdBox"
    },
    /**
    *number of rows of the box
    */
    rowNumber : {      
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeRowNumber"
    },    
    /**
    *number of columns of the box
    */
    columnNumber : {      
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeColumnNumber"
    }, 
   /**
    * list of place and type of place
    */
    place :{      
      init : null,
      nullable : true,
      check : "Array",
      event : "changePlace"
    }   
    
          	  	  
  },
	
  members : {
    
    __rowLabel : null,
    __columnLabel : null,
    __dateOn : null,
    __dateOff : null,


    
    /**
    * dessine le plan de la boite
    */
    __drowMap : function(){
      var layoutGrid=new qx.ui.layout.Grid()
      this.__plan._setLayout(layoutGrid);
      var listePlace = this.getPlace();//vecteur des emplacements
      var p=0;
      //1 -- boites ordonnées
      //*********************
      if (this.getRowNumber()) {
      //parcours tous les emplacements
      for (var l=0; l<this.getRowNumber(); l++) {
        for (var c=0; c<this.getColumnNumber(); c++){          
           layoutGrid.setColumnFlex(c+1,1);
           layoutGrid.setRowFlex(c+1,1);
          //creation du widget Place
          var place = new glams.ui.Place();
          var border = new qx.ui.decoration.Decorator().set({
            width: 1,
            style: "solid",
            color: "black"
          });        
 
          place.setDecorator(border);          
          place._setLayout(new qx.ui.layout.Grow());
          place.setMinHeight(25);
          place.setMinWidth(25);    
          place.setRow(l+1);
          place.setColumn(c+1);
          place.setPosition(p);
          place.setIdBox(this.getIdBox());
          place.setTube(listePlace[p]);

          //click droit sur place          
          var that=this;
          place.addListener("contextmenu", function(e){ 
                      that.__infoMessageWindow.moveTo(600,10);
                      that.__infoContainer.setContainer(this.getTube());
                      that.__infoMessageWindow.open();       
          },place);
            
          //move by drag and drop renvoie les idBox à actualiser l'affichage
          place.addListener("containerPutOn", function(e){
            that.fireDataEvent("containerPutOn", e.getData());
          },place);
          place.addListener("containerPutOff", function(e){
            that.fireDataEvent("containerPutOff", e.getData());
          },place);          
          //ajout de la place sur le plan        
          this.__plan._add(place, {row : l+1, column :c+1});         
          p++;
          //doubleclick sur la place renvoie les informations
          place.addListener("dbleClckPlace", function(e){ 
          this.fireDataEvent("dbleClckPlace",e.getData());
          },this);
        };//fin du parcours de tous les emplacements
      };
      //ajout des identifiants de ligne et colonnes
      for (var l=0; l<this.getRowNumber(); l++) {
        var listeChar=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        this.__plan._add(new qx.ui.basic.Label(listeChar[l]), {row : l+1, column :0});                
      };  
      for (var c=0; c<this.getColumnNumber(); c++){  
        var lab = c+1
        this.__plan._add(new qx.ui.basic.Label(lab.toString() ), {row : 0, column :c+1});          
      };
      
      }else{ 
        //2 -- Boites vrac
        //****************
        //parcours tous les emplacements
        for ( var v=0; v < listePlace.length ; v++){
          //creation du widget Place
          var place = new glams.ui.Place();
          place._setLayout(new qx.ui.layout.Grow());
          place.setHeight(25);
          place.setWidth(25);
          place.setRow(null);
          place.setColumn(null);
          place.setPosition(v);
          place.setTube(listePlace[v]);
          place.setIdBox(this.getIdBox());  
          
          //click droit sur place
          var that=this;
          place.addListener("contextmenu", function(e){ 
                      that.__infoMessageWindow.moveTo(600,10);
                      that.__infoContainer.setContainer(this.getTube());
                      that.__infoMessageWindow.open();         
          },place);       
          //move by drag and drop renvoie les idBox à actualiser l'affichage
          place.addListener("containerPutOn", function(e){
            that.fireDataEvent("containerPutOn", e.getData());
          },place);
          place.addListener("containerPutOff", function(e){
            that.fireDataEvent("containerPutOff", e.getData());
          },place);  
          //ajout de la place sur le plan
          //reset les plan avant la première place
          if (v==0){this.__plan.removeAll()};  
          this.__plan._add(place, {row : v/10 - v/10%1, column :v%10});       
        
        
        };  //fin de for v in listePlace 
        //ajout de 10 places vides
        for ( var v2=listePlace.length ; v2 < listePlace.length +10; v2++){
          //creation du widget Place
          var place = new glams.ui.Place();        
          place._setLayout(new qx.ui.layout.Grow());
          place.setHeight(25);
          place.setWidth(25);
          place.setRow(null);
          place.setColumn(null);
          place.setPosition(v2); 
          place.setIdBox(this.getIdBox());    
                
          //click droit sur place
          var that=this;
          place.addListener("contextmenu", function(e){ 
                      that.__infoMessageWindow.moveTo(600,10);
                      that.__infoContainer.setContainer(this.getTube());
                      that.__infoMessageWindow.open();         
          },place); 
          //move by drag and drop renvoie les idBox à actualiser l'affichage
          place.addListener("containerPutOn", function(e){
            that.fireDataEvent("containerPutOn", e.getData());
          },place);
          place.addListener("containerPutOff", function(e){
            that.fireDataEvent("containerPutOff", e.getData());
          },place);   
               
          //ajout de la place sur le plan                      
          this.__plan._add(place, {row : v2/10 - v2/10%1, column :v2%10});       
        };  //fin de ajout places libres                    
      }; //fin boite vrac               
    },  //fin de __drowMap 
        
    /**
    * update efface et re-compose un plan
    */
    update: function(){     
      this.__plan.removeAll();        
      this.__drowMap() ; 
    } 
  },
	
  construct : function(){
    this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    var layout = new qx.ui.layout.Grow();
    //layout.setSpacing(10);
    this._setLayout(layout);
    //ajout d'un container composite 
    this.__map = new qx.ui.core.Widget();
    this.__map._setLayout(new qx.ui.layout.VBox());
    this._add(this.__map);   
    //ajout d'un plan
    this.__plan= new qx.ui.container.Composite();
    this.__plan.setAllowStretchX(true);
    this.__plan.setAllowStretchY(true);
    this.__map._add(this.__plan,{flex:1});
    
    //ajout des boutons
    this.__choiceButton =new qx.ui.container.Composite(); 
    this.__choiceButton._setLayout(new qx.ui.layout.HBox());
    this.__resetButton = new qx.ui.form.Button("Reset");
    this.__saveButton = new qx.ui.form.Button("Save");
    this.__choiceButton._add(this.__resetButton); 
    this.__choiceButton._add(this.__saveButton);
    this.__map._add(this.__choiceButton);
    
    //fenêtre d'infos sur un container (activé par click droit)
    this.__infoMessageWindow = new qx.ui.window.Window("informations sur l'échantillon");
    this.__infoMessageWindow.setLayout(new qx.ui.layout.VBox());  
    this.__infoContainer = new glams.ui.InfoContainer();
    this.__infoContainer.setAllowStretchX(true,true);
    this.__infoMessageWindow.add(this.__infoContainer,{flex:1});
    
   /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
//    this.__services = glams.services.Sample.getInstance();    
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */ 
    
    //recupère toutes les infos des contenants dans la boite et dessine la boite
    this.addListener ("changePlace", function(e){  
      this.__drowMap(); 
      this.fireEvent ("drowed");  
    },this);
    //ferme la fenêtre informations sur échantillons
    this.__infoContainer.addListener("updateContainer", function(e){  
      var loadData=e.getData();
      //modification des données de un container dans dataBox
      this.fireDataEvent("updateOnecontainer", loadData);
      this.__infoMessageWindow.close();
    },this);    
    //savegarde de la boite
    this.__saveButton.addListener ("execute", function(e){  
      this.fireDataEvent("saveBox",this.getIdBox());
    },this);
    //reset des modifications de la boite
    this.__resetButton.addListener ("execute", function(e){  
      this.fireDataEvent("reInitialiseMapBox",this.getIdBox());
    },this);    
    
	}
});
