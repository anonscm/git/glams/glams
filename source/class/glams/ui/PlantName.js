/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for reading a list of Name platn variety.
 * Authors : Lysiane Hauguel,
 * November 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.PlantName", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * get information about the prelevement
     */
    "loadedSampleSelected": "qx.event.type.Data",
    /**
     * fire when a character is input
     */
    "changeSelectedString": "qx.event.type.Data"
  },

  properties: {

  },

  construct: function() {

		this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(10);
    this._setLayout(layout);

    //positionnement des labels
    this._add(new qx.ui.basic.Label(
      this.tr("Name plant : ")), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Filter Name: ")), {
      row: 0,
      column: 2
    });

    //initialisation des widgets
    this.__namePlantType = new glams.ui.FiltredComboBox();
    this.__namePlantType.setWidth(200);

    this.__all = new qx.ui.form.RadioButton("All");
    this.__espece = new qx.ui.form.RadioButton("espèce");
    this.__variete = new qx.ui.form.RadioButton("variété");
    this.__nom = new qx.ui.form.RadioButton("Nom");
    this.__radioGroup = new qx.ui.form.RadioGroup(this.__all, this.__espece, this.__variete, this.__nom);

    this.__radioGroup.setAllowEmptySelection(true);
    //Positionnement des widgets
    this._add(this.__namePlantType, {
      row: 0,
      column: 1
    });
    this._add(this.__all, {
      row: 0,
      column: 3
    });
    this._add(this.__espece, {
      row: 1,
      column: 3
    });
    this._add(this.__variete, {
      row: 2,
      column: 3
    });
    this._add(this.__nom, {
      row: 3,
      column: 3
    });

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();


    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */
    this.__radioGroup.addListener("changeSelection", function() {
      console.log(this.__radioGroup.getSelection());
      var valueRadio = this.__radioGroup.getSelection();
      var tab = {};
      tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
      tab['filterText'] = valueRadio;
      this.__services.QueryGetAllTypesNomVariete("getAllTypesNomVariete", this.__sessionId, tab);
    }, this);

    this.__services.addListener("getAllTypesNomVariete", function(e) {
      this.__listPlantName = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listPlantName.push({
        'label': loadData[i]['valeur']
        });
      };
      this.__namePlantType.setListModel(this.__listPlantName);
    }, this);
  },

  members: {
    /**
     * List of open boxes for control and modifications
     */
    __listeOpenBox: []

  }
});