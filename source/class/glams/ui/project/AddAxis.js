/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for save new project.
 * Authors : Lysiane Hauguel,
 * February 2019.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.project.AddAxis", {
  extend: qx.ui.core.Widget,

  events: {
    "addAxis": "qx.event.type.Event",

    "delAxis": "qx.event.type.Event"
  },

  properties: {
    axisName : {
      nullable : true,
      event : "changeAxisName"
    },

    activeAxis : {
      nullable : true,
      //check : "String",
      event : "changeActiveAxis"
    }
  },

  construct: function() {

    this.base(arguments);
    this.HBox = new qx.ui.layout.HBox(10);
    this._setLayout(this.HBox);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    var axis = new qxelvis.ui.FilteredComboBox();
    axis.setPlaceholder(this.tr("Filter Axis of research"));
    this._add(axis);
    this.btnAddAxis = new qx.ui.form.Button("+");
    this.btnAddAxis.setMaxHeight(22);
    this.btnAddAxis.setMaxWidth(25);
    this.btnDelAxis = new qx.ui.form.Button("-");
    this.btnDelAxis.setMaxHeight(22);
    this.btnDelAxis.setMaxWidth(25);

    this.addListener("changeActiveAxis", function(e) {
      var listAxis = [];
      var loadData = e.getData();
      for (var i in loadData) {
        listAxis.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      }
      axis.setListModel(listAxis);
    }, this);

    axis.bind("value", this, "axisName");
    this.bind("axisName", axis, "value");

    axis.addListener("changeSelection", function() {
      this._add(this.btnAddAxis);
    }, this);

    this.btnAddAxis.addListener("execute", function() {
      this.fireEvent("addAxis");
    }, this);

    this.btnDelAxis.addListener("execute", function() {
      this.fireEvent("delAxis");
    }, this);
  },

  members : {

    addAxis : function() {
      var fcbAxis = new glams.ui.project.AddAxis();
      this._remove(this.btnAddAxis);
      this._add(this.btnDelAxis);
      return fcbAxis;
    },

    delAxis : function() {
      this._remove(this.destroy());
    }
  }
});