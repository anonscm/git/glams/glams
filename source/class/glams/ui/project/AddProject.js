/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for save new project.
 * Authors : Lysiane Hauguel,
 * December 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.project.AddProject", {
  extend: qx.ui.core.Widget,

  events: {

    /**
     * Clear and close the page
     */
    "cancelProject": "qx.event.type.Event",

    /**
     * Clear the page
     */
    "resetProject": "qx.event.type.Event",

    /**
     * save as a draft
     */
    "saveDraft": "qx.event.type.Data",

    /**
     * load the draft
     */
    "loadDraft": "qx.event.type.Data",

    /**
    * Save data
    */
    "saveProject": "qx.event.type.Event",

    /**
    * Add axis
    */
    "addAxis": "qx.event.type.Event",

    /**
    * Delete axis
    */
    "delAxis": "qx.event.type.Event",

    /**
    * Add subproject
    */
    // "addSubProject": "qx.event.type.Event",

    /**
    * Add expérience
    */
    // "addExperience": "qx.event.type.Event",

    /**
    * Add ANAN
    */
    // "addANAN": "qx.event.type.Event",

    "changeSelectedString": "qx.event.type.Data"

  },

  properties: {
    /*
    Une propriete par donnee du formulaire
    */
    name : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeName"
    },

    projectList : {
      init : null,
      nullable : true,
      event : "changeProjectList"
    },

    title : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeTitle"
    },

    abstra : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeAbstra"
    },

     coordinator : {
       init : null,
       nullable : true,
       // check : "String",
       event : "changeCoordinator"
     },

    contributors : {
      init : null,
      nullable : true,
      //check : "String",
      event : "changeContributors"
    },

    funding : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeFunding"
    },

    axis : {
      //init : new Array(),
      //nullable : false,
      init : '',
      nullable : true,
      //check : "String",
      event : "changeAxis"
    },

    startDate : {
      init : null,
      nullable : true,
      event : "changeStartDate"
    },

    endDate : {
      init : null,
      nullable : true,
      event : "changeEndDate"
    },

    status : {
      init : "Actif-Private",
      nullable : true,
      // check : "Integer",
      event : "changeStatus"
    }
  },

  construct: function() {

    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    var sessionId = session.getSessionId();

    // var scrollContainer = new qx.ui.container.Scroll();
    // this._add(scrollContainer);

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(10);
    layout.setRowFlex(0, 1);
    layout.setRowFlex(1, 1);
    layout.setRowFlex(2, 1);
    layout.setRowFlex(3, 1);
    layout.setRowFlex(4, 1);
    layout.setRowFlex(5, 1);
    layout.setRowFlex(6, 1);
    layout.setRowFlex(7, 1);
    layout.setRowFlex(8, 1);
    layout.setRowFlex(9, 1);
    layout.setRowFlex(10, 1);
    layout.setRowAlign(10, "right", "middle");
    layout.setRowFlex(11, 1);
    layout.setRowFlex(12, 1);
    layout.setRowFlex(13, 1);
    layout.setRowFlex(14, 1);
    layout.setRowFlex(15, 1);
    layout.setColumnFlex(0, 1);
    layout.setColumnFlex(1, 1);
    layout.setColumnMinWidth(1, 200);
    layout.setColumnFlex(2, 1);
    layout.setColumnMinWidth(2, 200);
    layout.setColumnFlex(3, 1);
    layout.setColumnMinWidth(3, 200);
    this._setLayout(layout);
    // scrollContainer.add(this.layout);

    //Positionnement des labels
    this._add(new qx.ui.basic.Label().set({
      value: "Name <b style='color:red'>*</b>",
      rich : true}), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label().set({
      value: "Title <b style='color:red'>*</b>",
      rich : true}), {
      row: 1,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Abstract")), {
      row: 3,
      column: 0
    });
    this._add(new qx.ui.basic.Label().set({
      value: "Coordinator <b style='color:red'>*</b>",
      rich : true}), {
      row: 5,
      column: 0
    });
    this._add(new qx.ui.basic.Label().set({
      value: "All contributors",
      rich : true}), {
      row: 6,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Funding")), {
      row: 8,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Axis of reseach")), {
      row: 9,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Start date")), {
      row: 10,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("End date")), {
      row: 10,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Experiences")), {
      row: 12,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Project status")), {
      row: 13,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Save")), {
      row: 14,
      column: 0
    });

    // this.addListener('changeContributors', function(e) {
    //   this.debug(e.getData());
    // }, this);

    // this.addListener('changeFunding', function(e) {
    //   this.info(this.getFunding());
    // }, this);

    //Initialisation des widgets
    var fcbProjectName = new qxelvis.ui.FilteredComboBox();
    var btnDelete = new qx.ui.form.Button("","glams/corbeille.png");
    btnDelete.setMaxHeight(25);
    btnDelete.setMaxWidth(30);
    var tfTitleProject = new qx.ui.form.TextField();
    tfTitleProject.setEnabled(false);
    var taAbstractProject = new qx.ui.form.TextArea();
    taAbstractProject.setEnabled(false);
    var coord = new qxelvis.ui.users.UserSelector();
    coord.setEnabled(false);
    this.listContrib = new qxelvis.ui.users.ContributorsSelector();
    this.listContrib.setEnabled(false);
    var fcbFunding = new qxelvis.ui.FilteredComboBox();
    fcbFunding.setPlaceholder(this.tr("Filter Funding"));
    fcbFunding.setEnabled(false);
    var tfFunding = new qx.ui.form.TextField();
    tfFunding.setPlaceholder(this.tr("Enter new funding"));
    this.axisVBox = new qx.ui.container.Composite(new qx.ui.layout.VBox(10)); 
    this.fcbAxis = new glams.ui.project.AddAxis();
    this.fcbAxis.setEnabled(false);
    this.axisVBox._add(this.fcbAxis);
    // var fcbAxis = new qxelvis.ui.FilteredComboBox();
    // fcbAxis.setPlaceholder(this.tr("Filter Axis of reseach"));
    // fcbAxis.setEnabled(false);
    // this.__btnVBox = new qx.ui.core.Widget();
    // this.__btnVBox._setLayout(new qx.ui.layout.VBox(10));
    // this.__btnVBox.getAlignY("bottom");
    // var btnAddAxis = new qx.ui.form.Button("+");
    // btnAddAxis.setMaxHeight(22);
    // btnAddAxis.setMaxWidth(25);
    // btnAddAxis.setEnabled(false);
    // this.__btnVBox._add(btnAddAxis);
    var dateDebut = new qx.ui.form.DateField();
    dateDebut.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    dateDebut.setEnabled(false);
    var dateFin = new qx.ui.form.DateField();
    dateFin.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    dateFin.setEnabled(false);
    var btnAddSubProject = new qx.ui.form.Button("Add a subproject");
    btnAddSubProject.setEnabled(false);
    var btnAddExperience = new qx.ui.form.Button("Add an experience");
    btnAddExperience.setEnabled(false);
    var btnAnan = new qx.ui.form.Button("Make a request fot ANAN");
    btnAnan.setEnabled(false);
    var draft = new qx.ui.form.RadioButton("Draft");
    draft.setEnabled(false);
    var popupdraft = new qx.ui.popup.Popup(new qx.ui.layout.VBox());
    var labeldraft = new qx.ui.basic.Label().set({
      value: "Un brouillon ne peut être vu que par son auteur.",
      rich : true,
      width: 150
    });
    popupdraft.add(labeldraft);
    var privat = new qx.ui.form.RadioButton("Actif-Private");
    privat.setEnabled(false);
    var popupprivate = new qx.ui.popup.Popup(new qx.ui.layout.VBox());
    var labelprivate = new qx.ui.basic.Label().set({
      value: "Un projet privé ne peut être modifié que par les membres du\
      projet (ou sous-projet) et vu par l'ensemble des membres des équipes \
      impliquées.",
      rich : true,
      width: 150
    });
    popupprivate.add(labelprivate);
    var publique = new qx.ui.form.RadioButton("Actif-Public");
    publique.setEnabled(false);
    var popuppublic = new qx.ui.popup.Popup(new qx.ui.layout.VBox());
    var labelpublic = new qx.ui.basic.Label().set({
      value: "Un projet public est en lecture seul pour tout l'institut. \
      Il reste modifiable par les membres du projet.",
      rich : true,
      width: 150
    });
    popuppublic.add(labelpublic);
    var archive = new qx.ui.form.RadioButton("Archived");
    archive.setEnabled(false);
    var popuparchive = new qx.ui.popup.Popup(new qx.ui.layout.VBox());
    var labelarchive = new qx.ui.basic.Label().set({
      value: "Un projet archivé n'est plus visible, mais reste enregistré \
      dans la base de données de l'institut. Pour le supprimer \
      définitivement, contacter les administrateurs.",
      rich : true,
      width: 150
    });
    popuparchive.add(labelarchive);
    var btnHbox = new qx.ui.core.Widget();
    btnHbox._setLayout(new qx.ui.layout.HBox(100));
    var radioGroup = new qx.ui.form.RadioGroup(privat, draft, publique, archive);
    radioGroup.setAllowEmptySelection(false);
    radioGroup.setEnabled(false);
    btnHbox._add(draft);
    btnHbox._add(privat);
    btnHbox._add(publique);
    btnHbox._add(archive);
    var btnSave = new qx.ui.form.Button("Save");
    btnSave.setEnabled(false);
    var btnDraft = new qx.ui.form.Button("Save as draft");
    btnDraft.setEnabled(false);
    var btnAnnuler = new qx.ui.form.Button("Cancel");

    var userNameValidator = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        // use a timeout instad of a server request (async)
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Please enter a title");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var coordValidator = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        // use a timeout instad of a server request (async)
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Please enter a coordinator");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var validation = new qx.ui.form.validation.Manager();
    validation.add(fcbProjectName);
    validation.add(tfTitleProject, userNameValidator);
    validation.add(coord, coordValidator); // formItem.getValue is not a function

    //positionnement des widgets
    this._add(fcbProjectName, {
      row: 0,
      column: 1
    });
    this._add(btnDelete, {
      row: 0,
      column: 3
    });
    this._add(tfTitleProject, {
      row: 1,
      column: 1,
      colSpan: 3
    });
    this._add(taAbstractProject, {
      row: 3,
      column: 1,
      colSpan: 3
    });
    this._add(coord, {
      row: 5,
      column: 1
    });
    this._add(this.listContrib, {
      row: 6,
      column: 1,
      colSpan: 2
    });
    this._add(fcbFunding, {
      row: 8,
      column: 1
    });
    this._add(this.axisVBox, {
      row: 9,
      column: 1
    });
    // this._add(fcbAxis, {
    //   row: 9,
    //   column: 1
    // });
    // this._add(this.__btnVBox, {
    //   row: 9,
    //   column: 2
    // });
    this._add(dateDebut, {
      row: 10,
      column: 1
    });
    this._add(dateFin, {
      row: 10,
      column: 3
    });
    this._add(btnAddSubProject, {
      row: 12,
      column: 1
    });
    this._add(btnAddExperience, {
      row: 12,
      column: 2
    });
    this._add(btnAnan, {
      row: 12,
      column: 3
    });
    this._add(btnHbox, {
      row : 13,
      column : 1,
      colSpan: 3
    });
    this._add(btnSave, {
      row: 14,
      column: 1
    });
    this._add(btnDraft, {
      row: 14,
      column: 2
    });
    this._add(btnAnnuler, {
      row: 14,
      column: 3
    });

    //Bind

    fcbProjectName.bind("value", this, "name");
    this.bind("name", fcbProjectName, "value");
    fcbProjectName.bind("model", this, "projectList");
    this.bind("projectList", fcbProjectName, "model");
    tfTitleProject.bind("value", this, "title");
    this.bind("title", tfTitleProject, "value");
    taAbstractProject.bind("value", this, "abstra");
    this.bind("abstra", taAbstractProject, "value");
    coord.bind("manager", this, "coordinator");
    this.bind("coordinator", coord, "manager");
    this.listContrib.bind("contributors", this, "contributors");
    this.bind("contributors", this.listContrib, "contributors");
    this.info(this.getContributors());
    fcbFunding.bind("value", this, "funding");
    this.bind("funding", fcbFunding, "value");
    this.fcbAxis.bind("axisName", this, "axis");
    // this.bind("axis", fcbAxis, "value");
    dateDebut.bind("value", this, "startDate");
    this.bind("startDate", dateDebut, "value");
    dateFin.bind("value", this, "endDate");
    this.bind("endDate", dateFin, "value");
    // console.log(this.getProjectList());

    /*
    *******************************
    TEST ARBRE
    *******************************
    */
    // var store = qx.bom.Storage.getLocal();
    // var projectDraft = store.getItem("projectDraft");
    // var project = qx.lang.Json.stringify(projectDraft);

    // var tree = new qx.ui.tree.Tree();
    // this._add(tree, {row: 15, column: 1});
    // // create the controller
    // var controller = new qx.data.controller.Tree(null, tree, "kids", "name");

    // var status = new qx.ui.basic.Label("Loading...");

    // // create the data store
    // // create the data store
    // var url = qx.util.ResourceManager.getInstance().toUri("tree.json");
    // var store = new qx.data.store.Json(url);

    // // var url = qx.lang.Json.stringify(projectDraft);
    // // var store = new qx.data.store.Json(project);

    // // connect the store and the controller
    // store.bind("model", controller, "model");

    // // bind the status label
    // store.bind("state", status, "value");


    //liste de prems.SearchWidget
    // this.__axisWidgetList = new qx.data.Array();
    // this.__axisWidgetList.push(fcbAxis);
    // this.axisVBox._add(this.__axisWidgetList);

    

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    var services = glams.services.Sample.getInstance();
    var userGroups = session.getUserGroupsInfo().getReadGroupIds();
    var servicesE = glams.services.Experiment.getInstance();

    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */

    this.addListener("activate", function() {
      fcbProjectName.setListModel(this.getProjectList());
    }, this);

    fcbProjectName.addListener("changeValue", function(e) {
      var loadData = e.getData();
      // var tab = {};
      // tab['userId'] = session.getUserInfo().getId();
      // tab['types'] = 'project'; // TODO 
      // servicesE.QueryGetNameProject("getNameProject", sessionId, userGroups, tab);

      if (loadData != null && loadData.trim() != "") {
        tfTitleProject.setEnabled(true);
        taAbstractProject.setEnabled(true);
        coord.setEnabled(true);
        this.listContrib.setEnabled(true);
        fcbFunding.setEnabled(true);
        this.fcbAxis.setEnabled(true);
        dateDebut.setEnabled(true);
        dateFin.setEnabled(true);
        btnAddSubProject.setEnabled(true);
        btnAddExperience.setEnabled(true);
        draft.setEnabled(true);
        privat.setEnabled(true);
        publique.setEnabled(true);
        archive.setEnabled(true);
        btnSave.setEnabled(true);
        btnDraft.setEnabled(true);
      }
      else {
        tfTitleProject.setEnabled(false);
        taAbstractProject.setEnabled(false);
        coord.setEnabled(false);
        this.listContrib.setEnabled(false);
        fcbFunding.setEnabled(false);
        this.fcbAxis.setEnabled(false);
        dateDebut.setEnabled(false);
        dateFin.setEnabled(false);
        btnAddSubProject.setEnabled(false);
        btnAddExperience.setEnabled(false);
        draft.setEnabled(false);
        privat.setEnabled(false);
        publique.setEnabled(false);
        archive.setEnabled(false);
        btnSave.setEnabled(false);
        btnDraft.setEnabled(false);
      }
    }, this);

    // servicesE.addListener("getNameProject", function(e) {
    //   listProject = [];
    //   var loadData = e.getData();
    //   for (var i in loadData) {
    //     listProject.push({
    //       'label': loadData[i]['name'],
    //       'id': loadData[i]['id']
    //     });
    //   }
    //   this.setProjectList(listProject);
    //   fcbProjectName.setListModel(this.getProjectList());
    //    this.info(this.getProjectList());
    // }, this);

    this.addListener("loadDraft", function(e) {
      if (e.getData() != null) {
        tfTitleProject.setEnabled(true);
        taAbstractProject.setEnabled(true);
        coord.setEnabled(true);
        this.listContrib.setEnabled(true);
        // coord.setActiveUser(projectDraft['coordinator']);
        // listContrib.setUsers(projectDraft['contributors']);
        fcbFunding.setEnabled(true);
        this.fcbAxis.setEnabled(true);
        dateDebut.setEnabled(true);
        dateFin.setEnabled(true);
        btnAddSubProject.setEnabled(true);
        btnAddExperience.setEnabled(true);
        draft.setEnabled(true);
        privat.setEnabled(true);
        publique.setEnabled(true);
        archive.setEnabled(true);
        btnSave.setEnabled(true);
        btnDraft.setEnabled(true);
      }
    }, this);

    services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.listContrib.setUsers(loadData);
      coord.setActiveUser(loadData);
    }, this);

    this.addListener("appear", function() {
      servicesE.QueryGetFinancingTypesName("getFinancingTypesName", sessionId, userGroups);
    }, this);

    servicesE.addListener("getFinancingTypesName", function(e) {
      var listFunding = [];
      var loadData = e.getData();
      for (var i in loadData) {
        listFunding.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      }
      listFunding.push({
        'label': 'autre',
        'id': null
      })
      fcbFunding.setListModel(listFunding);
    }, this);

    fcbFunding.addListener("changeSelection", function(e) {
      var loadData = e.getData();
      if (loadData.getLabel() == 'autre'){
        this._add(tfFunding, {
          row: 8,
          column: 2
        });
      }
      else if (layout.getCellWidget(8, 2) != null) {
        this._remove(tfFunding);
      }
    }, this);

    tfFunding.addListener("changeValue", function() {
      this.setFunding(tfFunding.getValue());
    }, this);

    this.addListener("appear", function() {
      servicesE.QueryGetAxisTypesName("getAxisTypesName", sessionId, userGroups);
    }, this);

    servicesE.addListener("getAxisTypesName", function(e) {
      var loadData = e.getData();
      this.fcbAxis.setActiveAxis(loadData);
    }, this);

    this.fcbAxis.addListener("addAxis", function() {
      var newfcbAxis = this.fcbAxis.addAxis();
      this.axisVBox._add(newfcbAxis);
      servicesE.QueryGetAxisTypesName("getAxisTypesName", sessionId, userGroups);
      servicesE.addListener("getAxisTypesName", function(e) {
        var loadData = e.getData();
        newfcbAxis.setActiveAxis(loadData);
      }, this);

    }, this);

    this.fcbAxis.addListener("delAxis", function() {
      this.fcbAxis.delAxis();
    }, this);

    //   var listAxis = [];
    //   var loadData = e.getData();
    //   for (var i in loadData) {
    //     listAxis.push({
    //       'label': loadData[i]['name'],
    //       'id': loadData[i]['id']
    //     });
    //   }
    //   fcbAxis.setListModel(listAxis);
    // }, this);

    // btnAddAxis.addListener("execute", function() {
    //   this.fireEvent("addAxis");
    //   this.btnDelAxis.addListener("execute", function() {
    //     this.fireEvent("delAxis");
    //   }, this);

    //   this.addListener("delAxis", function() {
    //     this.delAxis();
    //   }, this);
    // }, this);

    // this.addListener("addAxis", function() {
    //   this.addAxis();
    // }, this);

    radioGroup.addListener("changeSelection", function() {
      var status = radioGroup.getSelection()[0].getLabel();
      this.setStatus(status);
    }, this);
    
    draft.addListener("mouseover", function(e) { // TODO search for proper popup implementation
      popupdraft.placeToPointer(e);
      popupdraft.show();
    }, this);

    draft.addListener("mouseout", function(e) {
      if (popupdraft != null) {
        popupdraft.setVisibility("excluded");
      }
    }, this);

    privat.addListener("mouseover", function(e) {
      popupprivate.placeToPointer(e);
      popupprivate.show();
    }, this);

    privat.addListener("mouseout", function(e) {
      if (popupprivate != null) {
        popupprivate.setVisibility("excluded");
      }
    }, this);

    publique.addListener("mouseover", function(e) {
      popuppublic.placeToPointer(e);
      popuppublic.show();
    }, this);

    publique.addListener("mouseout", function(e) {
      if (popuppublic != null) {
        popuppublic.setVisibility("excluded");
      }
    }, this);

    archive.addListener("mouseover", function(e) {
      popuparchive.placeToPointer(e);
      popuparchive.show();
    }, this);

    archive.addListener("mouseout", function(e) {
      if (popuparchive != null) {
        popuparchive.setVisibility("excluded");
      }
    }, this);

    btnDelete.addListener("execute", function() {
      this.fireEvent("resetProject");
      tfTitleProject.setEnabled(false);
      taAbstractProject.setEnabled(false);
      coord.setEnabled(false);
      this.listContrib.setEnabled(false);
      fcbFunding.setEnabled(false);
      this.fcbAxis.setEnabled(false);
      dateDebut.setEnabled(false);
      dateFin.setEnabled(false);
      btnAddSubProject.setEnabled(false);
      btnAddExperience.setEnabled(false);
      draft.setEnabled(false);
      privat.setEnabled(false);
      publique.setEnabled(false);
      archive.setEnabled(false);
      btnSave.setEnabled(false);
      btnDraft.setEnabled(false);
    }, this);

    btnAnnuler.addListener("execute", function() {
      this.fireEvent("cancelProject");
    }, this);

    btnSave.addListener("execute", function() {
      btnSave.setEnabled(false);
      validation.validate();
    }, this);

    validation.addListener("complete", function() {
      btnSave.setEnabled(true);
      if (validation.getValid()) {
        alert("You can save project");
        this.saveDraft();
        // this.close();
      } else {
        alert(validation.getInvalidMessages().join("\n"));
      }
    }, this);

    btnDraft.addListener("execute", function() {
      this.saveDraft();
    }, this);

    /*
    ********************
    Fonctions pour effacer tout ce qui a été saisi (cancel pour fermer la page)
    ********************
    */
    this.addListener("cancelProject", function() {
      this.reset();
      var store = qx.bom.Storage.getLocal();
      store.removeItem("projectDraft");
    }, this);

    this.addListener("resetProject", function() {
      this.reset();
      var store = qx.bom.Storage.getLocal();
      store.removeItem("projectDraft");
    }, this);
    /*
    ********************
    Fonction pour sauvegarder tout ce qui a été saisi
    ********************
    */
    this.addListener("saveProject", function() {
      this.fromDict();
      var store = qx.bom.Storage.getLocal();
      store.removeItem("projectDraft");
    }, this);

    // Code execution
    services.QueryAllUser("loadedAllUser", null, null); // Method to move to qxelvis: RequestAllUsers
  },

  members: {
    /**
     * Reset the form data
     */
    reset : function() {
      this.resetName();
      this.resetTitle();
      this.resetAbstra();
      this.resetCoordinator();
      this.resetContributors();
      this.resetFunding();
      this.resetAxis();
      this.resetStartDate();
      this.resetEndDate();
      this.resetStatus();
    },

    /**
     * First test save/print data
     */
    saveDraft : function() {
      this.fireDataEvent('saveDraft', this.toDict());
      // readTab = store.setItem("tab", tab);
      // this.info(store.getItem("tab"));
    },


    loadDraft : function() {
      this.fireDataEvent('loadDraft', this.fromDict());
    },


    /**
     * Add FiltredComboBox for axis
     */
    // addAxis : function() {
    //   var newfcbAxis = new glams.ui.FiltredComboBox();
    //   newfcbAxis.setPlaceholder(this.tr("Filter Axis of reseach"));
    //   newfcbAxis.bind("value", this, "axis");
    //   this.bind("axis", newfcbAxis, "value");
    //   this.__axisHBox._add(newfcbAxis);
    //   this.btnDelAxis = new qx.ui.form.Button("-");
    //   this.btnDelAxis.setMaxHeight(22);
    //   this.btnDelAxis.setMaxWidth(25);
    //   this.__btnVBox._add(this.btnDelAxis);
    // },

    // /**
    //  * Delete FiltredComboBox for axis
    //  */
    // delAxis : function() {
    //   this.__btnVBox._remove(this.btnDelAxis);
    //   this.__axisHBox._remove(this.newfcbAxis);
    // },

    /**
     * Renvoie un dictionnaire correspondant aux propriétés de l'objet
     *
     * @return {Map} Le dictionnaire
     */
    toDict : function() {
      var m = new Map();
      m['name'] = this.getName();
      m['title'] = this.getTitle();
      m['abstract'] = this.getAbstra();
      m['contributors'] = this.getContributors();
      m['coordinator'] = this.getCoordinator();
      m['funding'] = this.getFunding();
      m['axis'] = this.getAxis();
      m['startDate'] = this.getStartDate();
      m['endDate'] = this.getEndDate();
      m['status'] = this.getStatus();
      this.info(m);
      return(m);
    },

    /**
     *
     *
     */
    fromDict : function() {
      var store = qx.bom.Storage.getLocal();
      var projectDraft = store.getItem("projectDraft");
      if (projectDraft != null) {
        this.setName(projectDraft['name']);
        this.setTitle(projectDraft['title']);
        this.setAbstra(projectDraft['abstract']);
        this.setContributors(projectDraft['contributors']);
        this.setCoordinator(projectDraft['coordinator']);
        this.setFunding(projectDraft['funding']);
        this.fcbAxis.setAxisName(projectDraft['axis']);
        this.setStatus(projectDraft['status']);
        // this.setStartDate(projectDraft['startDate']);
        // this.setEndDate(projectDraft['endDate']);
        return(projectDraft);
      }
    }
  }
});
