/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Formulaire de description modification et ajout d'expériences
 * Mai 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.ExperimentNamesDescriptor", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * fire when the Experiment is updated
     */
    "updateExperiment": "qx.event.type.Event",
    /**
     * fire when the Experiment is created
     */
    "createdExperiment": "qx.event.type.Event"
  },

  properties: {

  },

  members: {
    __updateExperiment: function(item) {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
      container._add(new qx.ui.basic.Label(this.tr("Nom de l'Experience")));
      var name = new qx.ui.form.TextField();
      name.setValue(item.getLabel());
      container._add(name);
      container._add(new qx.ui.basic.Label(this.tr("Date de création")));
      var date = new qx.ui.form.DateField();
      date.setValue(new Date(item.getUserData("creationDate")));
      container._add(date);
      container._add(new qx.ui.basic.Label(this.tr("Description")));
      var description = new qx.ui.form.TextArea();
      description.setValue(item.getUserData("description"));
      container._add(description);
      var savebtn = new qx.ui.form.Button(this.tr("Save"));
      container._add(savebtn);
      savebtn.addListener("execute", function() {
        var tab = {};
        tab['name'] = name.getValue();
        tab['id'] = item.getUserData("id");
        tab['description'] = description.getValue();
        var newDate = date.getValue();
        newDate.setDate(date.getValue().getDate() + 1); //correction pour conserver la date
        tab['creationDate'] = newDate;
        this.__services.QueryUpdateExperiment("updateExperiment", this.__sessionId, tab);
        this.__services.addListener("updateExperiment", function(e) {
          this.fireEvent("updateExperiment")
        }, this);
      }, this);
      return (container)
    },
    __newExperiment: function() {
      var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
      container._add(new qx.ui.basic.Label(this.tr("Nom de l'Experience")));
      var name = new qx.ui.form.TextField();
      container._add(name);
      container._add(new qx.ui.basic.Label(this.tr("Date de création")));
      var date = new qx.ui.form.DateField();
      date.setValue(new Date());
      //date.setSelectable(false);
      container._add(date);
      container._add(new qx.ui.basic.Label(this.tr("Description")));
      var description = new qx.ui.form.TextArea();
      container._add(description);
      var listeGroup = new qx.ui.basic.Label("Groupe utilisateur : ");
      container._add(listeGroup);
      //recupère la liste des groupes
      this.__session.requestUserInfo();
      this.__session.requestGroupsInfo();
      this.__listeIdGroups = [];
      this.__session.addListenerOnce("gotGroupsInfo", function(e) {
        var listeGroups = e.getData();
        for (var g in listeGroups) {
          var gr = {}
          gr['name'] = listeGroups[g]['name'];
          gr['id'] = listeGroups[g]['id'];
          gr['active'] = listeGroups[g]['active'];
          gr['description'] = listeGroups[g]['description'];
          listeGroup.setValue(listeGroup.getValue() + ' ' + gr['name']);
          this.__listeIdGroups.push(gr['id']);
        };
      }, this);
      var cbl = new qx.ui.form.CheckBox("Autorisé en lecture ");
      cbl.setValue(true);
      container._add(cbl);
      var cbe = new qx.ui.form.CheckBox("Autorisé en écriture ");
      cbe.setValue(true);
      container._add(cbe);
      container._add(new qx.ui.basic.Label(this.tr("Est une composante de l'expérience : ")));
      this.__serie = new glams.ui.WidgetExperience();
      container._add(this.__serie);
      var savebtn = new qx.ui.form.Button(this.tr("Save"));
      container._add(savebtn);
      //listener
      name.addListener("changeValue", function() {
        if (name.getValue() == null || name.getValue() == '') {
          name.setValid(false);
        } else {
          name.setValid(true);
        };
      }, this);

      // Permet de récupérer les collections via le widgetCollection
      this.__serie.addListener("changeListCollection", function(e) {
        var loadData = e.getData();
        loadData = loadData.toArray();
        this.collection = loadData;
      }, this);
      //action de sauvegarde
      savebtn.addListener("execute", function() {
        if (name.getValue() == null || name.getValue() == '') {
          name.setValid(false);
          return;
        };
        var tab = {};
        tab['name'] = name.getValue();
        tab['description'] = description.getValue();
        var newDate = date.getValue();
        if (newDate != null) {
          newDate.setDate(date.getValue().getDate() + 1);
        }; //correction pour conserver la date
        tab['creationDate'] = newDate;
        tab['listeIdGroupes'] = this.__listeIdGroups;
        tab['ecriture'] = cbe.getValue();
        tab['lecture'] = cbl.getValue();
        tab['experience'] = this.collection;
        this.__services.QueryCreateExperiment("createExperiment", this.__sessionId, tab);
        this.__services.addListener("createExperiment", function(e) {
          this.fireEvent("createdExperiment")
        }, this);
      }, this);

      return (container)
    }
  },

  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */

    // Session management
    this.__session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = this.__session.getSessionId();

    var layout = new qx.ui.layout.VBox();
    layout.setSpacing(10);

    this._setLayout(layout);
    //creation des widgets
    this._add(new qx.ui.basic.Label(this.tr("Nom des Experiences")));

    this.__liste = new qx.ui.form.List().set({
      width: 250,
      allowStretchY: true,
      allowStretchX: true
    });
    this.__buttonEdit = new qx.ui.form.Button(this.tr("Modifier"));
    this.__buttonNew = new qx.ui.form.Button(this.tr("Ajouter"));


    //positionnement des widgets
    this._add(this.__liste, {
      flex: 1
    });
    this._add(this.__buttonEdit);
    this._add(this.__buttonNew);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    var tab = {};
    tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));
    this.__services.QueryGetAllExperiments("getAllExperiments", this.__sessionId, tab);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //Nom de l'experience
    this.__services.addListener("getAllExperiments", function(e) {
      this.__liste.removeAll();
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i]['name']);
        tempItem.setUserData("creationDate", loadData[i]['creation_date']);
        tempItem.setUserData("description", loadData[i]['description']);
        tempItem.setUserData("id", loadData[i]['id']);
        this.__liste.add(tempItem);
      };
    }, this);

    //bouton modifier
    this.__buttonEdit.addListener("execute", function() {
      var window = new qx.ui.window.Window("Modify Experiment name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__updateExperiment(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updateExperiment", function() {
        window.close()
      }, this);
    }, this);
    //doubleclick sur type de boite
    this.__liste.addListener("dblclick", function(e) {
      var window = new qx.ui.window.Window("Modify Experiment name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__updateExperiment(this.__liste.getSelection()[0]));
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("updateExperiment", function() {
        window.close()
      }, this);
    }, this);
    //bouton ajouter
    this.__buttonNew.addListener("execute", function(e) {
      var window = new qx.ui.window.Window("Add Experiment name");
      var layout = new qx.ui.layout.VBox();
      window.setLayout(layout);
      window.add(this.__newExperiment());
      window.open();
      window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      this.addListener("createdExperiment", function() {
        window.close();
      }, this);
    }, this);
    //mise à jour de la liste
    this.addListener("updateExperiment", function() {
      this.__services.QueryGetAllExperiments("getAllExperiments", this.__sessionId, tab);
    }, this);
    this.addListener("createdExperiment", function() {
      this.__services.QueryGetAllExperiments("getAllExperiments", this.__sessionId, tab);
    }, this);

  }
})