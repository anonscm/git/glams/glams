/**
 * Formulaire de creation de Box
 *
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.CreateBoxForm", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Fired when the new box is create
     */
    "newBoxCreated": "qx.event.type.Event"
  },

  properties: {

    /**
     *name type of the box
     */
    boxTypeName: {
      init: null,
      nullable: true,
      check: "String",
      event: "changeBoxTypeName"
    },    
    /**
     * date de la position de la boite dans un lieu
     */
    date_on: {
      init: null,
      nullable: true,
      event: "changeDate_on"
    }
  },

  members: {

    __boxPlaces: new qx.data.Array


  },

  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(10);
    layout.setColumnFlex(1, 1); // make row 1 flexible
    this._setLayout(layout);

    //positionnement des labels
    this._add(new qx.ui.basic.Label(
      this.tr("Type de boite : ")), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Code ou Nom : ")), {
      row: 1,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Positionnement : ")), {
      row: 2,
      column: 0
    });
    //init of the widget
    this.__name = new qx.ui.form.TextField();
    this.__typeBox = new qx.ui.basic.Label();
    this.__boxPlaces = new glams.ui.WidgetPlace();
    this.__saveButton = new qx.ui.form.Button("Enregistrer");



    //positionnement des widgets
    var i = 0 //increment des places
    this._add(this.__typeBox, {
      row: 0,
      column: 1
    });
    this._add(this.__name, {
      row: 1,
      column: 1
    });
    this._add(this.__boxPlaces, {
      row: 3,
      column: 0,
      colSpan: 3
    })
    this._add(this.__saveButton, {
      row: 4,
      column: 0
    });


    this.__typeBox.setValue(this.getBoxTypeName());

    /*
     *****************************************************************************
      SERVICE
     *****************************************************************************
     */
    var services = glams.services.Sample.getInstance();
    services.QueryAllTissuType("loadedAllTissuType", null, null); //déjà présent dans un autre onglet
    services.QueryGetAlltypesLieux("getAlltypesLieux", this.__sessionId, null); //déjà présent dans un autre onglet


    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */
    //affichage du type de boite
    this.addListener('changeBoxTypeName', function() {
      this.__typeBox.setValue(this.getBoxTypeName());
    }, this);

    this.__boxPlaces.addListener("changeListPlaces", function(e) {
      this.__tab = e.getData();
//      console.log('this.__boxPlaces.addListener()'); 
//      console.log(this.__tab);
      // for (var i in tab){
      //   var listPlace = {};
      //   listPlace[]
      // }
    }, this);

    //Enregistrer
    this.__saveButton.addListener("execute", function() {
      if (this.__name.getValue() != null && this.__name.getValue() != '') {

        //teste si boite existante
        this.__tabNewBox = {};
        this.__tabNewBox['nameBox'] = this.__name.getValue();
        this.__tabNewBox['nameBoxType'] = this.getBoxTypeName();
        this.__tabNewBox['placesBox'] = this.__tab.toArray();  
        console.log(this.__tab.toArray());
//        this.__tabNewBox['placesBox'] = this.__tab.toArray();       
        this.__tabNewBox['date_on'] = this.getDate_on();
        this.__tabNewBox['userGroup'] = JSON.parse(localStorage.getItem('readGroups'))[0];
        services.QueryGetValidationCreateBox("getValidationCreateBox", this.__sessionId, this.__tabNewBox);
      };
    }, this);
    //validation pour la création de la boite
//    services.addListenerOnce("getValidationCreateBox", function(e) {
    services.addListener("getValidationCreateBox", function(e) {    
      var loadData = e.getData();
//console.log(  loadData );   
      if (loadData == 'Validation = True') {        
        services.QuerySetCreateBox("setCreateBox", this.__sessionId, this.__tabNewBox);
      };
    }, this);
    //création de la boite
    services.addListener("setCreateBox", function(e) {
      var loadData = e.getData();
//console.log('setCreateBox');
//console.log(loadData)
      this.fireDataEvent("newBoxCreated");
    }, this);




  }
});
