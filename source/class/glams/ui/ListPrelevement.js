/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for reading a list of prelevement.
 * Authors : Lysiane Hauguel,
 * October 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.ListPrelevement", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * get information about the prelevement
     */
    "loadedSampleSelected": "qx.event.type.Data",
    /**
     * fire when a character is input
     */
    "changeSelectedString": "qx.event.type.Data"
  },

  properties: {},

  construct: function() {

    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setRowFlex(0, 1);
    layout.setRowFlex(1, 1); 
    this._setLayout(layout);

    this.__autoList = new glams.ui.FiltredComboBox();
    this.__autoList.setAllowStretchX(false, false);
    this.__autoList.setAllowStretchY(false, false);
    this._add(this.__autoList, {
      row: 0,
      column: 1
    });

    var container = new qx.ui.tabview.TabView();
    container.setBarPosition("left");
    var grid2 = new qx.ui.layout.Grid();
    grid2.setSpacing(5);
    var groupBoxInfoPrelev = new qx.ui.groupbox.GroupBox(this.tr("Info Prelevement"));
    groupBoxInfoPrelev.setLayout(grid2);
    var labelCodeP = new qx.ui.basic.Label().set({
      value: "Nom du prélèvement :",
      rich: true
    });

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();


    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */

    //récupération des prelevements
    this.__autoList.addListener("changeSelectedString",function(e){
      var loadData = e.getData();
      var tab = {};
      tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
      tab['filtredText'] = e.getData();
      this.__services.QueryGetAllContainersFiltred("getAllContainersFiltred", this.__sessionId, tab);
    }, this);

    this.__services.addListener("getAllContainersFiltred", function(e) {
      this.__listPrelev = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listPrelev.push({
          'label': loadData[i]['code'],
          'id': loadData[i]['id']
        });
      };
      this.__autoList.setListModel(this.__listPrelev);
    }, this);
  },

  members: {
    /**
     * List of open boxes for control and modifications
     */
    __listeOpenBox: []

    //************* affichage plans de boite*********************************************
    /**
     * display the map of the selected box
     * @param idBox {number} id of the selected box
     * @param label {string} label of the selected box
     */

    }

});