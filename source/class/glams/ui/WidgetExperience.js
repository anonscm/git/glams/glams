/* ************************************************************************

  Copyright: 2017 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, bioinfo team, IRHS
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Widget to affect one experience to on sample or a plant.
 * Authors : David Aurégan, Fabrice Dupuis,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.WidgetExperience", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Fire when the list of experience is changed
     */
    "changeListCollection": "qx.event.type.Data"
  },

  properties: {

  },

  members: {

  },

  construct: function() {
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    this.base(arguments);
    var layout = new qx.ui.layout.Grid()
    layout.setSpacing(5);
    this._setLayout(layout);

    //initialisation de l'autocompleBox
    this.__collection = new glams.ui.FiltredComboBox();
    this.__addButton = new qx.ui.form.Button("Add");
    this.__resetButton = new qx.ui.form.Button("Reset");
    this.__tabCollection = new qx.data.Array;
    var i = 0 //incrementation des places
    this._add(this.__collection, {
      row: 0 + i,
      column: 0
    });
    this._add(this.__addButton, {
      row: 0 + i,
      column: 1
    });

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
    // Affichage dans l'autocompleBox
    this.__collection.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      var tab = {};
      tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
      tab['filtredText'] = loadData;
      this.__services.QueryGetAllExperimentsFiltred("getAllExperimentsFiltred", this.__sessionId, tab);
    }, this);
    this.__services.addListener("getAllExperimentsFiltred", function(e) {
      this.__listCollection = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listCollection.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      };
      this.__collection.setListModel(this.__listCollection);
    }, this);

    // Affichage quand on ajoute une collection
    this.__addButton.addListener("execute", function() {
      if (this.__collection.getAllValues().getLabel() != null && this.__collection.getAllValues().getLabel().length > 0) {
        i = i + 1;
        this._add(new qx.ui.basic.Label(this.__collection.getAllValues().getLabel()), {
          row: 0 + i,
          column: 0
        });
        this._add(this.__resetButton, {
          row: 0 + i,
          column: 1
        });
        this._add(this.__collection, {
          row: 1 + i,
          column: 0
        });
        this._add(this.__addButton, {
          row: 1 + i,
          column: 1
        });
        var col = {};
        col['name'] = this.__collection.getAllValues().getLabel();
        col['id'] = this.__collection.getAllValues().getId();
        this.__tabCollection.push(col);
        this.__collection.setValue(null);
        this.fireDataEvent("changeListCollection", this.__tabCollection);
      };
    }, this);

    //boutton reset
    this.__resetButton.addListener("execute", function() {
      this._removeAll()
      var i = 0
      this._add(this.__collection, {
        row: 0 + i,
        column: 0
      });
      this._add(this.__addButton, {
        row: 0 + i,
        column: 1
      });
      this.__tabCollection.removeAll();
    }, this);
  }
});