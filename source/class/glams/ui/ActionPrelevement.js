/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Allows actions on samples.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.ActionPrelevement", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Save the new Culture
     */
    "saveAction": "qx.event.type.Event"
  },

  properties: {

  },

  members: {

  },
  construct: function() {
    this.base(arguments);

    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(10);
    this._setLayout(layout);

    //positionnement des labels
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du prelevement: ")), {
      row: 0,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type d'action : ")), {
      row: 1,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Date : ")), {
      row: 2,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Notations sur l'action : ")), {
      row: 4,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Nom de l'échantillon crée : ")), {
      row: 0,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Notations sur le contenu : ")), {
      row: 4,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Type de contenu : ")), {
      row: 1,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Protocole : ")), {
      row: 2,
      column: 2
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Agent : ")), {
      row: 6,
      column: 0
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Affecter à une expérience : ")), {
      row: 6,
      column: 2,
      rowSpan: 6
    });

    //initialisation des widgets
    this.__namePrelevOrigin = new glams.ui.FiltredComboBox();
    this.__namePrelevOrigin.setPlaceholder(this.tr("Filter Name ( 3 carracters minimum)"));
    this.__namePrelevOrigin.setWidth(200);
    this.__actionType = new qx.ui.form.SelectBox();
    this.__date = new qx.ui.form.DateField();
    this.__date.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__date.setValue(new Date());
    this.__namePrelevement = new qx.ui.form.TextField();
    this.__namePrelevement.setWidth(200);
    this.__user = new qxelvis.ui.users.UserSelector();
    this.__serie = new glams.ui.WidgetExperience();
    this.__collection = [];
    this.__protocole = new glams.ui.FiltredComboBox();
    this.__protocole.setPlaceholder(this.tr("Filter Name Protocole ( 3 carracters minimum)"));
    this.__box = new glams.ui.WidgetBox();
    // List pour ajout de notations
    this.__otherNotationCont = new qx.ui.form.List().set({
      height: 100
    });
    this.__otherNotationAct = new qx.ui.form.List().set({
      height: 100
    });
    //Ajout de notations sur le container
    var addWindowContainer = new glams.ui.AddContainerNotationWindow();
    var widgetNotationContainer = new qx.ui.core.Widget();
    widgetNotationContainer._setLayout(new qx.ui.layout.VBox());
    var addButtonC = new qx.ui.form.Button("+");
    var removeButtonC = new qx.ui.form.Button("-");
    widgetNotationContainer._add(this.__otherNotationCont);
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    widgetButton._add(addButtonC);
    widgetButton._add(removeButtonC);
    widgetNotationContainer._add(widgetButton);
    //ajout de notations sur l'action
    var addWindowAction = new glams.ui.AddActionNotationWindow();
    var widgetNotationAction = new qx.ui.core.Widget();
    widgetNotationAction._setLayout(new qx.ui.layout.VBox());
    var addButtonA = new qx.ui.form.Button("+");
    var removeButtonA = new qx.ui.form.Button("-");
    widgetNotationAction._add(this.__otherNotationAct);
    var widgetButton = new qx.ui.core.Widget();
    widgetButton._setLayout(new qx.ui.layout.HBox());
    widgetButton._add(addButtonA);
    widgetButton._add(removeButtonA);
    widgetNotationAction._add(widgetButton);
    this.__contenu = new qx.ui.form.SelectBox();

    //positionnement des widgets
    this._add(this.__namePrelevOrigin, {
      row: 0,
      column: 1
    });
    this._add(this.__actionType, {
      row: 1,
      column: 1
    });
    this._add(this.__date, {
      row: 2,
      column: 1
    });
    this._add(widgetNotationAction, {
      row: 4,
      column: 1
    });
    this._add(widgetNotationContainer, {
      row: 4,
      column: 3
    });
    this._add(this.__namePrelevement, {
      row: 0,
      column: 3
    });
    this._add(this.__serie, {
      row: 6,
      column: 3,
      rowSpan: 6
    });
    this._add(this.__contenu, {
      row: 1,
      column: 3
    })
    this._add(this.__protocole, {
      row: 2,
      column: 3
    });
    this._add(this.__user, {
      row: 6,
      column: 1
    });
    this._add(this.__box, {
      row: 0,
      column: 4,
      rowSpan: 6
    });

    this.__labelValidation = new qx.ui.basic.Label('Obligatoire : ');
    this.__labelValidation.setTextColor('red');
    this.__labelValidation.setVisibility("hidden");
    this._add(this.__labelValidation, {
      row: 7,
      column: 0,
      colSpan: 2
    });

    //enregistrement
    this.__saveActionButton = new qx.ui.form.Button("Save");
    this._add(this.__saveActionButton, {
      row: 8,
      column: 4
    });

    var validationProtocole = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez selectionner un Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var validationNameOrigine = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez selectionner échantillon");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var validationName = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez nommer votre nouvel échantillon");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    this.__manager = new qx.ui.form.validation.Manager();
    this.__manager.add(this.__protocole, validationProtocole);
    this.__manager.add(this.__namePrelevOrigin, validationNameOrigine);
    this.__manager.add(this.__namePrelevement, validationName);


    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryGetAllActionType("getAllActionType", null, null);
    this.__services.QueryAllUser("loadedAllUser", null, null);
    this.__services.QueryAllTypeContenu("loadedAllTypeContenu", null, null);

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //type de actions
    this.__services.addListener("getAllActionType", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        tempItem.setUserData("id", loadData[i]['id'])
        this.__actionType.add(tempItem);
      };
    }, this);

    //charge tous les types de contenus
    this.__services.addListener("loadedAllTypeContenu", function(e) {
      var loadData = e.getData();
      for (var i in loadData) {
        var tempItem = new qx.ui.form.ListItem(loadData[i].type);
        tempItem.setUserData("id", loadData[i]['id'])
        this.__contenu.add(tempItem);
      };
    }, this);

    //noms des utilisateurs pour le choix du user
    this.__services.addListener("loadedAllUser", function(e) {
      var loadData = e.getData();
      this.__user.setActiveUser(e.getData());
    }, this);

    //protocole
    // Fonctions qui chargent les protocole en fonction des carractères saisi dans la barre de texte
    this.__protocole.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {};
        tab['filtredName'] = e.getData();
        this.__services.QueryGetAllProtocolFiltred("getAllProtocolFiltred", this.__sessionId, tab);
      };
    }, this);
    // Listener pour récuperer les données protocoles
    this.__listProtocol = [];
    this.__services.addListener("getAllProtocolFiltred", function(e) {
      this.__listProtocol = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listProtocol.push({
          'label': loadData[i]['name'],
          'id': loadData[i]['id']
        });
      };
      this.__protocole.setListModel(this.__listProtocol);
    }, this);

    //Prelevement origine
    //Fonctions qui chargent les prelevements en fonction des carractères saisi dans la barre de texte
    this.__namePrelevOrigin.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {};
        tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
        tab['filtredText'] = e.getData();
        this.__services.QueryGetAllContainersFiltred("getAllContainersFiltred", this.__sessionId, tab);
      };
    }, this);
    // Listener pour récuperer les données prelevement
    this.__services.addListener("getAllContainersFiltred", function(e) {
      this.__listPrelev = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listPrelev.push({
          'label': loadData[i]['code'],
          'id': loadData[i]['id']
        });
      };
      this.__namePrelevOrigin.setListModel(this.__listPrelev);
    }, this);

    //Bouton Notation Contenu
    addButtonC.addListener("execute", function() {
      addWindowContainer.updateData();
      addWindowContainer.open();
      addWindowContainer.center();
    }, this);

    addWindowContainer.addListener("addNotationSample", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value + " ; " + loadData.date + " ; " + loadData.notateur + " ; " + loadData.remarque);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      tempItem.setUserData("date", loadData.date);
      tempItem.setUserData("notateur", loadData.notateur);
      tempItem.setUserData("remarque", loadData.remarque);
      this.__otherNotationCont.add(tempItem);
    }, this);

    removeButtonC.addListener("execute", function() {
      this.__other.getSelection()[0].destroy();
    }, this);

    //Bouton Notation Action
    addButtonA.addListener("execute", function() {
      addWindowAction.updateData();
      addWindowAction.open();
      addWindowAction.center();
    }, this);

    addWindowAction.addListener("addNotationAction", function(e) {
      var loadData = e.getData();
      var tempItem = new qx.ui.form.ListItem(loadData.type + " : " + loadData.value + " ; " + loadData.date + " ; " + loadData.notateur + " ; " + loadData.remarque);
      tempItem.setUserData("type", loadData.type);
      tempItem.setUserData("value", loadData.value);
      tempItem.setUserData("date", loadData.date);
      tempItem.setUserData("notateur", loadData.notateur);
      tempItem.setUserData("remarque", loadData.remarque);
      this.__otherNotationAct.add(tempItem);
    }, this);

    removeButtonA.addListener("execute", function() {
      this.__other.getSelection()[0].destroy();
    }, this);

    // Permet de récupérer les collections via le WidgetExperience
    this.__serie.addListener("changeListCollection", function(e) {
      var loadData = e.getData();
      loadData = loadData.toArray();
      this.__collection = loadData;
    }, this);

    this.__saveActionButton.addListener("execute", function() {
      this.__labelValidation.setValue('Important : ');
      this.__labelValidation.setVisibility("hidden");
      if (!this.__user.getId()) {
        this.__labelValidation.setVisibility("visible");
        this.__labelValidation.setValue(this.__labelValidation.getValue() + "  Renseigner l'utilisateur ");
      } else {
        this.__manager.validate();
      }
    }, this);

    this.__manager.addListener("complete", function(e) {
      if (this.__manager.getValid()) {
        this.fireEvent("saveAction");
      }
    }, this);

    this.addListener("saveAction", function() {
      this.__box.fireDataEvent("saveData");
      //Stockage Notation Contenu
      var tabNotationCont = [];
      for (var i in this.__otherNotationCont.getChildrenContainer()._getChildren()) {
        var tab = {};
        tab['type_notation'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("type");
        tab['valeur_notation'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("value");
        tab['remarque_notation'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("remarque");
        tab['date'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("date");
        tab['notateur'] = this.__otherNotationCont.getChildrenContainer()._getChildren()[i].getUserData("notateur");
        tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));
        tabNotationCont.push(tab);
      };
      //Stockage Notation Action
      var tabNotationAction = [];
      for (var i in this.__otherNotationAct.getChildrenContainer()._getChildren()) {
        var tab = {};
        tab['type_notation'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("type");
        tab['valeur_notation'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("value");
        tab['remarque_notation'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("remarque");
        tab['date'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("date");
        tab['notateur'] = this.__otherNotationAct.getChildrenContainer()._getChildren()[i].getUserData("notateur");
        tab['userGroup'] = JSON.parse(localStorage.getItem('writeGroups'));
        tabNotationAction.push(tab);
      };
      var tabBox = {};
      tabBox = this.__box.getDataBox();
      var tab = {};
      tab['idContainerM'] = this.__namePrelevOrigin.getAllValues().getId();
      tab['idTypeAction'] = this.__actionType.getSelection()[0].getUserData('id');
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      tab['date'] = format.format(this.__date.getValue());
      tab['Ccode'] = this.__namePrelevement.getValue();
      tab['idUser'] = this.__user.getId();
      tab['valid'] = 'true';
      tab['usable'] = 'true';
      tab['idTypeContainer'] = tabBox['idContainer'];
      tab['idLevel'] = 2;
      tab['idTypeContent'] = this.__contenu.getSelection()[0].getUserData('id');
      tab['idBoxC'] = tabBox['idBox'];
      tab['Ccolonne'] = tabBox['colName'];
      tab['Cligne'] = tabBox['rowName'];
      tab['experience'] = this.__collection;
      tab['idProtocol'] = this.__protocole.getAllValues().getId();
      tab['listeNotationsCont'] = tabNotationCont;
      tab['listeNotationsAct'] = tabNotationAction;
      //console.log(tab);
      this.__services.QueryCreateAction("createAction", this.__sessionId, tab);
    }, this);

    this.__services.addListener("createAction", function(e) {
      var loadData = e.getData();
    }, this);
  }
});