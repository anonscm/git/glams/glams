/**
 * This is the widget to view basic information of one Box
 *
 */
qx.Class.define("glams.ui.Container",{
  extend : qx.ui.core.Widget,	
  events : {    
    /**
     * Fired when the introduction is saved in the database.
     */
    "saved" : "qx.event.type.Event"
       },
	
  properties : {
  /**
    * id of the container
    */
    id : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeId"
    },
  /**
    * code of the container
    */
    code : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeCode"
    }, 
  /**
    * type of the container
    */
    typeContainer : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeTypeContainer"
    }, 
  /**
    * level of the container
    */
    level : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeLevel"
    }, 
  /**
    * content of the container
    */
    content : {      
      init : null,
      nullable : true,
      check : "String",
      event : "changeContent"
    },
  /**
    * remark
    */
    remark :{      
      init : null,
      nullable : true,
      check : "String",
      event : "changeRemark"
    },
  /**
    * valid
    */
    valid :{      
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeValid"
    },
  /**
    * usable
    */
    usable :{      
      init : null,
      nullable : true,
      check : "Boolean",
      event : "changeUsable"
    },
   /**
    * position ligne
    */
    row : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeRow"
    },
  /**
    * position colonne
    */
    column : {
      init : null,
      nullable : true,
      check : "String",
      event : "changeColumn"
    },
  /**
    * date on
    */
    dateOn : {
      init : null,
      nullable : true,
//      check : "",
      event : "changeDateOn"
    },       
  /**
    * date off
    */
    dateOff : {
      init : null,
      nullable : true,
//      check : "",
      event : "changeDateOff"
    },
   /**
    * idBox
    */
    idBox : {
      init : null,
      nullable : true,
      check : "Integer",
      event : "changeIdBox"
    },
   /**
    * Notations
    */
    notations : {
      init : null,
      nullable : true,
      check : "Array",
      event : "changeNotations"
    }
    
    
  },
  members : {    
   /**
    * renvoie les informations du container sous forme de dictionnaire
    */
    toDict : function(){
      var tab={};
      tab['idBox']=this.getIdBox ();
      tab['id']=this.getId() ;
      tab['code']=this.getCode ();
      tab['typeContainer']=this.getTypeContainer ();
      tab['level']=this.getLevel ();
      tab['remark']=this.getRemark ();
      tab['typeContent']=this.getContent ();
      tab['valid']=this.getValid ();
      tab['usable']=this.getUsable ();
      tab['row']=this.getRow ();
      tab['column']=this.getColumn ();
      tab['dateOn']=this.getDateOn ();
      tab['dateOff']=this.getDateOff ();
      tab['notations']=this.getNotations ();
    
    return tab;
    }

  },
    
  construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
		
    
   /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
		//var services = glamssample.services.Sample.getInstance();
    
    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
     
    
   
	}
});
