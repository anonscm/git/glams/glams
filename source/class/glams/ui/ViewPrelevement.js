/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * This is the main widget for reading prelevement information.
 * Authors : David Aurégan,
 * May 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.ViewPrelevement", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * get information about the prelevement
     */
    "loadedSampleSelected": "qx.event.type.Data",
    /**
     * fire when a character is input
     */
    "changeSelectedString": "qx.event.type.Data"
  },

  properties: {},

  members: {
    /**
     * List of open boxes for control and modifications
     */
    __listeOpenBox: [],

    //*************	affichage plans de boite*********************************************
    /**
     * display the map of the selected box
     * @param idBox {number} id of the selected box
     * @param label {string} label of the selected box
     */
    __showMapBox: function(idBox, label) {
      this.__idBox = idBox;
      //teste si la boite est déjà ouverte
      var boite = 'absente';
      for (var l in this.__listeOpenBox) {
        if (this.__idBox == this.__listeOpenBox[l]['idBox']) {
          var boite = 'présente';
          break;
        };
      };
      if (boite == 'absente') {
        //creation de la structure data dataBox
        this.__serviceBox.createBox(this.__idBox);
        //creation de la structure graphique MapBox
        this.__mapBox = new glams.ui.MapBox();
        this.__mapBox.setAllowStretchX(true);
        //creation de la fenêtre d'affichage du plan
        var planBoxWindow = new qx.ui.window.Window(label);
        planBoxWindow.setWidth(200);
        planBoxWindow.moveTo(Math.round(850 + Math.random() * 80), Math.round(70 + Math.random() * 80));
        planBoxWindow.setLayout(new qx.ui.layout.Grow());
        planBoxWindow.open();
        //listener sur la fenêtre planBoxWindow
        planBoxWindow.addListener("close", function(e) { //si fermeture alors mise à jour de la liste des boites ouvertes
          for (var l in this.__listeOpenBox) {
            if (planBoxWindow == this.__listeOpenBox[l]['planBoxWindow']) {
              this.__listeOpenBox[l]['mapBox'].destroy();
              this.__listeOpenBox[l]['dataBox'].destroy();
              this.__listeOpenBox.splice(l, 1);
              break;
            };
          };
        }, this);

        //listeners sur la couche data
        this.__serviceBox.addListenerOnce("createBox", function(e) { //toutes les données sont récupérées
          this.__dataBox = e.getData();
          this.__mapBox.setIdBox(this.__dataBox.getIdBox());
          this.__mapBox.setRowNumber(this.__dataBox.getRowNumber());
          this.__mapBox.setColumnNumber(this.__dataBox.getColumnNumber());
          this.__mapBox.setPlace(this.__dataBox.getListePlaces());
          var tab = {};
          tab['idBox'] = this.__idBox;
          tab['mapBox'] = this.__mapBox;
          tab['dataBox'] = this.__dataBox;
          tab['planBoxWindow'] = planBoxWindow;
          this.__listeOpenBox.push(tab);

          this.__dataBox.addListenerOnce("saved", function(e) { //les données sont sauvegardée dans la base. Effectue un reset d'affichage après
            for (var l in this.__listeOpenBox) {
              this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox", this.__listeOpenBox[l]['idBox']);
              this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function() { //mise à jour de la liste des places
                this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
                this.__listeOpenBox[l]['mapBox'].update();

              }, this);
            };
          }, this);

        }, this);
      };

      //listener sur la couche graphique
      this.__mapBox.addListener("drowed", function() { //la structure de dessin est réalisée
        planBoxWindow.add(this.__mapBox);
      }, this);
      this.__mapBox.addListener("updateOnecontainer", function(e) { //les valeurs d'un container sont modifiées
        var loadData = e.getData();
        for (var l in this.__listeOpenBox) {
          if (loadData['idBox'] == this.__listeOpenBox[l]['idBox']) {
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData);
            this.__listeOpenBox[l]['mapBox'].update();
            break;
          };
        };
      }, this);

      this.__mapBox.addListener("saveBox", function(e) { //sauvegarde la box sur elvis
        for (var l in this.__listeOpenBox) {
          this.__listeOpenBox[l]['dataBox'].saveBox();
        };
      }, this);

      this.__mapBox.addListener("reInitialiseMapBox", function(e) { //reinitialise toutes les box pour effacer les modifications
        for (var l in this.__listeOpenBox) {
          this.__listeOpenBox[l]['dataBox'].fireDataEvent("changeIdBox", this.__listeOpenBox[l]['idBox']);
          this.__listeOpenBox[l]['dataBox'].addListener("getAllContainerInformations", function() { //mise à jour de la liste des places
            this.__listeOpenBox[l]['mapBox'].setPlace(this.__listeOpenBox[l]['dataBox'].getListePlaces());
            this.__listeOpenBox[l]['mapBox'].update();
          }, this);
        };
      }, this);

      this.__mapBox.addListener("containerPutOff", function(e) { //action drag du drag and drop
        var loadData = e.getData();
        for (var l in this.__listeOpenBox) {
          if (loadData['idBox'] == this.__listeOpenBox[l]['idBox']) {
            loadData['container'].setDateOff(new Date());
            this.__listeOpenBox[l]['dataBox'].updateOneContainer(loadData['container']);
            break;
          };
        };
      }, this);

      this.__mapBox.addListener("containerPutOn", function(e) { //action drop du drag and drop
        var loadData = e.getData();
        //cree un nouveau container
        loadData['container'].setDateOn(new Date());
        loadData['container'].setDateOff(null);
        if (loadData['row'] == null) {
          loadData['container'].setRow(null);
        } else {
          loadData['container'].setRow(loadData['row'].toString());
        };
        if (loadData['column'] == null) {
          loadData['container'].setColumn(null);
        } else {
          loadData['container'].setColumn(loadData['column'].toString());
        };
        loadData['container'].setIdBox(loadData['idBox']);
        for (var l in this.__listeOpenBox) {
          if (loadData['idBox'] == this.__listeOpenBox[l]['idBox']) {
            this.__listeOpenBox[l]['dataBox'].addContainer(loadData['container'], loadData['container'].getRow(), loadData['container'].getColumn(), loadData['container'].getDateOn(), loadData['container'].getDateOff());
            break;
          };
        };
      }, this);

    } //fin de la methode affichage des boites ******************************************************************************************************************
  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    var layout = new qx.ui.layout.Grid();
    layout.setRowFlex(0, 1);
    layout.setRowFlex(1, 1);
    layout.setRowFlex(2, 1);
    layout.setRowFlex(3, 1);
    layout.setRowFlex(4, 1);
    layout.setRowFlex(5, 1);
    layout.setRowFlex(6, 1);
    layout.setRowFlex(7, 1);
    layout.setRowFlex(8, 1);
    layout.setRowFlex(9, 1);
    layout.setRowFlex(10, 1);
    layout.setRowFlex(11, 1);
    layout.setRowFlex(12, 1);
    layout.setColumnFlex(0, 1);
    layout.setColumnFlex(1, 1);
    layout.setColumnFlex(2, 1);
    layout.setSpacing(15);
    this._setLayout(layout);

    // this.__autoBox = new glams.ui.AutoCompleteBox(null, "getAllPrelevmentsGroupFiltred", null);
    this.__autoBox = new glams.ui.FiltredComboBox();
    this.__autoBox.setAllowStretchX(false, false);
    this.__autoBox.setAllowStretchY(false, false);
    this._add(this.__autoBox, {
      row: 0,
      column: 1
    });
    var container = new qx.ui.tabview.TabView();
    container.setBarPosition("left");
    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    var grid2 = new qx.ui.layout.Grid();
    grid2.setSpacing(5);
    var grid3 = new qx.ui.layout.Grid();
    grid2.setSpacing(5);
    var groupBoxInfoPrelev = new qx.ui.groupbox.GroupBox(this.tr("Info Prelevement"));
    groupBoxInfoPrelev.setLayout(grid2);
    var labelCodeP = new qx.ui.basic.Label().set({
      value: "Nom du prélèvement :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelCodeP, {
      row: 0,
      column: 0
    });
    this.__code = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__code, {
      row: 0,
      column: 1
    });
    var labeldate = new qx.ui.basic.Label().set({
      value: "Date prelevement :",
      rich: true
    });
    groupBoxInfoPrelev.add(labeldate, {
      row: 1,
      column: 0
    });
    this.__date = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__date, {
      row: 1,
      column: 1
    });
    var labelExperimentateur = new qx.ui.basic.Label().set({
      value: "Expérimentateur(s) :",
      rich: true

    });
    groupBoxInfoPrelev.add(labelExperimentateur, {
      row: 2,
      column: 0
    });
    this.__user = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    })
    groupBoxInfoPrelev.add(this.__user, {
      row: 2,
      column: 1
    });
    var labelStdDev = new qx.ui.basic.Label().set({
      value: "Stade de développement :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelStdDev, {
      row: 3,
      column: 0
    });
    this.__stade = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__stade, {
      row: 3,
      column: 1
    });
    var labelOrgane = new qx.ui.basic.Label().set({
      value: "Organe :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelOrgane, {
      row: 4,
      column: 0
    });
    this.__organe = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__organe, {
      row: 4,
      column: 1
    });
    var labelQuantite = new qx.ui.basic.Label().set({
      value: "Quantité prélevée :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelQuantite, {
      row: 5,
      column: 0
    });
    this.__quantity = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__quantity, {
      row: 5,
      column: 1
    });
    var labelTypeUnit = new qx.ui.basic.Label().set({
      value: "Type d'unité :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelTypeUnit, {
      row: 6,
      column: 0
    });
    this.__typeUnit = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__typeUnit, {
      row: 6,
      column: 1
    })
    var labelNbPlant = new qx.ui.basic.Label().set({
      value: "Nombre de plantes :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelNbPlant, {
      row: 7,
      column: 0
    });
    this.__numberPlant = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__numberPlant, {
      row: 7,
      column: 1
    });
    var labelRq = new qx.ui.basic.Label().set({
      value: "Remarque(s) :",
      rich: true
    });
    groupBoxInfoPrelev.add(labelRq, {
      row: 8,
      column: 0
    });
    this.__remarque = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPrelev.add(this.__remarque, {
      row: 8,
      column: 1
    });
    this._add(groupBoxInfoPrelev, {
      row: 1,
      column: 0,
      colSpan: 2
    });
    var groupBoxInfoPlant = new qx.ui.groupbox.GroupBox(this.tr("Info Plante"));
    groupBoxInfoPlant.setLayout(grid);
    var labelFondGen = new qx.ui.basic.Label().set({
      value: "Fond génétique :",
      rich: true
    });
    groupBoxInfoPlant.add(labelFondGen, {
      row: 1,
      column: 0
    });
    this.__fondGenetique = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlant.add(this.__fondGenetique, {
      row: 1,
      column: 1
    });
    var labelNomLot = new qx.ui.basic.Label().set({
      value: "Nom Lot :",
      rich: true
    });
    groupBoxInfoPlant.add(labelNomLot, {
      row: 0,
      column: 0
    });
    this.__name = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlant.add(this.__name, {
      row: 0,
      column: 1
    });
    var labelMutant = new qx.ui.basic.Label().set({
      value: "Mutant :",
      rich: true
    });
    groupBoxInfoPlant.add(labelMutant, {
      row: 2,
      column: 0
    });
    this.__mutant = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlant.add(this.__mutant, {
      row: 2,
      column: 1
    });
    var labelGeneMutant = new qx.ui.basic.Label().set({
      value: "Gène mutant :",
      rich: true
    });
    groupBoxInfoPlant.add(labelGeneMutant, {
      row: 4,
      column: 0
    });
    this.__geneMutant = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlant.add(this.__geneMutant, {
      row: 4,
      column: 1
    });
    var labelCondCul = new qx.ui.basic.Label().set({
      value: "Condition(s) de culture :",
      rich: true
    });
    groupBoxInfoPlant.add(labelCondCul, {
      row: 5,
      column: 0
    });
    this.__condition = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    });
    groupBoxInfoPlant.add(this.__condition, {
      row: 5,
      column: 1
    });
    var labelTrait = new qx.ui.basic.Label().set({
      value: "Traitement(s) :",
      rich: true
    });
    groupBoxInfoPlant.add(labelTrait, {
      row: 6,
      column: 0
    });
    this.__traitement = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    });
    groupBoxInfoPlant.add(this.__traitement, {
      row: 6,
      column: 1
    });
    this._add(groupBoxInfoPlant, {
      row: 2,
      column: 0,
      colSpan: 2
    });
    var groupBoxInfoPlace = new qx.ui.groupbox.GroupBox(this.tr("Info Place"));
    groupBoxInfoPlace.setLayout(grid3);
    var labelTypeCont = new qx.ui.basic.Label().set({
      value: "Type de contenant :",
      rich: true
    });
    groupBoxInfoPlace.add(labelTypeCont, {
      row: 1,
      column: 0
    });
    this.__contenant = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlace.add(this.__contenant, {
      row: 1,
      column: 1
    });
    var labelTypeBox = new qx.ui.basic.Label().set({
      value: "Type de Boite :",
      rich: true
    });
    groupBoxInfoPlace.add(labelTypeBox, {
      row: 2,
      column: 0
    });
    this.__boxType = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlace.add(this.__boxType, {
      row: 2,
      column: 1
    });
    var labelBox = new qx.ui.basic.Label().set({
      value: "Boite :",
      rich: true
    });
    groupBoxInfoPlace.add(labelBox, {
      row: 3,
      column: 0
    });
    this.__box = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlace.add(this.__box, {
      row: 3,
      column: 1
    });
    var labelEmpl = new qx.ui.basic.Label().set({
      value: "Emplacement :",
      rich: true
    });
    groupBoxInfoPlace.add(labelEmpl, {
      row: 4,
      column: 0
    });
    this.__placeBox = new qx.ui.basic.Label().set({
      font: "bold"
    });
    groupBoxInfoPlace.add(this.__placeBox, {
      row: 4,
      column: 1
    });
    this._add(groupBoxInfoPlace, {
      row: 3,
      column: 0,
      colSpan: 2
    });
    this.__locateBox = new qx.data.Array();

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();

    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */

    //récupérations des prelevements
    this.__autoBox.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var filtredText = loadData;
        this.__services.QueryAllPrelevmentsGroupFiltred("getAllPrelevmentsGroupFiltred", this.__sessionId, filtredText);
      };
    }, this);

    this.__services.addListener("getAllPrelevmentsGroupFiltred", function(e) {
      this.__listPrelev = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listPrelev.push({
          'label': loadData[i]['code_contenant'],
          'id': loadData[i]['id_prelevement'],
          'id_prelevment': loadData[i]['id_prelevement'],
          'id_container': loadData[i]['id_container'],
          'code_contenant': loadData[i]['code_contenant'],
          'remarque': loadData[i]['remarque'],
          'type_contenant': loadData[i]['type_contenant']
        });
      };
      this.__autoBox.setListModel(this.__listPrelev);
    }, this);

    this.__autoBox.addListener("changeAllValues", function() {
      this.idPrelevement = this.__autoBox.getAllValues().getId();
      this.idContainer = this.__autoBox.getAllValues().getId_container();
      this.__code.setValue(this.__autoBox.getAllValues().getCode_contenant());
      if (this.__autoBox.getAllValues().getRemarque() != null) {
        this.__remarque.setValue(this.__autoBox.getAllValues().getRemarque());
      } else {
        this.__remarque.setValue("Aucune")
      };
      this.__contenant.setValue(this.__autoBox.getAllValues().getType_contenant());
      var tab = {};
      tab['idPrelevement'] = this.idPrelevement
      this.__services.QueryGeneticsInformations("loadedGeneticsInformations", this.idPrelevement, null);
      var context = "'sample : plants informations'";
      this.__services.QueryPlantsInformations("loadedNotationsLotsforPrelev", this.idPrelevement, context);
      this.__services.QueryGetProtocolCultureLotByPrelevment("getProtocolCultureLotByPrelevment", this.__sessionId, tab);
      this.__services.QueryPrelevementsInformations("loadedPrelevementsInformations", this.idPrelevement, "'sample : prelevment informations'");
      this.__services.fireDataEvent("getListContainerAction", this.idContainer, 0);
    }, this);

    //chargement les informations suivantes : date de prelevement, fond genetique, gene mute, mutant et le nom
    this.__services.addListener("loadedGeneticsInformations", function(e) {
      var loadData = e.getData();
      var date = loadData['date prelevement'];
      var tabDate = date.split("-");
      if (tabDate[1] == "01") {
        tabDate[1] = "Janvier"
      };
      if (tabDate[1] == "02") {
        tabDate[1] = "Février"
      };
      if (tabDate[1] == "03") {
        tabDate[1] = "Mars"
      };
      if (tabDate[1] == "04") {
        tabDate[1] = "Avril"
      };
      if (tabDate[1] == "05") {
        tabDate[1] = "Mai"
      };
      if (tabDate[1] == "06") {
        tabDate[1] = "Juin"
      };
      if (tabDate[1] == "07") {
        tabDate[1] = "Juillet"
      };
      if (tabDate[1] == "08") {
        tabDate[1] = "Août"
      };
      if (tabDate[1] == "09") {
        tabDate[1] = "Septembre"
      };
      if (tabDate[1] == "10") {
        tabDate[1] = "Octobre"
      };
      if (tabDate[1] == "11") {
        tabDate[1] = "Novembre"
      };
      if (tabDate[1] == "12") {
        tabDate[1] = "Décembre"
      };
      date = tabDate[2] + ' ' + tabDate[1] + ' ' + tabDate[0];
      this.__date.setValue(date);
      this.__fondGenetique.setValue(loadData['fond genetique']);
      this.__name.setValue(loadData['name']);
      if (loadData['mutant'] != null) {
        this.__mutant.setValue(loadData['mutant']);
      } else {
        this.__mutant.setValue("Aucun");
      };
      if (loadData['gene mute'] != null) {
        this.__geneMutant.setValue(loadData['gene mute']);
      } else {
        this.__geneMutant.setValue("Aucun");
      };
    }, this);

    //chargement des informations sur les conditions de culture
    this.__services.addListener("getProtocolCultureLotByPrelevment", function(e) {
      var loadData = e.getData();
      if (loadData[0]) {
        if (loadData[0]['name'].length < 1) {
          this.__condition.setValue("Non renseigné");
        } else {
          var label = '';
          label = " - " + loadData[0]['name']
          this.__condition.setValue(label)
        };
      };
    }, this);

    //chargement des informations sur les conditions de culture et de traitements
    this.__services.addListener("loadedNotationsLotsforPrelev", function(e) {
      var loadData = e.getData();
      if (loadData['conditions de culture'][0]) {
        var label = ''
        var typcond = loadData['conditions de culture'][0];
        for (var i = 0; i < loadData['conditions de culture'].length; i++) {
          if (typcond == loadData['conditions de culture'][i]) {
            label = " - " + loadData['conditions de culture'][i];
          } else {
            label = label + "</br>" + "</br>" + "-" + loadData['conditions de culture'][i];
          };
        };
        this.__condition.setValue(label);
      };
      if (loadData['traitement particulier'].length < 1) {
        this.__traitement.setValue(("Aucun"));
      } else {
        var label = ''
        var typtrait = loadData['traitement particulier'][0];
        for (var i in loadData['traitement particulier']) {
          if (typtrait == loadData['traitement particulier'][i]) {
            label = " - " + loadData['traitement particulier'][i];
          } else {
            label = label + "</br>" + "</br>" + " - " + loadData['traitement particulier'][i];
          };
        };
        this.__traitement.setValue(label)
      };
    }, this);


    // chargement des informations sur les experimentateur
    this.__services.addListener("allUserByPrelevment", function(e) {
      var loadData = e.getData();
      if (loadData[0].firstname.length < 1) {
        this.__user.setValue("Non renseigné");
      } else {
        var label = '';
        var id_user = loadData[0].lastname;
        for (var i in loadData) {
          if (id_user == loadData[i].lastname) {
            label = loadData[i].lastname + " " + loadData[i].firstname
          } else {
            label = label + "</br>" + loadData[i].lastname + " " + loadData[i].firstname
          };
        };
        this.__user.setValue(label);
      };
      this.__services.QueryGetContainerPlace("getContainerPlace", null, this.idContainer);
    }, this);

    // chargement des informations sur le nombres de plantes, les organes de prelevement, la quantité prelevé et le stade de développement
    this.__services.addListener("loadedPrelevementsInformations", function(e) {
      var loadData = e.getData();
      this.__organe.setValue(loadData['organe']);
      if (loadData['quantité prélevée'].length < 1) {
        this.__quantity.setValue(this.tr("Non renseigné"));
      } else {
        var lotQuantity = loadData['quantité prélevée'];
        this.__quantity.setValue(lotQuantity[lotQuantity.length - 1]);
      };
      if (loadData['stade de développement'].length < 1) {
        this.__stade.setValue(this.tr("Non renseigné"));
      } else {
        var lotStade = loadData['stade de développement'];
        this.__stade.setValue(lotStade[lotQuantity.length - 1]);
      };
      if (loadData['nombre de plantes'].length < 1) {
        this.__numberPlant.setValue(this.tr("Non renseigné"));
      } else {
        var lotNumberPlant = loadData['nombre de plantes'];
        this.__numberPlant.setValue(lotNumberPlant[lotQuantity.length - 1]);
      };
      if (loadData['unité'].length < 1) {
        this.__typeUnit.setValue(this.tr("Non renseigné"));
      } else {
        var lotTypeUnit = loadData['unité'];
        this.__typeUnit.setValue(lotTypeUnit[lotQuantity.length - 1]);
      };
      this.__services.QueryGetAllUserByPrelevment("allUserByPrelevment", this.idPrelevement, null);
    }, this);



    this.__services.addListener("getContainerPlace", function(e) {
      var loadData = e.getData();
      if (loadData[0]['boxtype'].length < 1) {
        this.__boxType.setValue(this.tr("Non renseigné"));
      } else {
        var boxType = loadData[0]['boxtype'];
        this.__boxType.setValue(boxType);
      };
      if (loadData[0]['boxname'].length < 1) {
        this.__boxName.setValue(this.tr("Non renseigné"));
      } else {
        this.__boxName = loadData[0]['boxname'];
      };
      if (loadData[0]['column'].length < 1) {
        this.__column.setValue(this.tr("Non renseigné"));
      } else {
        this.__column = loadData[0]['column'];
      };
      if (loadData[0]['row'].length < 1) {
        this.__row.setValue(this.tr("Non renseigné"));
      } else {
        this.__row = loadData[0]['row'];
      };
      this.__box.setValue(this.__boxName + ", colonne : " + this.__column + ", rang : " + this.__row);
      var tab = {};
      tab['filtredText'] = this.__boxName;
      tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
      this.__services.QueryGetPlaceBox("getPlaceBox", null, tab);
    }, this);

    this.__services.addListener("getPlaceBox", function(e) {
      var loadData = e.getData();
      var nBox = 0;
      var label = '';
      var lab = '';
      for (var i in loadData) {
        if (loadData[i]['idbox'] != nBox) {
          label = '';
          nBox = loadData[i]['idbox'];
          label = " Boite : " + loadData[i]['name'] + " > " + loadData[i]['type'] + " : " + loadData[i]['valeur'] + " / ";
          lab = loadData[i]['type'] + " : " + loadData[i]['valeur'] + " / ";
        } else { //suite des informations
          label = label + loadData[i]['type'] + " : " + loadData[i]['valeur'] + " / ";
          lab = lab + loadData[i]['type'] + " : " + loadData[i]['valeur'] + " / ";
        };
      };
      this.__placeBox.setValue(lab);
      if (label != '') {
        var tempItem = new qx.ui.form.ListItem(label);
        tempItem.setUserData("label", label);
        tempItem.setUserData("idBox", loadData[i]['idbox']);
        this.__locateBox = tempItem;
      };
    }, this);

    this.__box.addListener("mouseover", function() {
      this.setCursor("pointer");
    }, this);

    this.__box.addListener("click", function() {
      this.__showMapBox(this.__locateBox.getUserData("idBox"), this.__locateBox.getUserData("label"));
    }, this);

    this.__box.addListener("mouseout", function() {
      this.setCursor("default");
    }, this);

  }
});