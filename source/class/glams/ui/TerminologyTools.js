/**
 * This is the widget to get information from terminology
 * Fabrice Dupuis
 * Avril 2018
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.TerminologyTools", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * get information about the liste of concept at the right
     */
    "findLeftConcept": "qx.event.type.Data"

  },

  properties: {
    //name of the terminology
    name: {
      init: null,
      event: "changeName"
    },
    //racine du graphe
    racine: {
      init: null,
      event: "changeRacine"
    },
    //feuille du graphe
    feuille: {
      init: null,
      event: "changeFeuille"
    },
    //context de la terminologie
    context: {
      init: null,
      event: "changeContext"
    },
    //liste des concept et termes de la terminologie
    listTerms: {
      init: null,
      event: "changeListTerms"
    } 
  },

  members: {
    __listConcept: [],
    __listRelation: [],
    __listeRacine: [],
    __listeFeuille: [],

    //Trouve les racines et feuilles dans la liste des concepts
    calculRacine: function() {
      var minLeft = 100;
      var minRight = 100;
      this.__listeRacine = [];
      this.__listeFeuille = [];
      //pour chaque concept teste le nombre de fois où il est en partie droite et gauche
      for (var i in this.__listConcept) {
        var conceptId = this.__listConcept[i]['concept_id'];
        //teste le nombre de fois où il est en partie droite
        var compteur = 0;
        for (var j in this.__listRelation) {
          if (this.__listRelation[j]['id_right_concept'] == conceptId) {
            compteur = compteur + 1;
          };
        };
        if (compteur < minRight) {
          minRight = compteur
        };
        this.__listConcept[i]['compteur_right'] = compteur;
        //teste le nombre de fois où il est en partie gauche
        var compteur = 0;
        for (var k in this.__listRelation) {
          if (this.__listRelation[k]['id_left_concept'] == conceptId) {
            compteur = compteur + 1;
          };
        };
        if (compteur < minLeft) {
          minLeft = compteur
        };
        this.__listConcept[i]['compteur_left'] = compteur;

      };
      //Retrouve les racines et feuilles dans la liste des concepts
      for (var i in this.__listConcept) {
        if (this.__listConcept[i]['compteur_left'] == minLeft) {
          this.__listeRacine.push(this.__listConcept[i]);
        };
        if (this.__listConcept[i]['compteur_right'] == minRight) {
          this.__listeFeuille.push(this.__listConcept[i]);
        };
      };
      this.setRacine(this.__listeRacine);
      this.setFeuille(this.__listeFeuille);
    },
    //renvoie les concepts à gauche du concept
    findLeftConcept: function(conceptId) {
      var leftConcept = [];
      for (var i in this.__listRelation) {
        //console.log(this.__listRelation[i]);
        if (conceptId == this.__listRelation[i]['id_right_concept']) {
          for (var l in this.__listConcept) {
            if (this.__listRelation[i]['id_left_concept'] == this.__listConcept[l]['concept_id']) {
              leftConcept.push(this.__listConcept[l]);
            };
          };
        };
      };
      this.fireDataEvent("findLeftConcept", leftConcept);
    }
  },


  construct: function() {
    this.base(arguments);
    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    //console.log("init TerminologyTools");
    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__services.QueryGetTerminologyContextByName("getContextByName", 'Général');


    /*
    *****************************************************************************
     LISTENER
    *****************************************************************************
    */
    this.addListener("changeName", function(e) {
      //var idContext = 16 //contexte Général
      this.__services.addListener("getContextByName", function(e) {
        var idContext = e.getData()['id'];
        this.setContext(idContext);
        this.__services.QueryGetTermsByTerminologyName("getTermsByTerminologyName", this.getName(), idContext);
        this.__services.QueryGetTerminologyByName("getTerminologyByName", this.getName());
      }, this);
    }, this);
    //recupère le id de la terminologie pour avoir les relations et le graph
    this.__services.addListener("getTerminologyByName", function(e) {
      var loadData = e.getData();
      var id = e.getData()['id'];
      this.__services.QueryGetConceptGraphByTerminology("getConceptGraphByTerminology", id);
    }, this);


    this.__services.addListener("getTermsByTerminologyName", function(e) {
      this.__listConcept = [];
      var loadData = e.getData("getTermsByTerminologyName");
      for (var i in loadData) {
        //console.log(loadData[i]);
        this.__listConcept.push(loadData[i]);
      };
      this.setListTerms(this.__listConcept);
    }, this);
    this.__services.addListener("getConceptGraphByTerminology", function(e) {
      this.__listRelation = [];
      var loadData = e.getData();
      for (var i in loadData) {
        this.__listRelation.push(loadData[i]);
      };
    }, this);


    this.__services.addListener("getTermsByTerminologyName", function(e) {
      this.__services.addListener("getConceptGraphByTerminology", function(e) {
        this.calculRacine();
      }, this);
    }, this);
    this.__services.addListener("getConceptGraphByTerminology", function(e) {
      this.__services.addListener("getTermsByTerminologyName", function(e) {
        this.calculRacine();
      }, this);
    }, this);
  }
});
