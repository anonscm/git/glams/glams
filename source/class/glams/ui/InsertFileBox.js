/**
 * Formulaire de charger les boites dans la BD à partir d'un fichier
 * Fabrice Dupuis mai 2018
 * modifié juin 2018 pour consulter places via onthologie
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.InsertFileBox",{
   extend : qx.ui.core.Widget,
	
   events : {
       /**
     * Fired when at the end of the validation.
    */
    "validateEnd" : "qx.event.type.Data",

    /**
     * Fired at the end of the import.
    */
    "end" : "qx.event.type.Data",

    /**
     * Fired when a line is saved.
    */
    "lineSaved" : "qx.event.type.Data",

    /**
     * Fired when all the data is retrieved.
    */
    "getDataEnd" : "qx.event.type.Data"
  },
	
  properties : {
  /**
  * id of the web session 
  */
    __sessionId : {
      init : null
    }
  },
	
  members : {    
    __boxNames : [],
    __typeLieux : [],
    __listeBoites : [],
    __typeBox : [],
    
    /**
     * get database data (lieux, noms)
    */
    __getDbData : function(){

      var writeGroup = JSON.parse(localStorage.getItem('writeGroups'));
      var tab={};
      tab['writeGroup'] = writeGroup[0];   

        //liste des boites existantes
    this.__services.QueryGetAllBox("getAllBox", this.__sessionId,tab); 
    this.__services.addListenerOnce("getAllBox", function(e) {
      this.__boxNames=[];
      if (e.getData().length > 0){
        for (var i in e.getData()){
          var name= e.getData()[i]['namebox'];
          this.__boxNames.push(name);
        };      
      };
      //liste des types de lieux    (sur onthologie)
      this.__services.QueryGetAlltypesLieuxO("getAlltypesLieuxO", null);
      this.__services.addListenerOnce("getAlltypesLieuxO", function(e) {
        this.__typeLieux=[];
        if (e.getData().length > 0){
          for (var i in e.getData()){
            var type= e.getData()[i]['term_label'];
            this.__typeLieux.push(type);
          };      
        };
        //liste des types de boites    
        this.__services.QueryGetAllTypesBox("getAllTypesBox", null);
        this.__services.addListenerOnce("getAllTypesBox", function(e) {
          this.__typeBox=[];
          if (e.getData().length > 0){
            for (var i in e.getData()){
              var type= e.getData()[i]['name'];
              this.__typeBox.push(type);
            };      
          };        
        
//          this.fireDataEvent("getDataEnd", this);
          this.__validate(this.__cols, this.__lines);
        }, this);
      }, this);
    }, this);
    },   
    /**
     * Validate the file.
     *@param cols {Array} columns
     *@param lines {Array} lines
    */
    __validate : function(cols, lines){
//      var dataTV = new glamssample.data.DataTypeValidator();
      var errorMsg = "";
      var listColumn = ["boite","date","type"];
      this.__listeBoites = [];
      //validation colonnes
      for (var i in cols){
        if (listColumn.indexOf(cols[i]) < 0 && cols[i].indexOf("lieu:") < 0 ) {
            errorMsg += "Nom de colonne \"" + cols[i] + "\"non reconnu.\n"
        } ; 
        //Test des types de lieux
        if (cols[i].indexOf("lieu:") > -1){
          var typeLieu = cols[i].split(":")[1];
          if (this.__typeLieux.indexOf(typeLieu) < 0) {
            errorMsg += "Erreur type lieu : " + typeLieu + "\n";
          };
        };
      };
      //validation des cellules    
      for (var i in lines) {
        for (var j in lines[i]){
          //Valeur de la cellule
          var cellValue = lines[i][j];
          //console.log(i+" "+j+" "+cellValue);
          if (cellValue != ""){
//            //Test des dates
//            if (cols[j] == "date") {
//              if (!dataTV.isDate(cellValue)) {
//                errorMsg += ("Mauvais format de la date :" + cellValue + "\n");
//              };
//            };
                   
            //nom de la boite existant
            if (cols[j] == "boite") {
              if (this.__boxNames.indexOf(cellValue) > -1) {
                errorMsg += "Nom de boite existant : " + cellValue + "\n";
              };
            };
            //redondance dans les noms de boite      
            if (cols[j] == "boite") {
              if (this.__listeBoites.indexOf(cellValue) > -1) {
                errorMsg += "Nom de boite redondant : " + cellValue + "\n";
              }else{
                this.__listeBoites.push(cellValue);
              };
            };
            //type de la boite existant
            if (cols[j] == "type") {
              if (this.__typeBox.indexOf(cellValue) < 0) {
                errorMsg += "Type de boite inconnu : " + cellValue + "\n";
              };
            };            
                        
          };
        };
      };
      this.fireDataEvent("validateEnd", errorMsg);
    }
  },
	
	construct : function(){
		this.base(arguments);
		
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
		//Containers
    this._setLayout(new qx.ui.layout.Grow());
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._add(this.__container);

    //fonctions sur File Input output
    var fileChooser = new qxfileio.FileChooser();
    var fileReader = new qxfileio.FileReader();
    //message
    var message = new qx.ui.basic.Label("Pour cette procédure le nom des boites doit être unique");
    this.__container.add(message);
    
    //Button ouvrir fichier
    this.__fileButton = new qx.ui.form.Button("Ouvrir fichier...");
    this.__container.add(this.__fileButton);

    //Affichage nom du fichier
    var labelFile = new qx.ui.basic.Label("");
    this.__container.add(labelFile);

    //Bouton insérer
    this.__insertButton = new qx.ui.form.Button("Insérer");
    this.__container.add(this.__insertButton);

    //Boite affichage des messages d'erreurs
    this.__textArea = new qx.ui.form.TextArea();
    this.__container.add(this.__textArea);
    this.__textArea.setHeight(300);
    this.__textArea.setWidth(300);
    this.__textArea.setReadOnly(true);

    //Loading gif
    var loading = new qx.ui.basic.Image("glams/loading.gif");
    loading.setAlignX('center');
    this.__container.add(loading);
    loading.exclude();
        
		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
         

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */     
    //Bouton choix du fichier
    this.__fileButton.addListener("execute", function() {
      fileChooser.open();
    }, this);
    
     //Ouverture de la fenêtre de choix de fichier
    fileChooser.addListener("filesChange", function(e) {
      if (e.getData().length > 0){
        this.__file = e.getData()[0];
        labelFile.setValue(e.getData()[0]["name"]);
      }
    }, this);
    //Bouton insérer
    this.__insertButton.addListener("execute", function()    {
      this.__fileButton.setEnabled(false);
      this.__insertButton.setEnabled(false);
      fileReader.loadAsText(this.__file);
    }, this);
    //FileReader
    fileReader.addListener("load", function(e){
      loading.show(); 
      //code de fin de ligne pour les fichiers unix "\n" ou les fichiers windows "\r"     
      if (e.getData().indexOf("\r")==-1 ) { 
        var tabFile = e.getData().split("\n")
      }else{ 
        var tabFile = e.getData().split("\r\n")
      };

      //Noms de colonnes
      var cols = tabFile[0].split("\t");
      //Données
      var lines = [];
      for (var i = 1; i < tabFile.length; i++){
        var line = tabFile[i].split("\t");
        lines.push(line);
      }; 
      this.__lines = lines; //ensemble de toutes les lignes du fichier
      this.__cols = cols;   //ensemble de toutes les colonnes du fichier
      this.__getDbData();   //recupère les données control de la base
    }, this);
    
//    //début de la validation
//    this.addListener("getDataEnd", function() {
//      this.__validate(this.__cols, this.__lines);
//    }, this);
//    
    //validation réalisée
    this.addListener("validateEnd", function(e) {
//      console.log("validation réalisée");
      var writeGroup = JSON.parse(localStorage.getItem('writeGroups'));
      if (e.getData() == "") {
        var tab={};
        tab['cols']=this.__cols;
        tab['lines']=this.__lines;
        tab['writeGroup'] = writeGroup[0];
        this.__services.QueryCreateBoxs("createBoxs", this.__sessionId,tab);            
        this.__services.addListener("createBoxs", function(e) {
          this.fireDataEvent("end",e.getData());
        },this); 
      } else {
        this.fireDataEvent("end", e.getData());
      }
    },this)
    
    //Fin de l'import du fichier'
    this.addListener("end", function(e){
      this.__fileButton.setEnabled(true);
      this.__insertButton.setEnabled(true);
      loading.exclude();
      this.__textArea.show();
      this.__textArea.setValue(e.getData());
    }, this);
    
    
    
	}
});
