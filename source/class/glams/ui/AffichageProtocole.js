/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new lot from an variete lot in the database.
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.AffichageProtocole", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * Protocole to display
     */
    protocole: {
      init: null,
      event: "changeProtocole"
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(15);
    this._setLayout(layout);

    //Implémentation des widget visible à l'écran
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du Protocole : ")), {
      row: 0,
      column: 0
    });
    this.__nameProtocole = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__nameProtocole, {
      row: 0,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence du Protocole : ")), {
      row: 1,
      column: 0
    });
    this.__refProtocole = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__refProtocole, {
      row: 1,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Résumé du Protocole : ")), {
      row: 2,
      column: 0
    });
    this.__summaryProtocole = new qx.ui.basic.Label().set({
      padding: 5,
      rich: true,
      textAlign: "justify",
      selectable: true,
      width: 350
    });
    this._add(this.__summaryProtocole, {
      row: 2,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Adresse de stockage du Protocole : ")), {
      row: 3,
      column: 0
    });
    this.__adresseProtocole = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__adresseProtocole, {
      row: 3,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence Alfresco du Protocole : ")), {
      row: 4,
      column: 0
    });
    this.__alfrescoProtocole = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__alfrescoProtocole, {
      row: 4,
      column: 1
    });
    // var btnUpdate = new qx.ui.form.Button("Editer le Protocole");
    // container._add(btnUpdate, {
    //   row: 5,
    //   column: 0
    // });

    //Affiche les données relatives au protocol selectionné
    this.addListener("changeProtocole", function(e) {
      var loadData = e.getData();
      this.__nameProtocole.setValue(loadData.getLabel());
      this.__refProtocole.setValue(loadData.getRefLab());
      this.__summaryProtocole.setValue(loadData.getSummary());
      if (loadData.getAdress() != null) {
        this.__adresseProtocole.setValue(loadData.getAdress());
      } else {
        this.__adresseProtocole.setValue("Non renseigné");
      };
      if (loadData.getRefAl() != null) {
        this.__alfrescoProtocole.setValue(loadData.getRefAl());
      } else {
        this.__alfrescoProtocole.setValue("Non renseigné");
      };
    }, this);

    // //Ouvre la fenêtre d'édition du protocole
    // btnUpdate.addListener("execute", function() {
    //   var newContainer = new glams.ui.EditProtocol();
    //   // Permet d'envoyer les données du protocole a modifier dans la classe EditProtocol
    //   newContainer.setProtocole(this.__protocole.getAllValues());
    //   var container2 = new qx.ui.container.Composite(new qx.ui.layout.VBox());
    //   container2.add(newContainer);
    //   var window = new qx.ui.window.Window("Edit Protocol");
    //   var layout = new qx.ui.layout.VBox();
    //   window.setLayout(layout);
    //   window.add(container2);
    //   window.open();
    //   window.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
    //   //fonction de fermeture de la fenêtre d'édition du protocole
    //   newContainer.addListener("fermetureEditProtocol", function() {
    //     window.close();
    //   }, this);
    //   //Fonction qui recupère les données du protocole modifié
    //   newContainer.addListener("updateProt", function(e) {
    //     var loadData = e.getData();
    //     this.__nameProtocole.setValue(loadData.name);
    //     this.__refProtocole.setValue(loadData.referenceLaboratoryHandbook);
    //     this.__summaryProtocole.setValue(loadData.summary);
    //     if (loadData.adress != null) {
    //       this.__adresseProtocole.setValue(loadData.adress);
    //     } else {
    //       this.__adresseProtocole.setValue("Non renseigné");
    //     };
    //     if (loadData.reference_alfresco != null) {
    //       this.__alfrescoProtocole.setValue(loadData.reference_alfresco);
    //     } else {
    //       this.__alfrescoProtocole.setValue("Non renseigné");
    //     };
    //   }, this);
    // }, this);

  },

  members: {
    /**
     * __nameProtocole
     * @type {qx.ui.basic.Label}
     */
    __nameProtocole: null,
    /**
     * __refProtocole
     * @type {qx.ui.basic.Label}
     */
    __refProtocole: null,
    /**
     * __summaryProtocole
     * @type {qx.ui.basic.Label}
     */
    __summaryProtocole: null,
    /**
     * __adresseProtocole
     * @type {qx.ui.basic.Label}
     */
    __adresseProtocole: null,
    /**
     * __alfrescoProtocole
     * @type {qx.ui.basic.Label}
     */
    __alfrescoProtocole: null

  }
});