/**
 * Permet d'affecter les groupes de lecture et d'écriture
*/
qx.Class.define("glams.ui.GroupsChoice",
{
  extend : qx.ui.core.Widget, 
  events :{
  /**
  * Fire when thee group change
  */
     "groupChanged" : "qx.event.type.Event"
  },
  construct : function(){
    this.base(arguments);
   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */   
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    /*-- UI drawing --*/                
    //container global
    this.__containerAll = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    //container listes
    this.__containerList = new qx.ui.container.Composite(new qx.ui.layout.HBox(10));    
    
    //container groupe lecture
    this.__containerGR = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this.__containerGR.add(new qx.ui.basic.Label("liste des groupes en lecture"));
    //container groupe écriture
    this.__containerGW = new qx.ui.container.Composite(new qx.ui.layout.VBox(10));
    this.__containerGW.add(new qx.ui.basic.Label("liste des groupes en écriture"));    
    // button de validation
    this.__validateButton = new qx.ui.form.Button("Valider")
    this.__validateButton.setMaxWidth(110); 
    this.__validateButton.setAlignX("right");    
   
    /* -- Data Management --*/
    this.__allGroupes= []; //liste de dictionnaire contenant toutes les informations sur les groupes

    //fenêtre affichage
    this.__window = new qx.ui.window.Window("List of groups");      
    var layout = new qx.ui.layout.VBox();
    this.__window.setLayout(layout);
    this.__window.add(this.__containerAll);
    this.__window.moveTo(Math.round(550+Math.random()*80),Math.round(70+Math.random()*80)); 
    
    
    
   /*
    * Listener
    */    
    
    session.addListener("authenticationSucced", function(e) {
      session.requestUserInfo();
      session.requestGroupsInfo();
      session.addListener("gotGroupsInfo", function(e) { 
        var listeGroups = e.getData();
        for (var g in listeGroups) {
          var gr={}
          gr['name']=listeGroups[g]['name'];
          gr['id']=listeGroups[g]['id'];
          gr['active']=listeGroups[g]['active'];
          gr['description']=listeGroups[g]['description'];          
          this.__allGroupes.push(gr);
        }; 
        this.__viewGroups();
        this.__window.open();
      },this);
    }, this);

             
    /*-- Menu actions --*/  
    this.__validateButton.addListener("execute", function(){
        var readGroups=[];
        var writeGroups=[];
        for (var i in this.__allGroupes){
           this.__allGroupes[i]['selectedWrite']=this.__allGroupes[i]['cbxWrite'].getValue();
           this.__allGroupes[i]['selectedRead']=this.__allGroupes[i]['cbxRead'].getValue();       
           if (this.__allGroupes[i]['selectedWrite']) {writeGroups.push(this.__allGroupes[i]['id'])};
           if (this.__allGroupes[i]['selectedRead']) {readGroups.push(this.__allGroupes[i]['id'])};
        };
        localStorage.setItem('readGroups', JSON.stringify(readGroups));
        localStorage.setItem('writeGroups', JSON.stringify(writeGroups));
        this.fireEvent("groupChanged");   
        this.__window.close();   
        
//        console.log('readGroups ' +JSON.parse(localStorage.getItem('readGroups')));
//        console.log('writeGroups ' +JSON.parse(localStorage.getItem('writeGroups'))); 
//        console.log('readGroups ' +localStorage.getItem('readGroups'));
//        console.log('writeGroups ' +localStorage.getItem('writeGroups'));        
//        var rg = JSON.parse(localStorage.getItem('writeGroups'))
//        console.log( rg[0] )
        }, this);
  },
  members : {
    /**
    * ouverture de la fenêtre
    */ 
    showGroups : function () {
      this.__window.open();
    },
    /**
    * affiche les groupe dans la fenêtre
    */ 
    __viewGroups : function () {
       for ( var i in this.__allGroupes){       
       var cb = new qx.ui.form.CheckBox (this.__allGroupes[i]['name']);
       cb.setValue(true);
       this.__allGroupes[i]['selectedRead']=true;
       this.__allGroupes[i]['cbxRead']=cb;
       this.__containerGR.add(cb);             
    };    

    for ( var i in this.__allGroupes){       
       var cb = new qx.ui.form.CheckBox (this.__allGroupes[i]['name']);
       cb.setValue(true);
       this.__allGroupes[i]['cbxWrite']=cb;
       this.__containerGW.add(cb);        
    }; 

    
    this.__containerList.add(this.__containerGR);
    this.__containerList.add(this.__containerGW);
    this.__containerAll.add(this.__containerList);
    this.__containerAll.add(this.__validateButton)  
    }    
    
  }
});
