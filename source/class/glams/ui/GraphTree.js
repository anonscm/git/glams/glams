/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Draw the tree graph.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.GraphTree", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Event to Draw the graph
     */
    "draw": "qx.event.type.Event",
    /**
     * Event to display information about the clicked container
     */
    "affichage": "qx.event.type.Data"
  },

  properties: {
    /**
     * JSON to draw the tree graph
     */
    dataTree: {
      init: null,
      event: "changeDataTree"
    }

  },

  members: {

  },

  construct: function() {
    this.base(arguments);
    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();
    this._setLayout(new qx.ui.layout.Grow());
    this.__d3 = new qxd3.Svg();
    this._add(this.__d3);

    var d3 = this.__d3.getD3();
    this.setMinWidth(1200);
    this.setMinHeight(500);
    var margin = {
        top: 20,
        right: 50,
        bottom: 20,
        left: 60
      },
      width = 1200 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin

    var svgRoot = this.__d3.getD3SvgNode();
    svgRoot.selectAll("*").remove();

    var svg = svgRoot.append("g")
      .attr("width", width + margin.right + margin.left)
      .attr("height", height + margin.top + margin.bottom)

      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.addListener("changeDataTree", function() {
      var loadData = this.getDataTree();
      this.__dataTree = loadData;
      this.fireEvent("draw");
    }, this);

    this.addListener("draw", function() {

      var that = this;
      var i = 0;

      // declares a tree layout and assigns the size
      var tree = d3.layout.tree().size([height, width]);

      var diagonal = d3.svg.diagonal()
        .projection(function(d) {
          return [d.y, d.x];
        });


      // Assigns parent, children, height, depth
      var duration = 750;
      var root = this.__dataTree;

      update(root);

      function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
          links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function(d) {
          d.y = d.depth * 180;
        });

        // Declare the nodes.
        var node = svg.selectAll("g.node")
          .data(nodes, function(d) {
            return d.id || (d.id = ++i);
          });

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
          d.x0 = d.x;
          d.y0 = d.y;
        });

        // Enter the nodes.
        var nodeEnter = node.enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) {
            return "translate(" + source.y0 + "," + source.x0 + ")";
          })
          .on("click", click)
          .on("dblclick", dblclick);

        nodeEnter.append("circle")
          .attr("r", 10)
          .style("stroke", "black")
          .style("stroke-width", function(d) {
            if (d.children) {
              return ("3px");
            };
          })
          .style("fill", function(d) {
            if (d.type == "container") {
              return ("red");
            };
            if (d.type == "groupe") {
              return ("green");
            };
            if (d.type == "experiment") {
              return ("steelblue");
            };
          });

        nodeEnter.append("text")
          .attr("x", function(d) {
            return d.children || d._children ? -13 : 13;
          })
          .attr("dy", "2em")
          .attr("text-anchor", "middle")
          .text(function(d) {
            return d.name;
          })

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
          .duration(duration)
          .attr("transform", function(d) {
            return "translate(" + d.y + "," + d.x + ")";
          });

        nodeUpdate.select("circle")
          .attr("r", 10)
          .style("stroke", "black")
          .style("stroke-width", function(d) {
            if (d.children || d._children) {
              return ("3px");
            };
          })
          .style("fill", function(d) {
            if (d.type == "container") {
              return ("red");
            };
            if (d.type == "groupe") {
              return ("green");
            };
            if (d.type == "experiment") {
              return ("steelblue");
            };
          });

        nodeUpdate.select("text")
          .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
          .duration(duration)
          .attr("transform", function(d) {
            return "translate(" + source.y + "," + source.x + ")";
          })
          .remove();
        nodeExit.select("circle")
          .attr("r", 1e-6);
        nodeExit.select("text")
          .style("fill-opacity", 1e-6);

        // Update the links...
        var link = svg.selectAll("path.link")
          .data(links, function(d) {
            return d.target.id;
          });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
          .attr("class", "link")
          .attr("d", function(d) {
            var o = {
              x: source.x0,
              y: source.y0
            };
            return diagonal({
              source: o,
              target: o
            });
          })
          .style("stroke",
            function(d) {
              if (d.target.action != null) {
                return ("red");
              } else {
                return ("black");
              };
            })
          .style("fill", "none");

        // Transition links to their new position.
        link.transition()
          .duration(duration)
          .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
          .duration(duration)
          .attr("d", function(d) {
            var o = {
              x: source.x,
              y: source.y
            };
            return diagonal({
              source: o,
              target: o
            });
          })
          .remove();

      }
      // Toggle children on click.
      function click(d) {
        if (d.children) {
          d._children = d.children;
          d.children = null;
        } else {
          d.children = d._children;
          d._children = null;
        }
        update(d);
      }

      function dblclick(d) {
        if (d.type == "container") {
          var tab = {};
          tab['idContainer'] = d.id;
          that.__services.QueryGetInfosOnContainer("getInfosOnContainer", this.__sessionId, tab);
        };
      };
    }, this);

    this.__services.addListener("getInfosOnContainer", function(e) {
      var loadData = e.getData();
      console.log(loadData);
      // var newContainer = new glams.ui.VisualisationContainer();
      // this.setDataContainer(loadData);
      this.fireDataEvent("affichage", loadData);
      // var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
      // container.add(newContainer);
      // var window = new qx.ui.window.Window("Info Container");
      // var layout = new qx.ui.layout.VBox();
      // window.setLayout(layout);
      // window.add(container);
      // window.open();
      // window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
    }, this);

  }
});