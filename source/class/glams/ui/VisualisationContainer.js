/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Display the information about the clicked container.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.VisualisationContainer", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * data to show about the container clicked
     */
    dataContainer: {
      init: null,
      event: "changeDataContainer"
    }

  },

  members: {

  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */

    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(5);
    this._setLayout(layout);

    this._add(new qx.ui.basic.Label(
      this.tr("Nom de l'échantillon :")), {
      row: 0,
      column: 0
    });

    this.__code = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__code, {
      row: 0,
      column: 1
    });

    this._add(new qx.ui.basic.Label(
      this.tr("Type du container :")), {
      row: 1,
      column: 0
    });

    this.__typeContainer = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__typeContainer, {
      row: 1,
      column: 1
    });

    this._add(new qx.ui.basic.Label(
      this.tr("Nature de l'échantillon :")), {
      row: 2,
      column: 0
    });

    this.__contenuContainer = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__contenuContainer, {
      row: 2,
      column: 1
    });

    this._add(new qx.ui.basic.Label(
      this.tr("Date de création du container :")), {
      row: 3,
      column: 0
    });

    this.__date = new qx.ui.basic.Label().set({
      font: "bold"
    });
    this._add(this.__date, {
      row: 3,
      column: 1
    });

    this._add(new qx.ui.basic.Label(
      this.tr("Collaborateur(s):")), {
      row: 4,
      column: 0
    });

    this.__collaborateur = new qx.ui.basic.Label().set({
      font: "bold",
      rich: true
    });
    this._add(this.__collaborateur, {
      row: 4,
      column: 1
    });


    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.addListener("changeDataContainer", function() {
      var loadData = this.getDataContainer();
      this.__dataContainer = loadData;
      this.__code.setValue(this.__dataContainer.infos[0].code);
      this.__typeContainer.setValue(this.__dataContainer.infos[0].type);
      this.__contenuContainer.setValue(this.__dataContainer.infos[0].content);
      this.__date.setValue(this.__dataContainer.infos[0].date_on);
      if (this.__dataContainer.action[0]) {
        this.__collaborateur.setValue(this.__dataContainer.action[0].informations[0].firstname + " " + this.__dataContainer.action[0].informations[0].lastname + "\n");
        // }else if () {

      };
    }, this);


  }
});