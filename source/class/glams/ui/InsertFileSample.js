/**
 * Formulaire de charger les echantillons dans la BD à partir d'un fichier
 * Fabrice Dupuis décembre 2017
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.InsertFileSample",{
   extend : qx.ui.core.Widget,

   events : {
       /**
     * Fired when at the end of the validation.
    */
    "validateEnd" : "qx.event.type.Data",

    /**
     * Fired at the end of the import.
    */
    "end" : "qx.event.type.Data"

  },


  members : {
    __boxNames : [],
    __contentType : [],
    __containerLevel : [],
    __listTypeNot : [],
    __sessionId : null,

   /**
     * get database data (boite, type contenu, type contenant)
    */
    __getDbData : function(){


    //liste des boites existantes
    var writeGroup = JSON.parse(localStorage.getItem('writeGroups'));
    var tab={};
    tab['writeGroup'] = writeGroup[0];
    this.__services.QueryGetAllBox("getAllBox", this.__sessionId,tab);
    this.__services.addListenerOnce("getAllBox", function(e) {
      this.__boxNames=[];
      if (e.getData().length > 0){
        for (var i in e.getData()){
          var name= e.getData()[i]['namebox'];
          this.__boxNames.push(name);
        };
      };
      //liste des container level
      this.__services.QueryGetAllContainerLevel("getAllContainerLevel", null,null);
      this.__services.addListenerOnce("getAllContainerLevel", function(e) {
        this.__containerLevel=[];
        if (e.getData().length > 0){
          for (var i in e.getData()){
            var level= e.getData()[i]['level'];
            this.__containerLevel.push(level);
          };
        };

        //liste des types contenu
        this.__services.QueryGetAllContentType("getAllContentType", null, null);
        this.__services.addListenerOnce("getAllContentType", function(e) {
          this.__contentType=[];
          if (e.getData().length > 0){
            for (var i in e.getData()){
              var type= e.getData()[i]['type'];
              this.__contentType.push(type);
            };
          };

          //liste des types contenant
          this.__services.QueryAllTypeContenant("getAllTypeContenant", null, null);
          this.__services.addListenerOnce("getAllTypeContenant", function(e) {
            this.__contenairType=[];
            if (e.getData().length > 0){
              for (var i in e.getData()){
                var type= e.getData()[i]['type'];
                this.__contenairType.push(type);
              };
            };
            //liste des types de notations
            this.__services.QueryGetAllTypeNotationAll("getAllTypeNotationAll", null, null);
            this.__services.addListenerOnce("getAllTypeNotationAll", function(e) {
              this.__notationType=[];
              if (e.getData().length > 0){
                for (var i in e.getData()){
                  var type= e.getData()[i]['nom'];
                  this.__listTypeNot.push(type);
                };
              };
              //liste des experiments
              var writeGroup = JSON.parse(localStorage.getItem('writeGroups'));
              var tab={};
              tab['userGroup']= [writeGroup[0]];
              this.__services.QueryGetAllExperiments("getAllExperiments", null, tab);
              this.__services.addListenerOnce("getAllExperiments", function(e) {
                this.__experimentName=[];
                if (e.getData().length > 0){
                  for (var i in e.getData()){
                    var name= e.getData()[i]['name'];
                    this.__experimentName.push(name);
                  };
                };                                
                this.__validate(this.__cols, this.__lines);
              }, this);  
            }, this);
          }, this);
        }, this);
      }, this);
    }, this);
    },
    /**
     * Validate the file.
     *@param cols {Array} columns
     *@param lines {Array} lines
    */
    __validate : function(cols, lines){
//      var dataTV = new glamssample.data.DataTypeValidator();
      var errorMsg = "";
      var listColumn = ["boite","code","type contenant","type contenu","niveau","valide", "utilisable","remarque","colonne", "ligne", "date","expérience"];
      var listColumnFile=[];
      //validation colonnes
      for (var i in cols){
        listColumnFile.push(cols[i]);
        if (cols[i].substring(0,10)!="notationC:" ) {
          if (listColumn.indexOf(cols[i]) < 0) {
            errorMsg += "Nom de colonne \"" + cols[i] + "\"non reconnu.\n"
          };
        }else{
          var typeNot =cols[i].substring(10); 
         //teste si le type de notation existe
         if (this.__listTypeNot.indexOf(typeNot)<0){
           errorMsg += "Type de notation \"" + typeNot + "\"non reconnu.\n"
         };
        };
      };

      for (var i in listColumn){
        if (listColumnFile.indexOf(listColumn[i]) < 0) {
            errorMsg += "Nom de colonne \"" + listColumn[i] + "\" absent à ajouter.\n"
        };
      };
      //validation des cellules
      for (var i in lines) {
        for (var j in lines[i]){
          //Valeur de la cellule
          var cellValue = lines[i][j];
          if (cellValue == "" & cols[j] == "date" ){
            var ii=i+2;
            errorMsg += "ligne "+ii+" date obligatoire jj/mm/aaaa"+"\n";
          };
          if (cellValue != ""){
//            //Test des dates
//            if (cols[j] == "date") {
//              if (!dataTV.isDate(cellValue)) {
//                errorMsg += ("Mauvais format de la date :" + cellValue + "\n");
//              };
//            };

            //type contenu
            if (cols[j] == "type contenu") {
              if (this.__contentType.indexOf(cellValue) < 0) {
                errorMsg += "Erreur type contenu : " + cellValue + "\n";
              };
            };
            //type contenants
            if (cols[j] == "type contenant") {
              if (this.__contenairType.indexOf(cellValue) < 0) {
                errorMsg += "Erreur type contenant : " + cellValue + "\n";
              };
            };
            //valide
            if (cols[j] == "valide") {
              if (cellValue != "true" && cellValue != "false") {
                var ii=i+2;
                errorMsg += ("ligne "+ii+" La valeur valide doit être égale à true ou false.\n");
              };
            };
            //utilisable
            if (cols[j] == "utilisable") {
              if (cellValue != "true" && cellValue != "false") {
                var ii=i+2;
                errorMsg += ("ligne "+ii+"La valeur utilisable doit être égale à true ou false.\n");
              };
            };
            //nom de la boite
            if (cols[j] == "boite") {
              if (this.__boxNames.indexOf(cellValue) < 0) {
                errorMsg += "Nom de boite inconnu : " + cellValue + "\n";
              };
            };
            //nom de la boite
            if (cols[j] == "expérience") {
              if (this.__experimentName.indexOf(cellValue) < 0) {
                errorMsg += "Nom de l'expérience inconnu : " + cellValue + "\n";
              };
            };            
            //date on obligatoire
            if (cols[j] == "date") {
              if (cellValue.length <10 ) {
                var ii=i+2;
                errorMsg += "ligne "+ii+" date obligatoire jj/mm/aaaa"+"\n";
              };              
            };            
            //type de notation
            //TO DO verifier que le type de la valeur de notation est correcte            
            };
          };
        };
        //Non redondance des emplacements
        var listeEmplacements=[];
        for (var i in cols){
          if (cols[i]=="boite" ){ var colBoite=i };
          if (cols[i]=="colonne" ){ var colColonne=i };
          if (cols[i]=="ligne" ){ var colLigne=i };
        };
        for (var i in lines){
          if (lines[i][colColonne] != null ){
            var place= lines[i][colBoite]+' '+lines[i][colLigne]+' '+lines[i][colColonne];
            if(listeEmplacements.indexOf(place) ){
              listeEmplacements.push(place);
            }else{
              errorMsg += "Redondance d'emplacement : " + place + "\n";
            };
          };
        };
    this.fireDataEvent("validateEnd", errorMsg);
    }
  },

	construct : function(){
		this.base(arguments);

   /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();
		//Containers
    this._setLayout(new qx.ui.layout.Grow());
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
    this._add(this.__container);

    //fonctions sur File Input output
    var fileChooser = new qxfileio.FileChooser();
    var fileReader = new qxfileio.FileReader();

    //Button ouvrir fichier
    this.__fileButton = new qx.ui.form.Button("Ouvrir fichier...");
    this.__container.add(this.__fileButton);

    //Affichage nom du fichier
    var labelFile = new qx.ui.basic.Label("");
    this.__container.add(labelFile);

    //Bouton insérer
    this.__insertButton = new qx.ui.form.Button("Insérer");
    this.__container.add(this.__insertButton);

    //Boite affichage des messages d'erreurs
    this.__textArea = new qx.ui.form.TextArea();
    this.__container.add(this.__textArea);
    this.__textArea.setHeight(300);
    this.__textArea.setWidth(300);
    this.__textArea.setReadOnly(true);

//    //Loading gif
    var loading = new qx.ui.basic.Image("glams/loading.gif");
    loading.setAlignX('center');
    this.__container.add(loading);
    loading.exclude();


		/*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    this.__serviceBox = glams.services.Box.getInstance();




    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */




    //Bouton choix du fichier
    this.__fileButton.addListener("execute", function() {
      fileChooser.open();
    }, this);

     //Ouverture de la fenêtre de choix de fichier
    fileChooser.addListener("filesChange", function(e) {
      if (e.getData().length > 0){
        this.__file = e.getData()[0];
        labelFile.setValue(e.getData()[0]["name"]);
      }
    }, this);
    //Bouton insérer
    this.__insertButton.addListener("execute", function()    {
      this.__fileButton.setEnabled(false);
      this.__insertButton.setEnabled(false);
      fileReader.loadAsText(this.__file);
    }, this);
    //FileReader
    fileReader.addListener("load", function(e){
      loading.show();

      //code de fin de ligne pour les fichiers unix "\n" ou les fichiers windows "\r"
      if (e.getData().indexOf("\r")==-1 ) {
        var tabFile = e.getData().split("\n")
      }else{
        var tabFile = e.getData().split("\r\n")
      };

      //Noms de colonnes
      var cols = tabFile[0].split("\t");
      //Données
      var lines = [];
      for (var i = 1; i < tabFile.length; i++){
        var line = tabFile[i].split("\t");
        lines.push(line);
      }session.getApplicationId
      this.__lines = lines; //ensemble de toutes les lignes du fichier
      this.__cols = cols;   //ensemble de toutes les colonnes du fichier
      this.__getDbData();   //recupère les données control de la base

    }, this);


    //validation réalisée
    this.addListener("validateEnd", function(e) {
      if (e.getData() == "") {
        var tab={};
        tab['cols']=this.__cols;
        tab['lines']=this.__lines;
        var writeGroup = JSON.parse(localStorage.getItem('writeGroups'));
        tab['userGroup'] = writeGroup[0];
        this.__textArea.setValue("En cours de saisie des données .....");
        this.__services.QueryCreateSamples("createSamples", this.__sessionId,tab);
        this.__services.addListener("createSamples", function(e) {
          this.fireDataEvent("end",e.getData());
        },this);
      } else {
        this.fireDataEvent("end", e.getData());
      }
    },this)

    //Fin de l'import du fichier'
    this.addListener("end", function(e){
      this.__fileButton.setEnabled(true);
      this.__insertButton.setEnabled(true);
      loading.exclude();
      this.__textArea.show();
      this.__textArea.setValue(e.getData());
      //TO DO vérification de l'unicité des emplacements

    }, this);



	}
});
