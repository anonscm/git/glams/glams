/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Widget to select the project to show and display the area for the graph.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 */
qx.Class.define("glams.ui.Visualisation", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Event to send data to Graph.js
     */
    "sendData": "qx.event.type.Event"
    /**
     * Event to display information about the clicked container
     */
    // "affichage": "qx.event.type.Event"
  },

  properties: {
    /**
     * data to show about the clicked container
     */
    dataContainer: {
      init: null,
      event: "changeDataContainer"
    }
  },

  members: {
    /**
     * function to display the data about the clicked container
     * @param data {array} data about the clicked container
     * @return {container} Returns the container containing all the data about the clicked container
     */
    __affichage: function(data) {
      var container = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
        spacing: 5
      }));

      container._add(new qx.ui.basic.Label(
        this.tr("Nom de l'échantillon :")), {
        row: 0,
        column: 0
      });

      this.__code = new qx.ui.basic.Label().set({
        font: "bold"
      });
      container._add(this.__code, {
        row: 0,
        column: 1
      });

      container._add(new qx.ui.basic.Label(
        this.tr("Type du container :")), {
        row: 1,
        column: 0
      });

      this.__typeContainer = new qx.ui.basic.Label().set({
        font: "bold"
      });
      container._add(this.__typeContainer, {
        row: 1,
        column: 1
      });

      container._add(new qx.ui.basic.Label(
        this.tr("Nature de l'échantillon :")), {
        row: 2,
        column: 0
      });

      this.__contenuContainer = new qx.ui.basic.Label().set({
        font: "bold"
      });
      container._add(this.__contenuContainer, {
        row: 2,
        column: 1
      });

      container._add(new qx.ui.basic.Label(
        this.tr("Date de création du container :")), {
        row: 3,
        column: 0
      });

      this.__date = new qx.ui.basic.Label().set({
        font: "bold"
      });
      container._add(this.__date, {
        row: 3,
        column: 1
      });

      container._add(new qx.ui.basic.Label(
        this.tr("Collaborateur(s):")), {
        row: 4,
        column: 0
      });

      this.__collaborateur = new qx.ui.basic.Label().set({
        font: "bold",
        rich: true
      });
      container._add(this.__collaborateur, {
        row: 4,
        column: 1,
        colSpan: 4
      });

      this.__dataContainer = data;
      // console.log(this.__dataContainer);
      this.__code.setValue(this.__dataContainer.infos[0].code);
      this.__typeContainer.setValue(this.__dataContainer.infos[0].type);
      this.__contenuContainer.setValue(this.__dataContainer.infos[0].content);
      this.__date.setValue(this.__dataContainer.infos[0].date_on);
      if (this.__dataContainer.action[0]) {
        this.__collaborateur.setValue(this.__dataContainer.action[0].informations[0].firstname + " " + this.__dataContainer.action[0].informations[0].lastname + "\n");
      } else if (this.__dataContainer.userPrelevement[0]) {
        var label = null;
        for (var i in this.__dataContainer.userPrelevement) {
          if (label == null) {
            label = this.__dataContainer.userPrelevement[i].firstname + " " + this.__dataContainer.userPrelevement[i].lastname + "</br>";
          } else {
            label = label + this.__dataContainer.userPrelevement[i].firstname + " " + this.__dataContainer.userPrelevement[i].lastname + "</br>";
          };
        };
        this.__collaborateur.setValue(label);
      };


      return (container);
    }
  },

  construct: function() {
    this.base(arguments);
    /*
     *****************************************************************************
      INIT
     *****************************************************************************
     */
    // Session management
    var session = qxelvis.session.service.Session.getInstance();
    this.__sessionId = session.getSessionId();

    this._setLayout(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this.__selectedGraph = new qx.ui.form.SelectBox();
    this._add(this.__selectedGraph, {
      row: 0,
      column: 0
    });
    var depart = new qx.ui.form.ListItem("-please select-");
    this.__selectedGraph.add(depart);
    // var container = new qx.ui.form.ListItem("Container");
    // this.__selectedGraph.add(container);
    var experiment = new qx.ui.form.ListItem("Experiment");
    this.__selectedGraph.add(experiment);
    this.__selection = new glams.ui.FiltredComboBox();
    this.__selection.setEnabled(false);
    this._add(this.__selection, {
      row: 0,
      column: 1
    });
    this.__graph = new qx.ui.container.Stack().set({
      height: 500,
      width: 1200
    });
    this._add(this.__graph, {
      row: 1,
      column: 0,
      colSpan: 2
    });
    this.__info = new qx.ui.container.Composite(new qx.ui.layout.HBox());
    this._add(this.__info, {
      row: 2,
      column: 0,
      colSpan: 2
    });


    this.__classGraph = new glams.ui.GraphTree();

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */
    this.__services = glams.services.Sample.getInstance();
    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.__selectedGraph.addListener("changeSelection", function() {
      if (this.__selectedGraph.getSelection()[0].getLabel() == "-please select-") {
        this.__selection.setEnabled(false);
      }
      if (this.__selectedGraph.getSelection()[0].getLabel() == "Experiment") {
        this.__selection.setEnabled(true);
        this.__graph._add(this.__classGraph);
        //Fonctions qui chargent les prelevements en fonction des carractères saisi dans la barre de texte
        this.__selection.addListener("changeSelectedString", function(e) {
          var loadData = e.getData();
          if (loadData.length > 2) {
            var tab = {};
            tab['userGroup'] = JSON.parse(localStorage.getItem('readGroups'));
            tab['filtredText'] = e.getData();
            this.__services.QueryGetAllExperimentsFiltred("getAllExperimentsFiltred", this.__sessionId, tab);
          };
        }, this);
        // Listener pour récuperer les données prelevement
        this.__services.addListener("getAllExperimentsFiltred", function(e) {
          this.__listPrelev = [];
          var loadData = e.getData();
          for (var i in loadData) {
            this.__listPrelev.push({
              'label': loadData[i]['name'],
              'id': loadData[i]['id']
            });
          };
          this.__selection.setListModel(this.__listPrelev);
        }, this);
        this.__selection.addListener("changeAllValues", function() {
          var tab = {
            "idExperiment": this.__selection.getAllValues().getId()
          };
          this.__services.QueryGetTreeFromExperiment("getTreeFromExperiment", this.__sessionId, tab);
        }, this);
        this.__services.addListener("getTreeFromExperiment", function(e) {
          var loadData = e.getData();
          this.__classGraph.setDataTree(loadData);
        }, this);
      };
    }, this);

    this.__classGraph.addListener("affichage", function(e) {
      var loadData = e.getData();
      this.__info.removeAll();
      this.__info.add(this.__affichage(loadData));
    }, this);

  }
});