/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */
/**
 * Widget to create a new lot from an existing accession in the database.
 *
 * Authors : David Aurégan,
 *
 * July 2018.
 * @asset(glams/*)
 */

qx.Class.define("glams.ui.CreateNewLotFromAccession", {
  extend: qx.ui.core.Widget,

  events: {

  },

  properties: {
    /**
     * Genre of the plant to create
     */
    genre: {
      init: null,
      event: "changeGenre"
    },
    /**
     * Name of the original Accession
     */
    idClone: {
      init: null,
      event: "changeIdClone"
    },
    /**
     * Name of the new Lot
     */
    newNameLot: {
      init: null,
      event: "changeNewNameLot"
    },
    /**
     * Date of introduction of the new Lot
     */
    date: {
      init: null,
      event: "changeDate"
    }
  },

  construct: function() {
    this.base(arguments);

    var layout = new qx.ui.layout.HBox();
    layout.setSpacing(15);
    this._setLayout(layout);

    //Implémentation des widget visible à l'écran
    var containerSaisie = new qx.ui.container.Composite(new qx.ui.layout.VBox().set({
      spacing: 5
    }));
    this._add(containerSaisie);

    var containerAffichage = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));
    this._add(containerAffichage);

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var accessionOrigine = new qx.ui.container.Composite(grid);
    containerSaisie._add(accessionOrigine);

    accessionOrigine._add(new qx.ui.basic.Label(
      this.tr("Numéro d'accession d'origine")), {
      row: 0,
      column: 0
    });
    this.__accOrigine = new glams.ui.FiltredComboBox();
    accessionOrigine._add(this.__accOrigine, {
      row: 0,
      column: 1
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var newNameLot = new qx.ui.container.Composite(grid);
    containerSaisie._add(newNameLot);

    newNameLot._add(new qx.ui.basic.Label(
      this.tr("Numéro de Lot")), {
      row: 1,
      column: 0
    });

    this.__nameNewLot = new qx.ui.form.TextField();
    this.__nameNewLot.setPlaceholder(this.tr("Numéro de Lot"));
    newNameLot._add(this.__nameNewLot, {
      row: 1,
      column: 1,
      colSpan: 2
    });

    var grid = new qx.ui.layout.Grid();
    grid.setSpacing(5);
    grid.setColumnMaxWidth(200);
    grid.setColumnFlex(0, 1);
    grid.setColumnFlex(1, 2);
    var dateAccession = new qx.ui.container.Composite(grid);
    containerSaisie._add(dateAccession);

    dateAccession._add(new qx.ui.basic.Label(
      this.tr("Date d'introduction")), {
      row: 2,
      column: 0
    });
    this.__dateLot = new qx.ui.form.DateField();
    this.__dateLot.setDateFormat(new qx.util.format.DateFormat("dd/MM/YYYY"));
    this.__dateLot.setValue(new Date());
    var format = new qx.util.format.DateFormat("dd/MM/YYYY");
    var date = format.format(this.__dateLot.getValue());
    this.setDate(date);
    dateAccession._add(this.__dateLot, {
      row: 2,
      column: 1
    });

    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Variété d'origine : ")), {
      row: 0,
      column: 0
    });
    var lblVarOr = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblVarOr, {
      row: 0,
      column: 1
    });
    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Accession : ")), {
      row: 1,
      column: 0
    });
    var lblAccEx = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblAccEx, {
      row: 1,
      column: 1
    });
    containerAffichage._add(new qx.ui.basic.Label(
      this.tr("Lot déjà existante : ")), {
      row: 2,
      column: 0
    });
    var lblLotEx = new qx.ui.basic.Label().set({
      rich: true
    });
    containerAffichage._add(lblLotEx, {
      row: 2,
      column: 1
    });

    // var validationNomLot = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez renseigner le nom du nouveau Lot");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );
    // var validationSelectAcc = new qx.ui.form.validation.AsyncValidator(
    //   function(validator, value) {
    //     window.setTimeout(function() {
    //       if (value == null || value.length == 0) {
    //         validator.setValid(false, "Veuillez selectionner une Accession");
    //       } else {
    //         validator.setValid(true);
    //       }
    //     }, 1000);
    //   }
    // );

    // this.__manager.add(this.nameNewLot, validationNomLot);
    // this.__manager.add(this.__accOrigine, validationSelectAcc);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    //Récupère toutes les Accession pour le genre de la plante modèle
    this.__accOrigine.addListener("changeSelectedString", function(e) {
      var loadData = e.getData();
      if (loadData.length > 2) {
        var tab = {}
        tab["nomAcc"] = loadData;
        tab["genre"] = this.getGenre();
        this.__services.QueryGetListeAccessionByNomByGenre("getListeAccessionByNomByGenre", this.__sessionId, tab)
      };
    }, this);
    //
    this.__services.addListener("getListeAccessionByNomByGenre", function(e) {
      this.__listAccOrigine = [];
      var loadData = e.getData();
      for (var i in loadData) {
        for (var j in loadData[i]['accession']) {
          this.__listAccOrigine.push({
            'label': loadData[i]['accession'][j],
            'idVar': loadData[i]['id_variete'],
            'accession': loadData[i]['accession'],
            'lot': loadData[i]['lot'],
            'idClone': loadData[i]['idClone']
          });
        };
      };
      this.__accOrigine.setListModel(this.__listAccOrigine);
    }, this);

    // Affiche les informations relatives à l'Accession choisi par l'utilisateur
    this.__accOrigine.addListener("changeAllValues", function() {
      var idVariete = this.__accOrigine.getAllValues().getIdVar();
      this.__services.QueryGetAllNomsVarieteByIdVariete("getAllNomsVarieteByIdVariete", idVariete, null);
      this.__lot = this.__accOrigine.getAllValues().getLot().toArray();
      this.__accession = this.__accOrigine.getAllValues().getAccession().toArray();
      this.idClone = this.__accOrigine.getAllValues().getIdClone();
      var label = 1
      for (var i in this.__lot) {
        if (label == 1) {
          label = this.__lot[i] + "</br>"
        } else {
          label = label + this.__lot[i] + "</br>"
        };
      };
      lblLotEx.setValue(label);
      label = 1
      for (var i in this.__accession) {
        if (label == 1) {
          label = this.__accession[i] + "</br>"
        } else {
          label = label + this.__accession[i] + "</br>"
        };
      };
      lblAccEx.setValue(label);
      this.setIdClone(this.__accOrigine.getAllValues().getIdClone());
    }, this);

    //Récupère le nom de la variété en fonction de son ID et l'affiche
    this.__services.addListener("getAllNomsVarieteByIdVariete", function(e) {
      var loadData = e.getData();
      var label = 1
      for (var i in loadData) {
        var type = loadData[i].type;
        var nom = loadData[i].nom;
        if (label == 1) {
          label = type + " : " + nom + "</br>"
        } else {
          label = label + type + " : " + nom + "</br>"
        };
      }
      lblVarOr.setValue(label);
    }, this);

    this.__nameNewLot.addListener("changeValue", function(e) {
      var loadData = e.getData();
      this.setNewNameLot(loadData);
    }, this);

    this.__dateLot.addListener("changeValue", function() {
      var format = new qx.util.format.DateFormat("dd/MM/YYYY");
      var date = format.format(this.__dateLot.getValue());
      this.setDate(date);
    }, this);
  },

  members: {

  }

});