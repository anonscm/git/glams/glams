/**
 * VirtualCombobox
 * Fabrice Dupuis
 * Sylvain Gaillard
 * juin 2018
 */
qx.Class.define("glams.ui.FiltredComboBox", {
  extend: qx.ui.form.VirtualComboBox,
  events: {
    /**
     * Fired when the selection string is valid
     */
    "changeSelectedString": "qx.event.type.Data"
  },
  properties: {
    /**
     * toutes les valeurs associées au label
     */
    allValues: {
      init: null,
      nullable: true,
      event: "changeAllValues"
    }

  },


  members: {
    /**
     * Sets the model of the virtualcomboBox
     * @param array {Array} Array containing the values
     */
    setListModel: function(array) {
      var model = qx.data.marshal.Json.createModel(array);
      this.setModel(model);
      this.setLabelPath("label");
      //this.open();
    }
  },

  construct: function() {

    this.base(arguments);
    /********************
     *  Listeners
     *******************/
    //FireDataEvent quand quelque chose est entré dans la zone de texte.
    this.getChildControl("textfield").addListener("input", function(e) {
      var text = e.getData()
      //fireEvent pour lancer la requete sur la base de données
      this.fireDataEvent("changeSelectedString", e.getData());


      // filtre les items
      var delegate = {
        filter: function(item) {
          var index = item.getLabel().toLowerCase().indexOf(text.toLowerCase());
          return index >= 0 ? true : false;
        }
      };
      //active le filtre sur les items
      this.setDelegate(delegate);

    }, this);

    //renvoie les valeurs quand on click sur un élément de la liste
    this.getChildControl("dropdown").getSelection().addListener("change", function(e) {
      if (e.getData()['added'] != [] && e.getData()['added'][0]) {
        this.setAllValues(e.getData()['added'][0]);
      };
    }, this);
  }
});
