/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * David Aurégan, bioinfo team, IRHS

************************************************************************ */

/**
 * Widget to create a culture protocol.
 * Authors : David Aurégan,
 * June 2018.
 * @asset(glams/*)
 * */

qx.Class.define("glams.ui.CreateProtocoleCulture", {
  extend: qx.ui.core.Widget,

  events: {
    /**
     * Close the Window
     */
    "fermetureCreateProtocol": "qx.event.type.Event"
  },

  properties: {

  },

  members: {


  },

  construct: function() {
    this.base(arguments);

    /*
    *****************************************************************************
     INIT
    *****************************************************************************
    */
    var layout = new qx.ui.layout.Grid();
    layout.setSpacing(15);
    this._setLayout(layout);

    var container = new qx.ui.container.Composite(new qx.ui.layout.Grid().set({
      spacing: 5
    }));

    //Implémentation des widget visible à l'écran
    this._add(new qx.ui.basic.Label(
      this.tr("Nom du protocole")), {
      row: 0,
      column: 0
    });
    this.__nomProtocole = new qx.ui.form.TextField();
    this.__nomProtocole.setRequired(true);
    this._add(this.__nomProtocole, {
      row: 0,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Référence cahier de laboratoire")), {
      row: 1,
      column: 0
    });
    this.__refLabbook = new qx.ui.form.TextField();
    this._add(this.__refLabbook, {
      row: 1,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Résumé : ")), {
      row: 2,
      column: 0
    });
    this.__summary = new qx.ui.form.TextArea();
    this.__summary.setRequired(true);
    this._add(this.__summary, {
      row: 2,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Adresse web vers protocole Alfresco")), {
      row: 3,
      column: 0
    });
    this.__adresseWeb = new qx.ui.form.TextField();
    this._add(this.__adresseWeb, {
      row: 3,
      column: 1
    });
    this._add(new qx.ui.basic.Label(
      this.tr("Reference Alfresco")), {
      row: 4,
      column: 0
    });
    this.__nomAlfresco = new qx.ui.form.TextField();
    this._add(this.__nomAlfresco, {
      row: 4,
      column: 1
    });
    this.__btnSave = new qx.ui.form.Button(this.tr("Sauvegarder le protocole"));
    this._add(this.__btnSave, {
      row: 5,
      column: 0,
      colSpan: 2
    });
    //Initialisation des Validator pour rendre obligatoire la saisi du nom, du résumé et de la référence du Protocole
    var ValidationNom = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer un Nom du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var ValidationSummary = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer un Résumé du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var ValidationRef = new qx.ui.form.validation.AsyncValidator(
      function(validator, value) {
        window.setTimeout(function() {
          if (value == null || value.length == 0) {
            validator.setValid(false, "Veuillez entrer une référence du Protocole");
          } else {
            validator.setValid(true);
          }
        }, 1000);
      }
    );

    var manager = new qx.ui.form.validation.Manager();
    manager.add(this.__nomProtocole, ValidationNom);
    manager.add(this.__summary, ValidationSummary);
    manager.add(this.__refLabbook, ValidationRef);

    /*
    *****************************************************************************
     SERVICE
    *****************************************************************************
    */

    this.__services = glams.services.Sample.getInstance();

    /*
    *****************************************************************************
     LISTENERS
    *****************************************************************************
    */

    this.__btnSave.addListener("execute", function() {
      manager.validate();
    }, this);

    manager.addListener("complete", function(e) {
      if (manager.getValid()) {
        var tab = {};
        tab['name'] = this.__nomProtocole.getValue();
        tab['adress'] = this.__adresseWeb.getValue();
        tab['summary'] = this.__summary.getValue();
        tab['referenceLaboratoryHandbook'] = this.__refLabbook.getValue();
        tab['referenceAlfresco'] = this.__nomAlfresco.getValue();
        this.__services.QueryCreateProtocol("createProtocol", this.__sessionId, tab);
        // Déclenchement de la fonction de fermeture de la fenêtre
        this.fireEvent("fermetureCreateProtocol");
      };
    }, this);
  }
});