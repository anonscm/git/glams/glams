/* ************************************************************************

  Copyright: 2015 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Sylvain Gaillard, bioinfo team, IRHS
    * ANDRES hervé
    * Fabrice Dupuis
************************************************************************ */
/**
  Classe pour appeler les services
*/
qx.Class.define("glams.services.Sample", {
  type: "singleton",
  extend: qx.core.Object,

  members: {
    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    __AsyncQuery: function(serviceMethod, serviceEvent, idSample, context) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/glamsService_sample.py", "glamsService");
      rpc.setTimeout(25000);
      // console.log("test appel " + context);
      // console.log("test appel " + serviceEvent+ ' '+context );
      rpc.addListener("failed", function(e) {
        // console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        // console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, idSample, context);
    },

    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param nomContexte {string} Name of the contexte of notation
     *@param UserGroup {Array} id of the userGroup
     */
    __AsyncQueryGetAllTypeNotation: function(serviceMethod, serviceEvent, sessionId, nomContexte, UserGroup) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/glamsService_sample.py", "glamsService");
      rpc.setTimeout(25000);
      //console.log("test appel " + context);
      //console.log("test appel " + serviceEvent);
      rpc.addListener("failed", function(e) {
        //console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        //console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, sessionId, nomContexte, UserGroup);
    },

    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param idObject {integer} id of the object.
     */
    __AsyncQueryTerminologie: function(serviceMethod, serviceEvent, sessionId, idObject) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/terminologyService.py", "terminologyService");
      //console.log("test appel " + context);
      //console.log("test appel " + serviceEvent);
      rpc.addListener("failed", function(e) {
        //console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        //console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, sessionId, idObject);
    },
    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param idObject {integer} id of the object
     */
    __AsyncQueryTerminologieGetConceptGraphByTerminology: function(serviceMethod, serviceEvent, idObject) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/terminologyService.py", "terminologyService");
      //console.log("test appel " + context);
      //console.log("test appel " + serviceEvent);
      rpc.addListener("failed", function(e) {
        //console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        //console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, idObject);
    },
    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param nomContexte {string} Name of tehe contexte
     */
    __AsyncQueryGetTerminologyContextByName: function(serviceMethod, serviceEvent, nomContexte) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/terminologyService.py", "terminologyService");
      rpc.addListener("failed", function(e) {
      }, this);
      rpc.addListener("completed", function(e) {
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, nomContexte);
    },

    /**
     *Function which call the service "getNotationsTypeByContexte" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param nomContexte {string} Name of tehe contexte
     *@param UserGroup {array} id of the userGroup
     */
    QueryGetAllTypeNotation: function(serviceEvent, sessionId, nomContexte, UserGroup) {
      this.__AsyncQueryGetAllTypeNotation("getNotationsTypeByContexte", serviceEvent, sessionId, nomContexte, UserGroup);
    },

    /**
     *Function which call the service "QueryGetAllTypeNotationByContexte" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} array of informations
     */
    QueryGetAllTypeNotationByContexte: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllTypeNotationByContexte", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "getSampleGroup" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    QuerySampleGroup: function(serviceEvent, idSample, context) {
      this.__AsyncQuery("getSampleGroup", serviceEvent, idSample, context);
    },

    /**
     *Function which call the service "getAllNomsVarieteByIdVariete" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idVariete {integer} Id of the variete.
     *@param context {}
     */
    QueryGetAllNomsVarieteByIdVariete: function(serviceEvent, idVariete, context) {
      this.__AsyncQuery("getAllNomsVarieteByIdVariete", serviceEvent, idVariete, null);
    },
    /**
     *Function which call the service "QueryGetAllTypesNomVariete" and launch the event
     *@param serviceEvent {string} Name of the event to fire
     *@param idVariete {integer} Id of the variete.
     *@param tab {} Must be NULL.
     */
    QueryGetAllTypesNomVariete: function(serviceEvent, idVariete, tab) {
      this.__AsyncQuery("getAllTypesNomVariete", serviceEvent, idVariete, tab);
    },


    /**
     *Function which call the service "getLotGeneticsInformationsByPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    QueryGeneticsInformations: function(serviceEvent, idSample, context) {
      this.__AsyncQuery("getLotGeneticsInformationsByPrelevment", serviceEvent, idSample, context);
    },


    /**
     *Function which call the service "getInfosPlantsByPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    QueryPlantsInformations: function(serviceEvent, idSample, context) {
      this.__AsyncQuery("getInfosPlantsByPrelevment", serviceEvent, idSample, context);
    },

    /**
     *Function which call the service "getInfosPrelevmentByPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    QueryPrelevementsInformations: function(serviceEvent, idSample, context) {
      this.__AsyncQuery("getInfosPrelevmentByPrelevment", serviceEvent, idSample, context);
    },

    /**
     *Function which call the service "getAllPrelevmentsGroupFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param string {string} string to search.
     */
    QueryAllPrelevmentsGroupFiltred: function(serviceEvent, sessionId, string) {
      this.__AsyncQuery("getAllPrelevmentsGroupFiltred", serviceEvent, sessionId, string);
    },

    /**
     *Function which call the service "getAllLotsforPrelevFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idGroup {integer} Id of the Group.
     *@param string {string} string to search.
     */
    QueryAllLotsforPrelevFiltred: function(serviceEvent, idGroup, string) {
      this.__AsyncQuery("getAllLotsforPrelevFiltred", serviceEvent, idGroup, string);
    },
    /**
     *Function which call the service "getAllLotsforPrelevFiltredByGenre" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} array of informations
     */
    QueryAllLotsforPrelevFiltredByGenre: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllLotsforPrelevFiltredByGenre", serviceEvent, sessionId, tab);
    },


    /**
     *Function which call the service "getNotationsLotsforPrelev" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idLot {integer} Id of the Lot.
     *@param context {string} Context to use.
     */
    QueryNotationsLotsforPrelev: function(serviceEvent, idLot, context) {
      this.__AsyncQuery("getNotationsLotsforPrelev", serviceEvent, idLot, context);
    },

    /**
     *Function which call the service "getAllTypeTissu" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idNull {integer} Must be NULL.
     *@param contextNull {string} Must be NULL.
     */
    QueryAllTissuType: function(serviceEvent, idNull, contextNull) {
      this.__AsyncQuery("getAllTypeTissu", serviceEvent, idNull, contextNull);
    },

    /**
     *Function which call the service "getAllTypeTissu" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idNull {integer} Must be NULL.
     *@param contextNull {string} Must be NULL.
     */
    QueryAllUser: function(serviceEvent, idNull, contextNull) {
      this.__AsyncQuery("getAllUser", serviceEvent, idNull, contextNull);
    },

    /**
     *Function which call the service "getAllTypeContenu" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idNull {integer} Must be NULL.
     *@param contextNull {string} Must be NULL.
     */
    QueryAllTypeContenu: function(serviceEvent, idNull, contextNull) {
      this.__AsyncQuery("getAllTypeContenu", serviceEvent, idNull, contextNull);
    },

    /**
     *Function which call the service "getAllTypeContenant" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idNull {integer} Must be NULL.
     *@param contextNull {string} Must be NULL.
     */
    QueryAllTypeContenant: function(serviceEvent, idNull, contextNull) {
      this.__AsyncQuery("getAllTypeContenant", serviceEvent, idNull, contextNull);
    },

     /**
      *Function which call the service "getEtatContenant" and launch the event
      *@param serviceEvent {string} Name of the event to fire.
      *@param idNull {integer} Must be NULL.
      *@param contextNull {} Must be NULL.
      */
     QueryEtatContenant: function(serviceEvent, idNull, contextNull) {
       this.__AsyncQuery("getEtatContenant", serviceEvent, idNull, contextNull);
     },

    /**
     *Function which call the service "getAllGroupsByLot" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idLot {integer} id of the lot
     *@param contextNull {} Must be NULL.
     */
    QueryAllGroupsByLot: function(serviceEvent, idLot, contextNull) {
      this.__AsyncQuery("getAllGroupsByLot", serviceEvent, idLot, contextNull);
    },

    /**
     *Function which call the service "createPrelev" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations
     *@param contextNull {} Must be NULL.
     */
    QueryCreatePrelev: function(serviceEvent, tab, contextNull) {
      this.__AsyncQuery("createPrelev", serviceEvent, tab, contextNull);
    },

    /**
     *Function which call the service "addUserToPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idUser {integer} Id of one user.
     *@param idPrelev {integer} Id of one prelevement.
     */
    QueryAddUserToPrelevment: function(serviceEvent, idUser, idPrelev) {
      this.__AsyncQuery("addUserToPrelevment", serviceEvent, idUser, idPrelev);
    },

    /**
     *Function which call the service "getAllUserByPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idPrelev {integer} Id of one prelevement.
     *@param contextNull {string} Must be NULL.
     */
    QueryGetAllUserByPrelevment: function(serviceEvent, idPrelev, contextNull) {
      this.__AsyncQuery("getAllUserByPrelevment", serviceEvent, idPrelev, contextNull);
    },

    // TODO: Voir parce que potentiellement celui qui supprime les prelevement test
    /**
     *Function which call the service "deletePrelevementTest" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idNull {integer} Must be NULL.
     *@param contextNull {string} Must be NULL.
     */
    QueryDeletePrelevementTest: function(serviceEvent, idNull, contextNull) {
      this.__AsyncQuery("deletePrelevementTest", serviceEvent, idNull, contextNull);
    },

    /**
     *Function which call the service "addNotationPrelev" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations
     *@param sessionId {string} id of the session
     */
    QueryAddNotationPrelev: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("addNotationPrelev", serviceEvent, tab, sessionId);
    },

    /**
     *Function which call the service "addNotationPrelev" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param id_container {integer} ID of the cantainer.
     */
    QueryGetContainerPlace: function(serviceEvent, sessionId, id_container) {
      this.__AsyncQuery("getContainerPlace", serviceEvent, sessionId, id_container);
    },

    /**
     *Function which call the service "getAllBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {} Must be NULL.
     */
    QueryGetAllBoxTypeByContainer: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("getAllBoxTypeByContainer", serviceEvent, tab, sessionId);
    },
    /**
     *Function which call the service "getAllBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {} Must be NULL.
     */
    QueryGetAllBoxType: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("getAllBoxType", serviceEvent, tab, sessionId);
    },
    /**
     *Function which call the service "getAllContainerType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {} Must be NULL.
     */
    QueryGetAllContainerType: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("getAllContainerType", serviceEvent, tab, sessionId);
    },

    /**
     *Function which call the service "getAlltypesLieux" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {} Must be NULL.
     */
    QueryGetAlltypesLieux: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("getAlltypesLieux", serviceEvent, tab, sessionId);
    },
    /**
     *Function which call the service "getAlltypesLieuxO" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {} Must be NULL.
     */
    QueryGetAlltypesLieuxO: function(serviceEvent, tab, sessionId) {
      this.__AsyncQuery("getAlltypesLieuxO", serviceEvent, tab, sessionId);
    },

    // TODO: Utiliser que dans des fichier d'Hervé
    /**
     *Function which call the service "getAllConcept" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param idNull {string} Must be NULL.
     */
    QueryGetAllConcept: function(serviceEvent, sessionId, idNull) {
      this.__AsyncQueryTerminologie("getAllConceptSample", serviceEvent, sessionId, idNull);
    },

    // TODO: Utiliser que dans des fichier d'Hervé
    /**
     *Function which call the service "getTermByConcept" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param idConcept {integer} Id of the concept.
     */
    QueryGetTermByConcept: function(serviceEvent, sessionId, idConcept) {
      this.__AsyncQueryTerminologie("getTermByConceptSample", serviceEvent, sessionId, idConcept);
    },

    /**
     *Function which call the service "getListContainerAction" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idContainer {integer} Id of the container.
     *@param rang {integer} Rang of the recursive function, default=0.
     */
    QueryGetListContainerAction: function(serviceEvent, idContainer, rang) {
      this.__AsyncQuery("getListContainerAction", serviceEvent, idContainer, rang);
    },

    /**
     *Function which call the service "getPrelevementContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param idContainer {integer} Id of the container.
     *@param sessionId {string} id of the session.
     */
    QueryGetPrelevementContainer: function(serviceEvent, idContainer, sessionId) {
      this.__AsyncQuery("getPrelevementContainer", serviceEvent, idContainer, sessionId);
    },

    /**
    *Function which call the service "GetAllNameBoxByType" and launch the event
    *@param serviceEvent {string} Name of the event to fire.
    *@param sessionId {string}
    *@param tab {array} array of informations ['typeBox'], ['userGroup']

    */
    QueryGetAllNameBoxByType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllNameBoxByType", serviceEvent, sessionId, tab);
    },
    /**
    *Function which call the service "GetAllNameBoxByType" and launch the event
    *@param serviceEvent {string} Name of the event to fire.
    *@param sessionId {string}
    *@param tab {array} array of informations ['typeBox'], ['userGroup']

    */
    QueryGetAllTypeBoxByType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllTypeBoxByType", serviceEvent, sessionId, tab);
    },
    /**
    *Function which call the service "QueryGetAllValuesLieuxFiltred" and launch the event
    *@param serviceEvent {string} Name of the event to fire.
    *@param sessionId {string}
    *@param tab {array} array of informations ['typeLieu'], ['filtredText']

    */
    QueryGetAllValuesLieuxFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllValuesLieuxFiltred", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetValidationCreateBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} array of informations ['nameBox'], ['placeBox']
     */
    QueryGetValidationCreateBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getValidationCreateBox", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QuerySetCreateBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['nameBox'], ['typeBox'], ['placeBox']
     */
    QuerySetCreateBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("setCreateBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetInfosBoxBoxType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['name'], ['type']
     */
    QueryGetInfosBoxBoxType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getInfosBoxBoxType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllContainersByBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['idBox']
     */
    QueryGetAllContainersByBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllContainersByBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllInformationsByBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} ['idBox']
     *@param sessionId {string}
     */
    QueryGetAllInformationsByBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllInformationsByBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetInfosBoxByType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['typeBox']
     */
    QueryGetInfosBoxByType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getInfosBoxByType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllGroupsFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['filtredText'], ['userGroup']
     */
    QueryGetAllGroupsFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllGroupsFiltred", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QuerySetCreateContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['code'], ['date_on'], ['typeContainer'], ['typeContent'], ['typeBox'], ['nameBox'], ['typeContent'], ['remark'], ['serie'], ['notations'], ['colName'], ['rowName']
     */
    QuerySetCreateContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("setCreateContainer", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllContentType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllContentType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllContentType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllContentLevel" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllContainerLevel: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllContainerLevel", serviceEvent, sessionId, tab);
    },

     /**
      *Function which call the service "QueryUpdateContainer" and launch the event
      *@param serviceEvent {string} Name of the event to fire.
      *@param sessionId {string}
      *@param tab {array}
      */
     QueryUpdateContainer: function(serviceEvent, sessionId, tab) {
       this.__AsyncQuery("updateContainer", serviceEvent, sessionId, tab);
     },


     /**
     *Function which call the service "QueryUpdateContainerLocation" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array}
     *@param sessionId {string}
     */
     QueryUpdateContainerLocation: function(serviceEvent, sessionId, tab) {
       this.__AsyncQuery("updateContainerLocation", serviceEvent, sessionId, tab);
     },

    /**
     *Function which call the service "QueryGetNotationsByContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['idContainer']
     */
    QueryGetNotationsByContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getNotationsByContainer", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "UpdateBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['id'], ['type'], ['rowNumber'], ['colNumber'], ['typeContainerCorresponding']
     */
    QueryUpdateBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("updateBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllContainersFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['filtredText'], ['userGroup']
     */
    QueryGetAllContainersFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllContainersFiltred", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllBoxsFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['filtredText'], ['userGroup']
     */
    QueryGetAllBoxsFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllBoxsFiltred", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetPlaceContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['filtredText'], ['userGroup']
     */
    QueryGetPlaceContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getPlaceContainer", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetPlaceBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['filtredText'], ['userGroup']
     */
    QueryGetPlaceBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getPlaceBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateBoxs" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} array of informations ['cols'] ['lines']
     */
    QueryCreateBoxs: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createBoxs", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateSamples" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['cols'] ['lines']
     */
    QueryCreateSamples: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createSamples", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllTypesBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllTypesBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllTypesBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllTypeNotationAll" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllTypeNotationAll: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllTypeNotationAll", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateActions" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string}
     *@param tab {array} ['cols'] ['lines']
     */
    QueryCreateActions: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createActions", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllActionType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllActionType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllActionType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "getAllTypeContainerByBox" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['typeBox']
     */
    QueryGetAllTypeContainerByBox: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllTypeContainerByBox", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllExperiments" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['userGroup']
     */
    QueryGetAllExperiments: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllExperiments", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAllExperimentsFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['userGroup']['filtredText']
     */
    QueryGetAllExperimentsFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllExperimentsFiltred", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryUpdateBoxType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} array of informations ['id'], ['type'], ['rowNumber'], ['colNumber'], ['typeContainerCorresponding']
     */
    QueryUpdateBoxType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("updateBoxType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryUpdateContainerType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['type'], ['id']
     */
    QueryUpdateContainerType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("updateContainerType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateBoxType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['type'], ['rowNumber'], ['colNumber'], ['typeContainerCorresponding']
     */
    QueryCreateBoxType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createBoxType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateContainerType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['type']
     */
    QueryCreateContainerType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createContainerType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllValeurByNotation" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['typeNotation']
     */
    QueryGetAllValeurByNotation: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllValeurByNotation", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QuerySetCreateBoxAndContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} ['date_on'], ['date_on'], ['typeContainer'], ['typeContent'], ['nameBoxType'], ['nameOriginBox'], ['nameBox'], ['serie'], ['typeAction'], ['notationsAction'], ['notationsContainer'], ['placesBox'], ['userGroup'], ['idUser']
     */
    QuerySetCreateBoxAndContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("setCreateBoxAndContainer", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetTermsByTerminologyName" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param name {string} Name of the terminology
     *@param context {integer} id of the context
     */
    QueryGetTermsByTerminologyName: function(serviceEvent, name, context) {
      this.__AsyncQueryTerminologie("getTermsByTerminologyName", serviceEvent, name, context);
    },

    /**
     *Function which call the service "QueryGetConceptGraphByTerminology" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param terminology {integer} id of the context
     */
    QueryGetConceptGraphByTerminology: function(serviceEvent, terminology) {
      this.__AsyncQueryTerminologieGetConceptGraphByTerminology("getConceptGraphByTerminology", serviceEvent, terminology);
    },

    /**
     *Function which call the service "QueryGetConceptGraphByTerminology" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param terminology {integer} id of the context
     */
    QueryGetTerminologyByName: function(serviceEvent, terminology) {
      this.__AsyncQueryTerminologieGetConceptGraphByTerminology("getTerminologyByName", serviceEvent, terminology);
    },

    /**
     *Function which call the service "QueryGetTerminologyContextByName" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param context {string} name of the context
     */
    QueryGetTerminologyContextByName: function(serviceEvent, context) {
      this.__AsyncQueryGetTerminologyContextByName("getContextByName", serviceEvent, context);
    },

    /**
     *Function which call the service "QueryGetListeVarieteByNomByGenre" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['nomVar'], ['userGroup']
     */
    QueryGetListeVarieteByNomByGenre: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getListeVarieteByNomByGenre", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetListeAccessionByNomByGenre" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ["nomAcc"], ["userGroup"]
     */
    QueryGetListeAccessionByNomByGenre: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getListeAccessionByNomByGenre", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllBoxTypeAContainerType" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} Must be NULL.
     */
    QueryGetAllBoxTypeAContainerType: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllBoxTypeAContainerType", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllGenresByGroup" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['userGroup']
     */
    QueryGetAllGenresByGroup: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllGenresBygGoup", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAllGenres" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} NULL
     */
    QueryGetAllGenres: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllGenres", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAllEspeceByGenre" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} array of informations ['genre']
     */
    QueryGetAllEspeceByGenre: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllEspeceByGenre", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateExperiment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} array of informations
     */
    QueryCreateExperiment: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createExperiment", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryUpdateExperiment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer} 
     *@param tab {array} array of informations ['name'], ['id'], ['description'], ['creationDate']
     */
    QueryUpdateExperiment: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("updateExperiment", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateProtocol" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['name'], ['adress'], ['summary'], ['referenceLaboratoryHandbook'], ['referenceAlfresco']
     */
    QueryCreateProtocol: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createProtocol", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryUpdateProtocol" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['id'], ['name'], ['adress'], ['summary'], ['referenceLaboratoryHandbook'], ['referenceAlfresco']
     */
    QueryUpdateProtocol: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("updateProtocol", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetAllProtocolFiltred" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['filtredName']
     */
    QueryGetAllProtocolFiltred: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllProtocolFiltred", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateArbre" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {integer}
     *@param tab {array} ['lotOrigine'], ['userGroup'], ['date'], ['name'],['annee_premiere_pousse'], ['porte_greffe'], ['protocol'], ['listeNotationsLot']
     */
    QueryCreateArbre: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createArbre", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAllPepinieriste" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} Must be NULL
     *@param sessionId {string} id of the session
     */
    QueryGetAllPepinieriste: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllPepinieriste", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAllPepinieristeObtenteur" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} Must be NULL
     *@param sessionId {string} Must be NULL.
     */
    QueryGetAllPepinieristeObtenteur: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getAllPepinieristeObtenteur", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QuerySynchronyseAllPlaces" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations Must be NULL.
     *@param sessionId {string} Must be NULL.
     */
    QuerySynchronyseAllPlaces: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("synchronyseAllPlaces", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetProcolCultureByLot" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} tab['idLot']
     *@param sessionId {string} Must be NULL.
     */
    QueryGetProtocolCultureByLot: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getProtocolCultureByLot", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetProtocolCultureLotByPrelevment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations ['idLot']
     *@param sessionId {string} Must be NULL.
     */
    QueryGetProtocolCultureLotByPrelevment: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getProtocolCultureLotByPrelevment", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QuerySetVarieteANotation" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations ['idVariete'] ['idNotation']
     *@param sessionId {string} Must be NULL.
     */
    QuerySetVarieteANotation: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("setVarieteANotation", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryCreateAction" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations
     *@param sessionId {string} Must be NULL.
     */
    QueryCreateAction: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("createAction", serviceEvent, sessionId, tab);
    },

    /**
     *Function which call the service "QueryGetInfosOnAction" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} array of informations tab['idAction'] tab['']
     *@param sessionId {string} Must be NULL.
     */
    QueryGetInfosOnAction: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getInfosOnAction", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetInfosOnContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} tab['idContainer'] 
     *@param sessionId {string} Must be NULL.
     */
    QueryGetInfosOnContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getInfosOnContainer", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryAddExperimentToContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} tab['idContainer'] tab['idExperiment']
     *@param sessionId {string} Must be NULL.
     */
    QueryAddExperimentToContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("addExperimentToContainer", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetTreeFromExperiment" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} tab[''] tab['idExperiment']
     *@param sessionId {string} Must be NULL.
     */
    QueryGetTreeFromExperiment: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getTreeFromExperiment", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetGraphFromContainer" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param tab {array} tab[''] tab['idContainer']
     *@param sessionId {string} Must be NULL.
     */
    QueryGetGraphFromContainer: function(serviceEvent, sessionId, tab) {
      this.__AsyncQuery("getGraphFromContainer", serviceEvent, sessionId, tab);
    }
  },

  // TODO Voir comment chercher à factoriser les events ou les supprimer
  events: {
    /**
     * get All Prelevments
     */
    "loadedSampleGroup": "qx.event.type.Data",
    /**
     * get basic informations of one sample
     */
    "loadedGeneticsInformations": "qx.event.type.Data",
    /**
     * get culture condition about one lot sample
     */
    "loadedPlantsInformations": "qx.event.type.Data",
    /**
     * get more details informations about one sample
     */
    "loadedPrelevementsInformations": "qx.event.type.Data",
    /**
     * get All Prelevments filtred
     */
    "loadedAllPrelevmentsGroupFiltred": "qx.event.type.Data",
    /**
     * get All Lots filtred
     */
    "loadedAllLotsforPrelevFiltred": "qx.event.type.Data",
    /**
     * get Notations for a Lot
     */
    "loadedNotationsLotsforPrelev": "qx.event.type.Data",
    /**
     * get All tissu type
     */
    "loadedAllTissuType": "qx.event.type.Data",
    /**
     * get All user valide
     */
    "loadedAllUser": "qx.event.type.Data",
    /**
     * get All Type Contenu
     */
    "loadedAllTypeContenu": "qx.event.type.Data",
    /**
     * get All Type Contenant
     */
    "loadedAllTypeContenant": "qx.event.type.Data",
    /**
     * get Etat Contenant
     */
    // TODO:
    // "loadedEtatContenant": "qx.event.type.Data",
    /**
     * get All Groups By Lot
     */
    "loadedAllGroupsByLot": "qx.event.type.Data",
    /**
     * create a sample from existing lot
     */
    "createSampleFromExisting": "qx.event.type.Data",
    /**
     * Add User To Prelevment
     */
    "addUserToPrelevment": "qx.event.type.Data",
    /**
     * All User By Prelevment
     */
    "allUserByPrelevment": "qx.event.type.Data",
    /**
     * Delete Prelevement Test
     */
    // "deletePrelevementTest": "qx.event.type.Data",
    // TODO:
    /**
     * Get All Type Notation
     */
    "getAllTypeNotation": "qx.event.type.Data",
    /**
     * Add Notation to a prelevment
     */
    "addNotationPrelev": "qx.event.type.Data",
    /**
     * Get Container Place
     */
    "getContainerPlace": "qx.event.type.Data",
    /**
     * Get All Concept
     */
    "loadAllConcept": "qx.event.type.Data",
    /**
     * get Term By Concept
     */
    "loadTermByConcept": "qx.event.type.Data",
    /**
     * load all Box
     */
    "loadAllBoxTypeByContainer": "qx.event.type.Data",
    /**
     * load all type zone
     */
    "getAlltypesLieux": "qx.event.type.Data",
    /**
     * load all type zone by onthology
     */
    "getAlltypesLieuxO": "qx.event.type.Data",
    /**
     * load all actions on container
     */
    "getListContainerAction": "qx.event.type.Data",
    /**
     * load all actions on container
     */
    "getPrelevementContainer": "qx.event.type.Data",
    /**
     * load all name of Box by box type
     */
    "getAllNameBoxByType": "qx.event.type.Data",
    /**
     * load all type of Box by container type
     */
    "getAllTypeBoxByType": "qx.event.type.Data",
    /**
     * load all type of lieux filtred by string
     */
    "getAllValuesLieuxFiltred": "qx.event.type.Data",
    /**
     * validate la création d'une boite
     */
    "getValidationCreateBox": "qx.event.type.Data",
    /**
     * réalise création d'une boite
     */
    "setCreateBox": "qx.event.type.Data",
    /**
     * get informations on Box and BoxType
     */
    "getInfosBoxBoxType": "qx.event.type.Data",
    /**
     * get informations+ containers on Box
     */
    "getAllInformationsByBox": "qx.event.type.Data",
    /**
     * get informations on container into a box
     */
    "getAllContainersByBox": "qx.event.type.Data",
    /**
     * get informations on  BoxType
     */
    "getInfosBoxByType": "qx.event.type.Data",
    /**
     * get informations on  groups
     */
    "getAllGroupsFiltred": "qx.event.type.Data",
    /**
     * set creation de un échantillon
     */
    "setCreateContainer": "qx.event.type.Data",
    /**
     * get all content_type in gestion_echantillons
     */
    "getAllContentType": "qx.event.type.Data",
    /**
     * get all level in gestion_echantillons
     */
    "getAllContainerLevel": "qx.event.type.Data",
    /**
     * update Container informations
     */
    "updateContainer": "qx.event.type.Data",
    /**
     * update Container informations
     */
    // "updateContainerLocation": "qx.event.type.Data",
    /**
     * get all Notations of a Container
     */
    "getNotationsByContainer": "qx.event.type.Data",
    /**
     * update content of one box
     */
    "updateBox": "qx.event.type.Data",
    /**
     * get informations on  container
     */
    "getAllContainersFiltred": "qx.event.type.Data",
    /**
     * get informations on  box
     */
    "getAllBoxsFiltred": "qx.event.type.Data",
    /**
     * get informations on  the place of the container
     */
    "getPlaceContainer": "qx.event.type.Data",
    /**
     * get informations on  the place of the box
     */
    "getPlaceBox": "qx.event.type.Data",
    /**
     * get all names of all box
     */
    "getAllBox": "qx.event.type.Data",
    /**
     * create multiple Boxs
     */
    "createBoxs": "qx.event.type.Data",
    /**
     * create multiple Samples
     */
    "createSamples": "qx.event.type.Data",
    /**
     * get all types of Boxs
     */
    "getAllTypesBox": "qx.event.type.Data",
    /**
     * get all types of container
     */
    "getAllTypeContenant": "qx.event.type.Data",
    /**
     * get all types of notation
     */
    "getAllTypeNotationAll": "qx.event.type.Data",
    /**
     * create multiple Actions
     */
    "createActions": "qx.event.type.Data",
    /**
     * get All Action Type
     */
    "getAllActionType": "qx.event.type.Data",
    /**
     * get All type container by type Box
     */
    "getAllTypeContainerByBox": "qx.event.type.Data",
    /**
     * get All Experiments
     */
    "getAllExperiments": "qx.event.type.Data",
    /**
     * get All Experiments filtred
     */
    "getAllExperimentsFiltred": "qx.event.type.Data",
    /**
     * get Terms By Terminology Name
     */
    "getTermsByTerminologyName": "qx.event.type.Data",
    /**
     * get Concept Graph By Terminology
     */
    "getConceptGraphByTerminology": "qx.event.type.Data",
    /**
     * get Concept Graph By Terminology
     */
    "getTerminologyByName": "qx.event.type.Data",
    /**
     * get Context By Name
     */
    "getContextByName": "qx.event.type.Data",

    /**
     * set Create Box And Container
     */
    "setCreateBoxAndContainer": "qx.event.type.Data",
    /**
     * get All Valeur By Notation
     */
    "getAllValeurByNotation": "qx.event.type.Data",
    /**
     * get All Box type
     */
    "getAllBoxType": "qx.event.type.Data",
    /**
     * get All Container Type
     */
    "getAllContainerType": "qx.event.type.Data",
    /**
     * update Box Type
     */
    "updateBoxType": "qx.event.type.Data",
    /**
     * create Box Type
     */
    "createBoxType": "qx.event.type.Data",
    /**
     * get All Noms Variete By IdVariete
     */
    "getAllNomsVarieteByIdVariete": "qx.event.type.Data",
    /**
     * get All Lots
     */
    "getAllLotsforPrelevFiltred": "qx.event.type.Data",
    /**
     * get All Variete by name
     */
    "getListeVarieteByNomByGenre": "qx.event.type.Data",
    /**
     * get All Accession by name
     */
    "getListeAccessionByNomByGenre": "qx.event.type.Data",
    /**
     * get All BoxType_A_ContainerType
     */
    "getAllBoxTypeAContainerType": "qx.event.type.Data",
    /**
     * get All Genres By Group
     */
    "getAllGenresByGroup": "qx.event.type.Data",
    /**
     * get All Genres
     */
    "getAllGenres": "qx.event.type.Data",
    /**
     * get All Especes by Genre
     */
    "getAllEspeceByGenre": "qx.event.type.Data",
    /**
     * update Container Type
     */
    "updateContainerType": "qx.event.type.Data",
    /**
     * get All Types Nom Variete
     */
    "getAllTypesNomVariete": "qx.event.type.Data",
    /**
     * create Container Type
     */
    "createContainerType": "qx.event.type.Data",
    /**
    "createContainerType": "qx.event.type.Data",
    /**
     * update Experiment
     */
    "createExperiment": "qx.event.type.Data",
    /**
     * update Experiment
     */
    "updateExperiment": "qx.event.type.Data",
    /**
     * create Protocol
     */
    "createProtocol": "qx.event.type.Data",
    /**
     * update Protocol
     */
    "updateProtocol": "qx.event.type.Data",
    /**
     * get All Protocol Filtred
     */
    "getAllProtocolFiltred": "qx.event.type.Data",
    /**
     * create Arbre
     */
    "createArbre": "qx.event.type.Data",
    /**
     *getAllTypeNotationByContexte
     */
    "getAllTypeNotationByContexte": "qx.event.type.Data",
    /**
     *getAllPepinieriste
     */
    "getAllPepinieriste": "qx.event.type.Data",
    /**
     *getAllPepinieriste
     */
    "getAllPepinieristeObtenteur": "qx.event.type.Data",
    /**
     *getAllLotsforPrelevFiltredByGenre
     */
    "getAllLotsforPrelevFiltredByGenre": "qx.event.type.Data",
    /**
     *SynchronyseAllPlaces
     */
    "synchronyseAllPlaces": "qx.event.type.Data",
    /**
     *getProcolCultureByLot
     */
    "getProtocolCultureByLot": "qx.event.type.Data",
    /**
     *getProtocolCultureLotByPrelevment
     */
    "getProtocolCultureLotByPrelevment": "qx.event.type.Data",
    /**
     *getAllPrelevmentsGroupFiltred
     */
    "getAllPrelevmentsGroupFiltred": "qx.event.type.Data",
    /**
     *createAction
     */
    "createAction": "qx.event.type.Data",
    /**
     *getInfosOnAction
     */
    "getInfosOnAction": "qx.event.type.Data",
    /**
     *getInfosOnContainer
     */
    "getInfosOnContainer": "qx.event.type.Data",
    /**
     *AddExperimentToContainer
     */
    "addExperimentToContainer": "qx.event.type.Data",
    /**
     *getTreeFromExperiment
     */
    "getTreeFromExperiment": "qx.event.type.Data",
    /**
     *getGraphFromContainer
     */
    "getGraphFromContainer": "qx.event.type.Data"
  }
});
