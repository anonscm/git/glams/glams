/* ************************************************************************

  Copyright: 2015 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Lysiane Hauguel, bioinfo team, IRHS

************************************************************************ */
/**
  Classe pour appeler les services
*/
qx.Class.define("glams.services.Experiment", {
  type: "singleton",
  extend: qx.core.Object,

  members: {
    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    __AsyncQueryProjectName: function(serviceMethod, serviceEvent, sessionId, UserGroup, context) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServiceProject.py", "projectService");
      // console.log("test appel " + context);
      // console.log("test appel " + serviceEvent+ ' '+context );
      rpc.addListener("failed", function(e) {
        // console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        // console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, sessionId, UserGroup, context);
    },
    /**
     *Function which call the service and launch the event
     *@param serviceMethod {string} Name of the service to use in this methode.
     *@param serviceEvent {string} Name of the event to fire.
     *@param idSample {integer} Id of the sample.
     *@param context {string} Context to use.
     */
    __AsyncQueryProject: function(serviceMethod, serviceEvent, sessionId, context) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServiceProject.py", "projectService");
      // console.log("test appel " + context);
      // console.log("test appel " + serviceEvent+ ' '+context );
      rpc.addListener("failed", function(e) {
        // console.log(e.getData().toString());
      }, this);
      rpc.addListener("completed", function(e) {
        // console.log(e.getData());
        event = serviceEvent;
        this.fireDataEvent(event, e.getData().result);
      }, this);
      rpc.callAsyncListeners(true, serviceMethod, sessionId, context);
    },

    /**
     *Function which call the service "QueryGetNameProject" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param groupId {string} id of the group.
     *@param tab {array} array of informations
     */
    QueryGetNameProject: function(serviceEvent, sessionId, groupId, tab) {
      this.__AsyncQueryProjectName("getNameProject", serviceEvent, sessionId, groupId, tab);
    },
    /**
     *Function which call the service "QueryGetFinancingTypesName" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} array of informations.
     */
    QueryGetFinancingTypesName: function(serviceEvent, sessionId, tab) {
      this.__AsyncQueryProject("getFinancingTypesName", serviceEvent, sessionId, tab);
    },
    /**
     *Function which call the service "QueryGetAxisTypesName" and launch the event
     *@param serviceEvent {string} Name of the event to fire.
     *@param sessionId {string} id of the session.
     *@param tab {array} array of informations.
     */
    QueryGetAxisTypesName: function(serviceEvent, sessionId, tab) {
      this.__AsyncQueryProject("getAxisTypesName", serviceEvent, sessionId, tab);
    }
  },


  events: {
    /**
     *getNameProject
     */
    "getNameProject": "qx.event.type.Data",
    /**
     *getFinancingTypesName
     */
    "getFinancingTypesName": "qx.event.type.Data",
    /**
     *getAxisTypesName
     */
    "getAxisTypesName": "qx.event.type.Data"
  }
});