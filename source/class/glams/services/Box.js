/* ************************************************************************

  Authors:
    * Sylvain Gaillard, bioinfo team, IRHS
    * 
    * Fabrice Dupuis
************************************************************************ */
/**
  Classe pour appeler les services
*/
qx.Class.define("glams.services.Box", {
  type : "singleton",
  extend : qx.core.Object,
  
  events : {    
   /**
    * creation d'un dataBox
    */
    "createBox" : "qx.event.type.Data"
      	  
  },
  members : { 
     /**
    *Function which create a box 
    *@param idBox {integer} Id of the box.
    *
    */
    createBox : function(idBox){
      var dataB = new glams.data.Box();
      if (idBox){
        dataB.setIdBox(idBox);
        dataB.addListener("getAllContainerInformations", function(e){
          this.fireDataEvent("createBox", dataB);
        },this);
      }else{
        this.fireDataEvent("createBox", dataB);
      };
    }
    
        
                       
  }


});

