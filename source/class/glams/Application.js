/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/**
 * This is the main application class of your custom application "glams"
 *
 * @asset(glams/*)
 * @asset(qxelvis/*)
 */
qx.Class.define("glams.Application", {
  extend: qx.application.Standalone,



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members: {
    /**
     * This method contains the initial application code and gets called
     * during startup of the application
     *
     * @lint ignoreDeprecated(alert)
     */
    main: function() {
      // Call super class
      this.base(arguments);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug")) {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;
        // support additional cross-browser console. Press F7 to toggle visibility
        qx.log.appender.Console;
      }

      /*
      -------------------------------------------------------------------------
        Below is your actual application code...
      -------------------------------------------------------------------------
      */

      // Global layout dimensions
      var headerHeight = 50;
      var menuBarHeight = 20;
      var footerHeight = 15;

      // Session management
      var configuration = glams.Config.getInstance();
      var session = qxelvis.session.service.Session.getInstance();
      var appId = session.getSessionId();

      var loginForm = new qxelvis.ui.LoginForm();
      var groupChoice = new glams.ui.GroupsChoice();

      var header = new qx.ui.core.Widget();
      header.setHeight(headerHeight);
      header.setBackgroundColor("window-caption-active-end");

      var footer = new qx.ui.core.Widget();
      footer.setHeight(footerHeight);
      footer.setBackgroundColor("window-caption-active-end");
      //footer.setBackgroundColor("#475C3D");

      var body = new qx.ui.core.Widget();
      //body.setBackgroundColor("green");

      var menu = new qx.ui.menubar.MenuBar();
      menu.setHeight(menuBarHeight);
      //menu add project
      var saveProject = new qx.ui.menubar.Button(this.tr("Add Project"));
      menu.add(saveProject);
      //menu prelevement
      var menuPrelevement = new qx.ui.menu.Menu();
      var boutonPrelevement = new qx.ui.menubar.Button('Prelevement');
      menu.add(boutonPrelevement);
      boutonPrelevement.setMenu(menuPrelevement);
      var showPrelevement = new qx.ui.menu.Button(this.tr("View Prelevement"));
      menuPrelevement.add(showPrelevement);
      var createPrelevement = new qx.ui.menu.Button(("New Prelevement"), null, null, this.getNewPrelevement());
      menuPrelevement.add(createPrelevement);
      var listPrelevement = new qx.ui.menu.Button(this.tr("All Prelevement Names"));
      menuPrelevement.add(listPrelevement);
      var plantName = new qx.ui.menu.Button(this.tr("Plant Name"));
      menuPrelevement.add(plantName);

      //menu Action
      var menuAction = new qx.ui.menu.Menu();
      var boutonAction = new qx.ui.menubar.Button('Action');
      menu.add(boutonAction);
      boutonAction.setMenu(menuAction);
      var actionPrelevement = new qx.ui.menu.Button(this.tr("Action sur prelevement"));
      menuAction.add(actionPrelevement);
      //menu Visualisation
      var menuVisualisation = new qx.ui.menu.Menu();
      var boutonVisualisation = new qx.ui.menubar.Button('Visualisation');
      menu.add(boutonVisualisation);
      //menu descriptor
      var descriptorMenu = new qx.ui.menu.Menu();
      var descriptorBtn = new qx.ui.menubar.Button('Descriptor');
      menu.add(descriptorBtn);
      descriptorBtn.setMenu(descriptorMenu);
      var allBoxTypesBtn = new qx.ui.menu.Button(this.tr("All Box Types"));
      descriptorMenu.add(allBoxTypesBtn);
      var allContainerTypesBtn = new qx.ui.menu.Button(this.tr("All Container Types"));
      descriptorMenu.add(allContainerTypesBtn);
      var allExperimentNamesBtn = new qx.ui.menu.Button(this.tr("All Experiment Names"));
      descriptorMenu.add(allExperimentNamesBtn);
      var allTypeLieuBtn = new qx.ui.menu.Button(this.tr("All Place Types"));
      descriptorMenu.add(allTypeLieuBtn);
      var allTypeUnitBtn = new qx.ui.menu.Button(this.tr("All Unit Types"));
      descriptorMenu.add(allTypeUnitBtn);
      //menu sample
      var sampleMenu = new qx.ui.menu.Menu();
      var sampleBtn = new qx.ui.menubar.Button('Sample');
      menu.add(sampleBtn);
      sampleBtn.setMenu(sampleMenu);
      var newSample = new qx.ui.menu.Button(this.tr("New Sample"));
      sampleMenu.add(newSample);
      var findSample = new qx.ui.menu.Button(this.tr("Find Sample"));
      sampleMenu.add(findSample);
      var findBox = new qx.ui.menu.Button(this.tr("Find Box"));
      sampleMenu.add(findBox);
      var importSamples = new qx.ui.menu.Button(this.tr("Import Samples"));
      sampleMenu.add(importSamples);
      var importBoxes = new qx.ui.menu.Button(this.tr("Import Boxes"));
      sampleMenu.add(importBoxes);
      var importActions = new qx.ui.menu.Button(this.tr("Import Actions"));
      sampleMenu.add(importActions);
      var boxActions = new qx.ui.menu.Button(this.tr("Actions sur boites"));
      sampleMenu.add(boxActions);

      //menu user
      var loginBtn = new qxelvis.ui.menubar.UserButton();
      menu.add(new qx.ui.core.Spacer(), {
        flex: 1
      });
      menu.add(loginBtn);
      //menu groups
      var groupBtn = new qx.ui.menubar.Button("Groups", "glams/grp.png");
      menu.add(groupBtn);

      var doc = this.getRoot();
      doc.add(header, {
        top: 0,
        left: 0,
        right: 0
      });
      doc.add(menu, {
        top: headerHeight,
        left: 0,
        right: 0
      });
      doc.add(footer, {
        bottom: 0,
        left: 0,
        right: 0
      });
      doc.add(body, {
        top: headerHeight + menuBarHeight + 3,
        bottom: footerHeight,
        left: 0,
        right: 0
      });

      /*
       * listener
       */

      var loginWin = new qx.ui.window.Window("Login");
      loginWin.setLayout(new qx.ui.layout.Grow());
      loginWin.add(loginForm);
      loginWin.setModal(true);


      loginBtn.addListener("execute", function() {
        if (!session.userLoggedIn()) {
          session.addListenerOnce("authenticationSucced", function() {
            this.close();
          }, loginWin);
          loginWin.center();
          loginWin.show();
        }
      }, this);

      loginForm.addListener("validate", function(e) {
        session.userLogin(e.getData()["username"], e.getData()["password"]);
      }, this);

      newSample.addListener("execute", function(e) {
        var createContainer = new glams.ui.CreateContainerForm();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(createContainer);
        var window = new qx.ui.window.Window("Samples");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      findSample.addListener("execute", function(e) {
        var newContainer = new glams.ui.SearchContainerForm();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Search Samples");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      findBox.addListener("execute", function(e) {
        var newContainer = new glams.ui.SearchBoxForm();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Search Boxes");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      importSamples.addListener("execute", function(e) {
        var newContainer = new glams.ui.InsertFileSample();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Import Samples");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      importBoxes.addListener("execute", function(e) {
        var newContainer = new glams.ui.InsertFileBox();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Import Boxes");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      importActions.addListener("execute", function(e) {
        var newContainer = new glams.ui.InsertFileSampleAction();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Import Actions");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      boxActions.addListener("execute", function(e) {
        var newContainer = new glams.ui.ActionOnBoxForm();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Actions sur boites ");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this)

      showPrelevement.addListener("execute", function(e) {
        var newContainer = new glams.ui.ViewPrelevement();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("View Prelevement");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      listPrelevement.addListener("execute", function(e) {
        var newContainer = new glams.ui.ListPrelevement();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Prelevement Names");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.setHeight(250);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      plantName.addListener("execute", function(e) {
        var newContainer = new glams.ui.PlantName();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var window = new qx.ui.window.Window("Plant Name");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      saveProject.addListener("execute", function(e) {
        var newContainer = new glams.ui.project.AddProject();

        newContainer.addListener('saveDraft', function(e) {
          // this.debug(qx.lang.Json.stringify(e.getData()));
          var store = qx.bom.Storage.getLocal();
          store.setItem("projectDraft", e.getData());
        }, this);
        
        newContainer.loadDraft();
        
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.setAllowStretchX(true);
        container.setAllowStretchY(true);
        container.add(newContainer);
        var window = new qx.ui.window.Window("Add Project");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(150 + Math.random() * 80), Math.round(70 + Math.random() * 80));

        newContainer.addListener('cancelProject', function() {
          window.close();
        }, this);
        // newContainer.addListener('saveProject', function() {
        //   window.close();
        // }, this);
        
      }, this);
      groupBtn.addListener("execute", function(e) {
        groupChoice.showGroups();
      }, this);
      allBoxTypesBtn.addListener("execute", function(e) {
        var newContainer = new glams.ui.BoxTypeDescriptor();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Box Types");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      allContainerTypesBtn.addListener("execute", function(e) {
        var newContainer = new glams.ui.ContainerTypeDescriptor();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Container type");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      allExperimentNamesBtn.addListener("execute", function(e) {
        var newContainer = new glams.ui.ExperimentNamesDescriptor();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Experiment Names");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      allTypeLieuBtn.addListener("execute", function(e) {
        var newContainer = new glams.ui.PlaceTypeDescriptor();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Type Places");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);

      allTypeUnitBtn.addListener("execute", function(e) {
        var newContainer = new glams.ui.UnitTypeDescriptor();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow());
        container.add(newContainer);
        var window = new qx.ui.window.Window("All Type Unit");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);

      actionPrelevement.addListener("execute", function(e) {
        var newContainer = new glams.ui.ActionPrelevement();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow()).set({
          height: 350,
          width: 850
        });;
        container.add(newContainer);
        var window = new qx.ui.window.Window("Action on Prelevement");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);
      boutonVisualisation.addListener("execute", function(e) {
        var newContainer = new glams.ui.Visualisation();
        var container = new qx.ui.container.Composite(new qx.ui.layout.Grow()).set({
          height: 500,
          width: 1200
        });;
        container.add(newContainer);
        var window = new qx.ui.window.Window("Visualisation");
        var layout = new qx.ui.layout.VBox();
        window.setLayout(layout);
        window.add(container);
        window.open();
        window.moveTo(Math.round(50 + Math.random() * 80), Math.round(70 + Math.random() * 80));
      }, this);

    },

    /**
     * Get "New Prelevement" Menu.
     * @return {qx.ui.Menu} Menu "New Prelevement".
     */
    getNewPrelevement: function() {
      var menu = new qx.ui.menu.Menu;

      var createPrelevFromExisting = new qx.ui.menu.Button(this.tr("A partir d'une culture existante"));
      var createCulutreConditions = new qx.ui.menu.Button(this.tr("Créer culture"));

      // Listener sur le bouton "A partir d'une culture"
      createPrelevFromExisting.addListener("execute", function(e) {
        var newContainer = new glams.ui.CreatePrelevementFromExisting();
        var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        container.add(newContainer);
        var windowPrel = new qx.ui.window.Window("Create Prelevement From Existing Cutlure");
        var layout = new qx.ui.layout.VBox();
        windowPrel.setLayout(layout);
        windowPrel.add(container);
        windowPrel.open();
        windowPrel.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
        //Fonction de fermeture de la fenêtre
        newContainer.addListener("fermeture", function() {
          windowPrel.close();
        }, this);
      }, this);

      // Listener sur le bouton "Creer culture"
      createCulutreConditions.addListener("execute", function(e) {
        this.__createCulture = new glams.ui.NewCreateCulture();
        var container = new qx.ui.container.Scroll().set({
          height: 850,
          width: 600
        });
        container.add(this.__createCulture);
        var windowCulture = new qx.ui.window.Window("Create Culture");
        var layout = new qx.ui.layout.VBox();
        windowCulture.setLayout(layout);
        windowCulture.add(container);
        windowCulture.open();
        windowCulture.moveTo(Math.round(450 + Math.random() * 80), Math.round(70 + Math.random() * 80));
        // Fonction de fermeture de la fenêtre
        this.__createCulture.addListener("fermetureCulture", function() {
          windowCulture.close();
        }, this);

        this.__createCulture.addListener("saveNewCulture", function(e) {
          var loadData = e.getData();
          this.__infoNewCulture = loadData;
          this.__saveNewCulture = new glams.ui.SaveNewCulture();
          this.__saveNewCulture.createArbre(loadData);

          this.__saveNewCulture.addListener("return", function(e) {
            var loadData = e.getData();
            if (loadData.typeValidation == "Normal") {
              // Déclenchement de la fonction de fermeture de la fenêtre
              this.__createCulture.annuler();
              this.__createCulture.fireEvent("fermetureCulture")
            };
            if (loadData.typeValidation == "Culture") {
              // Déclenchement de la fonction de fermeture de la fenêtre
              this.__createCulture.annuler();
            };
            if (loadData.typeValidation == "Prelevement") {
              // Déclenchement de la fonction de fermeture de la fenêtre
              this.__createCulture.annuler();
              var newContainer = new glams.ui.CreatePrelevementFromExisting();
              var tab = {};
              tab['name'] = this.__infoNewCulture.name;
              if (this.__infoNewCulture.from == 'lot') {
                tab['accession'] = this.__infoNewCulture.lotOrigine.idClone;
              };
              if (this.__infoNewCulture.from == 'accession') {
                tab['accession'] = this.__infoNewCulture.accOrigine;
              };
              if (this.__infoNewCulture.from == 'variete') {
                tab['accession'] = this.__infoNewCulture.newAcc;
              };
              if (this.__infoNewCulture.from == 'rien') {
                tab['accession'] = this.__infoNewCulture.newAcc;
              };
              tab['idLot'] = loadData.IdNewCulture;
              newContainer.setPlanteSource(tab);
              var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());
              container.add(newContainer);
              var windowPrel = new qx.ui.window.Window("Create Prelevement From Existing Cutlure");
              var layout = new qx.ui.layout.VBox();
              windowPrel.setLayout(layout);
              windowPrel.add(container);
              windowPrel.open();
              windowPrel.moveTo(Math.round(250 + Math.random() * 80), Math.round(70 + Math.random() * 80));
              //Fonction de fermeture de la fenêtre
              newContainer.addListener("fermeture", function() {
                windowPrel.close();
              }, this);
              this.__createCulture.fireEvent("fermetureCulture");
            };
          }, this);
        }, this);
      }, this);

      menu.add(createPrelevFromExisting);
      menu.add(createCulutreConditions);

      return menu;
    }
  }
});
